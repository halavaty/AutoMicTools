# Installation


### Automatic Installation via Fiji update site (preferred, requires internet connection)

1. Start **Fiji**
2. Run *Help -> Update...* from the main Menu.
3. Press *Manage update sites*
4. On the bottom of the list add the URL of the **AutoMicTools** Fiji update site: *https://sites.imagej.net/AutoMicTools*

	1. Press [Add Update site] and in the line appeared on the bottom of the table type the URL in the **URL** column.
	2. Tick the corresponding checkbox in front of this item.
		
5. Close *Manage update sites* dialog
6. Click *Apply changes* and wait until new dependencies are downloaded
7. Restart **Fiji**.


### Manual Installation
1. Install the**AutoMicTools** library and its dependencies of  by copying following files to the ***Plugins*** subdirectory of your Fiji installation:
	1. `AutoMicTools_-x.x.x.jar` from [this page](https://git.embl.de/halavaty/AutoMicTools/-/releases) (use the latest available version, unless another version is recommedned to be compatible with one of AutoMicTools extensions libraries).
	2. `AutoMicTools-Photoactivation_-x.x.x.jar` from [this page](https://git.embl.de/grp-almf/automictools-photoactivation/-/releases) (use the latest available version).
	2. `reflections-0.9.12.jar` from [this page](https://javalibs.com/artifact/org.reflections/reflections)
	3. `CommMicroscope-1.1.3.jar` from [this page](https://github.com/ssgpers/CommMicroscope/releases/)
