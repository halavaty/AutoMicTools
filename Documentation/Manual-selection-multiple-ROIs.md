# Manual selection of multiple ROIs

This document describes how to configure parameters of the online image analysis Job used for the manual selection of multiple regions of interest (ROIs). This job is used, for example, in the semi-manual FRAP protocol [Plugins › Auto Mic Tools › Semi-Manual FRAP Online › Pick multiple bleach positions]

### Operation details

- The dialogue box for confirming made selections is automatically opened with together with the newly image. Pressing "OK" button takes selected regions for the follow up actions (e.g. FRAP). Pressing "Cancel" ignores the selected regions (equivalent to pressing "OK" without selecting any regions).
- If drawn regions **can not** be moved after drawing (see optional parameters below), the drawn region is added to the list of selected regions as soon as drawing is finished (no need to manually add them to the Roi Manager)
- If the regions **are allowed** be moved after drawing (see optional parameters below), use the "Right Arrow" key to add the Roi to the list (Overlay) of selected regions, otherwise the region is ignored.
- "Left arrow" key can be used to remove the last added region from the Selected List (Overlay).

### Options that can be configured via GUI

![manual-selection_multiple-rois.png](./images/manual-selection_multiple-rois.png)

- ***Selection window location X*** and ***Selection window location Y*** specify the positionn on the screen, where an image will be automatically opened. The provided position specifies top left corner of the image window. Position (0,0) corresponds to the top left corner of the screen

- ***Time to wait for the responce*** - time in seconds during which the program expects user input after opening the image. After the time expiration the program will either proceed with selected ROIs or ignore the selected ROIs depending on the next option (*Proceed with selected ROIs if time expires*) status.

- ***Proceed with selected ROIs if time expires*** - if selected, all ROIs are taken into account if time expires before user presses OK in the ROI selection confirmation dialog. If not selected, all selections are ignored if time expired before user confirms selection.

- ***Can move Roi after drawing*** - if selected, Roi can be moved with the mouse after drawing. If not selected, Roi is "caught" and converted to the overlay immediately after drawing.
**Important**: if this mode is used, use "Right Arrow" keyboard key to confirm Roi position. Otherwise, the Roi will be ignored when you start to draw the next ROI or when you press OK for confirming ROI selection.

- ***Bleaching region type*** has to be one of the predefined values. Currently "rectrangle", "circle" and "polygon" options are supported. Software will only allow to select regions which match this specification (corresponding selection tools are automatically activated each time mew image is opened):
	- "rectangle" - use rectangle drawing tool to select rectangular regions.
	- "polygon" - use polygon selection to select polygon regions with arbitrary number of vertexes.
	- "circle" - use point selection tool to specify the centers of the circular regions. All selected regions will be of the same size, specified by ***Bleaching radius for circle*** parameter.