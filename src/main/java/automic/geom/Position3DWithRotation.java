package automic.geom;

public class Position3DWithRotation extends Point3D {
	private Double angle;


	public Position3DWithRotation(Double _x, Double _y, Double _z, Double _angle) {
		super(_x,_y,_z);
		angle=_angle;
	}
	
	public Double getAngle(){
		return angle;
	}

	public void setAngle(Double _angle) {
		angle=_angle;
	}

}
