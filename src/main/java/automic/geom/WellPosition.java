package automic.geom;

public class WellPosition implements Comparable<WellPosition> {
	private String well;
	private Integer position;
	
	public WellPosition(String _well, int _position) {
		well=_well;
		position=_position;
	}
	
	
	public String getWell() {
		return well;
	}
	
	public int getPosition() {
		return position;
	}
	
	@Override
	public int compareTo(WellPosition reference){
		int wellComparison=this.well.compareTo(reference.well);
		if (wellComparison!=0)
			return wellComparison;
		return this.position.compareTo(reference.position);
	}
	
	@Override
	public String toString() {
		return String.format("Well: %s; Position: %d", well, position);
	}
	
	@Override
	public final int hashCode() {
	    int result = 1000;
	    if (well != null) {
	        result = 31 * result + well.hashCode();
	    }
	    if (position != null) {
	        result = 31 * result + position;
	    }
	    return result;
	}
	
	@Override
	public boolean equals(Object reference) {
		if (reference == this)
            return true;
        if (!(reference instanceof WellPosition))
            return false;
        WellPosition referenceO = (WellPosition) reference;
		
		return (this.compareTo(referenceO)==0);
	}
	
}
