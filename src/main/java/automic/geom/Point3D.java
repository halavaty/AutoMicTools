package automic.geom;

public class Point3D {
	private Double x;
	private Double y;
	private Double z;
	
	private String name = null;
	private String scene = null;
	private String microscopeJob = null;
	
	
	
	public Point3D(Double _x, Double _y, Double _z) {
		x=_x;
		y=_y;
		z=_z;
	}

	public Point3D(Double _x, Double _y, Double _z, String name, String scene, String microscopeJob) {
		this(_x,_y,_z);
		setScene(scene);
		setName(name);
		setMicroscopeJob(microscopeJob);
	}


	public Point3D(Integer _xInt, Integer _yInt, Integer _zInt) {
		x=(_xInt==null)?null:Double.valueOf(_xInt);
		y=(_yInt==null)?null:Double.valueOf(_yInt);
		z=(_zInt==null)?null:Double.valueOf(_zInt);
	}
	
	public Point3D(Integer _x, Integer _y, Integer _z, String name, String scene, String microscopeJob) {
		this(_x,_y,_z);
		setScene(scene);
		setName(name);
		setMicroscopeJob(microscopeJob);
	}

	
	public Double getX(){
		return x;
	}
	
	public Double getY(){
		return y;
	}

	public Double getZ(){
		return z;
	}
	
	public Integer getXInteger(){
		if (x==null)
			return null;
		return x.intValue();
	}
	
	public Integer getYInteger(){
		if (y==null)
			return null;
		return y.intValue();
	}

	public Integer getZInteger(){
		if (z==null)
			return null;
		return z.intValue();
	}
	
	
	public void setX(Double _x) {
		x=_x;
	}
	
	public void setY(Double _y) {
		y=_y;
	}
	
	public void setZ(Double _z) {
		z=_z;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}
	
	public String getScene() {
		return this.scene;
	}


	public void setScene(String scene) {
		this.scene = scene;
	}

	public String getMicroscopeJob() {
		return this.microscopeJob;
	}


	public void setMicroscopeJob(String microscopeJob) {
		this.microscopeJob = microscopeJob;
	}

}
