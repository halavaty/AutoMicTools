package automic.postaq.step;

import automic.parameters.ParametrisedFunction;
import automic.utils.DebugVisualiserSettings;

public interface StepFunctions extends ParametrisedFunction{

	String getStepInformation();

	/**
	 *  Perfore before iterating through datasets.
	 *  For example, add columns to the table where analysis results will be stored.
	 * @param _data is configuration of the step
	 */
	void doBeforeIteratingThroughDatasets(StepData _data) throws Exception;

	void doWithEachDataset(StepData _data,int _rowIndex) throws Exception;

	void doAfterIteratingThroughDatasets(StepData _data) throws Exception;

	DebugVisualiserSettings getDebugVisualiserSettings();
}