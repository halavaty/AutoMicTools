package automic.postaq.step.cell;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.TIFProcessorImageJ;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.segment.ParticleSegmentor;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;

public class Step_SegmentDapi implements StepFunctions {
	//input parameters
	
	String segmentedNucleiTag;
	String dapiImageTag;
	
	//non-passed parameters
	double nucMinSize=3000;
	double nucFilterSize=10;
	double nucThresh=10000;
	
	
	//?variables referenced by different functions
	ImagePlus dapiImage=null;
	Roi [] nucRois=null;
	
	@Override
	public String getStepInformation(){
		return "Segment nuclei";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Segmented Nuclei Roi Tag", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("DAPI Image Tag", 			null, null, ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		newParameterCollection.addParameter("Minimal nucleus size", 	null, nucMinSize, 	ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Filter Size Nucleus",		null, nucFilterSize,ParameterType.DOUBLE_PARAMETER);
		newParameterCollection.addParameter("Nucleus Threshold",		null, nucThresh,	ParameterType.DOUBLE_PARAMETER);
		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		segmentedNucleiTag=	(String)_stepParameterCollection.getParameterValue("Segmented Nuclei Roi Tag");
		dapiImageTag=		(String)_stepParameterCollection.getParameterValue("DAPI Image Tag");
			
		nucMinSize=			(Double)_stepParameterCollection.getParameterValue("Minimal nucleus size");
		nucFilterSize=		(Double)_stepParameterCollection.getParameterValue("Filter Size Nucleus");
		nucThresh=			(Double)_stepParameterCollection.getParameterValue("Nucleus Threshold");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		workingProcessor.addFileColumns(segmentedNucleiTag, "ROI");
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		dapiImage=TIFProcessorImageJ.openTIFImage(workingTable.getFile(_rowIndex, dapiImageTag, "IMG"));
		//dapiImage.show();
		
		nucRois=segmentNuclei(dapiImage,_data);
		
		if (ROIManipulator2D.isEmptyRoiArr(nucRois)){
			workingTable.setBooleanValue(false, _rowIndex, "Success");
		}else{
			workingTable.setBooleanValue(true, _rowIndex, "Success");
			workingProcessor.saveRoisToTable(_rowIndex, nucRois, segmentedNucleiTag, String.format("Dataset_%05d", _rowIndex+1));
		}
		return;
	}
	
	private Roi[] segmentNuclei(ImagePlus _rawDapiMaxImage,StepData _data)throws Exception{
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		_data.showDebugMessage("nucFilterSize="+nucFilterSize);
		_data.showDebugMessage("nucMinSize="+nucMinSize);
		_data.showDebugMessage("nucThresh="+nucThresh);
		
		_data.showDebugImage(_rawDapiMaxImage, "Original image", true);
		//IJ.run(_rawDapiMaxImage, "Subtract...", "value="+back_DAPIMax);
		IJ.run(_rawDapiMaxImage, "Median...", "radius="+nucFilterSize);
		_data.showDebugImage(_rawDapiMaxImage, "Filtered image", true);
		
		ParticleSegmentor nucSegm=new ParticleSegmentor((int)nucMinSize);
		/*ImagePlus nucMaskImg=*/nucSegm.SegSliceThreshold(_rawDapiMaxImage, ParticleAnalyzer.SHOW_MASKS, true, nucThresh);
		
		nucRois=rMan.getRoisAsArray();
			
		_data.showDebugImage(_rawDapiMaxImage, "Filtered image with overlay", true, nucRois);
		
		
		return nucRois;
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
}
