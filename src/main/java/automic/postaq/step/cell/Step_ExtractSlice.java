package automic.postaq.step.cell;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.ImagePlus;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;

public class Step_ExtractSlice  implements StepFunctions{
	
	
	//input parameters
	String inputImageTag;
	String outputImageTag;
	int channelIndex;
	int timeIndex;
	int zIndex;
	
	Duplicator duplicator=null;
	ZProjector maxProjector=null;
	
	@Override
	public String getStepInformation(){
		return "Get slice from (hyper-)stack";
	}
	
	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		newParameterCollection.addParameter("Input Image Tag", 	null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Output Image Tag", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Channel Index", 	null, null, ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Time Index", 		null, null, ParameterType.INT_PARAMETER);
		newParameterCollection.addParameter("Slice Index", 		null, null, ParameterType.INT_PARAMETER);
		
		return newParameterCollection;
	}

	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		inputImageTag=	(String)_stepParameterCollection.getParameterValue("Input Image Tag");
		outputImageTag=	(String)_stepParameterCollection.getParameterValue("Output Image Tag");
		channelIndex=	(int)	_stepParameterCollection.getParameterValue("Channel Index");
		timeIndex=		(int)	_stepParameterCollection.getParameterValue("Time Index");
		zIndex=			(int)	_stepParameterCollection.getParameterValue("Slice Index");
	}

	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		duplicator=new Duplicator();
		maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);

		TableProcessor tableProcessor=_data.getWorkingProcessor();
		tableProcessor.addFileColumns(outputImageTag, "IMG");

	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		ImagePlus inputImage,outputImage;
		
		final String fileName=String.format("Dataset_%05d", _rowIndex+1);
		
		inputImage=ImageOpenerWithBioformats.openImage(workingProcessor.getFile(_rowIndex, inputImageTag, "IMG"));

		outputImage=duplicator.run(inputImage, channelIndex, channelIndex, zIndex, zIndex, timeIndex, timeIndex);
		
		workingProcessor.saveImageToTable(_rowIndex, outputImage, outputImageTag, fileName);
	}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return null;
	}
}
