package automic.postaq.step.common;


import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.process.ImageStatistics;

public class Step_MeasureRoiImage implements StepFunctions {
	//input parameters
	
	String roiTag;
	String imageTag;
	String quantificationTagPrefix;
	
	String quantificationTagMean;
	String quantificationTagArea;
	
	
	ImageOpener imageOpener;
	
	@Override
	public String getStepInformation(){
		return "Measure Roi on the image";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		//parameters without default values
		newParameterCollection.addParameter("Roi Tag", 					null, null, 		ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Image Tag", 				null, null, 		ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Quantification Tag Prefix",null, null, 		ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		return newParameterCollection;
	}
	
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		roiTag=					(String)_stepParameterCollection.getParameterValue("Roi Tag");
		imageTag=				(String)_stepParameterCollection.getParameterValue("Image Tag");
		quantificationTagPrefix=(String)_stepParameterCollection.getParameterValue("Quantification Tag Prefix");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{
		imageOpener=new ImageOpener();
		
		
		quantificationTagMean=quantificationTagPrefix+".Mean";
		quantificationTagArea=quantificationTagPrefix+".Area";
		
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		//workingProcessor.addFileColumns(nucleiMaskImageTag, "IMG");
		workingProcessor.addValueColumn(quantificationTagMean, "NUM");
		workingProcessor.addValueColumn(quantificationTagArea, "NUM");
		
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		
		
		TableModel workingTable=_data.getWorkingTable();
		TableProcessor workingProcessor=_data.getWorkingProcessor();
		
		if (!workingTable.getBooleanValue(_rowIndex, "Success"))
			return;
		
		ImagePlus inputImage=imageOpener.openImage(workingTable.getFile(_rowIndex, imageTag, "IMG"));
		Roi objectRoi=ROIManipulator2D.flsToRois(workingTable.getFile(_rowIndex, roiTag, "ROI"))[0];
		
		IJ.run(inputImage, "Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
		
		inputImage.setRoi(objectRoi);
		ImageStatistics stat=inputImage.getStatistics();
		workingProcessor.setValue(stat.mean, _rowIndex, quantificationTagMean, "NUM");
		workingProcessor.setValue(stat.area, _rowIndex, quantificationTagArea, "NUM");

		
		return;
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}
	
	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
}
