package automic.postaq.step;

import java.awt.Color;
import java.io.File;
import java.util.Date;

import automic.Settings;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiser;
import automic.utils.DebugVisualiserSettings;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.roi.ROIManipulator3D;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import mcib3d.geom.Objects3DPopulation;

public class StepData {
	
	private TableModel workingTable=null;
	private TableProcessor workingProcessor=null;
	
	private File inputTableFile=null;
	private File outputTableFile=null;
	
	private DebugVisualiser debugVisualiser=null;
	private boolean debugFlag=false;
	
	protected StepData() {}
	
	public StepData(File _inputTableFile, File _outputTableFile)throws Exception{
		this();
		
		inputTableFile=_inputTableFile;
		outputTableFile=_outputTableFile;
		
		if (!inputTableFile.exists())
			throw new Exception("Unexpected input for _inputTableFile");
		
		if (inputTableFile.isFile()) { 
			workingTable=new TableModel(inputTableFile);
			workingTable.setRootPathDeep(_outputTableFile.getParent());
		}
		if (inputTableFile.isDirectory())
			workingTable=new TableModel(outputTableFile.getParent());

		workingProcessor=new TableProcessor(workingTable);
			
	}
	
	public void setWorkingTable(TableModel _table){
		workingTable=_table;
		workingProcessor=new TableProcessor(workingTable);
	}
	
	public TableModel getWorkingTable(){
		return workingTable;
	}

	public TableProcessor getWorkingProcessor(){
		return workingProcessor;
	}

	public String getAnalysisPath(){
		return (outputTableFile!=null)?outputTableFile.getParent():null;
	}
	
	public File getInputFile() {
		return inputTableFile;
	}
	
	public void saveTableOutput()throws Exception{
		if (outputTableFile==null)
			throw new Exception("Output Table file is not defined");
		
		if(!workingTable.isRootPath(outputTableFile.getParent()))
			workingTable.setRootPathDeep(outputTableFile.getParent());
		
		workingTable.writeNewFile(outputTableFile.getName(), true);
	}

	//methods to work with debug
	public void setDebug(boolean _debug, DebugVisualiserSettings _settings){
		debugFlag=_debug;
		if (!debugFlag){
			debugVisualiser=null;
			return;
		}
		if(_settings==null)
			throw new NullPointerException("Trying to debug the step which currently has no defined Debug Visualisation settings");
		debugVisualiser=new DebugVisualiser(_settings);
	}
	
	public boolean getDebugFlag(){
		return debugFlag;
	}

	public void showDebugImage(ImagePlus _image,String _title,boolean _imageCopy){
		if(!debugFlag)return;
		debugVisualiser.addImage(_image, _title, _imageCopy);
	}

	public void showDebugImage(ImagePlus _image,String _title,boolean _imageCopy,Overlay _overlay){
		if(!debugFlag)return;
		debugVisualiser.addImage(_image, _title, _imageCopy, _overlay);
	}

	public void showDebugImage(ImagePlus _image,String _title,boolean _imageCopy,Roi _roi){
		if(!debugFlag)return;
		debugVisualiser.addImage(_image, _title, _imageCopy, new Overlay(_roi));
	}
	
	public void showDebugImage(ImagePlus _image,String _title,boolean _imageCopy,Roi[] _rois){
		if(!debugFlag)return;
		debugVisualiser.addImage(_image, _title, _imageCopy, ROIManipulator2D.roisToOverlay(_rois));
	}

	public void showDebugImage(ImagePlus _image,String _title,boolean _imageCopy,Objects3DPopulation _objects3D, Color _color){
		if(!debugFlag)return;
		debugVisualiser.addImage(_image, _title, _imageCopy, ROIManipulator3D.populationToOverlay(_objects3D, _color));
	}
	
	public void showDebugMessage(String _message){
		if(!debugFlag)return;
		System.out.println(String.format("%s [Debug message]: %s",Settings.dateFormat.format(new Date()),_message));
	}
	
	public void closeDebugImages(){
		if(!debugFlag)return;
		debugVisualiser.closeAllImages();
	}
}
