package automic.postaq.step;

public class RunningStepFactory {
	private final Class<? extends StepFunctions> type;

	public RunningStepFactory(Class<? extends StepFunctions> type) {
		if (!StepFunctions.class.isAssignableFrom(type)) {
			throw new ClassCastException(
				String.format(
					"Step type %s must be derived from %s",
					type.getName(),
					StepFunctions.class.getSimpleName()
				)
			);
		}

		this.type = type;
	}
	
	public RunningStep create() throws InstantiationException, IllegalAccessException {
		return new RunningStep(type.newInstance());
	}
}
