package automic.postaq.step;

import java.io.File;

import automic.parameters.Parameter;
import automic.parameters.ParameterCollection;
import automic.utils.IndexRange;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.roi.ROIManipulator2D;
import ij.gui.WaitForUserDialog;

public class RunningStep {
	StepData data;

	StepFunctions functions;
	ParameterCollection parameters;
	
	public static Logger logger;
	
	static{
		logger=ApplicationLogger.getLogger();
	}
	
	public RunningStep(StepFunctions _functions){
		functions=_functions;
		parameters=functions.createParameterCollection();
		data=null;
	}
	
	public ParameterCollection getParameterCollection(){
		return parameters;
	}
	
	public void fillUnderfindedParametersWithDefaultValues(){
		Parameter parameter;
		
		for (String key:parameters.getParametersIndetifiers()){

			parameter=parameters.getParameterObject(key);
			if(parameter.isDefined()) continue;
			if(parameter.hasDefault()){
				parameter.setUnderfinedValueFromDefault();
				logger.sendWarningMessage(String.format("Parameter %s of the step %s is set by its default value %s", key,getStepName(),parameter.getStringValue()));
			}else{
				logger.sendErrorMessage(String.format("Parameter %s of the step %s is not specified and has not default value. Processing willl be aborted", key,getStepName()));
				throw new IllegalStateException("Problem with pipeline consistency");
			}
		}
	}
	
	public void parseFunctionParameters(){
		functions.parseInputSettingValues(parameters);
	}
	
	public String getStepName(){
		return functions.getClass().getSimpleName();
	}
	
	/**
	 * needed to set parameter in the predefined pipeline (code or script)
	 * @param _key
	 * @param _value
	 * @throws Exception when provided parameter object can not be casted to the expected parameter type
	 */
	public void setInputSettingValue(String _key, Object _value){
		parameters.setParameterValue(_key,_value);
	}
	
	/**
	 * 
	 * @param _key
	 * @param _stringValue
	 * @throws Exception when can not parse _stringValue to the right Object type
	 */
	public void parseInputStringSettingValue(String _key, String _stringValue)throws Exception{
		parameters.setParameterValueFromString(_key,_stringValue);
	}
	
	public void setStepData(StepData _data)throws Exception{
		data=_data;
	}
	
	public void setStepData(File _inputTableFile, File _outputTableFile, boolean _debugRun){
		try{
			data=new StepData(_inputTableFile,_outputTableFile);
		}catch (Exception ex){
			throw new RuntimeException(ex.getMessage());
		}
		data.setDebug(_debugRun, functions.getDebugVisualiserSettings());
	}
	
	public StepData getStepData(){
		return data;
	}
	
	//running functions
	public void run(int _saveoutputTableIterations,int _startDatasetIndex,int _endDatasetIndex)throws Exception{
		logger.sendMessage("Running step: "+this.getStepName());
		ROIManipulator2D.getEmptyRm();
		
		functions.doBeforeIteratingThroughDatasets(data);
		
		int counter=0;
		int nData=data.getWorkingTable().getRowCount();
		
		IndexRange loopRange=new IndexRange(_startDatasetIndex, _endDatasetIndex, nData);

		if((loopRange.start>0)||(loopRange.end<nData))
			logger.sendWarningMessage(String.format("Only %d datasets from %d datasets will be processed", loopRange.end-loopRange.start,nData));

		logger.sendMessage("");
		
		boolean debugMode=data.getDebugFlag();
		
		for(int i=loopRange.start;i<loopRange.end;i++){
			logger.sendUpdatedMessage(String.format("Processing dataset %d out of %d",i+1,nData));//IJ.log(String.format("\\Update: Processing dataset %d out of %d",i+1,nData));

			data.closeDebugImages();
			functions.doWithEachDataset(data,i);
			
			if (debugMode)
				new WaitForUserDialog("Check Step output", "Check step output").show();
			
			counter++;
			if(counter>=_saveoutputTableIterations){
				data.saveTableOutput();
				counter=0;
			}
		}
		
		data.closeDebugImages();//close debug images after last dataset was checked
		
		functions.doAfterIteratingThroughDatasets(data);
		
		data.saveTableOutput();

		logger.sendUpdatedMessage("Step DONE!");
	}
}
