package automic.postaq.step.table;

import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.roi.ROIManipulator3D;
import mcib3d.geom.Objects3DPopulation;

public class Step_ExpandSingle3DRoiObjects implements StepFunctions {
	//input parameters
	//String analysisPath;
	String groupRoiTag;
	String singleRoiTag;
	String processBooleanTag;
	
	TableModel newTable;
	TableProcessor newTableProcessor;
	String[] oldTableColumnNames;
	
	@Override
	public String getStepInformation(){
		return "Expand table by creating individual row for each 3D object.";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		//parameters without default values
		newParameterCollection.addParameter("Grouped Rois column Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Individual Rois column Tag",		null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Process flag column tag",			null, null, ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		groupRoiTag=				(String)_stepParameterCollection.getParameterValue("Grouped 3D Rois column Tag");
		singleRoiTag=				(String)_stepParameterCollection.getParameterValue("Individual 3D Rois column Tag");
		processBooleanTag=			(String)_stepParameterCollection.getParameterValue("Process flag column tag");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{

		TableModel oldTable=_data.getWorkingTable();
		oldTableColumnNames=oldTable.getColumnNames();
		
		newTable=new TableModel(0, oldTableColumnNames, oldTable.getRootPath());
		newTableProcessor=new TableProcessor(newTable);
		newTableProcessor.addFileColumns(singleRoiTag, "ROI");
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{
		TableModel oldTable=_data.getWorkingTable();
		
		
		boolean processFlag=oldTable.getBooleanValue(_rowIndex, processBooleanTag);
		if (!processFlag){
			copyOldValues(oldTable, newTable, oldTableColumnNames, _rowIndex);
			return;
		}
		File groupObjectsFile=oldTable.getFile(_rowIndex, groupRoiTag, "ROI");
		Objects3DPopulation groupPopulation=ROIManipulator3D.fileToPopulation(groupObjectsFile);
		//int objectCount=0;
		int nGroupObjects=groupPopulation.getNbObjects();
		for (int objectGroupIndex=0;objectGroupIndex<nGroupObjects;objectGroupIndex++){
			//objectCount++;
			int newRowIndex=copyOldValues(oldTable, newTable, oldTableColumnNames, _rowIndex);
			Objects3DPopulation singleObjectPopulation=new Objects3DPopulation();
			singleObjectPopulation.addObject(groupPopulation.getObject(objectGroupIndex));
			newTableProcessor.save3DRoisToTable(newRowIndex, singleObjectPopulation, singleRoiTag,String.format("%s_%04d", FileUtils.cutExtension(groupObjectsFile.getName()),objectGroupIndex+1));
		}
			
	}
	
	private int copyOldValues(TableModel _fromTable, TableModel _toTable,String[] _columnNames, int _fromTableRow){
		int toTableRow=_toTable.getRowCount();
		_toTable.addRow();

		for (String columnName:_columnNames)
			_toTable.setValueAt(_fromTable.getValueAt(_fromTableRow, _fromTable.getColumnIndex(columnName)), toTableRow, _toTable.getColumnIndex(columnName));
		
		return toTableRow;
	}
	
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{
		_data.setWorkingTable(newTable);
	}

	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return new DebugVisualiserSettings(-2,10,10,2);
	}
	
}
