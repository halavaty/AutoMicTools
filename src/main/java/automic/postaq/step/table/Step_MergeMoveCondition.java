package automic.postaq.step.table;

import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableTools;
import automic.utils.DebugVisualiserSettings;

public class Step_MergeMoveCondition implements StepFunctions {
	//input parameters
	//String analysisPath;
	String wellPositionImageTag;
	String wellPositionRegularExpression;
	String experimentTableSubPathTemplate;
	String experimentColumnTag;
	String wellColumnTag;
	
	Pattern fileNamePattern;
	
	TableModel manualTable;
	
	class ConditionMapper{
		Map<String, Map<Integer,Integer>> manualTableIndexMap;
		
		public ConditionMapper(TableModel _conditionTable,String _experimentColumnTag,String _wellColumnTag)throws Exception{
			manualTableIndexMap=new LinkedHashMap<String, Map<Integer,Integer>>();
			
			int manualTableRowCount=manualTable.getRowCount();
			String expTag;
			int well;
			
			Map<Integer,Integer> expMap;
			for (int i=0;i<manualTableRowCount;i++){
				expTag=manualTable.getFactorValue(i, experimentColumnTag);
				
				if (manualTableIndexMap.containsKey(expTag)){
					expMap=manualTableIndexMap.get(expTag);
				}else{
					expMap=new LinkedHashMap<Integer,Integer>();
					manualTableIndexMap.put(expTag, expMap);
				}
				well=(manualTable.hasValueTag(_wellColumnTag, "NUM"))?(int)manualTable.getNumericValue(i, wellColumnTag):1;
				if(expMap.containsKey(well))
					throw new Exception("Repeated Experiment-well pair in manual table");
				expMap.put(well, i);
				
			}
		}
		
		public void mapTable(TableModel _dataTable, String _experimentTag)throws Exception{
			fileNamePattern=Pattern.compile(wellPositionRegularExpression);
			//
			String a="[a-zA-Z0-9-_]+_W(?<Well>\\d+)_P(?<Position>\\d+)_T(?<TimePoint>\\d+).lsm";
			System.out.println(wellPositionRegularExpression);
			System.out.println(StringUtils.difference(wellPositionRegularExpression, a));
			//
			
			Map<Integer,Integer> experimentMap=manualTableIndexMap.get(_experimentTag);
			if (experimentMap==null)
				throw new Exception("Error of the index map");
			int rowCount=_dataTable.getRowCount();
			int dataColumnCount=_dataTable.getColumnCount();
			int conditionColumnCount=manualTable.getColumnCount();
			
			for(String columnName:manualTable.getColumnNames())
				_dataTable.addColumn(columnName);
			
					
			for (int rowIndex=0;rowIndex<rowCount;rowIndex++){
				String fileName=_dataTable.getFileName(rowIndex, wellPositionImageTag, "IMG");
				if(fileName==null) continue;
				Matcher matcher=fileNamePattern.matcher(fileName);
				if(!matcher.matches())
					throw new Exception("No match to the file name");
				int well=Integer.parseInt(matcher.group(wellColumnTag));
				Integer conditionTableIndex=experimentMap.get(well);
				if(conditionTableIndex==null)
					throw new Exception("Underfined condition table index");
				for (int columnIndex=0;columnIndex<conditionColumnCount;columnIndex++){
					_dataTable.setValueAt(manualTable.getValueAt(conditionTableIndex, columnIndex), rowIndex, columnIndex+dataColumnCount);
				}
				//TableTools.addColumnsFromTable(_dataTable, getWorkingTable(), conditionTableIndex);
			}
		}
		
		public Collection<String> getExperimentTags(){
			return manualTableIndexMap.keySet();
		}
	}	
	
	@Override
	public String getStepInformation(){
		return "Merge, move and condition table(s)";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		//parameters without default values
		newParameterCollection.addParameter("Well Position File Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Well Position Regular Expression", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Experiment Table SubPath Template",null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Experiment Column Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Well Column Tag", 					null, null, ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		wellPositionImageTag=			(String)_stepParameterCollection.getParameterValue("Well Position File Tag");
		wellPositionRegularExpression=	(String)_stepParameterCollection.getParameterValue("Well Position Regular Expression");
		experimentTableSubPathTemplate=	(String)_stepParameterCollection.getParameterValue("Experiment Table SubPath Template");
		experimentColumnTag=			(String)_stepParameterCollection.getParameterValue("Experiment Column Tag");
		wellColumnTag=					(String)_stepParameterCollection.getParameterValue("Well Column Tag");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{

		final String analysisPath=_data.getAnalysisPath();
		
		manualTable=_data.getWorkingTable();
		String oldRootPath=manualTable.getRootPath();
		
		ConditionMapper conditionMapper=new ConditionMapper(manualTable,experimentColumnTag,wellColumnTag);
		
		int expCount=0;
		TableModel mergedTable=null, addTable=null;
		for (String expTag:conditionMapper.getExperimentTags()){
			addTable=new TableModel(new File(oldRootPath,String.format(experimentTableSubPathTemplate, expTag)));
			conditionMapper.mapTable(addTable, expTag);
			addTable.setRootPathDeep(analysisPath);
			if(expCount==0){
				mergedTable=addTable;
			}else{
				TableTools.copyAllRows(mergedTable, addTable);
			}
			expCount++;
		}
		
		_data.setWorkingTable(mergedTable);
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}

	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return null;
	}
}
