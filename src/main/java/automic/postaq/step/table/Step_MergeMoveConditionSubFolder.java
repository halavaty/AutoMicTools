package automic.postaq.step.table;

import java.io.File;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.postaq.step.StepData;
import automic.postaq.step.StepFunctions;
import automic.table.TableModel;
import automic.table.TableTools;
import automic.utils.DebugVisualiserSettings;

public class Step_MergeMoveConditionSubFolder implements StepFunctions {
	//input parameters
	//String analysisPath;
	String wellPositionImageTag;
	String wellPositionRegularExpression;
	String experimentTableSubPathTemplate;
	String experimentColumnTag;
	String subfolderColumnTag;

	
	Pattern fileNamePattern;
	
	TableModel manualTable;
	
	class ConditionMapper{
		Map<String, Map<String,Integer>> manualTableIndexMap;
		
		public ConditionMapper(TableModel _conditionTable,String _experimentColumnTag,String _subfolderColumnTag)throws Exception{
			manualTableIndexMap=new LinkedHashMap<String, Map<String,Integer>>();
			
			int manualTableRowCount=manualTable.getRowCount();
			String expTag;
			String subfolder;
			
			Map<String,Integer> expMap;
			for (int i=0;i<manualTableRowCount;i++){
				subfolder=manualTable.getStringValue(i, subfolderColumnTag);
				
				if (manualTableIndexMap.containsKey(subfolder)){
					expMap=manualTableIndexMap.get(subfolder);
				}else{
					expMap=new LinkedHashMap<String,Integer>();
					manualTableIndexMap.put(subfolder, expMap);
				}
				expTag=manualTable.getFactorValue(i, experimentColumnTag);
				if(expMap.containsKey(expTag))
					throw new Exception("Repeated subfolder-experiment pair in manual table");
				expMap.put(expTag, i);
				
			}
		}
		
		public void mapTable(TableModel _dataTable, String _experimentTag, String _subfolder)throws Exception{
			fileNamePattern=Pattern.compile(wellPositionRegularExpression);
			//
			String a="[a-zA-Z0-9-_]+_W(?<Well>\\d+)_P(?<Position>\\d+)_T(?<TimePoint>\\d+).lsm";
			System.out.println(wellPositionRegularExpression);
			System.out.println(StringUtils.difference(wellPositionRegularExpression, a));
			//
			
			Map<String,Integer> experimentMap=manualTableIndexMap.get(_subfolder);
			if (experimentMap==null)
				throw new Exception("Error of the index map");
			int rowCount=_dataTable.getRowCount();
			int dataColumnCount=_dataTable.getColumnCount();
			int conditionColumnCount=manualTable.getColumnCount();
			
			for(String columnName:manualTable.getColumnNames())
				_dataTable.addColumn(columnName);
			
					
			for (int rowIndex=0;rowIndex<rowCount;rowIndex++){
				String fileName=_dataTable.getFileName(rowIndex, wellPositionImageTag, "IMG");
				if(fileName==null) continue;
				Matcher matcher=fileNamePattern.matcher(fileName);
				if(!matcher.matches())
					throw new Exception("No match to the file name");
				//int well=Integer.parseInt(matcher.group("Well"));
				Integer conditionTableIndex=experimentMap.get(_experimentTag);
				if(conditionTableIndex==null)
					throw new Exception("Underfined condition table index");
				for (int columnIndex=0;columnIndex<conditionColumnCount;columnIndex++){
					_dataTable.setValueAt(manualTable.getValueAt(conditionTableIndex, columnIndex), rowIndex, columnIndex+dataColumnCount);
				}
				//TableTools.addColumnsFromTable(_dataTable, getWorkingTable(), conditionTableIndex);
			}
		}
		
		public Collection<String> getExperimentTags(){
			return manualTableIndexMap.keySet();
		}
	}	
	
	@Override
	public String getStepInformation(){
		return "Merge, move and condition table(s)";
	}

	@Override
	public ParameterCollection createParameterCollection() {
		ParameterCollection newParameterCollection=new ParameterCollection();
		
		//parameters without default values
		newParameterCollection.addParameter("Well Position File Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Well Position Regular Expression", null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Experiment Table SubPath Template",null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Experiment Column Tag", 			null, null, ParameterType.STRING_PARAMETER);
		newParameterCollection.addParameter("Subfolder Column Tag", 			null, null, ParameterType.STRING_PARAMETER);
		
		//parameters with default values
		
		return newParameterCollection;
	}
	
	@Override
	public void parseInputSettingValues(ParameterCollection _stepParameterCollection){
		wellPositionImageTag=			(String)_stepParameterCollection.getParameterValue("Well Position File Tag");
		wellPositionRegularExpression=	(String)_stepParameterCollection.getParameterValue("Well Position Regular Expression");
		experimentTableSubPathTemplate=	(String)_stepParameterCollection.getParameterValue("Experiment Table SubPath Template");
		experimentColumnTag=			(String)_stepParameterCollection.getParameterValue("Experiment Column Tag");
		subfolderColumnTag=				(String)_stepParameterCollection.getParameterValue("Subfolder Column Tag");
	}
	
	@Override
	public void doBeforeIteratingThroughDatasets(StepData _data)throws Exception{

		final String analysisPath=_data.getAnalysisPath();
		
		manualTable=_data.getWorkingTable();
		String oldRootPath=manualTable.getRootPath();
		
		ConditionMapper conditionMapper=new ConditionMapper(manualTable,experimentColumnTag,subfolderColumnTag);
		
		int expCount=0;
		TableModel mergedTable=null, addTable=null;
		
		for (int manualTableIndex=0;manualTableIndex<manualTable.getRowCount();manualTableIndex++){
		
		//for (String expTag:conditionMapper.getExperimentTags()){
			String expTag=manualTable.getFactorValue(manualTableIndex, experimentColumnTag);
			String subfolder=manualTable.getStringValue(manualTableIndex, subfolderColumnTag);
			addTable=new TableModel(new File(new File(oldRootPath,subfolder),String.format(experimentTableSubPathTemplate, expTag)));
			conditionMapper.mapTable(addTable, expTag,subfolder);
			addTable.setRootPathDeep(analysisPath);
			if(expCount==0){
				mergedTable=addTable;
			}else{
				TableTools.copyAllRows(mergedTable, addTable);
			}
			expCount++;
		}
		
		_data.setWorkingTable(mergedTable);
	}
	
	@Override
	public void doWithEachDataset(StepData _data,int _rowIndex)throws Exception{}
	
	@Override
	public void doAfterIteratingThroughDatasets(StepData _data) throws Exception{}

	@Override
	public DebugVisualiserSettings getDebugVisualiserSettings(){
		return null;
	}
}
