package automic.postaq.pipeline.predefined;

import java.util.List;

import automic.postaq.step.RunningStep;

public interface PredefinedPipelineSteps{
	
	List<RunningStep> createStepsList() throws Exception;
}
