package automic.postaq.pipeline;

import automic.postaq.step.RunningStep;

public interface PipelineInterface {
	/**
	 * setup protocol
	 * @throws Exception
	 */
	public abstract RunningStep[] getStepsArray();
	
	/**
	 * run processing protocol
	 */
	//public abstract void runProtocol(String _options)throws Exception;

	/**
	 * parses array of strings to the class input. Will be run when arguments are set via console
	 * @param _args is array of strings to parse
	 */
	//public abstract void parseConsoleInput(String[] _args);
	
	
	/**
	 * set default input arguments (for the test runs with IDE)
	 */
	//public abstract void parseDefaultInput();
	
	//public abstract File getLastOutputTableFile();
}
