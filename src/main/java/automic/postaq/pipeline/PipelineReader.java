package automic.postaq.pipeline;

import automic.postaq.step.RunningStep;

public class PipelineReader{
	PipelineInterface pipeline;
	
	public PipelineReader(PipelineInterface _pipeline) {
		pipeline=_pipeline;
	}
	
	public String[] getStepNames(){
		RunningStep[] steps=pipeline.getStepsArray();
		int nSteps=steps.length;
		String[] stepNames=new String[nSteps];
		for (int i=0;i<steps.length;i++)
			stepNames[i]=steps[i].getStepName();
		
		return stepNames;
	}
}
