package automic.postaq.pipeline;

import java.util.ArrayList;
import java.util.List;
//import java.util.Set;

//import automic.parameters.Parameter;
//import automic.parameters.ParameterCollection;
import automic.postaq.pipeline.predefined.PredefinedPipelineSteps;
import automic.postaq.step.RunningStep;
//import automic.utils.logger.ApplicationLogger;
//import automic.utils.logger.Logger;

class PipelineBuilder implements PipelineInterface{
	RunningStep[] processingSteps;
	
	public PipelineBuilder(RunningStep[] _stepArray){
		processingSteps=_stepArray;
	}
	
	public PipelineBuilder(List<RunningStep> _stepList) {
		processingSteps=_stepList.toArray(new RunningStep[_stepList.size()]) ;
	}
	
	@Override
	public RunningStep[] getStepsArray(){
		return processingSteps;
	}
	

}

public class PipelineFactory{
	List<RunningStep> processingSteps;
	
	public PipelineFactory(){
		processingSteps=new ArrayList<RunningStep>();
	}
	
	public PipelineFactory(RunningStep[] _analysisSteps){
		this();
		for (RunningStep step:_analysisSteps)
			this.addStep(step);
	}
	
	public PipelineFactory(List<RunningStep> _analysisSteps){
		processingSteps=_analysisSteps;
	}
	
	public PipelineFactory(PredefinedPipelineSteps _predefinedSteps)throws Exception{
		this(_predefinedSteps.createStepsList());
	}
	
	public void addStep(RunningStep _analysisStep){
		processingSteps.add(_analysisStep);
	}
	
	public PipelineInterface getPipeline(){
		PipelineInterface pipeline=new PipelineBuilder(processingSteps);
		this.setActualParameters(pipeline);
		return pipeline;
	}
	
	private void setActualParameters(PipelineInterface _pipeline){
		
		for (RunningStep step:_pipeline.getStepsArray()){
			step.fillUnderfindedParametersWithDefaultValues();
			
			step.parseFunctionParameters();
		}
	}
}
