package automic.postaq.pipeline.plugins;

import java.io.File;
import java.util.Locale;

import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;
import automic.postaq.pipeline.xml.PipelineXmlReader;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.IJ;
import ij.ImageJ;
import ij.Prefs;
import ij.plugin.PlugIn;
import fiji.util.gui.GenericDialogPlus;


public class Test_pipeline_step_XML implements PlugIn {
	private static final String pipelinePrefsTag="automic.postaq.pipeline.plugins.Run_pipeline_XML.pipelinePath";
	private static final String inputPrefsTag="automic.postaq.pipeline.plugins.Run_pipeline_XML.inputSummaryPath";
	private static final String analysisPrefsTag="automic.postaq.pipeline.plugins.Run_pipeline_XML.analysisSubFolder";
	
	PipelineInterface processingPipeline=null;
	
	String pipelinePath="";
	String inputSummaryPath="";
	String analysisFolderName="Analysis1";
	String inputTableFileName;
	
	int testStep=0;
	int startDataset=0;
	int endDataset=1;
	
	
	@Override
	public void run(String arg) {
		Locale.setDefault(Locale.UK);
		
		Logger logger=new TextWindowLogger("Test XML pipeline");
		ApplicationLogger.setLogger(logger);
		
		

		//select input file and folder
		//OpenDialog dlo = new OpenDialog ("Choose summary file for analysis", null);
		//rootPath=dlo.getDirectory();
		//inputTableFileName=dlo.getFileName();
		//if (inputTableFileName==null){
		//	return; //stop if input file was not selected 
		//}
			
		//create new generic dialog and fill-in settings
		GenericDialogPlus gd_in=new GenericDialogPlus("XML pipeline parameters");

		gd_in.addFileField("Pipeline path", Prefs.get(pipelinePrefsTag, ""), 30);
		gd_in.addDirectoryOrFileField("Input data path", Prefs.get(inputPrefsTag, ""),30);
		
		
		gd_in.addStringField("Analysis folder name: ", Prefs.get(analysisPrefsTag, ""),30);
			
		gd_in.addNumericField("Test analysis step ", 	testStep+1, 0);
		gd_in.addNumericField("Start dataset", 			startDataset+1, 0);
		gd_in.addNumericField("End dataset", 			endDataset+1, 0);

		gd_in.showDialog();
		if (gd_in.wasCanceled()){
			IJ.log("Plugin is aborted by user.");
			return;
		}
    
		pipelinePath=gd_in.getNextString();
		inputSummaryPath=gd_in.getNextString();
		analysisFolderName=gd_in.getNextString();
		testStep=(int)gd_in.getNextNumber()-1;
		startDataset=(int)gd_in.getNextNumber()-1;
		endDataset=(int)gd_in.getNextNumber()-1;
		
		Prefs.set(pipelinePrefsTag, pipelinePath);
		Prefs.set(inputPrefsTag, inputSummaryPath);
		Prefs.set(analysisPrefsTag, analysisFolderName);
		Prefs.savePreferences();
		
		//load pipeline
		PipelineXmlReader xmlReader=new PipelineXmlReader(pipelinePath);
		
		try{
			processingPipeline=xmlReader.getPipeline();
		}catch (Exception ex){
			logger.sendExceptionMessage("Can not initialise pipeline.\n Plugin stops now", ex);
			return;
		}
		
		//setup pipeline input
		File inputFile=new File(inputSummaryPath);
		PipelineInput pipelineInput=new PipelineInput();
		pipelineInput.dataPath=inputFile.getParent();
		pipelineInput.subPathAnalysis =analysisFolderName;
		pipelineInput.globalExperimentTableName=inputFile.getName();
		pipelineInput.startStep=testStep;
		pipelineInput.endStep=testStep;
		pipelineInput.startDataset=startDataset;
		pipelineInput.endDataset=endDataset;

		try{
			PipelineRunner pipelineRunner=new PipelineRunner(processingPipeline,pipelineInput);
			pipelineRunner.runPipeline(true);
		}catch (Exception e){
			logger.sendExceptionMessage("Exception raised when running pipeline test.\nPlugin will stop",e);
			return;
		}
	}
	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = Test_pipeline_step_XML.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
		//DEBUG=true;
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		IJ.runPlugIn(clazz.getName(), "");
	}

}
