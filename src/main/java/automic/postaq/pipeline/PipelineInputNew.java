package automic.postaq.pipeline;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;

public class PipelineInputNew {
	private ParameterCollection parameterCollection;
	
	public static final String DataPathKey					="Data path";
	public static final String SubPathAnalysisKey			="Analysis subpath";
	public static final String InputExperimentTableNameKey	="Input Experiment table Name";
	public static final String StartStepKey					="Start Step";
	public static final String EndStepKey					="End Step";
	public static final String StartDatasetKey				="Start Dataset";
	public static final String EndDatasetKey				="End Dataset";
	
	public PipelineInputNew(){
		parameterCollection=new ParameterCollection();
		parameterCollection.addParameter(DataPathKey, 				null, null, ParameterType.STRING_PARAMETER);
		parameterCollection.addParameter(SubPathAnalysisKey, 		null, null, ParameterType.STRING_PARAMETER);
		parameterCollection.addParameter(InputExperimentTableNameKey, null, null, ParameterType.STRING_PARAMETER);
		
		parameterCollection.addParameter(StartStepKey,	null, -1, ParameterType.INT_PARAMETER);
		parameterCollection.addParameter(EndStepKey,	null, -1, ParameterType.INT_PARAMETER);
		parameterCollection.addParameter(StartDatasetKey,null, -1, ParameterType.INT_PARAMETER);
		parameterCollection.addParameter(EndDatasetKey,	null, -1, ParameterType.INT_PARAMETER);
	}
	
	public PipelineInputNew(String[] _options)throws Exception{
		parameterCollection.setParameterValueFromString(DataPathKey, _options[0]);
		parameterCollection.setParameterValueFromString(SubPathAnalysisKey, _options[1]);
		parameterCollection.setParameterValueFromString(InputExperimentTableNameKey, _options[2]);
		if(_options.length<4) return;
		parameterCollection.setParameterValueFromString(StartStepKey, _options[3]);
		parameterCollection.setParameterValueFromString(EndStepKey, _options[4]);
		if(_options.length<6) return;
		parameterCollection.setParameterValueFromString(StartDatasetKey,_options[3]);
		parameterCollection.setParameterValueFromString(EndDatasetKey, 	_options[4]);
	}
	
	public PipelineInputNew(String _options)throws Exception{
		this(_options.split("\\s+"));
	}
}
