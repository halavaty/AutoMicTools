package automic.postaq.pipeline;

public class PipelineInput {
	public String dataPath;
	public String subPathAnalysis;
	public String globalExperimentTableName;
	public int startStep=-1;
	public int endStep=-1;
	public int startDataset=-1;
	public int endDataset=-1;
	
	public PipelineInput(){}
	
	public PipelineInput(String[] _options) {
		dataPath=_options[0];
		subPathAnalysis=_options[1];
		globalExperimentTableName=_options[2];
		if(_options.length<4) return;
		startStep=Integer.parseInt(_options[3]);
		endStep=Integer.parseInt(_options[4]);
		if(_options.length<6) return;
		startDataset=Integer.parseInt(_options[5]);
		endDataset=Integer.parseInt(_options[6]);
	}
	
	public PipelineInput(String _options) {
		this(_options.split("\\s+"));
	}
}
