package automic.postaq.pipeline.xml;

import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import automic.parameters.ParameterCollection;
import automic.parameters.xml.Converter;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.step.RunningStep;
import automic.utils.FileUtils;

public class PipelineXMLWriter {
	public PipelineInterface pipeline;
	public File targetFile;
	
	private static DocumentBuilder documentBuilder;
	
	static {
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}	
	}
	
	public PipelineXMLWriter(){}
	
	private void setPipeline(PipelineInterface _pipeline){
		pipeline=_pipeline;
	}
	
	private void setFileToStore(File _file){
		if (!FileUtils.getExtension(_file).equals("xml"))
			throw new IllegalArgumentException("Wrong file exception");
		targetFile=_file;
	}
	
	private Document createDocument()throws Exception{
		Document document=documentBuilder.newDocument();
		
		//create root element
		Element rootElement=document.createElement(PipelineXMLTags.RootTagName);
		document.appendChild(rootElement);
		
		//create header
		Element headerElement=document.createElement(PipelineXMLTags.HeaderTagName);
		rootElement.appendChild(headerElement);
		//TODO: Fill header with pipeline input settings
		
		//add element for steps
		Element stepsElement=document.createElement(PipelineXMLTags.StepsTagName);
		
		//get pipeline steps
		RunningStep[] steps=pipeline.getStepsArray();
		
		Element stepElement;
		Attr stepTypeAttribute;
		String stepTypeName, stepName;
		ParameterCollection stepParameterCollection;
		
		for (RunningStep step:steps){
			stepTypeName=step.getStepName();
			if (!stepTypeName.startsWith(PipelineXMLTags.StepPrefix))
				throw new Exception("Step class is not named correctly");
			stepName=stepTypeName.replaceFirst(PipelineXMLTags.StepPrefix, "");
			
			stepElement=document.createElement(PipelineXMLTags.StepTagName);
			
			stepTypeAttribute=document.createAttribute(PipelineXMLTags.StepTypeAttributeName);
			stepTypeAttribute.setValue(stepName);
			stepElement.setAttributeNode(stepTypeAttribute);
			
			stepParameterCollection=step.getParameterCollection();
			Converter.fillXmlElementFromParameterCollection(stepParameterCollection,document,stepElement);
			
			stepsElement.appendChild(stepElement);
		}
		rootElement.appendChild(stepsElement);
		
		return document;
	}
	
	private void writeDocumentToFile(Document _document)throws Exception{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		
		DOMSource source = new DOMSource(_document);
        
		FileOutputStream fos = new FileOutputStream(targetFile);
		StreamResult result = new StreamResult(fos);
		transformer.transform(source, result);
		// Output to console for testing
        StreamResult consoleResult =	new StreamResult(System.out);
        transformer.transform(source, consoleResult);
        
        fos.close();
	}
	
	public void writePipelineToFile(PipelineInterface _pipeline, File _file)throws Exception{
		this.setPipeline(_pipeline);
		this.setFileToStore(_file);
		
		Document domDocument=createDocument();
		writeDocumentToFile(domDocument);
	}
}
