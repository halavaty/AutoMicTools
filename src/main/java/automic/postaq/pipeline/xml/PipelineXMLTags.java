package automic.postaq.pipeline.xml;

public abstract class PipelineXMLTags {
	public static final String RootTagName = 				"pipeline";
	public static final String HeaderTagName = 				"header";
	public static final String HeaderSettingName = 			"strring";
	public static final String StepsTagName = 				"steps";
	public static final String StepTagName = 				"step";
	public static final String StepTypeAttributeName = 		"type";
	
	public static final String StepPrefix = "Step_";

}
