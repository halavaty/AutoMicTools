package automic.postaq.pipeline.xml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.reflections.Reflections;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
//import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import automic.parameters.ParameterCollection;
import automic.parameters.xml.Converter;
import automic.postaq.pipeline.PipelineFactory;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.step.RunningStep;
//import automic.postaq.step.AbstractStep;
import automic.postaq.step.RunningStepFactory;
import automic.postaq.step.StepFunctions;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;

public class PipelineXmlReader{

	
	
	private static DocumentBuilder documentBuilder;
	private static Map<String, RunningStepFactory> stepFactoriesByName;
	
	private Document document;
	
	static {
		stepFactoriesByName = new HashMap<String, RunningStepFactory>();
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}	
		Reflections reflections = new Reflections("automic.postaq.step");
		Set<Class<? extends StepFunctions>> stepFunctionClasses = 
				reflections.getSubTypesOf(StepFunctions.class);
		for (Class<? extends StepFunctions> type : stepFunctionClasses) {
			String typeName = type.getSimpleName();
			if (!typeName.startsWith(PipelineXMLTags.StepPrefix)) {
				throw new RuntimeException(String.format(
						"%s step definition must start with %s", type.getName(), PipelineXMLTags.StepPrefix));
			}
			String stepNameKey = typeName.replaceFirst(PipelineXMLTags.StepPrefix, "");
			stepFactoriesByName.put(stepNameKey, new RunningStepFactory(type));
		}
	}
	
	public PipelineXmlReader(String _xmlFileAbsolutePath) {
		ApplicationLogger.getLogger().sendInfoMessage("Reading XML file with the pipeline");
		try {
			this.document = documentBuilder.parse(_xmlFileAbsolutePath);			
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
	
	public PipelineXmlReader(InputStream _xmlStream) {
		ApplicationLogger.getLogger().sendInfoMessage("Reading XML stream with the pipeline");
		try {
			this.document = documentBuilder.parse(_xmlStream);			
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
	
	public PipelineInterface getPipeline() throws Exception {
		Logger logger=ApplicationLogger.getLogger();
		logger.sendInfoMessage("Converting XML document to the pipeline");
		
		List<RunningStep> steps = new ArrayList<RunningStep>();
		
		Element root = this.document.getDocumentElement();
		
		// NodeList headers = root.getElementsByTagName(HeaderTagName);
		
		Node pipelineNode = root.getElementsByTagName(PipelineXMLTags.StepsTagName).item(0);
		if (pipelineNode.getNodeType()!=Node.ELEMENT_NODE)
			throw new RuntimeException();
		Element pipelineElement=(Element)pipelineNode;
		NodeList stepNodes = pipelineElement.getElementsByTagName(PipelineXMLTags.StepTagName);
		
		for (int i = 0; i < stepNodes.getLength(); i++) {
			Node stepNode = stepNodes.item(i);
			
			//if(stepNode.getNodeType()!=Node.ELEMENT_NODE)
			//	throw new RuntimeException("Wrong step node");
			//getElementsByTagName() should provide only element nodes 
			
			Element stepElement=(Element)stepNode;
			
			String stepName =stepElement.getAttribute(PipelineXMLTags.StepTypeAttributeName);
			RunningStep step = createRunningStepByName(stepName);//stepFactoriesByName.get(stepName).create();
			ParameterCollection stepParameters=step.getParameterCollection();
			
			Converter.fillParameterCollectionFromXmlElement(stepElement,stepParameters);
			
			steps.add(step);
		}
		
		return new PipelineFactory(steps.toArray(new RunningStep[steps.size()])).getPipeline();
	}
	
	private RunningStep createRunningStepByName(String _name) {
		try {
			return stepFactoriesByName.get(_name).create();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
	}

}
