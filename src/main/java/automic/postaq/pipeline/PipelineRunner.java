package automic.postaq.pipeline;

import automic.Settings;
import automic.postaq.step.RunningStep;
import automic.utils.FileUtils;
import automic.utils.IndexRange;
import automic.utils.Utils;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.WindowManager;
import ij.plugin.frame.RoiManager;

import java.awt.Window;
import java.io.File;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.util.Date;

public class PipelineRunner {
	//public static boolean hideLogAndRm=true;
	
	private	PipelineInterface	pipeline=null;		//pipeline to run
	private int 				pipelineLength;
	
	private PipelineInput 		pipelineInput=null;	//settings to run the pipeline
	private String 				analysisPath;
	private IndexRange 			stepLoopRange;
	
	public PipelineRunner(PipelineInterface _pipelineToRun){
		setPipeline(_pipelineToRun);
	}
	
	public PipelineRunner(PipelineInterface _pipelineToRun, PipelineInput _pipelineInput){
		this(_pipelineToRun);
		this.setPipelineInput(_pipelineInput);
	}
	
	public void setPipeline(PipelineInterface _pipelineToRun){
		pipeline=_pipelineToRun;
		pipelineLength=pipeline.getStepsArray().length;
	}
	
	public void setPipelineInput(PipelineInput _pipelineInput){
		pipelineInput=_pipelineInput;
		analysisPath=Paths.get(_pipelineInput.dataPath, _pipelineInput.subPathAnalysis).toString();
	}
	
	public void runPipeline()throws Exception{
		this.runPipeline(false);
	}
	
	public void runPipeline(boolean _debugRun)throws Exception{
		if (pipeline==null)
			throw new Exception("Pipeline is not defined");
		if (pipelineInput==null)
			throw new Exception("Pipeline input is not defined");
		
		Logger logger=ApplicationLogger.getLogger();
		
		RunningStep[] pipelineSteps=pipeline.getStepsArray();
		
		final DateFormat dateTimeFormat = Settings.dateFormat;
		stepLoopRange=new IndexRange(pipelineInput.startStep, pipelineInput.endStep, pipelineSteps.length);
		if(stepLoopRange.start==0)	FileUtils.createCleanFolder(new File(analysisPath));
		
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		Window logWindow=null;
		
		long startTime,endTime;
		startTime=System.currentTimeMillis();
		
		
		logger.sendInfoMessage("Pipeline start at "+dateTimeFormat.format(new Date(startTime)));
		logger.sendInfoMessage(String.format("%d of %d pipeline steps are to be performed[%d-%d]",stepLoopRange.end-stepLoopRange.start,pipelineSteps.length,stepLoopRange.start+1,stepLoopRange.end));

		
		if(!_debugRun){
			IJ.log("");
			logWindow=WindowManager.getWindow("Log");
			logWindow.setVisible(false);
			rMan.setVisible(false);
		}
		
		RunningStep stepToRun;
		try{
			for (int stepIndex=stepLoopRange.start;stepIndex<stepLoopRange.end;stepIndex++){
				IJ.freeMemory();
				stepToRun=pipelineSteps[stepIndex];
				logger.sendInfoMessage(String.format("Staring Step %d \"%s\"",stepIndex+1,stepToRun.getStepName()));
				stepToRun.setStepData(getStepInputFile(stepIndex), getStepOutputFile(stepIndex), _debugRun);
				stepToRun.run(-1,pipelineInput.startDataset, pipelineInput.endDataset);
			}
		}catch(Exception ex){
			logger.sendExceptionMessage("Exception raised when running pipeline "+pipeline.getClass().getName(), ex);
			throw new RuntimeException();
		}
		
		if(!_debugRun){
			logWindow.setVisible(true);
			rMan.setVisible(true);
		}
		endTime=System.currentTimeMillis();
		logger.sendInfoMessage("Pipeline end at "+dateTimeFormat.format(new Date(endTime)));
		logger.sendInfoMessage("Execution time: "+Utils.getTimeString(endTime-startTime));
		
	}
	
	private File getStepOutputFile(int _stepIndex){
		String fileName=String.format("analysisSummary_%s_step%d",getPipelineStep(_stepIndex).getStepName(),_stepIndex+1);
		if(_stepIndex==(pipelineLength-1))
			fileName+="_final";
		return new File(analysisPath,fileName+".txt");
	}
	
	private File getStepInputFile(int _stepIndex){
		if(_stepIndex==0) {
			if (pipelineInput.globalExperimentTableName==null)
				return new File(pipelineInput.dataPath);
			else
				return new File(pipelineInput.dataPath,pipelineInput.globalExperimentTableName);
		}
		return getStepOutputFile(_stepIndex-1);
	}
	
	public File getLastOutputTableFile(){
		return getStepOutputFile(stepLoopRange.end-1);
	}
	
	private RunningStep getPipelineStep(int _stepIndex){
		return pipeline.getStepsArray()[_stepIndex];
	}
}
