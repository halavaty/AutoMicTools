package automic.online.jobs.common;

//import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
//import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import ij.IJ;
import ij.ImageJ;
import ij.gui.Overlay;

public class Job_RunKnimeAnalysis  extends Job_Default{
	public static final String KEY_WORKFLOW_PATH="Knime workflow path";
	
	//private static final Roi nullRoi=null;
	private String workflowPath="C:\\Halavatyi_Work\\Development\\KNIME_workspace\\Image-Machine-Learning-Spindle_20190225_online";
	
	@Override
	protected void cleanIterOutput(){
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		TimeUnit.MILLISECONDS.sleep(2000);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		
        String commandString=String.format("knime -consoleLog -nosplash -reset -application org.knime.product.KNIME_BATCH_APPLICATION -workflowDir=%s -nosave -workflow.variable=imagepath,%s,String", workflowPath,newImgFile.getAbsolutePath());
		
		String tempDirectory=IJ.getDirectory("current");//IJ.getDirectory("temp"); 
		
		File tempFile=new File(tempDirectory,"knimeCommand.bat");
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        writer.write(commandString);
        writer.close();

        
        //Process p = Runtime.getRuntime().exec(tempFile.getAbsolutePath());
        //p.waitFor();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
//        String line=reader.readLine();
//
//        while (line != null) {    
//            System.out.println(line);
//            line = reader.readLine();
//        }
        //tempFile.delete();
        //return (p.exitValue()==0);
        return true;
	}
	
	@Override
	protected Overlay createOverlay(){
		return new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
	}
	
	
	
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_WORKFLOW_PATH,	null, workflowPath, 		ParameterType.STRING_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.workflowPath=			(String)_jobParameterCollection.getParameterValue(KEY_WORKFLOW_PATH);

	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="C:/tempDat/AutoFRAP_test";
		String tblFnm="summary_tst8_.txt";
		Job_RunKnimeAnalysis testJob=new Job_RunKnimeAnalysis();
		testJob.initialise(null, "Raw.Image", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
