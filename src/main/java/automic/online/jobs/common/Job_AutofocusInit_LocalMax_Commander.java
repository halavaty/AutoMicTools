package automic.online.jobs.common;

import java.awt.Color;
import java.io.File;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.ImageJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Line;
import ij.gui.Overlay;
import ij.gui.ProfilePlot;
import ij.gui.Roi;
import ij.plugin.filter.MaximumFinder;

public class Job_AutofocusInit_LocalMax_Commander  extends Job_Default{
	public static final String KEY_FOCUS_SLICE_OFFSET="Slice offset";
	public static final String KEY_TOLERANCE="Local Maximum tolerance";
	public static final String KEY_FOCUS_HIGHER="Focus Higher Local maximum";

	
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	int zSubmit=-1;
	Boolean isLineScan=null;
	int[] localMaximaIndices;
	
	int sliceOffset=0;
	double tolerance=10;
	boolean focustHighest=true;

	
	@Override
	protected void cleanIterOutput(){
		maxind=-1;
		zSubmit=-1;
		img=null;
		isLineScan=null;
		localMaximaIndices=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);

		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		//TODO: deal with identifying experiment name in different cases
		String Exper_nm="AutoMicTools";
		
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		TimeUnit.MILLISECONDS.sleep(1000);
		ImageOpenerWithBioformats.turnLineScans=true;
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		isLineScan=(img.getNSlices()==1);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		ImageOpenerWithBioformats.turnLineScans=true;
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		isLineScan=(img.getNSlices()==1);
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		
		double[] zProfileValues=null;
		
		if (isLineScan) {
			zProfileValues=getZProfileValuesLine(img);
		}else {
			zProfileValues=getZProfileValuesFrameStack(img);
		}

		
		localMaximaIndices=MaximumFinder.findMaxima(zProfileValues, tolerance, false);
		if (localMaximaIndices==null)
			return false;
		if (localMaximaIndices.length==0)
			return false;
		
		Arrays.sort(localMaximaIndices);
		
		if (focustHighest) {
			maxind=localMaximaIndices[localMaximaIndices.length-1];
		}else {
			maxind=localMaximaIndices[0];
		}
		
		
		if (isLineScan) {
			zSubmit=img.getHeight()-maxind-1;
		}else {
			zSubmit=maxind;
		}
		
		
		return true;
		
	}
	
	private double[] getZProfileValuesLine(ImagePlus _image) {

		//find focus and send to the microscope macro
		_image.setRoi(0,0,_image.getWidth(), _image.getHeight());
		double [] vals=new ProfilePlot(_image,true).getProfile();
		_image.setRoi((Roi)null);
		return vals;
	}
	
	private double[] getZProfileValuesFrameStack(ImagePlus _image) {
		//find focus and send to the microscope macro
		_image.setRoi((Roi)null);

		
		ImageStack stack=img.getStack();
		int nSlices=stack.getSize();
		double[] vals=new double[nSlices]; 
		for (int i=0;i<nSlices;i++){
			vals[i]=stack.getProcessor(i+1).getStatistics().mean;
		}
		
		return vals;

	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		
		
		if(!isLineScan)
			return o;
		
		if (maxind<0) {
			return o;
		}
		/*
		if(isLineScan&&(maxind>0))
			o.add(new Line(0, maxind, img.getWidth()-1, maxind));

		*/
		
		int nMaxima=localMaximaIndices.length;
		Line[] lineRois=new Line[nMaxima];
		for (int maximumIndex=0; maximumIndex<nMaxima;maximumIndex++) {
			lineRois[maximumIndex]=new Line(0, localMaximaIndices[maximumIndex], img.getWidth()-1, localMaximaIndices[maximumIndex]);
			lineRois[maximumIndex].setStrokeColor(Color.RED);
		}
		
		if (focustHighest) {
			lineRois[nMaxima-1].setStrokeColor(Color.GREEN);
		}else {
			lineRois[0].setStrokeColor(Color.GREEN);
		}
		
		
		for (Roi lineRoi:lineRois) {
			o.add(lineRoi);
		}
		
		return o;
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);

		if ((!isLineScan) && (maxind>0)) {
			img.setSlice(maxind);
		}
	}
	
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_FOCUS_SLICE_OFFSET, null, sliceOffset, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_TOLERANCE, null, tolerance, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_FOCUS_HIGHER, null, focustHighest, ParameterType.BOOL_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.sliceOffset=(Integer)_jobParameterCollection.getParameterValue(KEY_FOCUS_SLICE_OFFSET);
		this.tolerance=(Double)_jobParameterCollection.getParameterValue(KEY_TOLERANCE);
		this.focustHighest=(Boolean)_jobParameterCollection.getParameterValue(KEY_FOCUS_HIGHER);
	}

	
	@Override
	public void  postProcessSuccess()throws Exception{
		this.microscopeCommander.submitJobPixels(newImgFile, "", new Point3D(null,null,(double)zSubmit));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}

	@Override
	public void  postProcessFail()throws Exception{
		this.microscopeCommander.submitJobPixels(newImgFile, "", new Point3D(null,null,(Double)null));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}
	
	
	@Override
	public void  postProcessOffline()throws Exception{
		this.microscopeCommander.submitJobPixels(newImgFile, "", new Point3D(null,null,(double)zSubmit));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
		this.visualise(1000, 100);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		Job_AutofocusInit_LocalMax_Commander testJob=new Job_AutofocusInit_LocalMax_Commander();
		testJob.initialise(null, "AFocus", false);
		testJob.testImageFile(new File("C:\\tempdat\\AutoMic-test\\AF--W0000--P0001-T0067.czi"));
		//testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
