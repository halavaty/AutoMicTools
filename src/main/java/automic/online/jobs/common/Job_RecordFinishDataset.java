package automic.online.jobs.common;

import java.io.File;
import java.util.Date;

import automic.online.jobs.Job_Default;
import automic.table.TableModel;
import automic.table.TableSettings;


public class Job_RecordFinishDataset  extends Job_Default{
	
	@Override
	protected void preProcessOnline()throws Exception{
		Integer highZoomCounter=(Integer)this.getSharedValue("High Zoom Counter");
		System.out.println(highZoomCounter);
		if (highZoomCounter==null)
			highZoomCounter=-1;
		
		
		highZoomCounter++;
		
		String Exper_nm=(String)getSharedValue("Experiment Name");// newImgFile.getName();
		File experimentTableFile=new File(currentTable.getRootPath(),"summary_"+Exper_nm+".txt");
		TableModel experimentTable=new TableModel(experimentTableFile);
		experimentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, highZoomCounter, "Date.Time");
		experimentTable.setFileAbsolutePath(newImgFile, highZoomCounter, imgColumnNm, "IMG");
		//outTbl.setStringValue(new Date().toString(), "Date.Time", 0);
		
		experimentTable.writeNewFile("summary_"+Exper_nm+".txt", true);
		this.setSharedValue("High Zoom Counter", highZoomCounter);
	}

	
	@Override
	public void postProcessSuccess()throws Exception{}
}
