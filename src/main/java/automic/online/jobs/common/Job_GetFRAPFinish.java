package automic.online.jobs.common;

import automic.online.jobs.Job_Default;
import automic.utils.Utils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.ImagePlus;
import ij.gui.ImageWindow;
import ij.gui.Roi;

public class Job_GetFRAPFinish  extends Job_Default{
	
	@Override
	public void visualise(int _xvis, int _yvis){
		ImagePlus img=null;
		try{	
			img=ImageOpenerWithBioformats.openImage(newImgFile, false);
			ImageWindow.setNextLocation(_xvis, _yvis);
			img.show();
			Roi bRoi=ImageOpenerWithBioformats.getImageRois(newImgFile.getAbsolutePath())[0];
			bRoi.setPosition(0);
			bRoi.setPosition( 0, 0, 0);
		}catch(Exception ex){
			Utils.generateErrorMessage("Can not visualise FRAP", ex);
		}
		
	}
	
	@Override
	public void postProcessSuccess()throws Exception{
		this.recordSummaryDataset(true);
		//sharData.clear();
	}
}
