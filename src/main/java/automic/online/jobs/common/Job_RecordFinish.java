package automic.online.jobs.common;

import java.util.Date;

import automic.online.jobs.Job_Default;
import automic.table.TableSettings;

public class Job_RecordFinish  extends Job_Default{
	
	protected void addTableColumns() {
		currentTable.addValueColumn("Success", "BOOL");
		currentTable.addColumn("Date.Time");
	}

	
	
	@Override
	public void postProcessSuccess()throws Exception{
		
		currentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, 0, "Date.Time");
		currentTable.setBooleanValue(true, 0, "Success");//.setSuccess(0, true);//rec.addRecordToFl(true);

		this.recordSummaryDataset();
	}
	
	@Override
	protected void postProcessFail()throws Exception{
		currentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, 0, "Date.Time");
		currentTable.setBooleanValue(false, 0, "Success");//.setSuccess(0, true);//rec.addRecordToFl(true);
		
		this.recordSummaryDataset();
	}

}
