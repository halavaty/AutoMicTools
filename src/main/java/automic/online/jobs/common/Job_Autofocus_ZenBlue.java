package automic.online.jobs.common;

import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissLSM800;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.ImageJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.Roi;

public class Job_Autofocus_ZenBlue  extends Job_Default{
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	Roi ln=null;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
		ln=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		TimeUnit.MILLISECONDS.sleep(1000);
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		//find focus and send to the microscope macro
		//img.setRoi(0,0,img.getWidth(), img.getHeight());
		//double [] vals=new ProfilePlot(img,true).getProfile();
		//img.setRoi((Roi)null);
		//int npoints=vals.length;

		
		double maxv=0;
		maxind=-1;
		
		ImageStack stack=img.getStack();
		int nSlices=stack.getSize();
		
		double currentValue;
		for (int i=0;i<nSlices;i++){
			currentValue=stack.getProcessor(i+1).getStatistics().mean;
			if (currentValue>maxv){
				maxind=i;
				maxv=currentValue;
			}
		}
		
		//ln=new Line(0,maxind,img.getWidth(),maxind);
		//ln.setStrokeColor(Color.ORANGE);
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
		if (maxind>0)
			img.setSlice(maxind);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", new Point3D(null,null,(double)maxind));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}

	@Override
	public void  postProcessOffline()throws Exception{
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", new Point3D(null,null,(double)maxind));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		Job_Autofocus_ZenBlue testJob=new Job_Autofocus_ZenBlue();
		testJob.initialise(null, "AFocus", false);
		testJob.testImageFile(new File("W:\\halavaty\\Tobias-LSM800-data\\20190219-183148\\AF\\AF--W0000--P0001-T0001.czi"));
		//testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
