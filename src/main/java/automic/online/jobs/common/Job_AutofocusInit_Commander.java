package automic.online.jobs.common;

import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.ImageJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Line;
import ij.gui.Roi;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.ProfilePlot;
import ij.gui.Roi;

public class Job_AutofocusInit_Commander  extends Job_Default{
	private static final String KEY_WAIT_IMAGE_OPENING="Wait for image opening (milliseconds)";
	public static final String KEY_FOCUS_SLICE_OFFSET="Slice offset";
	private static final String KEY_INVERT="Invert Z Position";
	private static final String KEY_CLEAN_TABLE="Clean Table Entry";
	

	
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	int zSubmit=-1;
	Boolean isLineScan=null;
	
	private int imageOpeningTimeDelay=1000;
	int sliceOffset=0;
	boolean invert=false;
	boolean cleanTable=true;

	
	@Override
	protected void cleanIterOutput(){
		img=null;
		isLineScan=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		if (cleanTable) {
			super.clearSharedData();
			currentTable.cleanRecord(curDInd);
		}
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		//TODO: deal with identifying experiment name in different cases
		String Exper_nm="AutoMicTools";
		
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		TimeUnit.MILLISECONDS.sleep(imageOpeningTimeDelay);
		ImageOpenerWithBioformats.turnLineScans=true;
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		isLineScan=(img.getNSlices()==1);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		ImageOpenerWithBioformats.turnLineScans=true;
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		isLineScan=(img.getNSlices()==1);
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		
		if (isLineScan) {
			maxind=getFocusValueLine(img);
			//zSubmit=img.getHeight()-maxind-1;
			if (invert) {
				zSubmit=maxind;
			}else {
				zSubmit=img.getHeight()-maxind-1;
			}
		}else {
			maxind=getFocusValueFrameStack(img);
			//zSubmit=maxind;
			if (invert) {
				zSubmit=img.getNSlices()-maxind-1;
			}else {
				zSubmit=maxind;
			}
		}
		
		

		return true;
		
	}
	
	private int getFocusValueLine(ImagePlus _image) {

		//find focus and send to the microscope macro
		_image.setRoi(0,0,_image.getWidth(), _image.getHeight());
		double [] vals=new ProfilePlot(_image,true).getProfile();
		_image.setRoi((Roi)null);
		int npoints=vals.length;
		double maxv=0;
		int max_id=-1;
		for (int i=0;i<npoints;i++){
			if (vals[i]>maxv){
				max_id=i;
				maxv=vals[i];
			}
		}
		
		return max_id;

	}
	
	private int getFocusValueFrameStack(ImagePlus _image) {
		//find focus and send to the microscope macro
		_image.setRoi((Roi)null);

		double maxv=0;
		int max_id=-1;
		
		ImageStack stack=img.getStack();
		int nSlices=stack.getSize();
		
		double currentValue;
		for (int i=0;i<nSlices;i++){
			currentValue=stack.getProcessor(i+1).getStatistics().mean;
			if (currentValue>maxv){
				max_id=i;
				maxv=currentValue;
			}
		}
		
		return max_id;

	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		
		if(isLineScan&&(maxind>0))
			o.add(new Line(0, maxind, img.getWidth()-1, maxind));
		else {
			if (maxind>0) {
				Roi r=new PointRoi(img.getWidth()/2, img.getHeight()/2);
				r.setPosition(maxind+1);
				
				o.add(r);
			}
		}
		return o;
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);

		if ((!isLineScan) && (maxind>-1)) {
			img.setSlice(maxind+1);
		}
	}
	
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_WAIT_IMAGE_OPENING,	null, imageOpeningTimeDelay,ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_FOCUS_SLICE_OFFSET, null, sliceOffset, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_INVERT, null, invert, ParameterType.BOOL_PARAMETER);
		jobCollection.addParameter(KEY_CLEAN_TABLE, null, cleanTable, ParameterType.BOOL_PARAMETER);
		
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.imageOpeningTimeDelay=	(Integer)_jobParameterCollection.getParameterValue(KEY_WAIT_IMAGE_OPENING);
		this.sliceOffset=(Integer)_jobParameterCollection.getParameterValue(KEY_FOCUS_SLICE_OFFSET);
		this.invert=(Boolean)_jobParameterCollection.getParameterValue(KEY_INVERT);
		this.cleanTable=(Boolean)_jobParameterCollection.getParameterValue(KEY_CLEAN_TABLE);

	}

	
	@Override
	public void  postProcessSuccess()throws Exception{
		this.microscopeCommander.submitJobPixels(newImgFile, this.expTag, new Point3D(null,null,(double)zSubmit));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
	}

	@Override
	public void  postProcessOffline()throws Exception{
		this.microscopeCommander.submitJobPixels(newImgFile, this.expTag, new Point3D(null,null,(double)zSubmit));
		Roi[] rs=getOverlay().toArray();
		if (!ROIManipulator2D.isEmptyRoiArr(rs))
			saveRoisForImage(newImgFile,rs);
		this.visualise(1000, 100);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		Job_AutofocusInit_Commander testJob=new Job_AutofocusInit_Commander();
		testJob.initialise(null, "AFocus", false);
		testJob.testImageFile(new File("X:\\group\\Aliaksandr\\User_data\\Marco_Cosenza\\test-AF-update\\AF--W0000--P0001-T0001.czi"));
		//testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
