package automic.online.jobs.common;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.gui.Toolbar;
import ij.gui.WaitForUserDialog;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

public class Job_SelectMultipleFocusPointsManual  extends Job_Default{
	
	
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	private Roi selectedPointsRoi;
	//private Integer selectedXPosition;
	//private Integer selectedYPosition;
	private Integer numberOfSelectedPoints;
	private Point2D.Double[] selectedPoints;

	//private boolean selectionMade=false;

	//private boolean invert_Z;
	//private int zoomIterations;
	//private int initialLocationX;
	//private int initialLocationY;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
		selectedPointsRoi=null;
		//selectedXPosition=null;
		//selectedYPosition=null;
		//selectedYPosition=null;
		numberOfSelectedPoints=null;
		selectedPoints=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		
		TimeUnit.MILLISECONDS.sleep(2000);		
		//img=ImageOpenerWithBioformats.openImage(newImgFile);
		img=openSelectedSlices(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=openSelectedSlices(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
	}
	
	
	private ImagePlus openSelectedSlices(File _imageFile)throws Exception{
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(true);
		options.setId(_imageFile.getAbsolutePath());
		
		return BF.openImagePlus(options)[0];
	}
	@Override
	protected boolean runProcessing()throws Exception{
		img.setDisplayMode(IJ.COMPOSITE);
		
		img.show();
		
		IJ.setTool(Toolbar.POINT);
		
		new WaitForUserDialog ("Select Embryo Positions").show();

		selectedPointsRoi=img.getRoi();
		if ( selectedPointsRoi==null) return false;
		if(!(selectedPointsRoi instanceof PointRoi))	return false;
		selectedPointsRoi.setPosition(1, 1, 1);
		selectedPointsRoi.setStrokeColor(Color.yellow);
		
		
		Polygon pointPolygon=selectedPointsRoi.getPolygon();
		
		numberOfSelectedPoints=pointPolygon.npoints;
		if (numberOfSelectedPoints<1) return false;

		int x,y;
		selectedPoints=new Point2D.Double[numberOfSelectedPoints];
		for (int i=0;i<numberOfSelectedPoints;i++){
			x=pointPolygon.xpoints[i];
			y=pointPolygon.ypoints[i];
			selectedPoints[i]=new Point2D.Double(x, y);
		}
		
		img.hide();
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		//this.visualiseImg(img, getOverlay(), _xvis, _yvis);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		
		PointRoi saveRoi=null;
		//form  comma-separated strings with coordinates
		String xstr="", ystr="",zstr="";
		// submit 3d points in future
		for (int i=0;i<numberOfSelectedPoints;i++){
			if (i>0){
				xstr+=";";
				ystr+=";";
				zstr+=";";
			}
			xstr+=selectedPoints[i].x;
			ystr+=selectedPoints[i].y;
			zstr+="0";
			if (i==0)
				saveRoi=new PointRoi(selectedPoints[i].x,selectedPoints[i].y);
			else
				saveRoi=saveRoi.addPoint(selectedPoints[i].x,selectedPoints[i].y);
		}

		
		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr,ystr,zstr,"","","","","");
		
		saveRoiForImage(newImgFile,saveRoi);
		
		this.setSharedValue("Zoom Points", selectedPoints);
		this.setSharedValue("Zoom Counter", -1);

	}
	
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
	}
	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="C:/tempDat/AutoFRAP_test";
		String tblFnm="summary_tst8_.txt";
		Job_SelectMultipleFocusPointsManual testJob=new Job_SelectMultipleFocusPointsManual();
		testJob.initialise(null, "Raw.Image", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
