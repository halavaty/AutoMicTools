package automic.online.jobs.common;

import java.awt.Color;
import java.util.concurrent.TimeUnit;

import automic.online.microscope.ZeissKeys;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.Overlay;
import ij.gui.ProfilePlot;
import ij.gui.Roi;
import ij.plugin.MontageMaker;

public class Job_AutofocusInitOffset  extends Job_Default{
	private static final String KEY_WAIT_IMAGE_OPENING="Wait for image opening (milliseconds)";
	private static final String KEY_FOCUS_SLICE_OFFSET="Slice offset";
	private static final String KEY_INVERT="Invert Z Position";
	
	
	private int imageOpeningTimeDelay=1000;
	int sliceOffset=0;
	boolean invert=false;
	
	
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	int zSubmit;
	Roi ln=null;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
		ln=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		
		TimeUnit.MILLISECONDS.sleep(imageOpeningTimeDelay);		
		try {
			img=ImageOpenerWithBioformats.openImage(newImgFile);
			img=new MontageMaker().makeMontage2(img, 1, img.getNSlices(), 1.0, 1, img.getNSlices(), 1, 0, false);
		}catch(Exception ex) {
			img=null;
		}
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=new MontageMaker().makeMontage2(img, 1, img.getNSlices(), 1.0, 1, img.getNSlices(), 1, 0, false);
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		if (img==null) {//image could not be opened
			return false;
		}
		//find focus and send to the microscope macro
		img.setRoi(0,0,img.getWidth(), img.getHeight());
		double [] vals=new ProfilePlot(img,true).getProfile();
		img.setRoi((Roi)null);
		int npoints=vals.length;
		double maxv=0;
		maxind=-1;
		for (int i=0;i<npoints;i++){
			if (vals[i]>maxv){
				maxind=i;
				maxv=vals[i];
			}
		}
		IJ.log("maxind: "+maxind);
		ln=new Line(0,maxind,img.getWidth(),maxind);
		ln.setStrokeColor(Color.ORANGE);
		
		if (invert) {
			zSubmit=img.getHeight()-maxind-1;
		}else {
			zSubmit=maxind;
		}
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return (ln!=null)?new Overlay(ln):new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		
		ZeissKeys.submitCommandsToMicroscope("focus", ""+(img.getWidth()-1)/2.0,"0",""+(zSubmit+sliceOffset),"","","","","");
		saveRoiForImage(newImgFile,ln);
	}

	@Override

	protected void postProcessFail()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("nothing",null,null,null,null,null,null,null, "Suitable focus is not found");
	}
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();

		jobCollection.addParameter(KEY_WAIT_IMAGE_OPENING,	null, imageOpeningTimeDelay,ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_FOCUS_SLICE_OFFSET, null, sliceOffset, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_INVERT, null, invert, ParameterType.BOOL_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){

		this.imageOpeningTimeDelay=	(Integer)_jobParameterCollection.getParameterValue(KEY_WAIT_IMAGE_OPENING);
		this.sliceOffset=(Integer)_jobParameterCollection.getParameterValue(KEY_FOCUS_SLICE_OFFSET);
		this.invert=(Boolean)_jobParameterCollection.getParameterValue(KEY_INVERT);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="D:/tempDat/Crocker_group/fly feedback files 20180410/ttt";
		String tblFnm="summary_test05_.txt";
		Job_AutofocusInitOffset testJob=new Job_AutofocusInitOffset();
		testJob.initialise(null, "AFocus", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
		
		System.out.print("test end");
	}
	
}
