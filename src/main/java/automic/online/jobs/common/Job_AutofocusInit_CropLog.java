package automic.online.jobs.common;


import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Overlay;
import ij.gui.ProfilePlot;
import ij.plugin.Duplicator;

public class Job_AutofocusInit_CropLog  extends Job_Default{
	
	public static final String INPUT_IMG_TAG="Focus";
	//public static String FRAP_IMG_TAG="PA";
	
	//private boolean selectionMade=false;
	
	public static final String KEY_CROP_WIDTH="Crop width";
	public static final String KEY_CROP_HEIGHT="Crop height";
	
	private int cropWidth=400;
	private int cropHeight=200;
	

	
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		//analyse last time point
		if (img.getNFrames()>1) {
			img=new Duplicator().run(img, 1, img.getNChannels(), 1, img.getNSlices(), img.getNFrames(), img.getNFrames());
		}
		showDebug(img, "input", true);
		//find focus and send to the microscope macro
		int imageWidth=img.getWidth();
		int imageHeight=img.getWidth();
		
		
		if((cropWidth<imageWidth)&&(cropHeight<imageHeight)) {
			img.setRoi((imageWidth-cropWidth)/2,(imageHeight-cropHeight)/2,cropWidth,cropHeight);
		}else {
			img.setRoi(0,0,imageWidth, imageHeight);
		}
		showDebug(img, "input roi", true);

		double maxv=0;
		maxind=-1;
		
		ImageStack stack=img.getStack();
		int nSlices=stack.getSize();

		
		double currentValue;
		for (int i=0;i<nSlices;i++){
			currentValue=stack.getProcessor(i+1).getStatistics().mean;
			if (currentValue>maxv){
				maxind=i;
				maxv=currentValue;
			}
		}

		
		
		IJ.log("MaxInd="+maxind);
		
		//ln=new Line(0,maxind,img.getWidth(),maxind);
		//ln.setStrokeColor(Color.ORANGE);
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
		if (maxind>-1) {
			img.setZ(maxind);
		}

	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("trigger1", ""+(img.getWidth()-1)/2.0,""+(img.getHeight()-1)/2.0,""+maxind,"","","","","");
		currentTable.setNumericValue(maxind, 0, "Selected.Z");
	}
	
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();

		jobCollection.addParameter(KEY_CROP_WIDTH, null, cropWidth, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_CROP_HEIGHT, null, cropHeight, ParameterType.INT_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.cropWidth=			(Integer)_jobParameterCollection.getParameterValue(KEY_CROP_WIDTH);
		this.cropHeight=		(Integer)_jobParameterCollection.getParameterValue(KEY_CROP_HEIGHT);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="X:\\group\\ALMFstuff\\Aliaksandr\\User_data\\Daniel_Krueger";
		String tblFnm="summary_test4_.txt";
		Job_AutofocusInit_CropLog testJob=new Job_AutofocusInit_CropLog();
		testJob.initialise(null, "Stack", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
