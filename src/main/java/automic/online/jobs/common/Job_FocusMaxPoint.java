package automic.online.jobs.common;


import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;

public class Job_FocusMaxPoint  extends Job_Default{
	//private static final Roi nullRoi=null;
	private ImagePlus image=null;
	private double xMax;
	private double yMax;
	private double zMax;
	
	private PointRoi selectedPointRoi;
	
	@Override
	protected void cleanIterOutput(){
		image=null;
		
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		image=IJ.openImage(newImgFile.getAbsolutePath());
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		image=IJ.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG").getAbsolutePath());
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		//find focus and send to the microscope macro
		
		image.setRoi((Roi)null);
		
		xMax=15;
		yMax=30;
		zMax=0;
		
		selectedPointRoi=new PointRoi(xMax,yMax);
		
		selectedPointRoi.setPosition(0);
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return (selectedPointRoi!=null)?new Overlay(selectedPointRoi):new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		
		this.visualiseImg(image, getOverlay(), _xvis, _yvis);
		if (selectedPointRoi==null) return;
		int sliceNum=(int)Math.round(zMax)+1;
		if(sliceNum<1) sliceNum=1;
		if(sliceNum>image.getZ()) sliceNum=image.getZ();
		image.setZ(sliceNum);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("focus", Double.toString(xMax),Double.toString(yMax),Double.toString(zMax),"","","","","");
		saveRoiForImage(newImgFile,selectedPointRoi);
	}
	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="C:/tempDat/2colExp/01122015-2colCellLines siRNA fixed";
		String tblFnm="summary_gr1_.txt";
		Job_FocusMaxPoint testJob=new Job_FocusMaxPoint();
		testJob.initialise(null, "AFocus", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
