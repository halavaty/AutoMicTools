package automic.online.jobs.common;

import java.awt.Rectangle;
import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.table.TableSettings;
import automic.utils.ArrIndUtils;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import loci.plugins.in.ImporterOptions;

public class Job_GetMultipleImagingPositions_Script_Commander_InitTable extends Job_Default{
	public static final String INPUT_IMG_TAG="Selection";
	//public static String FRAP_IMG_TAG="PA";
	
	
	
	
	
	public static final String KEY_TIME_DELAY="Time delay (milliseconds)";

	public static final String KEY_SCRIPT_PATH="Script path";
	
	public static final String KEY_POSITION_MAX_COUNT="Maximal number of positions to select";
	
	public static final String KEY_FLIP_Z="Invert z offset?";
	
	
	private ImagePlus inImg;
	private Roi[] identifiedObjectRois;
	private Point3D[] identifiedPoints;
	private Point3D[] selectedPoints;

	int timeDelay=1000;

	private String scriptPath="";
	
	private int positionMaxCount=3;
	
	boolean invert=false;
	
	boolean cleanTable=true;



	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleImagingPositions_Script_Commander_InitTable(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		inImg=null;
		identifiedObjectRois=null;
		identifiedPoints=null;
		selectedPoints=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		if (cleanTable) {
			super.clearSharedData();
			currentTable.cleanRecord(curDInd);
		}
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		//TODO: deal with identifying experiment name in different cases
		String Exper_nm="AutoMicTools";
		
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		TimeUnit.MILLISECONDS.sleep(timeDelay);
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		
		//String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		//this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		this.showDebug(inImg, "original image", true);

		ROIManipulator2D.getEmptyRm();
		inImg.show();
		
		ij2.script().run(new File(scriptPath), false,new HashMap<String,Object>()).get();
		
		inImg.hide();

		Overlay segmentationOverlay=inImg.getOverlay();
		if (segmentationOverlay==null)
			return false;

		
		identifiedObjectRois=segmentationOverlay.toArray();

		if (ROIManipulator2D.isEmptyRoiArr(identifiedObjectRois)) {
			return false;
		}
		
		double x,y,z;

		//inImg.getCalibration();
		identifiedPoints=new Point3D[identifiedObjectRois.length];
		for(int iObject=0;iObject<identifiedObjectRois.length;iObject++) {
			Rectangle roiBounds=identifiedObjectRois[iObject].getBounds();
			x=roiBounds.getCenterX();
			y=roiBounds.getCenterY();
			z= 0.0;
			if (inImg.isStack()) {
				int zPos = identifiedObjectRois[iObject].getZPosition();
				if (invert) {
					z=inImg.getNSlices()-zPos-1;
				}else {
					z=zPos*1.0;
				}
			}
			
			
			identifiedPoints[iObject]=new Point3D(x, y, z);
		}
		
		if (identifiedPoints.length<=positionMaxCount){
			selectedPoints=identifiedPoints;
		}
		else{
			int[] selectedIndexes=ArrIndUtils.getRandomIndexes(identifiedPoints.length, positionMaxCount);
			selectedPoints=new Point3D[positionMaxCount];
			for (int i=0;i<positionMaxCount;i++)
				selectedPoints[i]=identifiedPoints[selectedIndexes[i]];
		}
		
		
		
		return true;
	}
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (!ROIManipulator2D.isEmptyRoiArr(identifiedObjectRois)) {
			for (Roi r:identifiedObjectRois) {
				o.add(r);
			}
		}
		
		if (selectedPoints!=null)
			for (Point3D selPoint: selectedPoints)
				o.add(new PointRoi(selPoint.getX(),selPoint.getY()));
			
		return o;
		
	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		
		this.microscopeCommander.submitJobPixels(newImgFile, this.expTag, selectedPoints);
		
		int nPositions=selectedPoints.length;
		IJ.log(String.format("Launcing HighZoom imaging job with %d selected regions", nPositions));
		
		
		Roi[] overlayRoiArray=getOverlay().toArray();
		if(!ROIManipulator2D.isEmptyRoiArr(overlayRoiArray)) {
			saveRoisForImage(newImgFile, overlayRoiArray);
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		currentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, 0, "Date.Time");
		currentTable.setBooleanValue(true, 0, "Success");//.setSuccess(0, true);//rec.addRecordToFl(true);

		this.recordSummaryDataset();
		
		this.microscopeCommander.submitJobPixels(newImgFile, "", new Point3D[0]);


		IJ.log("No region was selected. Continue with selection imaging job");
	}

	@Override
	public void postProcessOffline(){
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_TIME_DELAY, null, timeDelay, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_SCRIPT_PATH, null, scriptPath, ParameterType.FILEPATH_PARAMETER);
		
		jobCollection.addParameter(KEY_POSITION_MAX_COUNT,null, positionMaxCount, ParameterType.INT_PARAMETER);
		
		jobCollection.addParameter(KEY_FLIP_Z, null, invert, ParameterType.BOOL_PARAMETER);
		
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.timeDelay=(Integer)_jobParameterCollection.getParameterValue(KEY_TIME_DELAY);
		
		this.scriptPath=(String)_jobParameterCollection.getParameterValue(KEY_SCRIPT_PATH);
		
		this.positionMaxCount=(Integer)_jobParameterCollection.getParameterValue(KEY_POSITION_MAX_COUNT);
		
		this.invert=(Boolean)_jobParameterCollection.getParameterValue(KEY_FLIP_Z);
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="C:\\Users\\Manu\\Documents\\Desktop\\Mereke\\summary_AutoMicTools.txt";
		final String imgFnm="RPE1_mitosis_run2_DE_2_W0002_P0001_T0001.czi";
		Job_GetMultipleImagingPositions_Script_Commander_InitTable testJob=new Job_GetMultipleImagingPositions_Script_Commander_InitTable();
		testJob.scriptPath="C:\\Users\\Manu\\git\\automictools-zeiss880-centrioles-mereke\\scripts\\spotDetection_v01.groovy";
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
