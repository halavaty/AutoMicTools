package automic.online.jobs.common;

import java.awt.Color;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Line;
import ij.gui.Overlay;
import ij.gui.ProfilePlot;
import ij.gui.Roi;

public class Job_AutofocusInit  extends Job_Default{
	//private static final Roi nullRoi=null;
	private ImagePlus img=null;
	int maxind=-1;
	Roi ln=null;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
		ln=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getName();
		//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		Exper_nm="experiment";
		this.setSharedValue("Experiment Name", Exper_nm);
		
		img=ImageOpenerWithBioformats.openImage(newImgFile);
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
	}
	
	
	@Override
	protected boolean runProcessing()throws Exception{
		//find focus and send to the microscope macro
		img.setRoi(0,0,img.getWidth(), img.getHeight());
		double [] vals=new ProfilePlot(img,true).getProfile();
		img.setRoi((Roi)null);
		int npoints=vals.length;
		double maxv=0;
		maxind=-1;
		for (int i=0;i<npoints;i++){
			if (vals[i]>maxv){
				maxind=i;
				maxv=vals[i];
			}
		}
		
		ln=new Line(0,maxind,img.getWidth(),maxind);
		ln.setStrokeColor(Color.ORANGE);
		
		return true;
		
	}
	
	@Override
	protected Overlay createOverlay(){
		return (ln!=null)?new Overlay(ln):new Overlay();
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("focus", ""+(img.getWidth()-1)/2.0,"0",""+maxind,"","","","","");
		saveRoiForImage(newImgFile,ln);
	}
	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String tblPth="C:/tempDat/2colExp/01122015-2colCellLines siRNA fixed";
		String tblFnm="summary_gr1_.txt";
		Job_AutofocusInit testJob=new Job_AutofocusInit();
		testJob.initialise(null, "AFocus", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
