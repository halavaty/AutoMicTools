package automic.online.jobs.common;

import java.awt.Rectangle;
import java.io.File;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import automic.geom.Point3D;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.table.TableSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ManualRoiDefiner;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import loci.plugins.in.ImporterOptions;

public class Job_GetMultipleImagingPositions_Manual_Commander extends Job_Default{
	public static final String INPUT_IMG_TAG="Selection";
	//public static String FRAP_IMG_TAG="PA";
	
	
	public static final String KEY_TIME_DELAY="Time delay (milliseconds)";
	
	public static final String KEY_LOCATION_X="Selection window location X";
	public static final String KEY_LOCATION_Y="Selection window location Y";
	
	public static final String KEY_RESPONCE_TIME="Time to wait for responce";
	public static final String KEY_USE_ROIS_TIME_END="Proceed with selected ROIs if time expires";
	public static final String KEY_CAN_MOVE_POSITION="Can move position after drawing";
	
	private ImagePlus inImg;
	
	private int timeDelay=1000;
	private int initialLocationX=1000;
	private int initialLocationY=120;
	private int responceTime=30;
	private boolean useRoisTimeEnd=true;
	private boolean canMovePosition=false;
	
	private ManualRoiDefiner roiDefiner;
	private Point3D[] selectedPositions;
	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleImagingPositions_Manual_Commander(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		
		TimeUnit.MILLISECONDS.sleep(timeDelay);
		
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		roiDefiner=new ManualRoiDefiner(initialLocationX, initialLocationY, responceTime, useRoisTimeEnd, canMovePosition, ManualRoiDefiner.CIRCLE_REGION, 5);
		
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
		roiDefiner=new ManualRoiDefiner(initialLocationX, initialLocationY, responceTime, useRoisTimeEnd, canMovePosition, ManualRoiDefiner.CIRCLE_REGION, 5);
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		Roi[] selectedPositionRois=roiDefiner.getManualRois(inImg);

		if ( ROIManipulator2D.isEmptyRoiArr(selectedPositionRois)) return false;
		
		int nPositions=selectedPositionRois.length;
		
		selectedPositions=new Point3D[nPositions];
		
		for (int iPosition=0;iPosition<nPositions;iPosition++) {
			Rectangle roiBounds=selectedPositionRois[iPosition].getBounds();
			selectedPositions[iPosition]=new Point3D(roiBounds.getCenterX(),roiBounds.getCenterY(),null);
		}
		
		return true;
	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (selectedPositions==null)
			return o;

		int nPositions=selectedPositions.length;

		for (int iPosition=0;iPosition<nPositions;iPosition++) {
			PointRoi pRoi=new PointRoi(selectedPositions[iPosition].getX(), selectedPositions[iPosition].getY());
			o.add(pRoi);
		}
		
		return o;
	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);

	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		
		this.microscopeCommander.submitJobPixels(newImgFile, this.expTag, selectedPositions);
		
		int nPositions=selectedPositions.length;
		IJ.log(String.format("Launcing imaging job with %d selected regions", nPositions));
		
		if (nPositions>0) {
			saveRoisForImage(newImgFile, getOverlay().toArray());
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		currentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, 0, "Date.Time");
		currentTable.setBooleanValue(false, 0, "Success");//.setSuccess(0, true);//rec.addRecordToFl(true);

		this.recordSummaryDataset();		
		
		
		this.microscopeCommander.submitJobPixels(newImgFile, "", new Point3D[0]);

		IJ.log("No region was selected. Continue with selection imaging jobs in the next default position");
	}

	@Override
	public void postProcessOffline(){
		boolean selectionMade=(selectedPositions!=null);
		IJ.log("Selection made "+selectionMade);
		if (selectionMade)
			IJ.log("Number of selected positions " + this.selectedPositions.length);
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_TIME_DELAY, null, timeDelay, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_LOCATION_X, null, initialLocationX, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_LOCATION_Y, null, initialLocationY, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_RESPONCE_TIME, null, responceTime, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_USE_ROIS_TIME_END, null, useRoisTimeEnd, ParameterType.BOOL_PARAMETER);
		jobCollection.addParameter(KEY_CAN_MOVE_POSITION, null, canMovePosition, ParameterType.BOOL_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.timeDelay=(Integer)_jobParameterCollection.getParameterValue(KEY_TIME_DELAY);
		this.initialLocationX=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_X);
		this.initialLocationY=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_Y);
		this.responceTime=(Integer)_jobParameterCollection.getParameterValue(KEY_RESPONCE_TIME);
		this.useRoisTimeEnd=(Boolean)_jobParameterCollection.getParameterValue(KEY_USE_ROIS_TIME_END);
		this.canMovePosition=(Boolean)_jobParameterCollection.getParameterValue(KEY_CAN_MOVE_POSITION);
		
	}
	
	@SuppressWarnings("deprecation")
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="X:\\group\\ALMFstuff\\Aliaksandr\\test_Feedback_LSM900_Sanjana\\20200204-084938\\summary_.txt";
		final String imgFnm="LowZoom--W0000--P0002-T0001.czi";
		Job_GetMultipleImagingPositions_Manual_Commander testJob=new Job_GetMultipleImagingPositions_Manual_Commander();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
