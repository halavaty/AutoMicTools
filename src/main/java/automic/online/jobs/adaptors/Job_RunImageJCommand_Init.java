package automic.online.jobs.adaptors;

import java.awt.geom.Point2D;
import java.io.File;
import java.util.concurrent.TimeUnit;

import automic.online.microscope.ExternalScriptCommander;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.DebugVisualiserSettings;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.measure.ResultsTable;
import ij.plugin.frame.RoiManager;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

public class Job_RunImageJCommand_Init  extends Job_Default{
	
	
	public static final String KEY_COMMAND_NAME="Command name";
	public static final String KEY_X_COLUMN_NAME="X position column";
	public static final String KEY_Y_COLUMN_NAME="Y position column";
	//public static final String KEY_Z_COLUMN_NAME="Z position column";
	
	public static final String KEY_BLOCK_COUNT="Block count";
	public static final String KEY_DELAY_MILLIS="File opening delay (milliseconds)";

	
	
	private ImagePlus img=null;
	private Point2D.Double[] identifiedPoints;
	private Point2D.Double[] selectedPoints;
	//private double selectedZ;
	private Roi[] identifiedCellRois;
	
	private String commmandName="SmartImaging DetectAnimal";
	private String xColumn="X";
	private String yColumn="Y";
	//private String zColumn="Z";
	
	private int blockCount=2;
	private int delayMillis=1000;
	double x,y,xMic,yMic;
	String well;
	
	@Override
	protected void cleanIterOutput(){
		img=null;
		identifiedPoints=null;
		selectedPoints=null;
		identifiedCellRois=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		//String oldExperimentName= (String)this.getSharedValue("Experiment Name");
		
		//super.clearSharedData();
		currentTable.cleanRecord(curDInd);
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
		
		String Exper_nm=newImgFile.getParentFile().getName();
		//if((oldExperimentName==null)||(!Exper_nm.equals(oldExperimentName))){
		

			//Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
			this.setSharedValue("Experiment Name", Exper_nm);
		//}			
			
		
		TimeUnit.MILLISECONDS.sleep(delayMillis);		
		//img=ImageOpenerWithBioformats.openImage(newImgFile);
		img=openSelectedSlices(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//img=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, imgColumnNm, "IMG"));
		img=openSelectedSlices(newImgFile);
	}
	
	
	private ImagePlus openSelectedSlices(File _imageFile)throws Exception{
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(true);
		options.setId(_imageFile.getAbsolutePath());
		
		return BF.openImagePlus(options)[0];
	}
	@Override
	protected boolean runProcessing()throws Exception{
		this.showDebug(img, "Original image", true);
		
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		
		ResultsTable rt=ResultsTable.getResultsTable();
		if (rt!=null)
			rt.reset();
		
		img.show();
		
		IJ.run(img, commmandName, "");
		
		
		img.hide();
		
		//new Close_all_forced();
		
		
		rt=ResultsTable.getResultsTable();
		
		identifiedCellRois=rm.getRoisAsArray();
		
		rm.runCommand("Reset");
		identifiedPoints=new Point2D.Double[rt.size()];
		
		
		for(int iRow=0;iRow<rt.size();iRow++) {
			x=rt.getValueAsDouble(rt.getColumnIndex("X"), iRow);
			y=rt.getValueAsDouble(rt.getColumnIndex("Y"), iRow);
			xMic=rt.getValueAsDouble(rt.getColumnIndex("X-mic"), iRow);
			yMic=rt.getValueAsDouble(rt.getColumnIndex("Y-mic"), iRow);
			well=rt.getStringValue(rt.getColumnIndex("Well"), iRow);
			System.out.println("Well: "+well);
			identifiedPoints[iRow]=new Point2D.Double(x, y);
		}
		
		rt.reset();
		
		
		selectedPoints=identifiedPoints;
		
		
		return true;
		
	}
	
	

	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (identifiedCellRois!=null)
			for (Roi r:identifiedCellRois){
				o.add(r);
			}
		if (selectedPoints!=null)
			for (Point2D.Double p:selectedPoints) {
				o.add(new PointRoi(p.x, p.y));
			}
		
		return o;
		
	}
	
	@Override 
	public void visualise(int _xvis, int _yvis){
		img.setDisplayMode(IJ.COLOR);
		this.visualiseImg(img, getOverlay(), _xvis, _yvis);
	}
	
	
	@Override
	public void  postProcessSuccess()throws Exception{
		
//		//PointRoi saveRoi=null;
//		//form  comma-separated strings with coordinates
//		String xstr="", ystr="",zstr="";
//		// submit 3d points in future
		for (int i=0;i<selectedPoints.length;i++){
//			if (i>0){
//				xstr+=";";
//				ystr+=";";
//				zstr+=";";
//			}
//			xstr+=selectedPoints[i].x;
//			ystr+=selectedPoints[i].y;
//			zstr+=selectedZ;
			currentTable.setNumericValue(x, curDInd, "X");
			currentTable.setNumericValue(y, curDInd, "Y");
			currentTable.setNumericValue(xMic, curDInd, "X-mic");
			currentTable.setNumericValue(yMic, curDInd, "Y-mic");
			currentTable.setStringValue(well, curDInd, "Well");
			Integer lowZoomCounter=(Integer)this.getSharedValue("Low Zoom Counter");
			if (lowZoomCounter==null)
				lowZoomCounter=-1;
			this.setSharedValue("Low Zoom Counter", lowZoomCounter+1);
			this.recordSummaryDataset(true);
		}
//
//		
//		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr,ystr,zstr,"","","","","");
		
//		
		saveRoisForImage(newImgFile, getOverlay().toArray());
//		
//		this.setSharedValue("Zoom Points", selectedPoints);
//		this.setSharedValue("Zoom Counter", -1);
//		this.setSharedValue("Selected Embryo Rois", identifiedCellRois);
		Integer lowZoomCounter=(Integer)this.getSharedValue("Low Zoom Counter");
		if (lowZoomCounter==null)
			lowZoomCounter=-1;
		Integer highZoomCounter=(Integer)this.getSharedValue("High Zoom Counter");
		if (highZoomCounter==null)
			highZoomCounter=-1;
		
		
		if ((lowZoomCounter-highZoomCounter)>=blockCount){
			microscopeCommander.submitPositionsFromTable(new File(currentTable.getRootPath(),"summary_"+(String)getSharedValue("Experiment Name")+".txt").getAbsolutePath(), highZoomCounter+1, lowZoomCounter);
			//ExternalScriptCommander.submitPositions_temp(new File(currentTable.getRootPath(),"summary_"+(String)getSharedValue("Experiment Name")+".txt"), highZoomCounter+1, lowZoomCounter);
			//this.setSharedValue("High Zoom Counter", lowZoomCounter);
		}
		

	}
	
	@Override
	public void  postProcessFail()throws Exception{
		this.recordSummaryDataset(false);
		Integer lowZoomCounter=(Integer)this.getSharedValue("Low Zoom Counter");
		if (lowZoomCounter==null)
			lowZoomCounter=-1;
		
		this.setSharedValue("Low Zoom Counter", lowZoomCounter+1);
	}
	
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_COMMAND_NAME, null, commmandName, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_X_COLUMN_NAME, null, xColumn, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_Y_COLUMN_NAME, null, yColumn, ParameterType.STRING_PARAMETER);
		
		jobCollection.addParameter(KEY_BLOCK_COUNT,null, blockCount, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_DELAY_MILLIS,null, delayMillis, ParameterType.INT_PARAMETER);

		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.commmandName=(String)_jobParameterCollection.getParameterValue(KEY_COMMAND_NAME);
		this.xColumn=(String)_jobParameterCollection.getParameterValue(KEY_X_COLUMN_NAME);
		this.yColumn=(String)_jobParameterCollection.getParameterValue(KEY_Y_COLUMN_NAME);
		
		this.blockCount=(Integer)_jobParameterCollection.getParameterValue(KEY_BLOCK_COUNT);
		this.delayMillis=(Integer)_jobParameterCollection.getParameterValue(KEY_DELAY_MILLIS);

	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-2, 10,10,2);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		System.getProperties().setProperty("plugins.dir", "D:/Alex_work/runnung soft/Fiji.app_2019/plugins");
		
		// start ImageJ
		new ImageJ();
		
		String tblPth="D:\\Alex_work\\feedback-microscopy-workshop-data\\mitotic-cells--original-data";
		String tblFnm="experiment_summary.txt";
		Job_RunImageJCommand_Init testJob=new Job_RunImageJCommand_Init();
		testJob.initialise(null, "LZ.Image", false);
		testJob.testJobMicTable(0, tblPth, tblFnm);
	}
	
}
