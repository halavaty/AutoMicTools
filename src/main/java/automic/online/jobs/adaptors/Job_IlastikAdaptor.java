package automic.online.jobs.adaptors;


import java.awt.geom.Point2D;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FilenameUtils;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissLSM800;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.table.TableModel;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;


public class Job_IlastikAdaptor  extends Job_Default{

	
	public static final String INPUT_IMG_TAG="Selection";

	
	public static final String KEY_ILASTIK_PATH="Ilastik path";
	public static final String KEY_PROJECT_PATH="Project path";
	
	public static final String KEY_MAX_OBJECT_NUMBER="Max Object number";
	public static final String KEY_BLEACH_REGION_RADIUS="Bleach region radius";

	
	
	//parameters
	String ilastikPath="C:\\Program Files\\ilastik-1.3.3post2\\ilastik.exe";
	String projectPath="D:\\tempDat\\temp_test_ilastik_automic\\mitocheck_2d+t\\manualTracking.ilp";
	int maxObjectNumber=5;
	int bleachRegionRadius=10;
	

	//variables
	private ImagePlus originalImage=null;
	File cecogInFile;
	File cecogOutFile;
	double[] xPositions;
	double[] yPositions;
	double[] radiusValues;
	Roi[] 	 selectedRois;
	
	
	Roi[] nucRois=null;
	Roi selNucleus=null;

	Point2D.Double zoomPoint=null;
	
	@Override
	protected void cleanIterOutput(){
		originalImage=null;
		zoomPoint=null;
		cecogInFile=null;
		cecogOutFile=null;
		xPositions=null;
		yPositions=null;
		radiusValues=null;
		selectedRois=null;
	}

	@Override
	protected void preProcessOnline()throws Exception{
		super.preProcessOnline();
		Thread.sleep(500);
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		originalImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//dapiImage=LSMprocessor.openLSM(curTbl.getFile(curDInd, DapiTag, "IMG"));
		ImageOpenerWithBioformats.colorMode=ImporterOptions.COLOR_MODE_COMPOSITE;
		originalImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}

	@Override
	protected boolean runProcessing()throws Exception{
//		cecogInFile=new File(newImgFile.getParentFile().getParentFile(),"CecogInput");
//		if(!cecogInFile.isDirectory())
//			cecogInFile.mkdir();
//		cecogOutFile=new File(newImgFile.getParentFile().getParentFile(),"CecogOutput");
//		if(!cecogOutFile.isDirectory())
//			cecogOutFile.mkdir();
//		
//		//get basename
//		String basename=FilenameUtils.getBaseName(newImgFile.getName());
		
		
		
		//save individual files to CecogIn directory
		
//		String cecogCommand=String.format("\"%s\" -o %s -s %s", this.cecogPath,this.cecogOutFile.getAbsolutePath(),this.classifierConfPath);
//		for (int channelIndex=1;channelIndex<=originalImage.getNChannels();channelIndex++) {
//			ImagePlus channelImage=new Duplicator().run(originalImage, channelIndex, channelIndex, 1, originalImage.getNSlices(), 1, originalImage.getNFrames());
//			String savePath=new File(cecogInFile,String.format("%s--C%02d.tif", basename,channelIndex)).getAbsolutePath();
//			IJ.save(channelImage, savePath);
//			if(channelIndex<=2)
//				cecogCommand = cecogCommand + String.format(" -i%d %s", channelIndex, savePath);
//		}
//		
//		showDebug(originalImage, "Original Image", true);
//		
//		
//		
//		//run Cecog analysis
//		
//		String tempDirectory=cecogInFile.getAbsolutePath();//IJ.getDirectory("temp"); 
//		
//		File tempFile=new File(tempDirectory,"cecongCommand.bat");
//		
//		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
//        writer.write(cecogCommand);
//        writer.close();
//
//        
//        Process p = Runtime.getRuntime().exec(tempFile.getAbsolutePath());
//        p.waitFor();
//        
//        //read cecogOutput tables
//        TableModel primaryTableRaw=new TableModel(new File(cecogOutFile,String.format("%s--C01.csv", basename)),",");
//        TableModel secondaryTableRaw=new TableModel(new File(cecogOutFile,String.format("%s--C02.csv", basename)),",");
//		
//        List<Integer> selectedIndexes = new ArrayList<Integer>();
//        
//		for (int rawIndex=0;rawIndex<primaryTableRaw.getRowCount();rawIndex++) {
//			int objectIdPrimary=Integer.parseInt(primaryTableRaw.getStringValue(rawIndex, "ObjectId"));
//			int objectIdSecondary=Integer.parseInt(secondaryTableRaw.getStringValue(rawIndex, "ObjectId"));
//			
//			if (objectIdPrimary!=objectIdSecondary)
//				throw new Exception("Object Indexes do not match");
//			
//			double primaryProbability=Double.parseDouble(primaryTableRaw.getStringValue(rawIndex, primaryClass));
//			double secondaryProbability=Double.parseDouble(secondaryTableRaw.getStringValue(rawIndex, secondaryClass));
//			
//			if((primaryProbability>primaryThreshold)&&(secondaryProbability>secondaryThreshold)) {
//				selectedIndexes.add(objectIdPrimary);
//			}
//			
//		}

//		if(selectedIndexes.size()<1)
//			return false;
//		
//		Random r=new Random();
//		while (selectedIndexes.size()>maxObjectNumber){
//			selectedIndexes.remove(r.nextInt(selectedIndexes.size()));
//		}
//
//		int nObjects=selectedIndexes.size();
//		selectedRois=new Roi[nObjects];
//		ImagePlus labelImage=BF.openImagePlus(new File(cecogOutFile,String.format("%s--C01-lables_primary_primary.tif", basename)).getAbsolutePath())[0];
//		
//		showDebug(labelImage, "Nuclear masks", true);
//		
//		for (int iObject=0;iObject<nObjects;iObject++) {
//			IJ.setThreshold(labelImage, selectedIndexes.get(iObject), selectedIndexes.get(iObject));
//			RoiManager roiManager = ROIManipulator2D.getEmptyRm();
//			int analyzerOptions = ParticleAnalyzer.ADD_TO_MANAGER | ParticleAnalyzer.SHOW_NONE;
//			ParticleAnalyzer particleAnalyzer = new ParticleAnalyzer(analyzerOptions, 0, null, 0, Double.POSITIVE_INFINITY,
//					0, 1);
//			particleAnalyzer.analyze(labelImage);
//
//			
//			
//			selectedRois[iObject]=roiManager.getRoi(0);
//		}
//		
//		showDebug(labelImage, "Nuclear masks with ROIs", true,ROIManipulator2D.roisToOverlay(selectedRois));
//		
//		
//		xPositions=new double[nObjects];
//		yPositions=new double[nObjects];
//		radiusValues=new double[nObjects];
//		for (int roiIndex=0;roiIndex<nObjects;roiIndex++) {
//			Roi sr=selectedRois[roiIndex];
//			xPositions[roiIndex]=sr.getXBase()+sr.getFloatWidth()/2;
//			yPositions[roiIndex]=sr.getYBase()+sr.getFloatHeight()/2;
//			radiusValues[roiIndex]=bleachRegionRadius;
//		}
//
		return true;
	}
	
	
	@Override
	protected void postProcessSuccess()throws Exception{
		
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", null, null, null,xPositions,yPositions,radiusValues);

	}

	@Override
	protected void postProcessFail()throws Exception{
		
		ZeissLSM800.submitJobPixels(newImgFile, "spindle-LZ", null, null, null,xPositions,yPositions,radiusValues);

	}
	
	@Override
	public void postProcessOffline(){
		
		this.visualise(1000, 120);
	}


	@Override 
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		
		if (!ROIManipulator2D.isEmptyRoiArr(selectedRois)){
			o=(ROIManipulator2D.roisToOverlay(selectedRois));
		}
		return o;
	}

	@Override
	public void visualise(int _xvis, int _yvis){
		Overlay o=getOverlay();
		this.visualiseImg(originalImage, o, _xvis, _yvis);
		
	}
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();

		jobCollection.addParameter(KEY_ILASTIK_PATH, null, ilastikPath, ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_PROJECT_PATH, null, projectPath, ParameterType.STRING_PARAMETER);
		
		jobCollection.addParameter(KEY_MAX_OBJECT_NUMBER, null, maxObjectNumber, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_BLEACH_REGION_RADIUS, null, bleachRegionRadius, ParameterType.INT_PARAMETER);
		
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){

		this.ilastikPath=		(String)_jobParameterCollection.getParameterValue(KEY_ILASTIK_PATH);
		this.projectPath=		(String)_jobParameterCollection.getParameterValue(KEY_PROJECT_PATH);

		this.maxObjectNumber=	(Integer)_jobParameterCollection.getParameterValue(KEY_MAX_OBJECT_NUMBER);
		this.bleachRegionRadius=(Integer)_jobParameterCollection.getParameterValue(KEY_BLEACH_REGION_RADIUS);

	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-2, 10,10,2);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="X:\\group\\ALMFstuff\\Aliaksandr\\test_Feedback_LSM900_Sanjana\\20200204-084938\\summary_.txt";
		final String imgFnm="LowZoom--W0000--P0002-T0001.czi";
		Job_IlastikAdaptor testJob=new Job_IlastikAdaptor();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
		//testJob.testJobMicTable(7, tblPth, tblFnm);
	}
}
