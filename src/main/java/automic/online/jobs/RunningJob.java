package automic.online.jobs;

import java.io.File;
import java.util.Map;

import automic.parameters.ParameterCollection;
//import automic.table.TableModel;
//import automic.utils.DebugVisualiser;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import ij.gui.Overlay;

public class RunningJob {

	JobFunctions functions;
	ParameterCollection jobParameters;
	
	private Overlay overlay;
	
	private String imageColumnTag;		//tag of the file columns in the output table where triggering image is stored
	private String imageFileTag;				//unique name of the triggering tag
	private boolean visualiseOnline;		//flag telling whether visualisation should run online
	
	private Map<String,Object> sharedData;
	

	public static Logger logger;

	static{
		logger=ApplicationLogger.getLogger();
	}
	
	
	/**
	 * sets protocol, specific settings for this job
	 * @param _imageFileTag		file tag of new file identified
	 * @param _columnTag		column Tag of new File identified
	 * @param _visualiseOnline	whether visualisation procedure to be called online
	 */
	public void initialise(String _imageFileTag,String _columnTag, boolean _visualiseOnline){
		this.imageFileTag=_imageFileTag;
		this.imageColumnTag=_columnTag;
		this.visualiseOnline=_visualiseOnline;
	}
	
	public String getFileTag(){
		return imageFileTag;
	}
	
	public String getColumnTag(){
		return imageColumnTag;
	}
	
	public boolean getVisualisationFlag(){
		return visualiseOnline;
	}
	
	public boolean goodForFileName(String _fileName){
		return _fileName.indexOf(imageFileTag)>0;
	}
	
	public Map<String,Object> getSharedData(){
		return sharedData;
	}
	
	public void setSharedData(Map<String,Object> _sharedDt){
		sharedData=_sharedDt;
	}
	
	protected Object getSharedValue(String _key){
		return sharedData.get(_key);
	}
	
	protected void setSharedValue(String _key, Object _val){
		sharedData.put(_key, _val);
	}
	
	protected void clearSharedData(){
		sharedData.clear();
	}

	/**
	 * runs image processing
	 */
	public void doJobOnline(JobData _data, File _newImgFile,boolean _svOutput,boolean _debugRun)throws Exception{
		boolean processingSuccess;
		
		//if(_debugRun)
		//	DebugVis=new DebugVisualiser(this.getDebugVisualiserSettings());
		
		functions.cleanTemporaryData();
		//curTbl=_tbl;
		//curDInd=_dind;
		//newImgTag=_newImgTag;
		//if(_newImgFile==null)//get file from table
		//	newImgFile=_tbl.getFile(_dind, imgColumnNm, "IMG");
		//else
		//	newImgFile=_newImgFile;
		
		if(!_debugRun)
			functions.preProcessOnline(_data);
		else
			functions.preProcessOffline(_data);
//		this.getTableInput(_dind);
		processingSuccess=functions.runProcessing(_data);

		if(visualiseOnline||_debugRun)
			overlay=functions.createOverlay();

		if(processingSuccess){
			functions.postProcessSuccess();
		}else{
			functions.postProcessFail();
		}
	}
	
	public void doJobTest(){
		
	}
	

}
