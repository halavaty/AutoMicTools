package automic.online.jobs.cells;


import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.DebugVisualiserSettings;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.segment.ParticleSegmentor;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;


public class Job_GetLowZoomNuclei  extends Job_Default{

	public static final String KEY_NUCLEI_CHANNEL_INDEX="Nuclei channel index";
	public static final String KEY_NUCLEI_THRESHOLD="Nuclei threshold";
	public static final String KEY_NUCLEI_FILTER_RADIUS="Nuclei filter radius";
	public static final String KEY_NUCLEI_MINIMAL_AREA="Nuclei minimal area";

	
	
	//parameters
	int nucChannel;
	double nucMinSize;
	double nucFilterSize;
	double nucThresh;
	
	public static String DapiTag="DAPI.LZ";
	
	private ImagePlus dapiImage=null;
	
	Roi[] nucRois=null;
	Roi selNucleus=null;

	Point2D.Double zoomPoint=null;
	
	@Override
	protected void cleanIterOutput(){
		dapiImage=null;
		zoomPoint=null;
	}

	@Override
	protected void preProcessOnline()throws Exception{
		super.preProcessOnline();
		Thread.sleep(500);
		dapiImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//dapiImage=LSMprocessor.openLSM(curTbl.getFile(curDInd, DapiTag, "IMG"));
		dapiImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}

	@Override
	protected boolean runProcessing()throws Exception{
		zoomPoint=null;
		nucRois=segmentNuclei(dapiImage, nucChannel);
		if (nucRois==null) return false;
		selNucleus=ROIManipulator2D.getRandRoi(nucRois);
		Rectangle rectangle=selNucleus.getBounds();
		zoomPoint=new Point2D.Double(rectangle.getCenterX(), rectangle.getCenterY());
		
		//prepare for visualisation
		ROIManipulator2D.removeSliceInfo(nucRois);
		for (Roi rnuc:nucRois){
			rnuc.setStrokeColor((rnuc.equals(selNucleus))?Color.yellow:Color.cyan);
		}
		
		return true;
	}
	
	private Roi[] segmentNuclei(ImagePlus _inputImage, int _channelIndex)throws Exception{
		if(_inputImage.getNChannels()>1)
			_inputImage=new Duplicator().run(_inputImage, _channelIndex, _channelIndex, 1, _inputImage.getNSlices(), 1,1);

		IJ.run(_inputImage, "Median...", "radius="+nucFilterSize+" stack");
		
		ZProjector maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		maxProjector.setImage(_inputImage);
		maxProjector.doProjection();
		ImagePlus maxImage=maxProjector.getProjection();
		
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		
		//IJ.run(_rawDapiMaxImage, "Subtract...", "value="+back_DAPIMax);
		
		ParticleSegmentor nucSegm=new ParticleSegmentor((int)nucMinSize);
		/*ImagePlus nucMaskImg=*/nucSegm.SegSliceThreshold(maxImage, ParticleAnalyzer.SHOW_MASKS, true, nucThresh);
		
		nucRois=rMan.getRoisAsArray();
		
		return nucRois;
	}
	
	@Override
	protected void postProcessSuccess()throws Exception{
		//form  comma-separated strings with coordinates
		String xstr=""+zoomPoint.x;
		String ystr=""+zoomPoint.y;
		String zstr=""+((dapiImage.getNSlices()-1)/2);
		
		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr, ystr, zstr,null,null,null,null,"");
		saveRoisForImage(newImgFile, nucRois);
	}

	@Override 
	protected Overlay createOverlay(){
		Overlay o=ROIManipulator2D.roisToOverlay(nucRois);
		if (zoomPoint!=null){
			o.add(new PointRoi(zoomPoint.x,zoomPoint.y));
		}

		return o;
	}

	@Override
	public void visualise(int _xvis, int _yvis){
		Overlay o=getOverlay();
		this.visualiseImg(dapiImage, o, _xvis, _yvis);
		
		//make 2 images syncronised?
	}
	
	@Override
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_NUCLEI_CHANNEL_INDEX, null, 1, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEI_FILTER_RADIUS, null, 2.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEI_THRESHOLD, null, 25.0, ParameterType.DOUBLE_PARAMETER);
		jobCollection.addParameter(KEY_NUCLEI_MINIMAL_AREA, null, 200.0, ParameterType.DOUBLE_PARAMETER);


		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){

		this.nucChannel=(Integer)_jobParameterCollection.getParameterValue(KEY_NUCLEI_CHANNEL_INDEX);
		this.nucFilterSize=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEI_FILTER_RADIUS);
		this.nucThresh=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEI_THRESHOLD);
		this.nucMinSize=(Double)_jobParameterCollection.getParameterValue(KEY_NUCLEI_MINIMAL_AREA);

	}
	
	@Override
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(-2, 10,10,2);
	}

	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		String imagePth="E:/test-20180524/test5_DE_W0001_P0001";
		String imageFnm="test5_DE_2_W0001_P0001_T0001.lsm";
		Job_GetLowZoomNuclei testJob=new Job_GetLowZoomNuclei();
		testJob.initialise(null, DapiTag, false);
		testJob.testImageFile(new File(imagePth,imageFnm));
		//testJob.testJobMicTable(7, tblPth, tblFnm);
	}
}
