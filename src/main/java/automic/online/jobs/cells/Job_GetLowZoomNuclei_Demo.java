package automic.online.jobs.cells;


import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
//import java.io.File;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ZProjector;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
//import process.segment.ParticleSegmentor;


public class Job_GetLowZoomNuclei_Demo  extends Job_Default{

	//non-passed parameters
	double nucMinSize=3000;
	double nucMaxSize=10000;
	double nucMinCirc=0.5;
	double nucFilterSize=2;
	double nucThresh=6000;
	
	public static String DapiTag="DAPI.LZ";
	
	private ImagePlus dapiImage=null;
	
	Roi[] nucRois=null;
	Roi selNucleus=null;

	Point2D.Double zoomPoint=null;
	
	@Override
	protected void cleanIterOutput(){
		dapiImage=null;
		zoomPoint=null;
	}

	@Override
	protected void preProcessOnline()throws Exception{
		super.preProcessOnline();
		Thread.sleep(500);
		dapiImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		//dapiImage=LSMprocessor.openLSM(curTbl.getFile(curDInd, DapiTag, "IMG"));
		dapiImage=ImageOpenerWithBioformats.openImage(newImgFile);
	}

	@Override
	protected boolean runProcessing()throws Exception{
		zoomPoint=null;
		nucRois=segmentNuclei(dapiImage, 1);
		if (nucRois==null) return false;
		selNucleus=ROIManipulator2D.getRandRoi(nucRois);
		Rectangle rectangle=selNucleus.getBounds();
		zoomPoint=new Point2D.Double(rectangle.getCenterX(), rectangle.getCenterY());
		
		//prepare for visualisation
		ROIManipulator2D.removeSliceInfo(nucRois);
		for (Roi rnuc:nucRois){
			rnuc.setStrokeColor((rnuc.equals(selNucleus))?Color.yellow:Color.cyan);
		}
		
		return true;
	}
	
	private Roi[] segmentNuclei(ImagePlus _inputImage, int _channelIndex)throws Exception{
		if(_inputImage.getNDimensions()>3)
			_inputImage=new Duplicator().run(_inputImage, _channelIndex, _channelIndex, 1, _inputImage.getNSlices(), 1,1);

		IJ.run(_inputImage, "Median...", "radius="+nucFilterSize+" stack");
		
		ZProjector maxProjector=new ZProjector();
		maxProjector.setMethod(ZProjector.MAX_METHOD);
		maxProjector.setImage(_inputImage);
		maxProjector.doProjection();
		ImagePlus maxImage=maxProjector.getProjection();
		
		RoiManager rMan=ROIManipulator2D.getEmptyRm();
		
		//IJ.run(_rawDapiMaxImage, "Subtract...", "value="+back_DAPIMax);
		
		//ParticleSegmentor nucSegm=new ParticleSegmentor((int)nucMinSize);
		/*ImagePlus nucMaskImg=*///nucSegm.SegSliceThreshold(maxImage, ParticleAnalyzer.SHOW_MASKS, true, nucThresh);
		
		ImageProcessor pmax=maxImage.getProcessor();
		//IJ.setThreshold(_init,_mintr, _init.getStatistics(ImageStatistics.MIN_MAX).max);
		pmax.setThreshold(nucThresh, maxImage.getStatistics(ImageStatistics.MIN_MAX).max, pmax.getLutUpdateMode());

		//segmentation through particle analyser class
		int part_opt=ParticleAnalyzer.INCLUDE_HOLES|ParticleAnalyzer.FOUR_CONNECTED|ParticleAnalyzer.ADD_TO_MANAGER
				|ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES;

		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,nucMinSize,nucMaxSize,nucMinCirc,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(maxImage,pmax);

		pmax.resetThreshold();
		
		
		
		nucRois=rMan.getRoisAsArray();
		
		return nucRois;
	}
	
	@Override
	protected void postProcessSuccess()throws Exception{
		//form  comma-separated strings with coordinates
		String xstr=""+zoomPoint.x;
		String ystr=""+zoomPoint.y;
		String zstr=""+((dapiImage.getNSlices()-1)/2);
		
		ZeissKeys.submitCommandsToMicroscope("trigger1", xstr, ystr, zstr,null,null,null,null,"");
		saveRoisForImage(newImgFile, nucRois);
	}

	@Override 
	protected Overlay createOverlay(){
		Overlay o=ROIManipulator2D.roisToOverlay(nucRois);
		if (zoomPoint!=null){
			o.add(new PointRoi(zoomPoint.x,zoomPoint.y));
		}

		return o;
	}

	@Override
	public void visualise(int _xvis, int _yvis){
		Overlay o=getOverlay();
		this.visualiseImg(dapiImage, o, _xvis, _yvis);
		IJ.run(dapiImage,"Out [-]", "");
		IJ.run(dapiImage,"Out [-]", "");
		
		//make 2 images syncronised?
	}
	
	/**
	 * offline debugging
	 * @param args unsused
	 */
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		//String imagePth="Z:/halavaty/New/demo--Kyoto--Fixed--3chan--20170831/p3_DE_W0001_P0001";
		//String imageFnm="p3_DE_2_W0001_P0001_T0001.lsm";
		Job_GetLowZoomNuclei_Demo testJob=new Job_GetLowZoomNuclei_Demo();
		testJob.initialise(null, DapiTag, false);
		testJob.Debug=true;
		//testJob.testImageFile(new File(imagePth,imageFnm));
		
		testJob.testJobMicTable(0, "Z:/halavaty/New/demo--Kyoto--Fixed--3chan--20170831", "summary_p3_.txt");
	}
}
