package automic.online.jobs;

import automic.parameters.ParametrisedFunction;
import ij.gui.Overlay;

public interface JobFunctions extends ParametrisedFunction{

	/**
	 * clean output data of the previous iteration so that it does not influence current processing
	 */
	void cleanTemporaryData();
	
	/**
	 * preprocessing activities in the online mode. Any data modification.
	 * E.g. adding new image file attributes to the data table
	 * @param _data
	 * @throws Exception
	 */
	void preProcessOnline(JobData _data)throws Exception;
	
	
	/**
	 * preprocessing activities in the offline mode.
	 * @param _data
	 * @throws Exception
	 */
	void preProcessOffline(JobData _data)throws Exception;
	
	/**
	 * function for the actual image processing
	 * @return true if image analysis was successful and false othervise
	 * @throws Exception
	 */
	boolean runProcessing(JobData _data)throws Exception;
	
	
	/**
	 * creates overlay based on the results of online image analysis
	 * @return
	 * @throws Exception
	 */
	Overlay createOverlay()throws Exception;
	
	/**
	 * visualise results of online image analysis
	 * @param _xvis	x coordinate in screen pixels of the window upper left corner
	 * @param _yvis y coordinate in screen pixels of the window upper left corner
	 */
	public void visualise(int _xvis, int _yvis);
	
	/**
	 * Post processing activities in case image was succesfull.
	 * E.g.submitting feedback commands to the microscope
	 */
	void postProcessSuccess()throws Exception;
	
	/**
	 * Post processing activities in case image analysis failed to find objects of interest.
	 */
	void postProcessFail()throws Exception;
	
	/**
	 * postprocessing steps in case of offline testing.
	 * ? check whether indeed needed 
	 */
	
	//void postProcessOffline();
}
