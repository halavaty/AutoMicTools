package automic.online.jobs.frap;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;



import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.Toolbar;
import mcib3d.geom.Point3D;

public class Job_GetManualBleachRoi extends Job_Default{
	public static final String INPUT_IMG_TAG="Selection";
	public static String FRAP_IMG_TAG="FRAP";
	
	private boolean selectionMade=false;
	
	public static final String KEY_LOCATION_X="Selection window location X";
	public static final String KEY_LOCATION_Y="Selection window location Y";
	
	public static final String KEY_RESPONCE_TIME="Time to wait for responce";
	public static final String KEY_REGION_TYPE="Bleacing region type (circle, rectangle, polygon)";
	public static final String KEY_BLEACH_RADIUS="Bleach radius for circle";
	
	//region keys
	public static final String CIRCLE_REGION="circle";
	public static final String RECTANGLE_REGION="rectangle";
	public static final String POLYGON_REGION="polygon";
	
	
	
	private ImagePlus inImg;
	
	Roi selectedRegion;
	Point3D selPoint;
	
	private int initialLocationX;
	private int initialLocationY;
	private int responceTime;
	private String regionType;
	private int bleachRadius;
	
	double selectedXPosition;
	double selectedYPosition;
	String xRoiString;
	String yRoiString;
	String zenRoiString;

	
	class SelectionListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			selectionMade=true;
			//IJ.showMessage("Mouse clicked");
		}
		
		@Override
		public void mousePressed(MouseEvent e) {}
		
		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}

	}
	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetManualBleachRoi(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		selPoint=null;
		selectionMade=false;
		selectedRegion=null;
		xRoiString=null;
		yRoiString=null;
		zenRoiString=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);

		
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);

		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		
		super.preProcessOnline();
		
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		ImageWindow.setNextLocation(initialLocationX,initialLocationY);
		//img=new Duplicator().run(img, 3, 4, 1, img.getNSlices(), 1, 1);
		

		
		
		
		switch(regionType) {
			case CIRCLE_REGION: IJ.setTool("point"); break;
			case RECTANGLE_REGION: IJ.setTool(Toolbar.RECTANGLE); break;
			case POLYGON_REGION: IJ.setTool(Toolbar.POLYGON); break;
			default: throw new Exception ("Region type specified wrongly");
		}
		
		
		inImg.show();
		
		
		
		if (regionType.equals(CIRCLE_REGION)) {
			SelectionListener selectionListener=new SelectionListener();

			ImageCanvas imgCanvas=inImg.getCanvas();

			imgCanvas.addMouseListener(selectionListener);
			long startTime=System.currentTimeMillis();
			while ((System.currentTimeMillis()-startTime)<responceTime*1000) {
				if (selectionMade)
					break;
				TimeUnit.MILLISECONDS.sleep(50);
			}
			imgCanvas.removeMouseListener(selectionListener);
		}else {
			selectionMade=getSelectionMadeFromDialog(responceTime*1000);
		}

		
		
		
		
		inImg.hide();

		if (!selectionMade)
			return false;
		
		
		//new WaitForUserDialog ("Select Shift Point").show();

		selectedRegion=inImg.getRoi();
		if ( selectedRegion==null) return false;
		switch(regionType) {
			case CIRCLE_REGION: 
				if(selectedRegion.getType()!=Roi.POINT)
					return false;
				Polygon pointPolygon=selectedRegion.getPolygon();
				selectedXPosition=pointPolygon.xpoints[0];
				selectedYPosition=pointPolygon.ypoints[0];
				
				xRoiString=""+selectedXPosition+", "+(selectedXPosition+bleachRadius);
				yRoiString=""+selectedYPosition+", "+(selectedYPosition);
				zenRoiString="circle";

				
				break;
			
			case RECTANGLE_REGION: 
				if(selectedRegion.getType()!=Roi.RECTANGLE)
					return false;
				
				Rectangle bounds=selectedRegion.getBounds();
				xRoiString=""+bounds.x+", "+(bounds.x+bounds.width);
				yRoiString=""+bounds.y+", "+(bounds.y+bounds.height);
				zenRoiString="rectangle";
				break;
			
			case POLYGON_REGION: 
				if(selectedRegion.getType()!=Roi.POLYGON)
					return false;
				
				
				Polygon pointPolygon2=selectedRegion.getPolygon();
				int nPoints=pointPolygon2.npoints;
				xRoiString="";
				yRoiString="";
				for (int iPoint=0;iPoint<nPoints;iPoint++) {
					if(iPoint!=0) {
						xRoiString+=", ";
						yRoiString+=", ";
					}
					xRoiString+=pointPolygon2.xpoints[iPoint];
					yRoiString+=pointPolygon2.ypoints[iPoint];
				}
				
				zenRoiString="polyline";
				break;
			
			default: throw new Exception ("Region type specified wrongly");
		}
	
		
		selectedRegion.setStrokeColor(Color.yellow);
		
		//invert zPosition if required
		
		
		
		
		return true;
		
		
		

	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		if (selectedRegion!=null)
			o.add(selectedRegion);
		return o;
	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);

		if (selPoint==null) return;
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("trigger1",""+(inImg.getWidth()/2.0-1),""+(inImg.getHeight()/2.0-1),"0",zenRoiString,"bleachAnalyse", xRoiString, yRoiString,"");
		
		saveRoiForImage(newImgFile, selectedRegion);
	}
	
	@Override
	public void postProcessFail()throws Exception{
		super.postProcessFail();
		IJ.log("No region was selected");
	}

	@Override
	public void postProcessOffline(){
		
		IJ.log(""+selectionMade);
//		if(selPoint!=null){
//			System.out.println("Selected point: ");
//			System.out.println("x="+selPoint.getX());
//			System.out.println("y="+selPoint.getY());
//			System.out.println("z="+selPoint.getZ());
//		}else{
//			System.out.println("No point was selected for FRAP");
//		}
//		
//		inImg.setCalibration(new Calibration());
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_LOCATION_X, null, 1000, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_LOCATION_Y, null, 120, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_RESPONCE_TIME, null, 10, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_REGION_TYPE, null, "rectangle", ParameterType.STRING_PARAMETER);
		
		jobCollection.addParameter(KEY_BLEACH_RADIUS, null, 10, ParameterType.INT_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.initialLocationX=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_X);
		this.initialLocationY=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_Y);
		this.responceTime=(Integer)_jobParameterCollection.getParameterValue(KEY_RESPONCE_TIME);
		this.regionType=(String)_jobParameterCollection.getParameterValue(KEY_REGION_TYPE);
		
		this.bleachRadius=(Integer)_jobParameterCollection.getParameterValue(KEY_BLEACH_RADIUS);
	}
	
	private boolean getSelectionMadeFromDialog(int _maximumDelayTimeMillis)throws Exception {
		final JOptionPane msg = new JOptionPane("Select region?", JOptionPane.QUESTION_MESSAGE, JOptionPane.YES_NO_OPTION);
	    final JDialog dlg = msg.createDialog("Select Yes or No");
        dlg.setModal(false);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.addComponentListener(new ComponentAdapter() {
	            
    		@Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                final Timer t = new Timer(_maximumDelayTimeMillis,new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dlg.setVisible(false);
                    }
                });
                t.start();
            }
        });
        dlg.setVisible(true);
        while(dlg.isVisible())
        	TimeUnit.MILLISECONDS.sleep(50);
        Object dialogValue=msg.getValue();
        if(dialogValue.equals(JOptionPane.YES_OPTION))
        	return true;
		
		
		
		return false;
	}
	
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="X:\\group\\ALMFstuff\\Aliaksandr\\User_data\\Matteo_Rauzi_ZenBlack_Automation\\fake_sata_to_optimise\\summary_tst1.txt";
		final String imgFnm="tst2_DE_1_W0001_P0002_T0001.lsm";
		Job_GetManualBleachRoi testJob=new Job_GetManualBleachRoi();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
