package automic.online.jobs.frap;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import automic.online.jobs.Job_Default;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.utils.imagefiles.ImageOpenerWithBioformats;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.Toolbar;
import mcib3d.geom.Point3D;

public class Job_GetMultipleManualBleachRois extends Job_Default{
	public static final String INPUT_IMG_TAG="Selection";
	public static String FRAP_IMG_TAG="FRAP";
	
	private boolean selectionMade=false;
	
	public static final String KEY_LOCATION_X="Selection window location X";
	public static final String KEY_LOCATION_Y="Selection window location Y";
	
	public static final String KEY_RESPONCE_TIME="Time to wait for responce";
	public static final String KEY_USE_ROIS_TIME_END="Proceed with selected ROIs if time expires";
	public static final String KEY_CAN_MOVE_ROI="Can move Roi after drawing";
	public static final String KEY_REGION_TYPE="Bleacing region type (circle, rectangle, polygon)";
	public static final String KEY_BLEACH_RADIUS="Bleach radius for circle";
	
	
	//region keys
	public static final String CIRCLE_REGION="circle";
	public static final String RECTANGLE_REGION="rectangle";
	public static final String POLYGON_REGION="polygon";
	
	
	
	private ImagePlus inImg;
	
	Point3D selPoint;
	
	private int initialLocationX;
	private int initialLocationY;
	private int responceTime;
	private boolean useRoisTimeEnd;
	private boolean canMoveRoi;
	
	private String regionType;
	private int bleachRadius;
	
	String xRoisString;
	String yRoisString;
	
	SelectionManadgement selectedRoiManager;
	
	class RoiCommand{
		private String xRoiString;
		private String yRoiString;
		
		public RoiCommand(Roi _selectedRoi) {
			
			if(_selectedRoi.getType()!=getExpectedSelectionType())
				throw new RuntimeException ("Region does not match specified type");

			
			switch(regionType) {
				case CIRCLE_REGION: 
					
					Polygon pointPolygon=_selectedRoi.getPolygon();
					int selectedXPosition=pointPolygon.xpoints[0];
					int selectedYPosition=pointPolygon.ypoints[0];
						
					xRoiString=""+selectedXPosition+", "+(selectedXPosition+bleachRadius);
					yRoiString=""+selectedYPosition+", "+(selectedYPosition);
					
					break;
				
				case RECTANGLE_REGION: 
					
					Rectangle bounds=_selectedRoi.getBounds();
					xRoiString=""+bounds.x+", "+(bounds.x+bounds.width);
					yRoiString=""+bounds.y+", "+(bounds.y+bounds.height);
					break;
				
				case POLYGON_REGION: 
					
					Polygon pointPolygon2=_selectedRoi.getPolygon();
					int nPoints=pointPolygon2.npoints;
					xRoiString="";
					yRoiString="";
					for (int iPoint=0;iPoint<nPoints;iPoint++) {
						if(iPoint!=0) {
							xRoiString+=", ";
							yRoiString+=", ";
						}
						xRoiString+=pointPolygon2.xpoints[iPoint];
						yRoiString+=pointPolygon2.ypoints[iPoint];
					}
					
					break;
				
				default: throw new RuntimeException ("Region type specified wrongly");
			}
		}
		
		
		public String getXRoiString() {
			return xRoiString;
		}
		
		public String getYRoiString() {
			return yRoiString;
		}
		
		
	}

	class SelectionManadgement{//singleton class to manage multiple seletions
		
		public List<Roi> selectedRegions;
		public String xCommandString="";
		public String yCommandString="";
		public String roiAimCommandString;
		public String roiTypeCommandString;
		
		public SelectionManadgement() {
			selectedRegions=new ArrayList<Roi>();
		}
		
		public void removeLasetSelectedRoi() {
			int nRegionsOld=selectedRegions.size();
			if (nRegionsOld<1)
				return;
			
			selectedRegions.remove(nRegionsOld-1);
		}
		
		
		public void updateCommandStrings(){
			xCommandString="";
			yCommandString="";
			roiAimCommandString="";
			roiTypeCommandString="";
			
			for (Roi selectedRegion:selectedRegions) {
				if(!xCommandString.equals("")) {
					xCommandString+="; ";
					yCommandString+="; ";
					roiAimCommandString+="; ";
					roiTypeCommandString+="; ";
				}
				RoiCommand roiCommand=new RoiCommand(selectedRegion);
				xCommandString+=roiCommand.getXRoiString();
				yCommandString+=roiCommand.getYRoiString();
				roiAimCommandString+="bleachAnalyse";
				roiTypeCommandString+=getZenRoiString();
			}
		}
		
		public void updateSelectionDisplay() {
			Overlay o=createOverlay();
			inImg.setOverlay(o);
			IJ.log(String.format("%d ROIs are currently selected",selectedRoiManager.selectedRegions.size()));
		}

	}
	
	
	class BleachMouseListener implements MouseListener{
		
		//MouseListener methods
		@Override
		public void mouseClicked(MouseEvent e) {}
		
		@Override
		public void mousePressed(MouseEvent e) {}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			if (canMoveRoi)
				return;
			catchSelectedRoi(inImg);
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}

	}
	
	class BleachKeyboardListener implements KeyListener{
		//KeyListener methods
		@Override
		public void  keyPressed(KeyEvent e) {
			
			int keyCode=e.getKeyCode();
			
			switch(keyCode) {
				case KeyEvent.VK_LEFT: //react on left arrow Key
					//remove last selected region
					selectedRoiManager.removeLasetSelectedRoi();
					selectedRoiManager.updateSelectionDisplay();
					break;

				case KeyEvent.VK_RIGHT: //react on right arrow Key
					//confirm region selection in case of "Can move Roi" mode
					if (!canMoveRoi)
						return;
					catchSelectedRoi(inImg);
					break;

					
				default:return;
			}
		}
		
		@Override
		public void  keyReleased(KeyEvent e) {}

		@Override
		public void  keyTyped(KeyEvent e) {}

	}
	
	protected void catchSelectedRoi(ImagePlus _img) {
		
		Roi selectedRoi=_img.getRoi();
		
		if (selectedRoi==null)
			return;
		
		// check if ROI drawing is completed
		if(selectedRoi.getState()!=Roi.NORMAL)
			return;
		
		selectedRoiManager.selectedRegions.add(selectedRoi);
		_img.setRoi((Roi)null);
		selectedRoiManager.updateSelectionDisplay();

	}
	
	/**
	 * initialise all service classes and structures
	 */
	public	Job_GetMultipleManualBleachRois(){
		super();
	}
	
	@Override
	protected void cleanIterOutput(){
		selPoint=null;
		selectionMade=false;
		selectedRoiManager=null;
	}
	
	@Override
	protected void preProcessOnline()throws Exception{
		super.clearSharedData();
		currentTable.cleanRecord(curDInd);

		
		inImg=ImageOpenerWithBioformats.openImage(newImgFile);
		selectedRoiManager= new SelectionManadgement();
		
		String Exper_nm=newImgFile.getName();
		Exper_nm=Exper_nm.substring(0, Exper_nm.indexOf(fileTag));
		this.setSharedValue("Experiment Name", Exper_nm);
		super.preProcessOnline();
	}
	
	@Override
	protected void preProcessOffline()throws Exception{
		inImg=ImageOpenerWithBioformats.openImage(currentTable.getFile(curDInd, INPUT_IMG_TAG, "IMG"));
		selectedRoiManager= new SelectionManadgement();
	}

	
	/**
	 * calculation of 3d ERES position for bleaching in pixel coordinates
	 */
	@Override
	protected boolean runProcessing()throws Exception{
		
		ImageWindow.setNextLocation(initialLocationX,initialLocationY);
		
		setImageJDrawingTool();
		

		//selecting photomanipulation regions
		
		inImg.show();
		inImg.getWindow().setAlwaysOnTop(true);
		
		inImg.getWindow().setTitle("Selection Image");
		
		
		BleachMouseListener bleachSelectionListener=new BleachMouseListener();
		BleachKeyboardListener bleachKeyboardListener=new BleachKeyboardListener();
		ImageCanvas imgCanvas=inImg.getCanvas();
		
		for( KeyListener kl : imgCanvas.getKeyListeners() ) {
	        imgCanvas.removeKeyListener( kl );
	    }
		
		
		imgCanvas.addMouseListener(bleachSelectionListener);
		imgCanvas.addKeyListener(bleachKeyboardListener);
		selectionMade=getSelectionMadeFromDialog(inImg.getWindow(),responceTime*1000);
		
		imgCanvas.removeMouseListener(bleachSelectionListener);
		imgCanvas.removeKeyListener(bleachKeyboardListener);
		
		inImg.hide();

		
		//checking if selections were performed
		if ( selectedRoiManager.selectedRegions.isEmpty()) return false;

		if (!selectionMade) return false;

		return true;
	}
	
	
	@Override
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		List<Roi> selectedRegions=selectedRoiManager.selectedRegions;
		if (selectedRegions.isEmpty())
			return o;
		
		for(Roi selectedRegion:selectedRegions) {
			o.add(convertSelectedRoiToOverlayRoi(selectedRegion));
		}
		
		
		return o;
	}
	
	private void setImageJDrawingTool() {
		switch(regionType) {
			case CIRCLE_REGION: IJ.setTool("point"); break;
			case RECTANGLE_REGION: IJ.setTool(Toolbar.RECTANGLE); break;
			case POLYGON_REGION: IJ.setTool(Toolbar.POLYGON); break;
			default: throw new RuntimeException ("Region type specified wrongly");
		}

	}

	private String getZenRoiString() {
		switch(regionType) {
			case CIRCLE_REGION: return "circle";
			case RECTANGLE_REGION: return "rectangle";
			case POLYGON_REGION: return "polyline";
			default: throw new RuntimeException ("Region type specified wrongly");
		}

	}

	private int getExpectedSelectionType() {
		switch(regionType) {
			case CIRCLE_REGION: return Roi.POINT;
			case RECTANGLE_REGION: return Roi.RECTANGLE;
			case POLYGON_REGION: return Roi.POLYGON;
			default: throw new RuntimeException ("Region type specified wrongly");
		}

	}

	
	private Roi convertSelectedRoiToOverlayRoi(Roi _selectedRoi) {
		Roi overlayRoi=null;

		if(_selectedRoi.getType()!=getExpectedSelectionType())
			throw new RuntimeException ("Region does not match specified type");

		
		switch(regionType) {
			case CIRCLE_REGION: 
				
				Polygon pointPolygon=_selectedRoi.getPolygon();
				int centerX=pointPolygon.xpoints[0];
				int centerY=pointPolygon.ypoints[0];

				overlayRoi=new OvalRoi(centerX-bleachRadius, centerY-bleachRadius, 2*bleachRadius, 2*bleachRadius);
				break;
				
			case RECTANGLE_REGION: 
				overlayRoi=_selectedRoi;
				break;
			
			case POLYGON_REGION: 
				overlayRoi=_selectedRoi;
				break;
			
			default: throw new RuntimeException ("Region type specified wrongly");
		}
		
		overlayRoi.setStrokeColor(Color.pink);
		return overlayRoi;

	}
	
	/**
	 * visualises results of image analysis
	 */
	@Override
	public void visualise(int _xvis,int _yvis){
		this.visualiseImg(inImg, getOverlay(), _xvis, _yvis);

		if (selPoint==null) return;
	}
	
	/**
	 * submits command to the microscope
	 */
	@Override
	public void postProcessSuccess()throws Exception{
		selectedRoiManager.updateCommandStrings();
		ZeissKeys.submitCommandsToMicroscope("trigger1",""+(inImg.getWidth()/2.0-1),""+(inImg.getHeight()/2.0-1),"0",selectedRoiManager.roiTypeCommandString,selectedRoiManager.roiAimCommandString, selectedRoiManager.xCommandString, selectedRoiManager.yCommandString,"");
		IJ.log(String.format("Launcing photomanipulation imaging job with %d selected regions", this.selectedRoiManager.selectedRegions.size()));
		
		int nRois=selectedRoiManager.selectedRegions.size();
		if (nRois>0) {
			Roi[] selectedRegionsArray=selectedRoiManager.selectedRegions.toArray(new Roi[nRois]);
		
			saveRoisForImage(newImgFile, selectedRegionsArray);
		}
	}
	
	@Override
	public void postProcessFail()throws Exception{
		super.postProcessFail();
		IJ.log("No region was selected. Continue with selection imaging job");
	}

	@Override
	public void postProcessOffline(){
		
		IJ.log("Selection made "+selectionMade);
		if (selectionMade)
			IJ.log("Number of selected ROIs " + this.selectedRoiManager.selectedRegions.size());
		
		this.visualise(1000, 120);
	}
	
	public ParameterCollection createJobParameters(){
		ParameterCollection jobCollection=new ParameterCollection();
		
		jobCollection.addParameter(KEY_LOCATION_X, null, 1000, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_LOCATION_Y, null, 120, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_RESPONCE_TIME, null, 30, ParameterType.INT_PARAMETER);
		jobCollection.addParameter(KEY_USE_ROIS_TIME_END, null, true, ParameterType.BOOL_PARAMETER);
		jobCollection.addParameter(KEY_CAN_MOVE_ROI, null, false, ParameterType.BOOL_PARAMETER);

		
		jobCollection.addParameter(KEY_REGION_TYPE, null, "rectangle", ParameterType.STRING_PARAMETER);
		jobCollection.addParameter(KEY_BLEACH_RADIUS, null, 10, ParameterType.INT_PARAMETER);
		
		return jobCollection;
	}

	@Override
	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		this.initialLocationX=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_X);
		this.initialLocationY=(Integer)_jobParameterCollection.getParameterValue(KEY_LOCATION_Y);
		this.responceTime=(Integer)_jobParameterCollection.getParameterValue(KEY_RESPONCE_TIME);
		this.useRoisTimeEnd=(Boolean)_jobParameterCollection.getParameterValue(KEY_USE_ROIS_TIME_END);
		this.canMoveRoi=(Boolean)_jobParameterCollection.getParameterValue(KEY_CAN_MOVE_ROI);
		
		this.regionType=(String)_jobParameterCollection.getParameterValue(KEY_REGION_TYPE);
		this.bleachRadius=(Integer)_jobParameterCollection.getParameterValue(KEY_BLEACH_RADIUS);
	}
	
	private boolean getSelectionMadeFromDialog(ImageWindow imageWindow,int _maximumDelayTimeMillis)throws Exception {
		final JOptionPane msg = new JOptionPane("Confirm with OK when regions are selected or press Cancel to skip all regions", JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
	    final JDialog dlg = msg.createDialog("Select region");
	    
	    dlg.setLocation(100, 100);
        dlg.setModal(false);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.addComponentListener(new ComponentAdapter() {
	            
    		@Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                final Timer t = new Timer(_maximumDelayTimeMillis,new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dlg.setVisible(false);
                    }
                });
                t.start();
            }
        });
        dlg.setVisible(true);
        
        while(dlg.isVisible())
        	TimeUnit.MILLISECONDS.sleep(50);
        Object dialogValue=msg.getValue();
        
        if(dialogValue.equals(JOptionPane.OK_OPTION))
        	return true;
        if(dialogValue.equals(JOptionPane.CANCEL_OPTION))
        	return false;
        
		return useRoisTimeEnd;
	}
	
	public static void main(String[] args)throws Exception{
		// start ImageJ
		new ImageJ();
		
		final String tblAPth="X:\\group\\ALMFstuff\\Aliaksandr\\User_data\\Matteo_Rauzi_ZenBlack_Automation\\fake_sata_to_optimise\\summary_tst1.txt";
		final String imgFnm="tst2_DE_1_W0001_P0002_T0001.lsm";
		Job_GetMultipleManualBleachRois testJob=new Job_GetMultipleManualBleachRois();
		testJob.initialise(null, INPUT_IMG_TAG, false);
		testJob.testJobMicTable(imgFnm, INPUT_IMG_TAG, new File(tblAPth));
	}

}
