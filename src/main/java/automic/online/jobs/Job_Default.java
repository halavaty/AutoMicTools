package automic.online.jobs;
import java.io.File;
import java.util.Date;
import java.util.Map;


import automic.online.microscope.MicroscopeCommanderInterface;
import automic.online.microscope.ZeissKeys;
import automic.parameters.ParameterCollection;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.table.TableSettings;
import automic.utils.DebugVisualiser;
import automic.utils.DebugVisualiserSettings;
import automic.utils.FileUtils;
import automic.utils.Utils;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.roi.ROIManipulator3D;
import automic.utils.roi.RoiSelector3D;

import ij.ImagePlus;
import ij.gui.ImageWindow;
import ij.gui.Overlay;
import ij.gui.Roi;
import net.imagej.ImageJ;


/**
 * Describes default behaviour of online image analysis Job
 * All Other Jobs must be extensions of this class redefining methods if required
 * @author Aliaksandr Halavatyi
 *
 */
public class Job_Default {
	
	
	public static ImageJ ij2;
	public static String watchTablePath;
	
	protected File newImgFile;
	
//	public static boolean saveOvl;
	DebugVisualiser DebugVis=null;
	
	private boolean processingSuccess;	//flag that determines whether last processing round was successful
	
	protected boolean Debug;
	protected TableModel currentTable;
	protected TableProcessor currentTableProcessor;
	protected int curDInd;
	protected String expTag="";
	
	protected Overlay ovl;
	
	protected String imgColumnNm;	//tag of the file columns in the output table where triggering image is stored
	protected String fileTag;		//unique name of the triggering tag
	protected boolean visOnline;	//flag telling whether visualisation should run online
	protected MicroscopeCommanderInterface microscopeCommander;
	
	
	private Map<String,Object> sharData;

	
	/**
	 * sets protocol, specific settings for this job
	 * avoid using, should override method with the Microscope Commander as a parameter instead
	 * @param _fTag			file tag of new file identified
	 * @param _colNm		column Tag of new File identified
	 * @param _visOnline	whether visualisation procedure to be called online
	 */
	@Deprecated
	public void initialise(String _fTag,String _colNm, boolean _visOnline){
		this.initialise(_fTag, _colNm, _visOnline, null);
	}
	
	@Deprecated
	public void initialise(String _fTag,String _colNm, boolean _visOnline, MicroscopeCommanderInterface _microscopeCommander){
		this.fileTag=_fTag;
		this.imgColumnNm=_colNm;
		this.visOnline=_visOnline;
		this.microscopeCommander=_microscopeCommander;
	}

	
	public void initialise(String _fTag,String _colNm, boolean _visOnline, MicroscopeCommanderInterface _microscopeCommander, TableModel _tableModel, TableProcessor _tableProcessor){
		this.fileTag=_fTag;
		this.imgColumnNm=_colNm;
		this.visOnline=_visOnline;
		this.microscopeCommander=_microscopeCommander;
		this.currentTable=_tableModel;
		this.currentTableProcessor=_tableProcessor;
		
		this.currentTable.addFileColumns(imgColumnNm, "IMG");
		this.addTableColumns();
	}
	
	public void initialise(String _fTag,String _colNm, String _expTag, boolean _visOnline, MicroscopeCommanderInterface _microscopeCommander, TableModel _tableModel, TableProcessor _tableProcessor){
		this.fileTag=_fTag;
		this.imgColumnNm=_colNm;
		this.expTag=_expTag;
		this.visOnline=_visOnline;
		this.microscopeCommander=_microscopeCommander;
		this.currentTable=_tableModel;
		this.currentTableProcessor=_tableProcessor;
		
		this.currentTable.addFileColumns(imgColumnNm, "IMG");
		this.addTableColumns();
	}
	
	protected void addTableColumns() {
		
	}

//	public void initialise(String _fTag,String _colNm, boolean _visOnline) {
//		throw new  RuntimeException("to be implemented for offline testing");
//	}
	
	public int getCurDInd() {
		return curDInd;
	}
	
	public void setCurDInd(int _curDInd) {
		curDInd=_curDInd;
	}
	
	public MicroscopeCommanderInterface getMicroscopeCommander() {
		return microscopeCommander;
	}
	
	
	public String getFileTag(){
		return fileTag;
	}
	
	public void setFileTag(String _fileTag) {
		fileTag=_fileTag;
	}
	
	public TableModel getCurrentTable() {
		return currentTable;
	}
	
	public File getNewImgFile() {
		return newImgFile;
	}
	
	public String getColTag(){
		return imgColumnNm;
	}
	
	public String getImgColumnNm(){
		return imgColumnNm;
	}
	
	public void setImgColumnNm(String _imgColumnNm){
		imgColumnNm=_imgColumnNm;
	}

	
	public boolean getVisFlag(){
		return visOnline;
	}

	public boolean getVisOnline(){
		return visOnline;
	}
	
	public void setVisOnline(boolean _visOnline){
		visOnline=_visOnline;
	}

	
	protected Overlay getOverlay(){
		return ovl;
	}
	
	public boolean goodForFnm(String _fnm){
		return _fnm.indexOf(fileTag)>=0;
	}
	
	public Map<String,Object> getSharedData(){
		return sharData;
	}
	
	public void setSharedData(Map<String,Object> _sharedDt){
		sharData=_sharedDt;
	}
	
	protected Object getSharedValue(String _key){
		return sharData.get(_key);
	}
	
	protected void setSharedValue(String _key, Object _val){
		sharData.put(_key, _val);
	}
	
	protected void clearSharedData(){
		sharData.clear();
	}
	
	public ParameterCollection createJobParameters(){
		return new ParameterCollection();
	}

	public void parseInputParameterValues(ParameterCollection _jobParameterCollection){
		
	}
	
	/**
	 * runs image processing
	 */
	@Deprecated
	public void doJob(int _dind, TableModel _tbl,TableProcessor _tProcessor, File _newImgFile,/* Map<String,Object> _shapedDt,*/ boolean _svOutput)throws Exception{
		if(Debug)
			DebugVis=new DebugVisualiser(this.getDebugVisualiserSettings());
		
		this.cleanIterOutput();
		currentTable=_tbl;
		currentTableProcessor=_tProcessor;
		curDInd=_dind;
		//newImgTag=_newImgTag;
		if(_newImgFile==null)//get file from table
			newImgFile=_tbl.getFile(_dind, imgColumnNm, "IMG");
		else
			newImgFile=_newImgFile;
		
		if(!Debug)
			this.preProcessOnline();
		else
			this.preProcessOffline();
//		this.getTableInput(_dind);
		this.processingSuccess=this.runProcessing();

		if(visOnline||Debug)
			ovl=this.createOverlay();
		if (Debug){
			this.postProcessOffline();
//			this.visualise(10, 10);
		}else{
			if(processingSuccess){
				this.postProcessSuccess();
			}else{
				this.postProcessFail();
			}
		}
	}
	
	public void doJob(int _dind, File _newImgFile,/* Map<String,Object> _shapedDt,*/ boolean _svOutput)throws Exception{
		if(Debug)
			DebugVis=new DebugVisualiser(this.getDebugVisualiserSettings());
		
		this.cleanIterOutput();
		curDInd=_dind;
		//newImgTag=_newImgTag;
		if(_newImgFile==null)//get file from table
			newImgFile=currentTable.getFile(_dind, imgColumnNm, "IMG");
		else
			newImgFile=_newImgFile;
		
		if(!Debug)
			this.preProcessOnline();
		else
			this.preProcessOffline();
//		this.getTableInput(_dind);
		this.processingSuccess=this.runProcessing();

		if(visOnline||Debug)
			ovl=this.createOverlay();
		if (Debug){
			this.postProcessOffline();
//			this.visualise(10, 10);
		}else{
			if(processingSuccess){
				this.postProcessSuccess();
			}else{
				this.postProcessFail();
			}
		}
	}
	
	
	/**
	 * clean output data of the previous iteration so that it does not influence current processing
	 */
	protected void cleanIterOutput(){
		
	}
	
	//nothing to be done on the images by default
	/**
	 * 
	 * @return true if image analysis was successful and false othervise
	 * @throws Exception
	 */
	protected boolean runProcessing()throws Exception{return true;}
	
	
	protected Overlay createOverlay()throws Exception{
		return null;
	}
	
	
	/**
	 * visualises results of image analysis. Default behaviour - open and show newly arrived file.
	 */
	public void visualise(int _xvis, int _yvis){
		//no default functionality because processed image is not seen here
		
		//ImageWindow.setNextLocation(_xvis, _yvis);
//		if (newImgFile!=null){
//			try{
//				ImagePlus img=LSMprocessor.OpenLSM(newImgFile);
//				if (ovl!=null)
//					img.setOverlay(ovl);
//				img.show();
//			}catch (Exception ex){
//				Utils.generErrMsg("Can not run visualisation", ex);
//			}
//		}
	}
	
	protected void visualiseImg(ImagePlus _img,Overlay _ovl,int _xvis, int _yvis){
		if (_img==null){
			Utils.generateErrorMessage("Unsuported operation", "Can not visualise nullimage object");
			return;
		}
		ImageWindow.setNextLocation(_xvis, _yvis);
		_img.setRoi((Roi)null);
		_img.setOverlay(_ovl.duplicate());
		_img.show();
	}
	
	public boolean getProcessingSuccess(){
		return this.processingSuccess;
	}
	

	/**
	 * Post processing activities in case image was successful, e.g. found objects for further image analysis jobs
	 * 
	 */
	protected void postProcessSuccess()throws Exception{}
	
	/**
	 * Post processing activities in case image analysis failed to find objects of interest.
	 * Default behaviour - submit "nothing" command
	 */
	//should omit the default behaviour. Each job has to define this method
	@Deprecated
	protected void postProcessFail()throws Exception{
		ZeissKeys.submitCommandsToMicroscope("nothing",null,null,null,null,null,null,null, "Suitable object is not found");
		this.recordSummaryDataset(false);
		
	}
	
	protected void postProcessOffline()throws Exception{
		
	}
	
	

	public void testImageFile(File _newImageFile)throws Exception{
		boolean tempDebug=Debug;
		Debug=true;
		
		this.doJob(-1, null,null, _newImageFile, false);
		
		Debug=tempDebug;
	}
	
	/**
	 * test job on the given DataSet in the table
	 * @param _dind
	 * @param _tbl
	 */
	public void testJobMicTable(int _dind, TableModel _tbl)throws Exception{
		boolean tempDebug=Debug;
		Debug=true;
//		saveOvl=false;

		//in test case do not clean table and do not write output
		
		this.doJob(_dind, _tbl,new TableProcessor(_tbl), null, false);
		
		Debug=tempDebug;
	}
	
	/**
	 * this function cleans outdated information in the record.
	 * JobDistributor runs it before adding current file path
	 * By default does nothing. Override only when required
	 */
	protected void preProcessOnline()throws Exception{
		currentTable.setFileAbsolutePath(newImgFile, curDInd, imgColumnNm, "IMG");
	}
	
	protected void preProcessOffline()throws Exception{
		
	}
	
	public void testJobMicTable(int _dind, String _tblPth, String _tblFnm)throws Exception{
		ParameterCollection defaultCollection=this.createJobParameters();
		defaultCollection.setUndefinedValuesFromDefaults();
		this.parseInputParameterValues(defaultCollection);
		
		TableModel tbl=new TableModel(new File(_tblPth,_tblFnm));
		this.testJobMicTable(_dind, tbl);
	}

	public void testJobMicTable(String _imgNm, String _imgTag, TableModel _tbl)throws Exception{
		ParameterCollection defaultCollection=this.createJobParameters();
		defaultCollection.setUndefinedValuesFromDefaults();
		this.parseInputParameterValues(defaultCollection);
		
		final int dind=_tbl.getRowIndexByFileName(_imgNm, _imgTag, "IMG");
		this.testJobMicTable(dind, _tbl);
	}
	
	public void testJobMicTable(String _imgNm, String _imgTag, File _tblFl)throws Exception{
		ParameterCollection defaultCollection=this.createJobParameters();
		defaultCollection.setUndefinedValuesFromDefaults();
		this.parseInputParameterValues(defaultCollection);

		
		TableModel tbl=new TableModel(_tblFl);
		final int dind=tbl.getRowIndexByFileName(_imgNm, _imgTag, "IMG");
		this.testJobMicTable(dind, tbl);
		
	}
	
	protected DebugVisualiserSettings getDebugVisualiserSettings(){
		 return new DebugVisualiserSettings(3, 10,10,2);
	}
	
	
	protected void showDebug(ImagePlus _img, String _title, boolean _imageCopy)throws Exception{
		if (!Debug) return;
		//this.visualise(0,0);
		//if (DebugVis==null) DebugVis=new DebugVisualiser(_zoomIterations,_firstX,_firstY,_numberOfWindowsPerColumn);
		DebugVis.addImage(_img, _title,_imageCopy);
	}
	
	protected void showDebug(ImagePlus _img, String _title, boolean _imageCopy , Overlay _ovl)throws Exception{
		if (!Debug) return;
		DebugVis.addImage(_img, _title,_imageCopy,_ovl);
	}

	protected void showDebug(ImagePlus _img, String _title, boolean _imageCopy , RoiSelector3D _selector)throws Exception{
		if (!Debug) return;
		DebugVis.addImage(_img, _title,_imageCopy,ROIManipulator3D.selectorToOverlay(_selector));
	}

	
	public static void saveRoisForImage(File _imgFl,Roi[] _rs)throws Exception{
		ROIManipulator2D.saveRoisToFile(_imgFl.getParent(), FileUtils.cutExtension(_imgFl.getName()), _rs);
	}

	public static void saveRoiForImage(File _imgFl,Roi _r)throws Exception{
		Roi[] rs=new Roi[1];
		rs[0]=_r;
		ROIManipulator2D.saveRoisToFile(_imgFl.getParent(), FileUtils.cutExtension(_imgFl.getName()), rs);
	}

	public static void save3DRoisForImage(File _imgFl,RoiSelector3D _rSel)throws Exception{
		_rSel.saveWithTags(_imgFl.getParent(),FileUtils.cutExtension(_imgFl.getName()));
	}
	
	public static Roi[] getRoisForImage(File _imgFl)throws Exception{
		File roiFl=new File(_imgFl.getParent(),FileUtils.changeExtension(_imgFl.getName(),ROIManipulator2D.ext));
		return (roiFl.exists())?ROIManipulator2D.flsToRois(roiFl):null;
	}
	
	@Deprecated	
	protected void recordSummaryDataset(boolean _succ)throws Exception{
		String Exper_nm=(String)getSharedValue("Experiment Name");// newImgFile.getName();
		currentTable.setStringValue(TableSettings.dateTimeFormat.format(new Date())/*new Date().toString()*/, 0, "Date.Time");
		//outTbl.setStringValue(new Date().toString(), "Date.Time", 0);
		
		currentTable.setBooleanValue(_succ, 0, "Success");//.setSuccess(0, true);//rec.addRecordToFl(true);
		currentTable.writeRecordOnline("summary_"+Exper_nm+".txt", 0);
	}

	protected void recordSummaryDataset() throws Exception{
		String folderToSaveTable=microscopeCommander.getSummarySaveFolder(watchTablePath, this.newImgFile.getAbsolutePath());
		String tableFileName=microscopeCommander.getSummaryTableName(watchTablePath, this.newImgFile.getAbsolutePath());
		
		recordSummaryDataset(folderToSaveTable, tableFileName);
		
	}
	
	protected void recordSummaryDataset(String _saveFolder,String _tableFileName)throws Exception{
		currentTable.setRootPathDeep(_saveFolder);
		currentTable.writeRecordOnline(_tableFileName, 0);
	}

	
	public String getJobName(){
		return this.getClass().getSimpleName();
	}
}


