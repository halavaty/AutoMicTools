package automic.online.jobs;

import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.gui.ParameterGuiManager;
import automic.table.TableModel;
import fiji.util.gui.GenericDialogPlus;
import ij.IJ;
import ij.Prefs;
import ij.gui.WaitForUserDialog;

public class TestJobGui {
	private static final String inputPrefsTag="automic.online.jobs.TestJobGui.tablePath";
	private static final String datasetPrefsTag="automic.online.jobs.TestJobGui.datasetIndex";

	
	
	Class<?> jobClass;
	Job_Default job;
	String tablePath;
	TableModel dataTable;
	String imageTag;
	int rowIndex;
	
	public TestJobGui(Class<?> _jobClass,String _imageTag){
		jobClass=_jobClass;
		imageTag=_imageTag;
		
		
	}
	
	public void initialiseValues(int _rowIndex, String _tablePath)throws Exception{
		rowIndex=_rowIndex;
		tablePath=_tablePath;
		dataTable=new TableModel(new File(tablePath));
	}

	public void initialiseGui()throws Exception{
		GenericDialogPlus gd_start=new GenericDialogPlus("Select dataset for testing job: "+jobClass.getSimpleName());
		gd_start.addFileField("Experiment summary path", Prefs.get(inputPrefsTag, ""), 30);
		gd_start.addNumericField("Dataset index", Prefs.getInt(datasetPrefsTag, 0)+1, 0);
		
		gd_start.showDialog();
		if (gd_start.wasCanceled()){
			IJ.log("Test is aborted by user.");
			return;
		}
		
		tablePath=gd_start.getNextString();
		dataTable=new TableModel(new File(tablePath));
		rowIndex=(int)gd_start.getNextNumber()-1;
		

		Prefs.set(inputPrefsTag, tablePath);
		Prefs.set(datasetPrefsTag, rowIndex);
		
	}
	
	public void runTest()throws Exception{
		job=(Job_Default)jobClass.newInstance();
		
		ParameterCollection jobParameters=job.createJobParameters();
		jobParameters.setUndefinedValuesFromDefaults();
		ParameterGuiManager pgManager=new ParameterGuiManager(jobParameters);
		try{
			pgManager.refineParametersViaDialog("Parameters for job: "+jobClass.getSimpleName());
		}catch(Exception _ex){
			new WaitForUserDialog("error in parameter values");
		}
		job.parseInputParameterValues(pgManager.getParameterCollection());
		
		
		//job.initialise(null, imageTag, true);
		File imageFile=dataTable.getFile(rowIndex, imageTag, "IMG");
		
		
		
		job.testImageFile(imageFile);
		job.visualise(100, 100);
	}
}
