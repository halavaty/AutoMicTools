package automic.online.microscope;

import java.io.File;
import java.io.Writer;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.FileWriter;

//import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import java.awt.Rectangle;

import automic.geom.Point3D;
import automic.parameters.ParameterCollection;
import automic.utils.FileUtils;
import ij.IJ;
import ij.gui.Roi;
import java.awt.Polygon;

public class ZenBlueCommander implements MicroscopeCommanderInterface{
	public static String FEEDBACK_POSTFIX="_feedback.json";
	
	//public static final String JOB_KEY="Job";
	public static final String X_KEY="X";
	public static final String Y_KEY="Y";
	public static final String Z_KEY="Z";
	public static final String POSITION_NAME_KEY="POSITION_NAME";
	public static final String SCENE_KEY="SCENE";
	public static final String MICJOB_KEY="MIC_JOB";
	
	
	
	public static final String WIDTH_KEY="WIDTH";
	public static final String HEIGHT_KEY="HEIGHT";
	
	
	public static final String XBLEACH_KEY="X_BLEACH";
	public static final String YBLEACH_KEY="Y_BLEACH";
	public static final String RADIUS_KEY="RADIUS";
	
	public static final String POSITIONS_KEY="POSITIONS";
	public static final String ROIS_KEY="ROIS";
	
	public static final String ROITYPE_KEY="TYPE";
	public static final String ROIPOSITION_KEY="";
	
	public static final String NO_JOB_NAME="None";
	

	@Override
	public void parseSettings(ParameterCollection _microscopeSettings) {
		
	}
	
	@Override
	public void connect() {
		
	}
	
	@Override
	public void skipJobsInCurrentPosition() {
		IJ.log("Calling Skip Job in a current position");
		//throw new RuntimeException("ZenBlueCommander.skipJobsInCurrentPosition(): method is not yet implemented");
	}
	
	@Override
	public void stopCommander() {}
	
	
	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions,
			Roi[] _bleachRois)throws Exception {

		File decisionFile=getDecisionFile(_sourceImageFile);
		JsonObject jObject=new JsonObject();

		JsonObject jpositions=new JsonObject();
		JsonObject jrois=new JsonObject();

		
		
		if (_positions!=null) {
			int nPositions=_positions.length;
			for (int iPosition=0;iPosition<nPositions;iPosition++) {
				String positionKey=String.format("Pos_%03d", iPosition+1,"_");
				JsonObject currentPosition=new JsonObject();
				addValueIfNotNull(_positions[iPosition].getX(), X_KEY, currentPosition);
				addValueIfNotNull(_positions[iPosition].getY(), Y_KEY, currentPosition);
				addValueIfNotNull(_positions[iPosition].getZ(), Z_KEY, currentPosition);
				addValueIfNotNull(_positions[iPosition].getScene(), SCENE_KEY, currentPosition);
				addValueIfNotNull(_positions[iPosition].getMicroscopeJob(), MICJOB_KEY, currentPosition);
				addValueIfNotNull(_positions[iPosition].getName(), POSITION_NAME_KEY, currentPosition);
				
				jpositions.add(positionKey, currentPosition);
			}
		}

		if (_bleachRois!=null) {
			for (Roi thisRoi:_bleachRois){
				String roiKey=thisRoi.getName();
				if(roiKey==null) {
					int counter=0;
					do {
						counter++;
						roiKey=String.format("R%04d", counter);
					} while (jrois.names().contains(roiKey));
				}

				JsonObject currentJRoi=new JsonObject();
				int roiType=thisRoi.getType();
				switch (roiType){
					case Roi.OVAL:
						
						currentJRoi.add(ROITYPE_KEY, "OVAL");
						Rectangle rect=thisRoi.getBounds();
						currentJRoi.add(X_KEY, rect.x);
						currentJRoi.add(Y_KEY, rect.y);
						currentJRoi.add(WIDTH_KEY, rect.width);
						currentJRoi.add(HEIGHT_KEY, rect.height);
						
						break;
					case Roi.RECTANGLE:
						currentJRoi.add(ROITYPE_KEY, "RECTANGLE");
						rect=thisRoi.getBounds();
						currentJRoi.add(X_KEY, rect.x);
						currentJRoi.add(Y_KEY, rect.y);
						currentJRoi.add(WIDTH_KEY, rect.width);
						currentJRoi.add(HEIGHT_KEY, rect.height);

						break;
					case Roi.POLYGON:case Roi.FREEROI: case Roi.TRACED_ROI:
						currentJRoi.add(ROITYPE_KEY, "POLYGON");
						Polygon poly=thisRoi.getPolygon();
						JsonObject polygonJPositions=new JsonObject();
						for (int iPosition=0;iPosition<poly.npoints;iPosition++) {
							JsonObject thisJPosition=new JsonObject();
							thisJPosition.add(X_KEY, poly.xpoints[iPosition]);
							thisJPosition.add(Y_KEY, poly.ypoints[iPosition]);
							
							polygonJPositions.add(String.format("P_%03d", iPosition), thisJPosition);
						}
						
						currentJRoi.add(POSITIONS_KEY, polygonJPositions);
						break;
					default:
						throw new RuntimeException();
				}
					
				
				jrois.add(roiKey,currentJRoi);
			}
		}


		//jObject.add(JOB_KEY, _jobNameToPerform);
		jObject.add(POSITIONS_KEY, jpositions);
		jObject.add(ROIS_KEY, jrois);
	
	
		writeJsonToFile(jObject, decisionFile);
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D _position) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, new Point3D[] {_position},null);
	}

	
	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, _positions, null);
	}
	

	
	private static File getDecisionFile(File _sourceImageFile){
		String newFileName=FileUtils.cutExtension(_sourceImageFile.getName())+FEEDBACK_POSTFIX;
		return new File(_sourceImageFile.getParentFile(),newFileName);
	}
	
	private static void writeJsonToFile(JsonObject _jObject,File _outputFile)throws Exception {
		Writer jSonWriter=new FileWriter(_outputFile);
		_jObject.writeTo(jSonWriter);
		jSonWriter.close();
	}
	
	private static void addValueIfNotNull(Double _value,String _key, JsonObject _jObject) {
		if (_value==null)
			return;
		_jObject.add(_key, _value);
	}

	private static void addValueIfNotNull(String _value,String _key, JsonObject _jObject) {
		if (_value==null)
			return;
		_jObject.add(_key, _value);
	}
	
	
	@Override
	public void submitPositionsFromTable(String _tablePath, int _startIndex, int _endIndex) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public String getSummarySaveFolder(String _watchPath, String _imageFilePath) {
		//assuming that for each new experiment run the dedicated subfolder is created
		Path imagePath=Paths.get(_imageFilePath);
		Path watchPath=Paths.get(_watchPath);
		
		if (imagePath.getParent().equals(watchPath))
			return _watchPath;
		
		Path tempPath=imagePath;
		
		//watchPath
		while (!tempPath.getParent().equals(watchPath)) {
			tempPath=tempPath.getParent();
		}
		
		return tempPath.toString();
	}

	
}
