package automic.online.microscope;

import java.io.File;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import automic.geom.Point3D;
import automic.parameters.ParameterCollection;
import ij.gui.Roi;

public class TestCommander implements MicroscopeCommanderInterface {
	Logger testLogger;
	
	
	@Override
	public void parseSettings(ParameterCollection _microscopeSettings) {
		
	}
	
	@Override
	public void connect() {
		testLogger=new TextWindowLogger("AutoMic pipeline test");
	}
	
	@Override
	public void skipJobsInCurrentPosition() {
		testLogger.sendInfoMessage("Received command skip Jobs in current position");
	}
	
	@Override
	public void stopCommander() {}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions,Roi[] _bleachRois)throws Exception{
		testLogger.sendInfoMessage("Received command for imaging job for positions"+_positions.length+"positions:");
		for (Point3D p:_positions) {
			testLogger.sendInfoMessage(String.format("X= %f; Y= %f; Z= %f;", p.getX(),p.getY(),p.getZ()));
		}
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D _position) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, new Point3D[] {_position},null);
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, _positions, null);
	}

	@Override
	public void submitPositionsFromTable(String _tablePath, int _startIndex, int _endIndex) {
		// TODO Auto-generated method stub
		
	}
	

}
