package automic.online.microscope;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import automic.geom.Point3D;
import automic.parameters.ParameterCollection;
import automic.table.TableModel;
import ij.gui.Roi;

public class ExternalScriptCommander implements MicroscopeCommanderInterface {
	public static final String KEY_TABLE_COMMAND_NAME="Table command name";
	String externalCommandName;
	
	
	@Override
	public void parseSettings(ParameterCollection _microscopeSettings) {
		externalCommandName= (String)_microscopeSettings.getParameterValue(KEY_TABLE_COMMAND_NAME);
	}
	
	@Override
	public void connect() {
		
	}
	
	@Override
	public void skipJobsInCurrentPosition() {
		
	}
	
	@Override
	public void stopCommander() {}
	
	@Override
	public void submitPositionsFromTable(String _tablePath, int _startIndex, int _endIndex){
		//IJ.runMacroFile(externalCommandName, String.format("tablePath=%s xTag=%s, yTag=%s startIndex=%d endIndex= %d", _tablePath,_xTag,_yTag,_startIndex,_endIndex));
		return;
		
	}
	
	public static void submitPositions_temp(File _tableFile,int _startIndex, int _endIndex) {
		
		try {
			TableModel table=new TableModel(_tableFile);
			File fl=new File("D:/IMAGING-DATA/JOBS/Job.job");
			fl.createNewFile();
			BufferedWriter bufsumfl = new BufferedWriter(new FileWriter(fl));
			
			bufsumfl.write("SetObjective(2)");
			for (int lineIndex=_startIndex; lineIndex<=_endIndex; lineIndex++) {
				bufsumfl.newLine();
				bufsumfl.write("SetWellNo(" + table.getStringValue(lineIndex, "Well") + ")");
				bufsumfl.newLine();
				bufsumfl.write("GotoXY(" + table.getNumericValue(lineIndex, "X-mic") + "," + table.getNumericValue(lineIndex, "Y-mic") + ",'Abs')");
				bufsumfl.newLine();
				bufsumfl.write("GotoZ(18901.7,'Abs')");
				bufsumfl.newLine();
				bufsumfl.write("SetLight(6,20,20,0.000)"); 
				bufsumfl.newLine();
				bufsumfl.write("Acquire(5,20,2,'D:/IMAGING-DATA/IMAGES/SCRIPTS','True')"); 
				bufsumfl.newLine();
				bufsumfl.write("SetLight(2,2,100,0.000)");
				bufsumfl.newLine();
				bufsumfl.write("Acquire(5,20,2,'D:/IMAGING-DATA/IMAGES/SCRIPTS','True')"); 
			}
			bufsumfl.newLine();
			bufsumfl.write("SetObjective(1)");
		
			bufsumfl.close();
		
		}catch(Exception ex){
			throw new RuntimeException();
		}
		
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions,
			Roi[] _bleachRois) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D _position) throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
}
