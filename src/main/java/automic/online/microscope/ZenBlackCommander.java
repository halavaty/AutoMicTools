package automic.online.microscope;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import automic.geom.Point3D;
import automic.parameters.ParameterCollection;
import ij.gui.Roi;

public class ZenBlackCommander implements MicroscopeCommanderInterface {
	@Override
	public void parseSettings(ParameterCollection _microscopeSettings) {
		
	}
	
	@Override
	public void connect() {
		
	}
	
	@Override
	public void skipJobsInCurrentPosition() {
		ZeissKeys.submitCommandsToMicroscope("nothing", "Exception raised in online image analysis",null,null,null,null,null,null,null);
	}
	
	@Override
	public void stopCommander() {}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions,Roi[] _bleachRois)throws Exception{
		String[] positions = generatePositionStringForRegistry(_positions);
		ZeissKeys.submitCommandsToMicroscope(_jobNameToPerform, positions[0], positions[1], positions[2],"","","","","");
		
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D _position) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, new Point3D[] {_position},null);
	}

	@Override
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions) 
	throws Exception{
		submitJobPixels(_sourceImageFile, _jobNameToPerform, _positions, null);
	}

	@Override
	public void submitPositionsFromTable(String _tablePath, int _startIndex, int _endIndex) {
		// TODO Auto-generated method stub
		
	}
	
	private String convertValueToString(Double _value){
		if (_value==null)
			return "0";
		return String.valueOf(_value);
	}
	
	private String[] generatePositionStringForRegistry(Point3D[] _points) {
		String posStringX ="";
		String posStringY ="";
		String posStringZ ="";
		
		String posStringX_tmp ="";
		String posStringY_tmp ="";
		String posStringZ_tmp ="";
		for (int i = 0; i < _points.length; i++) {
			
			if (i==0) {
				posStringX = _points[i].getX()==null?"0":String.valueOf(_points[i].getX());
				posStringY = _points[i].getY()==null?"0":String.valueOf(_points[i].getY());
				posStringZ = _points[i].getZ()==null?"0":String.valueOf(_points[i].getZ());
			}
			else {
				
				posStringX_tmp = _points[i].getX()==null?"0":String.valueOf(_points[i].getX());
				posStringY_tmp = _points[i].getY()==null?"0":String.valueOf(_points[i].getY());
				posStringZ_tmp = _points[i].getZ()==null?"0":String.valueOf(_points[i].getZ());
				
				posStringX = posStringX + ";" + posStringX_tmp;
				posStringY = posStringY + ";" + posStringY_tmp;
				posStringZ = posStringZ + ";" + posStringZ_tmp;
			}
		}
		String[] posString = new String[] {posStringX, posStringY, posStringZ};
		return posString;
	}
	
	@Override
	public String getSummaryTableName(String _watchPath, String _imageFilePath) {
		String fileNameRegex="(.*)_[^_]*_[^_]*_W([0-9]{4})_P([0-9]{4})_{1,2}T([0-9]{4})\\.czi";
		Pattern fileNamePattern=Pattern.compile(fileNameRegex);
		Matcher matcher=fileNamePattern.matcher(new File(_imageFilePath).getName());
		String experimentTag="";
		if (matcher.find()) {
			experimentTag=matcher.group(1);
		}else {
			return "summary_.txt";
		}

		if (experimentTag!=null) {
			return (String.format("summary_%s.txt", experimentTag));
		}else {
			return "summary_.txt";
		}
	}

	

}
