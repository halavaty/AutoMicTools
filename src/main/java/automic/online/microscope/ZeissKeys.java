package automic.online.microscope;

import automic.online.jobdistributors.WindowsRegistry;
import ij.IJ;

public final class ZeissKeys {
	    // Configurable parameters
		static final String regkey = "SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro";
	    //subkey_changed = "has_changed"
	    static final String subkey_codemic = "codeMic";
	    static final String subkey_codeoia = "codeOia";
	    static final String subkey_filename = "filepath";
	    static final String subkey_xpos = "X";
	    static final String subkey_ypos = "Y";
	    static final String subkey_zpos = "Z";
	    static final String subkey_rotpos = "rotation";
	    static final String subkey_fcsx = "fcsX";
	    static final String subkey_fcsy = "fcsY";
	    static final String subkey_fcsz = "fcsZ";
	    static final String subkey_roix = "roiX";
	    static final String subkey_roiy = "roiY";
	    static final String subkey_roitype = "roiType";
	    static final String subkey_roiaim = "roiAim";
	    static final String subkey_errormsg = "errorMsg";
	    
		public static void submitCommandsToMicroscope(String _codeM, String _X,String _Y,String _Z,String _roiT,String _roiA,String _roiX,String _roiY,String _errMsg){	
			if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
				if(_X!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_xpos, _X );
				if(_Y!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_ypos, _Y);
				if(_Z!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_zpos, _Z);

				if(_roiT!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roitype,_roiT);
				if(_roiA!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roiaim,_roiA);

				if(_roiX!=null)	WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roix,_roiX);
				if(_roiY!=null)	WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roiy,_roiY);

				if(_errMsg!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_errormsg, _errMsg);
				if(_codeM!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_codemic, _codeM);
			}
			
			IJ.log(String.format("Sending positions to the microscope %s: %s;%s: %s;%s: %s.", subkey_xpos,_X,subkey_ypos,_Y,subkey_zpos,_Z));
			
		}
		
		public static void submitCommandsToMicroscope(String _codeM, String _X,String _Y,String _Z,String _rotation,String _roiT,String _roiA,String _roiX,String _roiY,String _errMsg){	
			if (System.getProperty("os.name").toLowerCase().startsWith("win")) {
				if(_X!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_xpos, _X );
				if(_Y!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_ypos, _Y);
				if(_Z!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_zpos, _Z);
				if(_Z!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_rotpos, _rotation);
	
				if(_roiT!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roitype,_roiT);
				if(_roiA!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roiaim,_roiA);
	
				if(_roiX!=null)	WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roix,_roiX);
				if(_roiY!=null)	WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_roiy,_roiY);
	
				if(_errMsg!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_errormsg, _errMsg);

				if(_codeM!=null)WindowsRegistry.writeRegistry("HKCU\\SOFTWARE\\VB and VBA Program Settings\\OnlineImageAnalysis\\macro", subkey_codemic, _codeM);
			}
			
			IJ.log(String.format("Sending positions to the microscope %s: %s;%s: %s;%s: %s.", subkey_xpos,_X,subkey_ypos,_Y,subkey_zpos,_Z));
		}

}
