package automic.online.microscope;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.reflections.Reflections;

import automic.parameters.ParameterCollection;
import automic.utils.Utils;

public abstract class MicroscopeCommanderFactory {

	@Deprecated
	public static final String ZEISS_ZEN_BLACK="Zeiss_Zen_Black";
	@Deprecated
	public static final String ZEISS_ZEN_BLUE="ZenBlueCommander";

	
	public static Map<String,Class<? extends MicroscopeCommanderInterface>> identifyCommanders(){

		Map<String,Class<? extends MicroscopeCommanderInterface>> microscopeCommanderMap=new HashMap<>();
		
		Reflections reflections = new Reflections("automic.online.microscope");
		
		Set<Class<? extends MicroscopeCommanderInterface>> microscopeCommanderClasses =  reflections.getSubTypesOf(MicroscopeCommanderInterface.class);
		
		
		for (Class<? extends MicroscopeCommanderInterface> type : microscopeCommanderClasses) {
			microscopeCommanderMap.put(type.getSimpleName(), type);
		}
		
		return microscopeCommanderMap;
	}

	
	
	@Deprecated
	public static MicroscopeCommanderInterface createMicroscopeCommander(String _microscopeTypeTag, ParameterCollection _commanderParameters){
		switch (_microscopeTypeTag) {
			case ZEISS_ZEN_BLACK: return new ZenBlackCommander();
			case ZEISS_ZEN_BLUE: return new ZenBlueCommander();
			default: throw new RuntimeException(String.format("Microscope tag %s not defined", _microscopeTypeTag));
		}
	}
	
	@Deprecated
	public static MicroscopeCommanderInterface createMicroscopeCommander(Class<?> _microscopeCommanderClass, ParameterCollection _commanderParameters){
		MicroscopeCommanderInterface commander=null;

		if ((_microscopeCommanderClass==null)||(!MicroscopeCommanderInterface.class.isAssignableFrom(_microscopeCommanderClass)))
			Utils.generateErrorMessage("Can not create microscope commander", "Can not create microscope commander: "+_microscopeCommanderClass.getName());

		
		try{
			commander= (MicroscopeCommanderInterface) _microscopeCommanderClass.newInstance();
			commander.parseSettings(_commanderParameters);
		}catch(Exception ex) {
			throw new RuntimeException("Unable to ceate commander");
		}
		return commander;
	}

	public static MicroscopeCommanderInterface createMicroscopeCommander(Class<?> _microscopeCommanderClass){
		MicroscopeCommanderInterface commander=null;

		if ((_microscopeCommanderClass==null)||(!MicroscopeCommanderInterface.class.isAssignableFrom(_microscopeCommanderClass)))
			Utils.generateErrorMessage("Can not create microscope commander", "Can not create microscope commander: "+_microscopeCommanderClass.getName());

		
		try{
			commander= (MicroscopeCommanderInterface) _microscopeCommanderClass.newInstance();
		}catch(Exception ex) {
			throw new RuntimeException("Unable to ceate commander");
		}
		return commander;
	}
	
	
}
