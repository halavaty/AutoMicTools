package automic.online.microscope;

import java.io.File;


import automic.geom.Point3D;
import automic.parameters.ParameterCollection;
import ij.gui.Roi;

public interface MicroscopeCommanderInterface {
	public void parseSettings(ParameterCollection _microscopeSettings);
	
	public void connect();
	
	
	public void skipJobsInCurrentPosition();
	
	public void stopCommander();
	
	
	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions,Roi[] _bleachRois)throws Exception;

	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D _position) throws Exception;

	public void submitJobPixels(File _sourceImageFile, String _jobNameToPerform, Point3D[] _positions) throws Exception;

	public void submitPositionsFromTable(String _tablePath, int _startIndex, int _endIndex);
	
	default public String getSummarySaveFolder(String _watchPath, String _imageFilePath) {
		return _watchPath;
	}

	default public String getSummaryTableName(String _watchPath, String _imageFilePath) {
		return "summary_.txt";
	}
	
	default public ParameterCollection createSettings() {
		return new ParameterCollection();
	}
	
}
