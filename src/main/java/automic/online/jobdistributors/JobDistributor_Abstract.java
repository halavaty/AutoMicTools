package automic.online.jobdistributors;

import ij.IJ;
import ij.plugin.PlugIn;
import ij.gui.GenericDialog;
import ij.gui.WaitForUserDialog;
import ij.measure.ResultsTable;

import java.awt.Toolkit;
import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.scijava.command.Command;

//import javax.xml.parsers.DocumentBuilder;
//import javax.xml.parsers.DocumentBuilderFactory;

import org.scijava.java3d.loaders.IncorrectFormatException;
//import org.w3c.dom.Document;
//import org.w3c.dom.Element;
import org.w3c.dom.Node;

import automic.geom.Point3D;
import automic.online.jobdistributors.xml.JobDistributorXMLWriter;
import automic.online.jobs.Job_Default;
import automic.online.microscope.MicroscopeCommanderFactory;
import automic.online.microscope.MicroscopeCommanderInterface;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.parameters.gui.ParameterGuiManager;
import automic.parameters.gui.ParameterPrefsManager;
import automic.table.TableModel;
import automic.table.TableProcessor;
import automic.utils.Utils;
import embl.almf.RunJythonOnMonitoredFiles;
import de.embl.cba.utils.ij.Close_all_forced;


public abstract class JobDistributor_Abstract extends RunJythonOnMonitoredFiles implements PlugIn,Command{

	
	private static final String PREFS_PREFIX="Automic.JobDistributor";
	
	private static final String KEY_EXPERIMENT_PATH="Experiment folder";
	private static final String KEY_CHANGED_FILES="Analyse changed files";
	private static final String KEY_SHOW_DEMO="Show Demo";
	private static final String KEY_SAVE_PARAMETERS="Save Parameters";
	private static final String KEY_SHOW_CONFIGURATION="Show jobdistributor configuration";
	
	private static final String KEY_FILE_EXTENSION="Image File Extension";
	//private static final String KEY_FILL_JOB_PARAMETERS_FROM_FILE="Fill parameters from file";
	//private static final String KEY_INPUT_JOB_PARAMETERS="Input file with job paramters";
	private static final String KEY_MICROSCOPE_COMMANDER="Microscope commander";
	

	
	private Map<String,Class<? extends MicroscopeCommanderInterface>> microscopeCommanderMap;

	
	protected ParameterCollection distributorParameters;
	Node jobsNode; 
	public static int xvis=Toolkit.getDefaultToolkit().getScreenSize().width-600;
	public static int yvis=120;
	
	private TableModel outputTable=null;
	private TableProcessor outputTableProcessor=null;
	private int except_cnt=0;
	private ExceptionWriter ewr=null;

	private final Close_all_forced closer=new Close_all_forced();
	private String curImgFnm=null;
	private File imgFile=null;
	
	private List<Job_Default> JobList=null;
	protected Map<String,Object> SharedData=null;
	protected MicroscopeCommanderInterface microscopeCommander=null;
	
	public Job_Default getJobByFTag(String _fTag){
		for (Job_Default job:JobList){
			if (job.getFileTag().equals(_fTag))
				return job;
		}
		return null;
	}
	
	public Job_Default getJobForFnm(String _fnm){
		for (Job_Default job:JobList){
			if (job.goodForFnm(_fnm))
				return job;
		}
		return null;
	}
	
	protected abstract void fillJobList();
	
	
	protected TableModel constructTabModel(String _rpth) {
		TableModel newTable=new TableModel(_rpth);
		
		newTable.addRow(new Object[newTable.getColumnCount()]);
		
		return newTable;
	}
	
	
	
	protected void setDebugConfiguration() {}
	
	@Deprecated
	protected boolean 	showDialogInDebugRun() {
		return false;
	}

	@Deprecated
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog) {
		
	}
	
	@Deprecated
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog) {
		
	}

	
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		return new TableProcessor(_tModel);
	}
	
	public void setGeneralOptions(String _rootPath,boolean _showDemo, boolean _analyseChangedFiles){
		distributorParameters.setParameterValue(KEY_EXPERIMENT_PATH, _rootPath);
		distributorParameters.setParameterValue(KEY_CHANGED_FILES, _analyseChangedFiles);
		distributorParameters.setParameterValue(KEY_SHOW_DEMO, _showDemo);
	}
	
	@Override
	public void run(String arg) {
		
		try{

			/*
			if ((Boolean)distributorParameters.getParameterValue(KEY_FILL_JOB_PARAMETERS_FROM_FILE)) {
				DocumentBuilder documentBuilder;
				documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
				Document xmlDocument=documentBuilder.parse(distributorParameters.getParameterStringValue(KEY_INPUT_JOB_PARAMETERS));
				Element root = xmlDocument.getDocumentElement();
				
				
				jobsNode = root.getElementsByTagName("jobs").item(0);

			
			}
			*/
			
			if ((!arg.equals("Debug run"))||showDialogInDebugRun()){
				ParameterGuiManager startupDialog=new ParameterGuiManager(distributorParameters);
				if(!startupDialog.refineParametersViaDialog("JobDistributorParameters")){
					IJ.showMessage("Job distributor was aborted by user.");
					return;
				}
			}
			else{//debug without dialog in run
				this.setDebugConfiguration();
			}

			
			
			String experimentFolderPath=distributorParameters.getParameterStringValue(KEY_EXPERIMENT_PATH);
			//Start monitoring
			IJ.log("Start monitoring");
			
			this.setWatchpath(experimentFolderPath);
			microscopeCommander = createMicroscopeCommander();

			
			outputTable=this.constructTabModel(experimentFolderPath);
			outputTableProcessor=this.configureTableProcessor(outputTable);
			Job_Default.watchTablePath=experimentFolderPath;

			
			this.fillJobList();

			//outTbl.setRootPath(pth);
			
			if((Boolean)distributorParameters.getParameterValue(KEY_SAVE_PARAMETERS)) {
				Date date=new Date();
				DateFormat dFormat=new SimpleDateFormat("YYYYMMdd-HHmmss");
				String fileName=String.format("AutoMic_pareameters-%s.xml", dFormat.format(date));//"AutoMic_pareameters.xml";//
				new JobDistributorXMLWriter().writePipelineToFile(this, new File(experimentFolderPath,fileName));
			}
			if((Boolean)distributorParameters.getParameterValue(KEY_SHOW_CONFIGURATION)) {
				showJobDistributorConfiguration();
			}
			
			try{
				this.startFolderMonitoring();
			}catch (Exception e){
				Utils.generateErrorMessage("Unable to start monitoring", e);;
				return;
			}
			new WaitForUserDialog("File Monitor","Press OK to Stop Monitor").show();
			this.stopMonitor();
			microscopeCommander.stopCommander();
			IJ.log("Stop monitoring");
			IJ.log(this.except_cnt+" exceptions were raised");
		}catch (Exception e){
			Utils.generateErrorMessage("Exception raised when trying to monitor files.\nPlugin will stop", e);
			return;
		}
	}
	
	@Override
	public void run() {
		this.run("");
		
	}
	
	private void showJobDistributorConfiguration() {
		int nJobs=this.JobList.size();
		ResultsTable configurationTable=new ResultsTable(nJobs);

		for (int iJob=0;iJob<nJobs;iJob++) {
			Job_Default thisJob=JobList.get(iJob);
			
			configurationTable.setValue("AutoMic Job", iJob, thisJob.getJobName());
			configurationTable.setValue("Image File Tag", iJob, thisJob.getFileTag());
			configurationTable.setValue("AutoMic Table Tag", iJob, thisJob.getColTag());
			configurationTable.setValue("Visualise", iJob, String.format("%s", thisJob.getVisFlag()));
			
		}
		
		configurationTable.show("JobDistributor configuration table");		
	}
	
	private ParameterCollection createDistributorParameters() {
		ParameterCollection params=new ParameterCollection();
		
		params.addParameter(KEY_FILE_EXTENSION, "lsm", null, ParameterType.STRING_PARAMETER);
		params.addParameter(KEY_CHANGED_FILES, false, null, ParameterType.BOOL_PARAMETER);
		params.addParameter(KEY_SHOW_DEMO, true, null, ParameterType.BOOL_PARAMETER);
		params.addParameter(KEY_SAVE_PARAMETERS, true, null, ParameterType.BOOL_PARAMETER);
		params.addParameter(KEY_EXPERIMENT_PATH, "", null, ParameterType.FOLDERPATH_PARAMETER);
		//params.addParameter(KEY_FILL_JOB_PARAMETERS_FROM_FILE, false, null, ParameterType.BOOL_PARAMETER);
		//params.addParameter(KEY_INPUT_JOB_PARAMETERS, "", null, ParameterType.FILEPATH_PARAMETER);
		params.addParameter(KEY_SHOW_CONFIGURATION, false, null, ParameterType.BOOL_PARAMETER);
		
		Set<String> keySet=microscopeCommanderMap.keySet();
		String[] commanderNames=keySet.toArray(new String[keySet.size()]);
		params.addSelectionParameter(KEY_MICROSCOPE_COMMANDER, commanderNames[0], commanderNames[0], ParameterType.STRING_PARAMETER, commanderNames);
		
		
		
		return params;
	}
	
	

	public JobDistributor_Abstract(){
		super();
		
		
		microscopeCommanderMap=MicroscopeCommanderFactory.identifyCommanders();
		
		distributorParameters=createDistributorParameters();
		
		try{
			ParameterPrefsManager.setParametersFromFijiPrefs(distributorParameters, PREFS_PREFIX);
		}catch (Exception _ex){
			throw new RuntimeException("Can not read parameters from Prefs");
		}


		JobList=new ArrayList<Job_Default>();
		SharedData=new HashMap<String, Object>();


	}
	
	public JobDistributor_Abstract(String _microscopeTag,ParameterCollection _microscopeParameters){
		this();
	}	

	public JobDistributor_Abstract(MicroscopeCommanderInterface _microscopeCommander){
		this();
	}	
	
	
	public void addImageJob(Class<?> _JobClass,String _fileTag,String _colTag,boolean _visual)/*throws Exception*/{
		if (microscopeCommander==null)
	    	microscopeCommander = this.createMicroscopeCommander();

		
		Job_Default job;
		if ((_JobClass==null)||(!Job_Default.class.isAssignableFrom(_JobClass)))
			Utils.generateErrorMessage("Can not initialise Job", "Can not initialise Job: "+_JobClass.getName());
		else{
			try{
				job=(Job_Default)_JobClass.newInstance();    //  ();


				
				//
				ParameterCollection jobParameters=job.createJobParameters();
				int nParameters=jobParameters.getNumberOfParameters();
				jobParameters.setUndefinedValuesFromDefaults();
				if (nParameters>0) {
					ParameterGuiManager pgManager=new ParameterGuiManager(jobParameters);
					try{
						pgManager.refineParametersViaDialog("Parameters for"+_JobClass.getName());
					}catch(Exception _ex){
						new WaitForUserDialog("error in parameter values");
						throw new IncorrectFormatException();
					}
					
					job.parseInputParameterValues(pgManager.getParameterCollection());
				}
				//
				
				//job.initialise(_fileTag, _colTag, _visual, microscopeCommander);
				job.initialise(_fileTag, _colTag, _visual, microscopeCommander,outputTable,outputTableProcessor);
				JobList.add(job);
			}catch (Exception ex){
				Utils.generateErrorMessage("Can not initialise Job", "Can not initialise Job: "+_JobClass.getName());
			}
		}
	}
	
	public void addImageJob(Job_Default _job,String _fileTag,String _colTag,boolean _visual)/*throws Exception*/{
		if (microscopeCommander==null)
	    	microscopeCommander = this.createMicroscopeCommander();
			
		
		//_job.initialise(_fileTag, _colTag, _visual,microscopeCommander);
		_job.initialise(_fileTag, _colTag, _visual, microscopeCommander,outputTable,outputTableProcessor);
		JobList.add(_job);
	}
	
	//extended for handling of additional experiment tag expTag:
	public void addImageJob(Class<?> _JobClass, String _fileTag, String _colTag, String _expTag, boolean _visual)/*throws Exception*/{
		if (microscopeCommander==null)
	    	microscopeCommander = this.createMicroscopeCommander();
		
		Job_Default job;
		if ((_JobClass==null)||(!Job_Default.class.isAssignableFrom(_JobClass)))
			Utils.generateErrorMessage("Can not initialise Job", "Can not initialise Job: "+_JobClass.getName());
		else{
			try{
				job=(Job_Default)_JobClass.newInstance();    //  ();


				
				//
				ParameterCollection jobParameters=job.createJobParameters();
				int nParameters=jobParameters.getNumberOfParameters();
				jobParameters.setUndefinedValuesFromDefaults();
				if (nParameters>0) {
					ParameterGuiManager pgManager=new ParameterGuiManager(jobParameters);
					try{
						pgManager.refineParametersViaDialog("Parameters for"+_JobClass.getName());
					}catch(Exception _ex){
						new WaitForUserDialog("error in parameter values");
						throw new IncorrectFormatException();
					}
					
					job.parseInputParameterValues(pgManager.getParameterCollection());
				}
				//
				
				//job.initialise(_fileTag, _colTag, _visual, microscopeCommander);
				job.initialise(_fileTag, _colTag, _expTag, _visual, microscopeCommander,outputTable,outputTableProcessor);
				JobList.add(job);
			}catch (Exception ex){
				Utils.generateErrorMessage("Can not initialise Job", "Can not initialise Job: "+_JobClass.getName());
			}
		}
	}
	
	public List<Job_Default> getJobList(){
		return JobList;
	}
	
	public void runDistributor()throws Exception{
		
		ParameterPrefsManager.putParametersToFijiPrefs(distributorParameters, PREFS_PREFIX);
		
		Job_Default curImgJob;
		
		closer.run("");//IJ.run("Close all forced","");
		IJ.freeMemory();
		IJ.log ("Current memory is: " + IJ.currentMemory());
		curImgFnm=imgFile.getName();
		
		curImgJob=this.getJobForFnm(curImgFnm);
		
		if(curImgJob==null){
			IJ.log("This is unclassified image. Do nothing currently");
			return;
		}

		curImgJob.setSharedData(SharedData);
		try {
			//curImgJob.doJob(0,outputTable,outputTableProcessor,imgFile,true);
			curImgJob.doJob(0,imgFile,true);
		}catch(Exception ex) { // catch any exceptions produced by running the job
			this.microscopeCommander.submitJobPixels(imgFile, "", (Point3D)null);
			IJ.log("Exception rased: "+ex.getMessage());
			ewr.addRecord(imgFile.getName(), ex.getMessage());
			except_cnt+=1;
		}
		SharedData=curImgJob.getSharedData();
		
		if ((Boolean)distributorParameters.getParameterValue(KEY_SHOW_DEMO)&&curImgJob.getVisFlag())
			curImgJob.visualise(xvis,yvis);
	}
	
	@Override
	public void runOnNewFile(File file) {
		try{ //In case something goes wrong monitor does not have to crash. Current acquisition will not be recorded in summary file
			String fpth = file.getCanonicalPath();
			IJ.log("New file: " + fpth);
			if (file.isFile() && fpth.substring(fpth.lastIndexOf(".")+1).equals(distributorParameters.getParameterStringValue(KEY_FILE_EXTENSION))){
				imgFile=file;
				runDistributor();
			}
		}
		catch (Exception e){
			IJ.log("Exception rased: "+e.getMessage());
			ewr.addRecord(file.getName(), e.getMessage());
			except_cnt+=1;
		}
	}

	@Override
	public void runOnChangedFile(File file) {
		if((Boolean)distributorParameters.getParameterValue(KEY_CHANGED_FILES))
			runOnNewFile(file);
	}

	@Override
	public void runOnFileRemove(File file) {}
	
	
    public void startFolderMonitoring() throws Exception {

    	IJ.log("Created microscope commander: "+ microscopeCommander);
    	
		super.startMonitoring();
		ewr=new ExceptionWriter(getWatchpath());
	}
    
    public MicroscopeCommanderInterface createMicroscopeCommander() {
    	IJ.log("Microscope commander key: " + distributorParameters.getParameterStringValue(KEY_MICROSCOPE_COMMANDER));
    	MicroscopeCommanderInterface thisMicroscopeCommander=MicroscopeCommanderFactory.createMicroscopeCommander(microscopeCommanderMap.get(distributorParameters.getParameterStringValue(KEY_MICROSCOPE_COMMANDER)));
		
    	//setting distributor parameters
		ParameterCollection micParameters=thisMicroscopeCommander.createSettings();
		int nParameters=micParameters.getNumberOfParameters();
		micParameters.setUndefinedValuesFromDefaults();
		if (nParameters>0) {
			ParameterGuiManager pgManager=new ParameterGuiManager(micParameters);
			try{
				pgManager.refineParametersViaDialog("Parameters of microscope commander");
			}catch(Exception _ex){
				new WaitForUserDialog("error in microscope commander parameter values");
				throw new IncorrectFormatException();
			}
			
			thisMicroscopeCommander.parseSettings(pgManager.getParameterCollection());
		}
		//
    	return thisMicroscopeCommander;
    }
}
