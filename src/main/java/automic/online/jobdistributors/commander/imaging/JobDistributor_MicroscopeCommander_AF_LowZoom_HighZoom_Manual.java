package automic.online.jobdistributors.commander.imaging;


import net.imagej.ImageJ;

import org.scijava.command.Command;
import org.scijava.plugin.Plugin;

import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.common.Job_AutofocusInit_Commander;
import automic.online.jobs.common.Job_GetMultipleImagingPositions_Manual_Commander;
import automic.online.jobs.common.Job_RecordFinish;

@Plugin(type = Command.class, headless = true, menuPath ="Plugins>Auto Mic Tools>Applications>Imaging>AF LowZoom HighZoom - manual")
public class JobDistributor_MicroscopeCommander_AF_LowZoom_HighZoom_Manual extends JobDistributor_Abstract  implements Command{
	

	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_Commander.class,						"AF--",  "AF",	true);
		super.addImageJob(Job_GetMultipleImagingPositions_Manual_Commander.class,	"LowZoom--", "LowZoom",	true);
		super.addImageJob(Job_RecordFinish.class,									"HighZoom--", "HighZoom",true);

	}
	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:\\tempDat\\AutoMic-test";
		this.setGeneralOptions(searchPath, true, false);
	}
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(final String... args) {
		final ImageJ ij = new ImageJ();
		ij.launch(args);

		ij.command().run(JobDistributor_MicroscopeCommander_AF_LowZoom_HighZoom_Manual.class, true);

	}
	
}
