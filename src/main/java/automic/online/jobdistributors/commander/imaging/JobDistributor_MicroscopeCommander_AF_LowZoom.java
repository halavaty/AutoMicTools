package automic.online.jobdistributors.commander.imaging;


import ij.IJ;
import net.imagej.ImageJ;

import org.scijava.command.Command;
import org.scijava.plugin.Plugin;

import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.common.Job_AutofocusInit_Commander;
import automic.online.jobs.common.Job_RecordFinish;


@Plugin(type = Command.class, headless = true, menuPath ="Plugins>Auto Mic Tools>Applications>Imaging>AF LowZoom")
public class JobDistributor_MicroscopeCommander_AF_LowZoom extends JobDistributor_Abstract  implements Command{
	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_Commander.class,				"AF--",  "AF",	true);
		super.addImageJob(Job_RecordFinish.class,							"LowZoom--", "LowZoom",	true);

	}
	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:\\tempDat\\AutoMic-test";
		this.setGeneralOptions(searchPath, true, false);
	}
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		//Class<?> clazz = JobDistributor_MicroscopeCommander_AF_LowZoom.class;
		//String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		//String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		//System.setProperty("plugins.dir", pluginsDir);

		final ImageJ ij = new ImageJ();
		ij.launch(args);

		ij.command().run(JobDistributor_MicroscopeCommander_AF_LowZoom.class, true);

		
	}
}
