package automic.online.jobdistributors;
import ij.IJ;

import java.util.Date;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.Exception;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * creates and manages log file for online FRAP acquisition
 * @author Aliaksandr Halavatyi
 *
 */
public class ExceptionWriter{
	private static final DateFormat dtForm=new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	
	private static final String file_nm="OnlineExceptLog.txt";
	private File fl;
	private static final String delim="\t";

	/**
	 * Initialises file variable. Checks if file exists, otherwise initialises it
	 * @param _root  - main directory where all data are stored
	 */
	public ExceptionWriter(String _root){
		fl=new File(_root,file_nm);
		if (fl.exists())
			return;
		try{
			fl.createNewFile();
			fl.setReadable(true);
			fl.setWritable(true);

			BufferedWriter bufsumfl = new BufferedWriter(new FileWriter(fl));
		
			bufsumfl.write("Date.and.time");
			bufsumfl.write(delim);
			bufsumfl.write("FileName");
			bufsumfl.write(delim);
			bufsumfl.write("Message");
		
			bufsumfl.close();
		}catch (Exception e){
			IJ.error("Unable to create exceptions log file");
		}
	}
	
	public void addRecord(String _fnm, String _msg){
	    try{
	    	BufferedWriter bufsumfl = new BufferedWriter(new FileWriter(fl,true));
	    	bufsumfl.newLine();
	    	bufsumfl.write(dtForm.format(new Date())/*new Date().toString()*/);
	    	bufsumfl.write(delim);
	    	bufsumfl.write(_fnm);
	    	bufsumfl.write(delim);
	    	if (_msg==null)
	    		bufsumfl.write("null");
	    	else
	    		bufsumfl.write(_msg);
	    	
	    	bufsumfl.close();
	    }catch (Exception e){
	    	IJ.error("problem with writing to exceptions log file");
	    }
		
	}
}
