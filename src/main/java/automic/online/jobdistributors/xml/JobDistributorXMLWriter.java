package automic.online.jobdistributors.xml;

import java.util.List;
import java.io.File;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import automic.online.jobdistributors.JobDistributor_Abstract;
import automic.online.jobs.Job_Default;
import automic.parameters.ParameterCollection;
import automic.parameters.xml.Converter;
import automic.utils.FileUtils;

public class JobDistributorXMLWriter {
	public JobDistributor_Abstract jobDistributor;
	public File targetFile;
	
	private static DocumentBuilder documentBuilder;
	
	static {
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}	
	}
	
	public JobDistributorXMLWriter(){}
	
	private void setJobDistributor(JobDistributor_Abstract _jobDistributor){
		jobDistributor=_jobDistributor;
	}
	
	private void setFileToStore(File _file){
		if (!FileUtils.getExtension(_file).equals("xml"))
			throw new IllegalArgumentException("Wrong file exception");
		targetFile=_file;
	}
	
	private Document createDocument()throws Exception{
		Document document=documentBuilder.newDocument();
		
		//create root element
		Element rootElement=document.createElement(JobDistributorXMLTags.RootTagName);
		document.appendChild(rootElement);
		
		//create header
		Element headerElement=document.createElement(JobDistributorXMLTags.HeaderTagName);
		rootElement.appendChild(headerElement);
		//TODO: Fill header with pipeline input settings
		
		//add element for steps
		Element jobsElement=document.createElement(JobDistributorXMLTags.JobsTagName);
		
		//get pipeline steps
		List<Job_Default> jobs=jobDistributor.getJobList();
		
		Element jobElement;
		Attr jobTypeAttribute;
		String jobTypeName, jobName;
		ParameterCollection jobParameterCollection;
		
		for (Job_Default job:jobs){
			jobTypeName=job.getJobName();
			if (!jobTypeName.startsWith(JobDistributorXMLTags.JobPrefix))
				throw new Exception("Job class is not named correctly");
			jobName=jobTypeName.replaceFirst(JobDistributorXMLTags.JobPrefix, "");
			
			jobElement=document.createElement(JobDistributorXMLTags.JobTagName);
			
			jobTypeAttribute=document.createAttribute(JobDistributorXMLTags.JobTypeAttributeName);
			jobTypeAttribute.setValue(jobName);
			jobElement.setAttributeNode(jobTypeAttribute);
			
			jobParameterCollection=job.createJobParameters();
			jobParameterCollection.setUndefinedValuesFromDefaults();
			Converter.fillXmlElementFromParameterCollection(jobParameterCollection,document,jobElement);
			
			jobsElement.appendChild(jobElement);
		}
		rootElement.appendChild(jobsElement);
		
		return document;
	}
	
	private void writeDocumentToFile(Document _document)throws Exception{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
		
		DOMSource source = new DOMSource(_document);
        
		//changed in order to allow for file paths with spaces ("%20"):
		// StreamResult result = new StreamResult(targetFile);
		StreamResult result = new StreamResult(new FileOutputStream(targetFile));
		
		transformer.transform(source, result);
		// Output to console for testing
        StreamResult consoleResult =	new StreamResult(System.out);
        transformer.transform(source, consoleResult);
		
	}
	
	public void writePipelineToFile(JobDistributor_Abstract _jobDistributor, File _file)throws Exception{
		this.setJobDistributor(_jobDistributor);
		this.setFileToStore(_file);
		
		Document domDocument=createDocument();
		writeDocumentToFile(domDocument);
	}
}
