package automic.online.jobdistributors.xml;

public abstract class JobDistributorXMLTags {
	public static final String RootTagName = 				"jobdistributor";
	public static final String HeaderTagName = 				"header";
	public static final String HeaderSettingName = 			"string";
	public static final String JobsTagName = 				"jobs";
	public static final String JobTagName = 				"job";
	public static final String JobTypeAttributeName = 		"type";
	
	public static final String JobPrefix = "Job_";
	
}