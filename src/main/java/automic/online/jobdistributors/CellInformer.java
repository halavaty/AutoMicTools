package automic.online.jobdistributors;

//import java.awt.geom.Point2D;

//import ij.gui.Roi;
import mcib3d.geom.Point3D;

public interface CellInformer{
	/**
	 * @return pixel size in lateral dimension of low zoom image
	 */
	public double getPixSize();
	
//	public Roi getSelCell();
	
	public Point3D getZoomPoint(int _ind);
	
}
