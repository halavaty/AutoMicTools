package automic.online.jobdistributors;

import ij.*;
import ij.plugin.*;

import java.awt.Button;
import java.awt.Checkbox;
import java.awt.Choice;
import java.awt.Component;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.*;
import java.util.ArrayList;
import fiji.util.gui.GenericDialogPlus;

public class JobDistributorGui implements PlugIn, ActionListener {
	public GenericDialogPlus gd = new GenericDialogPlus("set JobDistributor");
	public Component[] initialcomp;
	public Component[] currentcomp;
	public Component[] previouscomp;
	public ArrayList<ArrayList<Component>> addedJobs =new ArrayList<>();

	public void run(String arg) {
//		GenericDialogPlus gd = new GenericDialogPlus("AddButton Example");
		gd.addMessage("Job assignments:");
//		gd.setInsets(20, 80, 5);
		gd.addButton("Add assignment", this);
		gd.addToSameRow();
//		gd.setInsets(5, 80, 10);
		gd.addButton("Remove all", this);
		initialcomp = gd.getComponents();
		currentcomp=initialcomp;
		gd.showDialog();
	}

	public void actionPerformed(ActionEvent e) {
		
		//name of the button:
		IJ.log("eventID: "+e.getActionCommand().toString());
		//the button:
		Button source = (Button) e.getSource();
		IJ.log("source name: "+source.getName());
		IJ.log("source: "+ source.toString());
		String buttonpressed = source.getLabel();
		if (buttonpressed.contentEquals("Add assignment")) {
			this.addJobAssignment();
		}
		if (buttonpressed.contentEquals("Remove all")) {
			this.removeAllJobAssignments();
		}
		if (buttonpressed.contentEquals("X")) {
			this.removeJobAssignment(source);
		}
		if (buttonpressed.contentEquals("set")) {
			this.setJobParameters(source);
		}
	}
	

	public void addJobAssignment() {
		previouscomp = currentcomp;
		gd.addStringField("prefix: ", "");
		gd.addToSameRow();
		gd.addStringField("tag: ", "");
		gd.addToSameRow();
		gd.addChoice("Job:",  new String[]{"J1", "J2", "J3"},  "J1");
		gd.addToSameRow();
		gd.addCheckbox("first", false);
		gd.addToSameRow();
		gd.addCheckbox("last", false);
		gd.addToSameRow();
		gd.addButton("set", this);
		gd.addToSameRow();
		gd.addButton("X", this);

		gd.validate();
		gd.showDialog();
		currentcomp = gd.getComponents();
//		int n =gd.getComponentCount();
//		IJ.log("component count: "+n);
//		
		addedJobs.add(getAddedComponents(currentcomp, previouscomp));
	}
	
	public void removeJobAssignment(Component _button) {
		for (int i=0; i<addedJobs.size(); i++) {
			ArrayList<Component> temp = addedJobs.get(i);
			if (temp.contains(_button)) {
				for (int j=0; j<temp.size(); j++) {
					gd.remove(temp.get(j));
				}
			}

		}
		gd.validate();
		gd.showDialog();
	}

	public void removeAllJobAssignments() {
		for (int i=0; i<addedJobs.size(); i++) {
			ArrayList<Component> temp = addedJobs.get(i);
			for (int j=0; j<temp.size(); j++) {
				gd.remove(temp.get(j));
			}
			
		}
		gd.validate();
		gd.showDialog();
		
	}
	
	private void setJobParameters(Button _button) {
		for (int i=0; i<addedJobs.size(); i++) {
			ArrayList<Component> temp = addedJobs.get(i);
			if (temp.contains(_button)) {
				for (int j=0; j<temp.size(); j++) {
					Component comp = temp.get(j);
					IJ.log("component " + j + " in Job line "+ i);

					if (comp instanceof Label) { 
						Label label = (Label) comp;
						IJ.log("content Label: "+label.getText());
					}

					if (comp instanceof Button) { 
						Button b = (Button) comp;
						IJ.log("content Button: "+b.getLabel());
					}

					if (comp instanceof TextField) { 
						TextField txt = (TextField) comp;
						IJ.log("content TextField: "+txt.getText());
					}
					
					if (comp instanceof Choice) { 
						Choice ch = (Choice) comp;
						IJ.log("content Choice: "+ch.getSelectedItem().toString());
					}
					if (comp instanceof Checkbox) { 
						Checkbox chb = (Checkbox) comp;
						IJ.log("content Checkbox: "+chb.getState());
					}
				}
			}

		}
		gd.validate();
		gd.showDialog();
	}

	
    // Function for finding elements which
    // are there in a[] but not in b[].
    static ArrayList<Component> getAddedComponents(Component[] a, Component[] b)
    {
    	ArrayList<Component> out= new ArrayList<>();
        // Store all elements of
        // second array in a hash table
        ArrayList<Component> s = new ArrayList<Component>();
        for (int i = 0; i < b.length; i++) {
            s.add(b[i]);
        }
        // Print all elements of first array
        // that are not present in hash table
        for (int i = 0; i < a.length; i++)

        	if (!s.contains(a[i])) {
//        		System.out.print(a[i] + " ");
        		out.add(a[i]);
        	}
        return out;
    }
	public static void main(String[] args) {
		Class<?> clazz = JobDistributorGui.class;
		new ImageJ();


		//IJ.runPlugIn(clazz.getName(),"Debug run");
		IJ.runPlugIn(clazz.getName(),"");

	}

}
