package automic.online.jobdistributors;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.WatchKey;
import java.nio.file.WatchEvent;
import java.nio.file.WatchService;
import java.nio.file.attribute.BasicFileAttributes;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.nio.file.StandardWatchEventKinds.*;
/**
 * Based on java tutorial
 * https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/essential/io/examples/WatchDir.java
 * @author Aliaksandr Halavatyi
 *
 */

@SuppressWarnings("unchecked")
abstract class FileMonitor {
	private Path watchPath;
	private Map<WatchKey,Path> keys;
	private boolean monitoringActive=false;
	
	private WatchService watcher;
	
	public abstract void runOnNewFile(File file);
	
	public abstract void runOnChangedFile(File file);
	
	public abstract void runOnFileRemove(File file);
	
	static final String ENTRY_CREATE_NAME=ENTRY_CREATE.name();
	static final String ENTRY_MODIFY_NAME=ENTRY_MODIFY.name();
	static final String ENTRY_DELETE_NAME=ENTRY_DELETE.name();

	
	/**
     * Register the given directory with the WatchService
     */
    private void register(Path dir) throws IOException {
        WatchKey key = dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);
        keys.put(key, dir);
    }
 
    /**
     * Register the given directory, and all its sub-directories, with the
     * WatchService.
     */
    private void registerAll(final Path start) throws IOException {
        // register directory and sub-directories
        Files.walkFileTree(start, new SimpleFileVisitor<Path>() {
            @Override
            public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs)
                throws IOException
            {
                register(dir);
                return FileVisitResult.CONTINUE;
            }
        });
    }
	
	
	public void startMonitoring() throws IOException,InterruptedException{
		monitoringActive=true;
		watcher=FileSystems.getDefault().newWatchService();
		this.keys = new HashMap<WatchKey,Path>();
		//register root directory with already existing subdirectories
		this.registerAll(watchPath);
		
		while (monitoringActive) {
			// wait for key to be signalled
            WatchKey key;
            try {
            	key = watcher.poll(1, TimeUnit.SECONDS);
            }catch(InterruptedException x) {
            	//TODO: log properly the issue in experiment logger
            	continue;
            }

            Path dir = keys.get(key);
            if (dir == null) {
                //System.err.println("WatchKey not recognized!!");
            	//TODO: log properly the issue in experiment logger
                continue;
            }
            
            for (WatchEvent<?> event: key.pollEvents()) {
                WatchEvent.Kind<?> kind = event.kind();
                if (kind == OVERFLOW) {
                	//TODO: log properly the issue in experiment logger
                	continue;
                }
                
                WatchEvent<Path> ev= (WatchEvent<Path>)event;
                Path name = ev.context();
                Path child = dir.resolve(name);
                
                try {
                    if (Files.isDirectory(child, NOFOLLOW_LINKS)) {
                        registerAll(child);
                    }else {
                    	/*switch (kind.name()) {
                    		case ENTRY_CREATE_NAME: runOnNewFile(child.toFile());break;
                    		case ENTRY_MODIFY_NAME: runOnChangedFile(child.toFile());break;
                    		case ENTRY_DELETE_NAME: runOnFileRemove(child.toFile()); break;
                    		default:{
                    			//TODO: log properly the issue in experiment logger
                    		}
                    	}
                    	*/
                    }
                } catch (IOException x) {
                	//TODO: log properly the issue in experiment logger
                }
            }
            key.reset();
            
		}
		
		watcher.close();
	}
	
	public void setWatchPath(String _watchPath) {
		watchPath=Paths.get(_watchPath);
	}
	
	public String getWatchPath(){
		return watchPath.toString(); 
	}
	
	public void stopMonitoring() {
		monitoringActive=false;
	}
}
