package automic.online.jobdistributors;

import java.util.List;

import ij.IJ;
import ij.ImageJ;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import automic.online.jobs.Job_Default;
import automic.online.jobs.common.Job_GetFRAPFinish;
import automic.online.jobs.frap.Job_GetMultipleManualBleachRois;
import automic.table.TableModel;

public class JobDistributor_FRAP_SemiManual_MulitpleRegions extends JobDistributor_Abstract implements PlugIn {
	
	
	@Override
	protected void fillJobList(){
		super.addImageJob(Job_GetMultipleManualBleachRois.class,	"DE_1_","Selection",	true);
		super.addImageJob(Job_GetFRAPFinish.class,	"TR1_1_", "FRAP",false);
				
	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");
		
		List<Job_Default> JobList=this.getJobList();
		for (Job_Default job:JobList)
			outTbl.addFileColumns(job.getColTag(), "IMG");
		
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}
	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="D:/tempDat/AutoMic_test";
		this.setGeneralOptions(searchPath, true, false);
	}

	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = JobDistributor_FRAP_SemiManual_MulitpleRegions.class;

		// start ImageJ
		new ImageJ();
		

		IJ.runPlugIn(clazz.getName(),"Debug run");

	}

}
