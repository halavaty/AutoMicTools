package automic.online.jobdistributors;


import ij.IJ;
import ij.ImageJ;
import ij.gui.GenericDialog;

import ij.plugin.PlugIn;

import automic.online.jobs.common.Job_AutofocusInit_Commander;
import automic.online.jobs.common.Job_RecordFinish;
import automic.table.TableModel;
import automic.table.TableProcessor;


public class JobDistributor_AFocus_Offset extends JobDistributor_Abstract implements PlugIn {
	

	
	@Override
	protected void fillJobList(){
		
		super.addImageJob(Job_AutofocusInit_Commander.class,	"DE_1_",  "AF",	true);
		super.addImageJob(Job_RecordFinish.class,	"DE_2_", "Result.Image",	true);

	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");
		
		outTbl.addFileColumns("AF", "IMG");
		outTbl.addFileColumns("Result.Image", "IMG");
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected TableProcessor configureTableProcessor(TableModel _tModel)throws Exception{
		TableProcessor tProcessor=new TableProcessor(_tModel);
		return tProcessor;
	}

	
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}


	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="C:/tempDat/AutoMic_test";
		this.setGeneralOptions(searchPath, true, false);
		//this.fileExtension="lsm";
	}
	
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		Class<?> clazz = JobDistributor_AFocus_Offset.class;
		new ImageJ();
		

		//IJ.runPlugIn(clazz.getName(),"Debug run");
		IJ.runPlugIn(clazz.getName(),"");

	}

	
}
