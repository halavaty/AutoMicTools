package automic.online.jobdistributors;

import java.util.List;

import ij.IJ;
import ij.ImageJ;
import ij.gui.GenericDialog;
import ij.plugin.PlugIn;
import automic.online.jobs.Job_Default;
import automic.online.jobs.common.Job_GetFRAPFinish;
import automic.online.jobs.frap.Job_GetManualBleachRoi;
import automic.table.TableModel;

public class JobDistributor_FRAP_SemiManual extends JobDistributor_Abstract implements PlugIn {
	//protocol specific settings
	//private static int zoomPointNumber;
	
	
	@Override
	protected void fillJobList(){
		super.addImageJob(Job_GetManualBleachRoi.class,	"DE_1_","Selection",	true);
		super.addImageJob(Job_GetFRAPFinish.class,	"TR1_1_", "FRAP",false);
				
	}
	
	@Override
	protected TableModel constructTabModel(String _rpth){
		TableModel outTbl=new TableModel(_rpth);

		outTbl.addColumn("Date.Time");
		
		List<Job_Default> JobList=this.getJobList();
		for (Job_Default job:JobList)
			outTbl.addFileColumns(job.getColTag(), "IMG");
		
		
		outTbl.addValueColumn("Success", "BOOL");
		
		outTbl.addRow(new Object[outTbl.getColumnCount()]);
		return outTbl;
	}
	
	@Override
	protected void putProtocolPreferencesToDialog(GenericDialog _dialog){
		//_dialog.addNumericField("Number of zoom positions", zoomPointNumber, 0);
	}

	@Override
	protected void getProtocolPreferencesFromDialog(GenericDialog _dialog){
		//zoomPointNumber=(int)_dialog.getNextNumber();
	}
	
	@Override
	protected boolean showDialogInDebugRun(){
		return false;
	}
	
	@Override
	protected void setDebugConfiguration(){
		final String searchPath="C:/tempDat/AutoMic_test";
		this.setGeneralOptions(searchPath, true, false);
		
		//zoomPointNumber=4;
	}


	
	
//	super.addImageJob(Job_AutofocusInit.class,"DE_1_","AFocus",	true);
//	super.addImageJob(Job_Default.class,		"DE_2_", "Z1.A647",	false);
//	super.addImageJob(Job_GetCellYfpCfp.class,"DE_3_", "Z1.YFP",	true);
//	
//	super.addImageJob(Job_SegERES_HZ_3D_Cargo.class,"TR1_1_", "HZ.YFP",	true);
//	super.addImageJob(Job_GetFRAPFinish.class,"TR2_1_", "FRAP.YFP",false);
	
	/**
	 * main method for debugging.
	 * Sets debug configuration via the method defined for each JobDistributor implementation
	 * Then executes JobDistributor
	 * 
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = JobDistributor_FRAP_SemiManual.class;

		// start ImageJ
		new ImageJ();
		

		IJ.runPlugIn(clazz.getName(),"Debug run");

	}

}
