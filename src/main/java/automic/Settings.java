package automic;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Global settings of automic package stored as static variables
 * @author Aliaksandr Halavatyi
 *
 */
public abstract class Settings {
	public static String dateFormatString="dd/MM/yyyy HH:mm:ss";
	public static DateFormat dateFormat;
	
	
	static{
		dateFormat=new SimpleDateFormat(dateFormatString);
	}
	
}
