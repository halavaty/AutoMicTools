package automic.parameters;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * Abstract class that defines types of possible parameters in two representations: strings and objects
 * It specifies conversions between these two types and and corresponding conversions of the values. 
 *
 */
public abstract class ParameterType{

	public static final String STRING_PARAMETER		="STRING";
	public static final String INT_PARAMETER		="INT";
	public static final String DOUBLE_PARAMETER		="DOUBLE";
	public static final String BOOL_PARAMETER		="BOOL";
	public static final String FOLDERPATH_PARAMETER	="FOLDER_PATH";
	public static final String FILEPATH_PARAMETER	="FILE_PATH";

	static Map<String,Class<?>> PARAMETER_VALUE_TYPES;

	static{

		HashMap<String,Class<?>> tmp = new HashMap<String,Class<?>>();
        tmp.put(STRING_PARAMETER,		String.class);
        tmp.put(INT_PARAMETER,			Integer.class);
        tmp.put(DOUBLE_PARAMETER,		Double.class);
        tmp.put(BOOL_PARAMETER,			Boolean.class);
        tmp.put(FOLDERPATH_PARAMETER,	String.class);
        tmp.put(FILEPATH_PARAMETER,		String.class);

        
        PARAMETER_VALUE_TYPES = Collections.unmodifiableMap(tmp);
    }

	
	
	public static String convertToString(Object _value,String _type){
		if(!isTypeMatching(_value, _type)) return null;
		return String.valueOf(_value);
	}
	
	public static Object parseToObject(String _stringValue,String _type){
		if(_stringValue==null) return null;
		if (_stringValue.equals(""))
			return (_type==STRING_PARAMETER)?_stringValue:null;
		switch(_type){
			case STRING_PARAMETER:		return _stringValue;
			case INT_PARAMETER:			return Integer.valueOf(_stringValue);
			case DOUBLE_PARAMETER:		return Double.valueOf(_stringValue);
			case BOOL_PARAMETER:		return Boolean.valueOf(_stringValue);
			case FOLDERPATH_PARAMETER:	return _stringValue;
			case FILEPATH_PARAMETER:	return _stringValue;
		}
		
		return null;
	}
	
	public static boolean isTypeMatching(Object _value,String _type){
		if(_value==null) return false;
		Class<?> parameterClass=getValueClass(_type);
		return (parameterClass==null)?false: parameterClass.isInstance(_value);
	}

	public static boolean isTypeMatching(Class<?> _valueClass,String _type){
		if(_valueClass==null) return false;
		Class<?> parameterClass=getValueClass(_type);
		return (parameterClass==null)?false: parameterClass.isAssignableFrom(_valueClass);
	}

	
	public static boolean isClassifiedType(String _type){
		return PARAMETER_VALUE_TYPES.containsKey(_type);//PARAMETER_IDENTIFIERS.contains(_type);
	}
	
	public static Class<?> getValueClass(String _type){
		return PARAMETER_VALUE_TYPES.get(_type);
	}
}
