package automic.parameters;

import java.util.LinkedHashMap;
import java.util.Set;

import automic.utils.logger.Logger;

public class ParameterCollection{

	LinkedHashMap<String, Parameter> parameters;		//here parameters are stored, and accessed by the key
														//?is it better to store parameterID inside structure
	public ParameterCollection(){
		parameters=new LinkedHashMap<String,Parameter>();
	}
	
	public void addParameter(String _addKey,Object _addValue,Object _addDefaultValue, String _addType){
		if(hasParameterKey(_addKey))
			throw new RuntimeException(String.format("Can not add parameter with the key %s because it already exists",_addKey));
		parameters.put(_addKey, new Parameter(_addValue, _addDefaultValue, _addType));
	}
	
	public void addSelectionParameter(String _addKey,Object _addValue,Object _addDefaultValue, String _addType, Object[] _addOptions){
		if(hasParameterKey(_addKey))
			throw new RuntimeException(String.format("Can not add parameter with the key %s because it already exists",_addKey));
		parameters.put(_addKey, new SelectionParameter(_addValue, _addDefaultValue, _addType, _addOptions));
	}

	public void addArrayParameter(String _addKey,Object _addValue,Object _addDefaultValue, String _addType){
		if(hasParameterKey(_addKey))
			throw new RuntimeException(String.format("Can not add parameter with the key %s because it already exists",_addKey));
		parameters.put(_addKey, new ArrayParameter(_addValue, _addDefaultValue, _addType));
	}

	
	/**
	 * sets new value for the parameter. Assumes that parameter with such key exists
	 * @param _key
	 * @param _newValue supposed to be of the right type. Type matching will be checked upon assignment.
	 */
	public void setParameterValue(String _key, Object _newValue){
		Parameter targetParameter=getParameterObject(_key);
		targetParameter.setValue(_newValue);
	}
	
	/**
	 * parses the provided string to the parameter value. 
	 * @param _key identifier of the parameter. Assumed to exist
	 * @param _newStringValue 
	 */
	public void setParameterValueFromString(String _key, String _newStringValue){
		Parameter targetParameter=getParameterObject(_key);
		targetParameter.setValueFromString(_newStringValue);
	}

	public boolean hasParameterKey(String _testKey){
		return parameters.containsKey(_testKey);
	}
	
	public void setUndefinedValuesFromDefaults(){
		for (Parameter p:parameters.values())
			p.setUnderfinedValueFromDefault();
	}
	
	public int getNumberOfParameters(){
		return parameters.size();
	}
	
	public Set<String> getParametersIndetifiers(){
		return parameters.keySet();
	}
	
	public Object getParameterValue(String _parameterKey){
		return getParameterObject(_parameterKey).getValue();
	}
	
	public String getParameterStringValue(String _parameterKey){
		return getParameterObject(_parameterKey).getStringValue();
	}
	
	public Parameter getParameterObject(String _parameterKey){
		Parameter parameterObject=parameters.get(_parameterKey);
		if (parameterObject==null)
			throw new IllegalArgumentException(String.format("No parameter with key %s",_parameterKey));
		return parameterObject;
	}
	
	public void setGuiVisibility(String _parameterKey,boolean _showInGui){
		getParameterObject(_parameterKey).setGuiVisibility(_showInGui);
	}
	
	public boolean getGuiVisibility(String _parameterKey){
		return getParameterObject(_parameterKey).getGuiVisibility();
	}
	
	public void printParametersToLogger(Logger _logger){
		Set<String> parameterKeys=getParametersIndetifiers();
		for (String key:parameterKeys)
			_logger.sendMessage(key+": "+getParameterStringValue(key));
	}
	
}
