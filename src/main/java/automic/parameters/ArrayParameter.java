package automic.parameters;

import java.lang.reflect.Array;

public class ArrayParameter extends Parameter{
	
	public static final String ARRRAY_SEPARATOR=";";
	
	Object[] arrayValue;
	
	public ArrayParameter(Object _value, Object _defaultValue, String _type){
		super(_value,_defaultValue,_type);
		arrayValue=(Object[])value;
	}
	
	@Override
	public void setValue(Object _newValue){
		super.setValue(_newValue);
		arrayValue=(Object[])value;
	}
	
	@Override
	public String getStringValue(){
		if (value==null)
			return null;
		if (arrayValue.length<1)
			return null;
		String outputString=ParameterType.convertToString(arrayValue[0], type);
		for (int index=1;index<arrayValue.length;index++)
			outputString+=ARRRAY_SEPARATOR+ParameterType.convertToString(arrayValue[index], type);
		return outputString;
	}
	
	@Override
	public void setValueFromString(String _newValue){
		if (_newValue==null)
			throw new NullPointerException("Array String can not be null");
		String[] stringElements=_newValue.split(ARRRAY_SEPARATOR);
		int nValues=stringElements.length;
		Object[] newArray=(Object[])Array.newInstance(ParameterType.getValueClass(type), nValues);
		for (int i=0;i<nValues;i++)
			newArray[i]=ParameterType.parseToObject(stringElements[i], type);
		setValue(newArray);
	}
	
	@Override
	protected void checkValue(Object _value,String _errorMessage){
		Class<?> arrayClass=_value.getClass();
		if (!arrayClass.isArray())
			throw new ClassCastException((_errorMessage==null)?"Can not set array parameter with non-array value":_errorMessage);
		if(!ParameterType.isTypeMatching(arrayClass.getComponentType(), type))
			throw new ClassCastException((_errorMessage==null)?"Type of array element does not match specified parameter type":_errorMessage);
	}
}
