package automic.parameters.xml;

import java.util.Set;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import automic.parameters.ParameterCollection;

public abstract class Converter {
	public static String StepSettingTagName = 		"setting";
	public static String StepSettingKeyAttributeName = "key";
	public static String StepSettingValueAttributeName = "value";
	
	public static void fillXmlElementFromParameterCollection(ParameterCollection _collection, Document _document, Element _xmlElement){
		Set<String> stepKeys;
		Element settingElement;
		Attr settingKeyAttribute;
		Attr settingValueAttribute;

		
		
		stepKeys=_collection.getParametersIndetifiers();
		for (String stepKey:stepKeys){
			settingElement=_document.createElement(StepSettingTagName);
			
			settingKeyAttribute=_document.createAttribute(StepSettingKeyAttributeName);
			settingKeyAttribute.setValue(stepKey);
			settingElement.setAttributeNode(settingKeyAttribute);
			
			settingValueAttribute=_document.createAttribute(StepSettingValueAttributeName);
			settingValueAttribute.setValue(_collection.getParameterStringValue(stepKey));
			settingElement.setAttributeNode(settingValueAttribute);
			
			_xmlElement.appendChild(settingElement);
		}

	}

	public static void fillParameterCollectionFromXmlElement(Element _xmlElement, ParameterCollection _collection){
		NodeList settingsNodes = _xmlElement.getElementsByTagName(StepSettingTagName);
		
		for (int j = 0; j < settingsNodes.getLength(); j++) {
			Node settingNode = settingsNodes.item(j);
			//if (settingNode.getNodeType()!=Node.ELEMENT_NODE) {
			//	throw new RuntimeException("Wrong setting node type");
			//}
			Element settingElement=(Element)settingNode;
			
			//NamedNodeMap settingAttributes = settingNode.getAttributes();
			
			String settingKey 	= settingElement.getAttribute(StepSettingKeyAttributeName);
			String settingValue = settingElement.getAttribute(StepSettingValueAttributeName);
			//if(settingValue.isEmpty())
			//	logger.sendWarningMessage(String.format("Empty value for the parameter \"%s\" of the step \"%s\"", settingKey,step.getStepName()));
			_collection.setParameterValueFromString(settingKey, settingValue);
		}
	}
	
}
