package automic.parameters.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import automic.parameters.ParameterCollection;
import automic.parameters.xml.Converter;
import automic.utils.logger.NoLogger;
import automic.utils.logger.Logger;

public class ParameterXmlReader{

	Logger logger;
	
	private static DocumentBuilder documentBuilder;
	
	private Document document;
	
	private ParameterCollection targetParameterCollection;
	
	static {
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}	
	}
	
	public void setLogger(Logger _logger){
		logger=_logger;
	}
	
	public ParameterXmlReader(){
		logger=new NoLogger();
	}
	
	private void setInputFile(File _xmlFile) {
		logger.sendInfoMessage("Reading XML file with the pipeline");
		try {
			this.document = documentBuilder.parse(_xmlFile);			
		} catch (Exception ex) {
			throw new RuntimeException(ex.getMessage());
		}
	}
	
	private void setParameterCollection(ParameterCollection _collection) {
		if (_collection==null)
			throw new IllegalArgumentException();
		targetParameterCollection=_collection;
	}
	
	
	public void fillParameterCollection(File _inputXmlFile, ParameterCollection _targetCollection){
		setInputFile(_inputXmlFile);
		setParameterCollection(_targetCollection);
		
		logger.sendInfoMessage("Filling ParameterCollection with values from the file");
		
		Element rootElement = this.document.getDocumentElement();
		Converter.fillParameterCollectionFromXmlElement(rootElement,targetParameterCollection);
		

	}

}
