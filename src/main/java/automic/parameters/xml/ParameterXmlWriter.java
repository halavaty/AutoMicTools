package automic.parameters.xml;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import automic.parameters.ParameterCollection;
import automic.parameters.xml.Converter;
import automic.utils.FileUtils;

public class ParameterXmlWriter {
	public ParameterCollection parameterCollection;
	public File targetFile;
	
	private static DocumentBuilder documentBuilder;
	
	static {
		try {
			documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
			throw new RuntimeException(e.getMessage());
		}	
	}
	
	public ParameterXmlWriter(){}
	
	private void setParameterCollection(ParameterCollection _collection){
		parameterCollection=_collection;
	}
	
	private void setFileToStore(File _file){
		if (!FileUtils.getExtension(_file).equals("xml"))
			throw new IllegalArgumentException("Wrong file exception");
		targetFile=_file;
	}
	
	private Document createDocument()throws Exception{
		Document document=documentBuilder.newDocument();
		
		//create root element
		Element rootElement=document.createElement("Parameter_Collection");
		document.appendChild(rootElement);
		
		Converter.fillXmlElementFromParameterCollection(parameterCollection,document,rootElement);
		
		return document;
	}
	
	private void writeDocumentToFile(Document _document) throws FileNotFoundException{
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		
		DOMSource source = new DOMSource(_document);
        
		StreamResult result = new StreamResult(new FileOutputStream(targetFile));
		
		try{

			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
			
			transformer.transform(source, result);

		}catch (Exception _ex){
			throw new RuntimeException("Can not wite ParameterCollection to XML file");
		}

		// Output to console for testing
        //StreamResult consoleResult =	new StreamResult(System.out);
        //transformer.transform(source, consoleResult);
		
	}
	
	public void writeParametersToFile(ParameterCollection _parameterCollection, File _file){
		this.setParameterCollection(_parameterCollection);
		this.setFileToStore(_file);
		
		try{
			Document domDocument=createDocument();
			writeDocumentToFile(domDocument);
		}catch (Exception _ex){
			throw new RuntimeException("Can not wite ParameterCollection to XML file");
		}	
	}
}
