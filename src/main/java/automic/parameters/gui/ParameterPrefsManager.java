package automic.parameters.gui;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import automic.parameters.Parameter;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.parameters.SelectionParameter;
import ij.Prefs;

public abstract class ParameterPrefsManager{
	
	public static String generatePrefsKey(String _prefix, String _tag) {
		return String.format("%s.%s", _prefix,_tag);
	}

	public static void setParametersFromFijiPrefs(ParameterCollection _parameters, String _prefsPrefix)throws Exception {
			
		for (String pKey:_parameters.getParametersIndetifiers()) {
			String prefsKey=generatePrefsKey(_prefsPrefix, pKey);	
			Parameter currentParameter=_parameters.getParameterObject(pKey);
			
			Object valueToSet=null;
			
			switch (currentParameter.getType()) {
				case ParameterType.STRING_PARAMETER:
					valueToSet=Prefs.get(prefsKey, (String)currentParameter.getValue());
					break;
	
					
				case ParameterType.BOOL_PARAMETER:
					valueToSet=Prefs.get(prefsKey, (Boolean)currentParameter.getValue());
					break;
					
				case ParameterType.DOUBLE_PARAMETER:
					valueToSet=Prefs.get(prefsKey, (Double)currentParameter.getValue());
					break;
	
				case ParameterType.INT_PARAMETER:
					//need to cast here because Prefs.get returns double value
					valueToSet=(int)(Prefs.get(prefsKey, (Integer)currentParameter.getValue()));
					break;
		
				case ParameterType.FILEPATH_PARAMETER:
					valueToSet=Prefs.get(prefsKey, (String)currentParameter.getValue());
					break;
					
				case ParameterType.FOLDERPATH_PARAMETER:
					valueToSet=Prefs.get(prefsKey, (String)currentParameter.getValue());
					break;
					
				default:
					throw new Exception ("parameter can not be saved in preferences");
			}
			
			//skip parsing value for Selection parameter for the new value which is not in the list.
			//generate proper warning message
			if(currentParameter instanceof SelectionParameter) {
				if (((SelectionParameter)currentParameter).getOptionsAsList().indexOf(valueToSet)<0) {
					System.out.println(String.format("[WARNING]: Can not set value %s for parameter %s. The value found in preferences is not in the list of allowed values", valueToSet.toString(),pKey));
					continue;
					
				}
			}
			currentParameter.setValue(valueToSet);
			
		}
	}

	public static void putParametersToFijiPrefs(ParameterCollection _parameters, String _prefsPrefix)throws Exception {
		for (String pKey:_parameters.getParametersIndetifiers()) {
			String prefsKey=generatePrefsKey(_prefsPrefix, pKey);	
			Parameter currentParameter=_parameters.getParameterObject(pKey);
			
			switch (currentParameter.getType()) {
				case ParameterType.STRING_PARAMETER:
					Prefs.set(prefsKey, (String)currentParameter.getValue());
					break;
	
					
				case ParameterType.BOOL_PARAMETER:
					Prefs.set(prefsKey, (Boolean)currentParameter.getValue());
					break;
					
				case ParameterType.DOUBLE_PARAMETER:
					Prefs.set(prefsKey, (Double)currentParameter.getValue());
					break;
	
				case ParameterType.INT_PARAMETER:
					Prefs.set(prefsKey, (Integer)currentParameter.getValue());
					break;
		
				case ParameterType.FILEPATH_PARAMETER:
					Prefs.set(prefsKey, (String)currentParameter.getValue());
					break;
					
				case ParameterType.FOLDERPATH_PARAMETER:
					Prefs.set(prefsKey, (String)currentParameter.getValue());
					break;
					
				default:
					throw new Exception ("parameter can not be saved in preferences");
			}
			
		}
		
	}

	
	
}
