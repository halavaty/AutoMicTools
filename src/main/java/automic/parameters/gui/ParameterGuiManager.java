package automic.parameters.gui;

import java.util.Set;

import automic.parameters.ArrayParameter;
import automic.parameters.Parameter;
import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;
import automic.parameters.SelectionParameter;
import fiji.util.gui.GenericDialogPlus;
import ij.gui.GenericDialog;

public class ParameterGuiManager{

	ParameterCollection workCollection;
	String [] keys;
	
	public ParameterGuiManager(ParameterCollection _collection){
		workCollection=_collection;
		Set<String> keySet=workCollection.getParametersIndetifiers();
		keys=keySet.toArray(new String[keySet.size()]);
	}
	
	
	private GenericDialog createGenericDialog(String _header)throws Exception{
		GenericDialogPlus gd=new GenericDialogPlus(_header);
		
		//for (int parameterIndex=0;parameterIndex<nParameters;parameterIndex++){
		for (String key:keys){
		
			Parameter currentParameter=workCollection.getParameterObject(key);
			
			if (currentParameter instanceof ArrayParameter) {
				gd.addStringField(key, (String)currentParameter.getStringValue());
				continue;
			}
			
			
			if (currentParameter instanceof SelectionParameter) {
				String[] options=((SelectionParameter)currentParameter).getOptionsAsStringArray();
				gd.addChoice(key, options, options[0]);
				continue;
			}
			
			
			
			switch (currentParameter.getType()) {
				case ParameterType.STRING_PARAMETER:
					gd.addStringField(key, (String)currentParameter.getValue(),25);
					break;
	
					
				case ParameterType.BOOL_PARAMETER:
					gd.addCheckbox(key, (boolean)currentParameter.getValue());
					break;
					
				case ParameterType.DOUBLE_PARAMETER:
					gd.addNumericField(key, (double)currentParameter.getValue(), 3,25,"");
					break;
	
				case ParameterType.INT_PARAMETER:
					
					gd.addNumericField(key, (int)currentParameter.getValue(), 0,25,"");
					break;
		
				case ParameterType.FILEPATH_PARAMETER:
					gd.addFileField(key,(String)currentParameter.getValue(),25);
					break;
					
				case ParameterType.FOLDERPATH_PARAMETER:
					gd.addDirectoryField(key,(String)currentParameter.getValue(),25);
					break;
					
				
					
				default:
					throw new Exception ("parameter can not be shown in GUI");
			}
		}
		
		
		return gd;
	}
	
	private void fillParameterValuesFromDialog (GenericDialog _dialog)throws Exception{
		//for (int parameterIndex=0;parameterIndex<nParameters;parameterIndex++){
		for (String key:keys){
		
			Parameter currentParameter=workCollection.getParameterObject(key);
			
			if (currentParameter instanceof ArrayParameter) {
				currentParameter.setValueFromString(_dialog.getNextString());
				continue;
			}
			
			if (currentParameter instanceof SelectionParameter) {
				currentParameter.setValueFromString(_dialog.getNextChoice());
				continue;
			}

			
			switch (currentParameter.getType()) {
				case ParameterType.STRING_PARAMETER:
					currentParameter.setValue(_dialog.getNextString());
					break;
	
					
				case ParameterType.BOOL_PARAMETER:
					currentParameter.setValue(_dialog.getNextBoolean());
					break;
					
				case ParameterType.DOUBLE_PARAMETER:
					currentParameter.setValue(_dialog.getNextNumber());
					break;
	
				case ParameterType.INT_PARAMETER:
					currentParameter.setValue((int)_dialog.getNextNumber());
					break;
					
				case ParameterType.FILEPATH_PARAMETER:
					currentParameter.setValue(_dialog.getNextString());
					break;
					
				case ParameterType.FOLDERPATH_PARAMETER:
					currentParameter.setValue(_dialog.getNextString());
					break;
					
				default:
					throw new Exception ("parameter can not be shown in GUI");
			}
		}
		
		
		
	}
	
	public boolean refineParametersViaDialog(String _header)throws Exception{
		GenericDialog gd=createGenericDialog(_header);
		
		gd.showDialog();
		if(gd.wasCanceled())
			return false;
		
		this.fillParameterValuesFromDialog(gd);
		return true;
	}
	
	public ParameterCollection getParameterCollection(){
		return workCollection;
	}
}
