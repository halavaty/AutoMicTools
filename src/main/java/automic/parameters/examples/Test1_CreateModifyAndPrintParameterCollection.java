package automic.parameters.examples;


import java.util.Set;

import automic.parameters.ParameterCollection;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;

//import ij.ImageJ;

public abstract class Test1_CreateModifyAndPrintParameterCollection {

	
	
	
	public static void main(String[] args) {

		// start ImageJ
		/*new ImageJ();*/

		//create logger and set it as application logger
		Logger logger=new TextWindowLogger("Parameter collection output");
	
		ParameterCollection newCollection=ExampleParameterCreator.createParameterCollection1();
		
		logger.sendInfoMessage("Initial definition of parameters");
		/*	printing individual parameters to the logger
		 * 	this code is here for clarity
		 * 	the same can be done with ParameterCollection.printParametersToLogger(Logger) method
		 */
		Set<String> parameterKeys=newCollection.getParametersIndetifiers();
		for (String key:parameterKeys){
			logger.sendMessage(key+": "+newCollection.getParameterStringValue(key));
			//logger.sendMessage(key+": "+newCollection.getParameterValue(key));
			/*
			 * getParameterStringValue(key) returns string representation of parameter value
			 * getParameterValue(key) returns native object (type specific) representation of parameter value
			 */
		}
		
		//set underfined parameters values from default
		newCollection.setUndefinedValuesFromDefaults();
		//and now print parameters again
		logger.sendInfoMessage("Underfined values set from defaults");
		newCollection.printParametersToLogger(logger);
		
		//modify values and catch exceptions when something goes wrong
		//defining local function to implement the logics
		logger.sendInfoMessage("test Parameter modifications");
		
		
		//_collection.setParameterValue(_parameterKey, _newValue);
		changeParameterAndCatchException(newCollection, "Slice Index", 3, logger);//ERROR: no parameter with such key is defined
		
		changeParameterAndCatchException(newCollection, "Channel Index", 3, logger);//CORRECT
		changeParameterAndCatchException(newCollection, "Channel Index", 3.1, logger);//ERROR: new value does not match expected type
		
		changeParameterAndCatchException(newCollection, "My Segmentation method", "Threshold", logger);//CORRECT
		changeParameterAndCatchException(newCollection, "My Segmentation method", "Weka", logger);//ERROR: new value is not in the list of possible values
		
		changeParameterAndCatchException(newCollection, "My Square", 25, logger);//CORRECT
		changeParameterAndCatchException(newCollection, "My Square", 27, logger);//ERROR: new value is not in the list of possible values

		changeParameterAndCatchException(newCollection, "My Double", 3.02, logger);//CORRECT
		changeParameterAndCatchException(newCollection, "My Double", 3.03, logger);//ERROR: new value is not in the list of possible values
		
		
		changeParameterAndCatchException(newCollection, "Thresholding methods", new String[]{"Method 1","Method 2"}, logger);//CORRECT
		changeParameterAndCatchException(newCollection, "Thresholding methods", new Integer[]{1,2}, logger);//ERROR: type of array element does not match
		
		changeParameterAndCatchException(newCollection, "Qautification channels", new String[]{"1","2"}, logger);//ERROR: type of array element does not match
		changeParameterAndCatchException(newCollection, "Qautification channels", new Integer[]{1,2}, logger);//CORRECT
		changeParameterAndCatchException(newCollection, "Qautification channels", 1, logger);//ERROR: new Value is not an array
	}
	
	private static void changeParameterAndCatchException(ParameterCollection _collection, String _parameterKey, Object _newValue, Logger _logger){
		try{
			_collection.setParameterValue(_parameterKey, _newValue);
			_logger.sendMessage(String.format("Value of parameter %s is changed to %s", _parameterKey,_collection.getParameterStringValue(_parameterKey)));
		}catch (RuntimeException _ex){
			_logger.sendErrorMessage(String.format("Value of parameter %s can not be changed to to %s. Problem is: %s", _parameterKey,_newValue.toString(), _ex.getMessage()));
		}
	}
	
}


