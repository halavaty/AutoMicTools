package automic.parameters.examples;

import automic.parameters.ParameterCollection;
import automic.parameters.ParameterType;

public abstract class ExampleParameterCreator {
	public static ParameterCollection createParameterCollection1(){
		//create collection
		ParameterCollection newCollection=new ParameterCollection();
				
		//fill with parameters
		//simple string
		newCollection.addParameter("Image Tag", null, 					"Default string",	ParameterType.STRING_PARAMETER); 	
		//file path
		newCollection.addParameter("File path", "C:/tempDat/Image.tif",	null, 				ParameterType.FILEPATH_PARAMETER);
		//folder path
		newCollection.addParameter("Folder path","C:/tempDat",null,			 				ParameterType.FOLDERPATH_PARAMETER);
		//numeric integer Value
		newCollection.addParameter("Channel Index",1,2, 									ParameterType.INT_PARAMETER);
		//numeric floating point Value
		newCollection.addParameter("Some coefficient",null,3.14,							ParameterType.DOUBLE_PARAMETER);
		//boolean value
		newCollection.addParameter("Quality control flag", true, null, 						ParameterType.BOOL_PARAMETER);
		
		//now add some parameters with selection options. All base types are supported
		
		//choice from strings
		String[] segmentationMethods=new String[]{"Threshold","Local Threshold","TopHat"};
		newCollection.addSelectionParameter("My Segmentation method", "TopHat", null, 		ParameterType.STRING_PARAMETER, segmentationMethods);
		//choice from integers
		Integer[] squares=new Integer[]{1,4,9,16,25};//for primitive types (e.g. number) have to use Object wrapper classes to define arrays of possible values
		newCollection.addSelectionParameter("My Square", 9, 16, 							ParameterType.INT_PARAMETER,	squares);
				
		//choice from doubles
		Double[] numbers=new Double[]{3.0,3.01,3.02};//for primitive types (e.g. number) have to use Object wrapper classes to define arrays of possible values
		newCollection.addSelectionParameter("My Double", 3.010, 3.00,							ParameterType.DOUBLE_PARAMETER,	numbers);
		
		//Array of Strings
		String [] thresholdingMethods=new String[]{"Otsu","Mean","Entropy"};
		newCollection.addArrayParameter("Thresholding methods", thresholdingMethods, null, ParameterType.STRING_PARAMETER);
		//Array of integers
		Integer [] quantificationChannels=new Integer[]{2,4,3};
		newCollection.addArrayParameter("Qautification channels", null, quantificationChannels, ParameterType.INT_PARAMETER);
		//Array of doubles
		Double [] roots=new Double[]{1.41,2.0,1.73};
		newCollection.addArrayParameter("Roots", roots, null, ParameterType.DOUBLE_PARAMETER);
		
		return newCollection;
	}
}
