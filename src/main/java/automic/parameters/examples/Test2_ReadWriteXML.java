package automic.parameters.examples;


import java.io.File;

import automic.parameters.ParameterCollection;
import automic.parameters.xml.ParameterXmlWriter;
import automic.parameters.xml.ParameterXmlReader;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import ij.gui.WaitForUserDialog;

//import ij.ImageJ;

public abstract class Test2_ReadWriteXML {

	static String xmlFilePath="C:/tempdat/testparameters.xml";
	
	
	public static void main(String[] args) {

		// start ImageJ
		/*new ImageJ();*/

		//create logger
		Logger logger=new TextWindowLogger("Parameter collection output");
	
		try{
			//create test ParameterCollection
			ParameterCollection newCollection=ExampleParameterCreator.createParameterCollection1();
			newCollection.setUndefinedValuesFromDefaults();
			
			//print initial ParameterCollection
			logger.sendInfoMessage("Initial definition of parameters");
			newCollection.printParametersToLogger(logger);
			
			
			//write Parameters to XML
			ParameterXmlWriter parameterWriter=new ParameterXmlWriter();
			parameterWriter.writeParametersToFile(newCollection, new File(xmlFilePath));
			
			//ask user to modify file manually with waiting dialog
			new WaitForUserDialog("Modify file and press OK", "Try to modify values and identifiers or the parameters keys.\nAlso can test non-compatible values that are supposed to produce an error").show();
			
			//read ParameterCollection from xml
			ParameterXmlReader parameterReader=new ParameterXmlReader();
			parameterReader.setLogger(logger);
			parameterReader.fillParameterCollection(new File(xmlFilePath), newCollection);
			//print red ParameterCollection
			logger.sendInfoMessage("Paraneter Collection read from XML file");
			newCollection.printParametersToLogger(logger);
		}catch(Exception _ex){
			logger.sendExceptionMessage("Exception generated during test", _ex);
		}
		
		
	}
	
	
}


