package automic.parameters;

import java.util.Arrays;
import java.util.List;

public class SelectionParameter extends Parameter{
	
	private final List<Object> options;
	int selectedIndex;
	
	public SelectionParameter(Object _value, Object _defaultValue, String _type, Object[] _options){
		super(_type);
		options=Arrays.asList(_options);
		initialiseValues(_value, _defaultValue);
	}
	
	@Override
	protected void checkValue(Object _value,String _errorMessage){
		super.checkValue(_value, _errorMessage);
		if (options.indexOf(_value)<0)
			throw new IllegalArgumentException("Specified value is not in the permitted list");
	}
	
	public String[] getOptionsAsStringArray() {
		String[] optionsArray=new String[options.size()];
		for (int i=0;i<options.size();i++) {
			optionsArray[i]=options.get(i).toString();
		}
		
		return optionsArray;
	}
	
	public List<Object> getOptionsAsList(){
		return options;
	}
}
