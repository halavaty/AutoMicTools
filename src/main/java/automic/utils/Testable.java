package automic.utils;

public interface Testable {
	public abstract void setup();
	
	public abstract void process();
	
	//TODO: Implement runner and procedures derived from that 
}
