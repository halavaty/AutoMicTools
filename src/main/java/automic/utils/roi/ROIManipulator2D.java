package automic.utils.roi;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
//import ij.plugin.RoiEnlarger;
import ij.plugin.filter.EDM;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ByteProcessor;
import ij.process.FloatPolygon;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import ij.gui.Overlay;
import ij.gui.PointRoi;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import ij.gui.Line;
import ij.gui.ShapeRoi;

import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ROIManipulator2D /* extends ROIManipulator */ {
	public static final String ext = "zip";

	private static EDM Edm = null;
	private static Random rndGen = null;

	public ROIManipulator2D() {
	}

	public static RoiManager getEmptyRm() {
		RoiManager rm = RoiManager.getInstance();
		if (rm == null)
			rm = new RoiManager();
		else
			rm.runCommand("Reset");
		return rm;
	}

	public static void saveRoisToFile(String _pth, String _fileNmNoExt, Roi[] _rois) {

		int p, c, z, t;

		if (_rois == null)
			return;
		int rnum = _rois.length;
		if (rnum == 0)
			return;

		RoiManager rm = getEmptyRm();
		Roi ra;
		for (Roi r : _rois) {
			if (r == null)
				continue;
			p = r.getPosition();
			c = r.getCPosition();
			z = r.getZPosition();
			t = r.getTPosition();

			rm.addRoi(r);
			ra = rm.getRoi(rm.getCount() - 1);

			if (p != 0)
				ra.setPosition(p);
			else
				ra.setPosition(c, z, t);
		}
		rm.runCommand("Deselect");// deselect ROIs to save them all
		rm.runCommand("Save", new File(_pth, _fileNmNoExt + "." + ext).getAbsolutePath());
	}

	public static void saveRoiToFile(String _pth, String _fileNmNoExt, Roi _roi) {
		saveRoisToFile(_pth, _fileNmNoExt, new Roi[] { _roi });
	}

	public static Roi[] flsToRois(File _fl) throws Exception {
		RoiManager rm = getEmptyRm();
		if (_fl.exists())
			rm.runCommand("Open", _fl.getAbsolutePath());
		else
			throw new Exception("AH_ROIManipulator.flsToRois: specified file not found");

		rm.runCommand("Deselect");// deselect ROIs to save them all
		return rm.getRoisAsArray();
	}

	public static Roi[] flsToRois(String _pth, String _fileNmNoExt) throws Exception {
		File fl;
		// fl=new File(_pth,_fileNmNoExt+".roi");
		// if (fl.exists())
		// rm.runCommand("Open", fl.getAbsolutePath());
		fl = new File(_pth, _fileNmNoExt + "." + ext);
		return flsToRois(fl);
	}

	public static Overlay flsToOverlay(File _fl) throws Exception {
		Roi[] rs = flsToRois(_fl);
		if (rs == null)
			return null;
		Overlay ovl = new Overlay();
		for (int i = 0; i < rs.length; i++)
			ovl.add(rs[i]);
		return ovl;
	}

	public static Overlay flsToOverlay(String _pth, String _fileNmNoExt) throws Exception {
		Roi[] rs = flsToRois(_pth, _fileNmNoExt);
		if (rs == null)
			return null;
		Overlay ovl = new Overlay();
		for (int i = 0; i < rs.length; i++)
			ovl.add(rs[i]);
		return ovl;
	}

	public static void removeSliceInfo(Roi _roi) {
		if (_roi == null)
			return;
		_roi.setPosition(0);
		_roi.setPosition(0, 0, 0);
	}

	public static void removeSliceInfo(Roi[] _rois) {
		if (_rois == null)
			return;
		for (Roi r : _rois)
			removeSliceInfo(r);
	}

	public static void removeSliceInfo(Overlay _ovl) {
		if (_ovl == null)
			return;
		removeSliceInfo(_ovl.toArray());
	}

	public static Overlay roisToOverlay(Roi[] _rs) {
		Overlay o = new Overlay();
		if (_rs == null)
			return o;

		for (Roi r : _rs)
			o.add(r);

		return o;

	}

	public static void svOvlToFl(String _pth, String _fileNmNoExt, Overlay _ovl) {
		saveRoisToFile(_pth, _fileNmNoExt, _ovl.toArray());
	}

	public static Roi getZoomRoi(double _xCenter, double _yCenter, double _zoomWidth, double _zoomHeight) {
		if ((_xCenter < 0) || (_yCenter < 0))
			return null;
		return new Roi(_xCenter - _zoomWidth / 2, _yCenter - _zoomHeight / 2, _zoomWidth, _zoomHeight);
	}

	public static Point2D.Double calcERESPos(double _xZm, double _yZm, Roi _broi, double _zoom_sz_X, double _zoom_sz_Y,
			double _pixRatio) {
		if ((_xZm < 0) || (_yZm < 0) || (_broi == null))
			return null;

		Point2D.Double res = new Point2D.Double();

		Rectangle blBounds = _broi.getBounds();
		double blx = blBounds.x + blBounds.width / 2;
		double bly = blBounds.y + blBounds.height / 2;
		res.x = _xZm - _zoom_sz_X / 2 + blx / _pixRatio;
		res.y = _yZm - _zoom_sz_Y / 2 + bly / _pixRatio;

		return res;
	}

	public static double[] calcERESPos(double _xZm, double _yZm, double x_eres_zoom, double y_eres_zoom,
			double _zoom_sz_X, double _zoom_sz_Y, double _pixRatio) {
		if ((_xZm < 0) || (_xZm < 0) || (x_eres_zoom < 0) || (y_eres_zoom < 0))
			return null;

		double[] res = new double[2];
		res[0] = _xZm - _zoom_sz_X / 2 + x_eres_zoom / _pixRatio;
		res[1] = _yZm - _zoom_sz_Y / 2 + y_eres_zoom / _pixRatio;

		return res;
	}

	public static Point2D.Double[] genRandPoints(Roi _cell, int _pnum) {
		if (rndGen == null)
			rndGen = new Random();
		if (_cell == null)
			return null;
		Rectangle crect = _cell.getBounds();
		int n = 0;
		Point2D.Double[] res = new Point2D.Double[_pnum];
		int xt, yt;
		while (n < _pnum) {
			xt = crect.x + rndGen.nextInt(crect.width);
			yt = crect.y + rndGen.nextInt(crect.height);
			if (_cell.contains(xt, yt)) {
				res[n] = new Point2D.Double(xt, yt);
				n += 1;
			}
		}
		return res;
	}

	public static Point2D.Double[] genRandPoints_InOut(Roi _cellIn, Roi _cellOut, int _pnum) {
		if (rndGen == null)
			rndGen = new Random();
		if ((_cellIn == null) || (_cellOut == null))
			return null;
		int pnum2 = _pnum / 2;

		// get inner points
		Rectangle crect = _cellIn.getBounds();
		int n = 0;
		Point2D.Double[] res = new Point2D.Double[_pnum];
		int xt, yt;
		while (n < pnum2) {
			xt = crect.x + rndGen.nextInt(crect.width);
			yt = crect.y + rndGen.nextInt(crect.height);
			if (_cellIn.contains(xt, yt)) {
				res[n] = new Point2D.Double(xt, yt);
				n += 1;
			}
		}

		// get outer points
		crect = _cellOut.getBounds();
		while (n < _pnum) {
			xt = crect.x + rndGen.nextInt(crect.width);
			yt = crect.y + rndGen.nextInt(crect.height);
			if (_cellOut.contains(xt, yt)) {
				res[n] = new Point2D.Double(xt, yt);
				n += 1;
			}
		}

		return res;
	}

	/**
	 * finds ShapeRoi that corresponds to union two ROIs which also supposed to
	 * be ShapeRois.
	 * 
	 * @param _roi1
	 *            first operand
	 * @param _roi2
	 *            second operand
	 * @return Returns the result of "OR" operation
	 */
	public static ShapeRoi RoiUnion(ShapeRoi _roi1, ShapeRoi _roi2) throws Exception {
		if ((_roi1 == null) || (_roi2 == null)) {
			throw new Exception("Null input in the union function");
		}
		ShapeRoi sroi1 = (ShapeRoi) (_roi1.clone());
		return sroi1.or(_roi2);
	}

	/**
	 * finds Roi that corresponds to Union of array of ROIs
	 * 
	 * @param _rois
	 *            array of input ROIs
	 * @return Returns the result of "OR" operation. Returns null if at least
	 *         one of the input ROIs is null
	 */
	public static Roi RoiUnion(Roi[] _rois) throws Exception {
		if (_rois == null)
			return null;
		int tot_rnum = _rois.length;
		ShapeRoi r_res;

		if (tot_rnum == 0)
			return null;
		r_res = new ShapeRoi(_rois[0]);
		for (int j = 1; j < tot_rnum; j++) {
			r_res = RoiUnion(r_res, new ShapeRoi(_rois[j]));
			if (r_res == null)
				return null;
		}
		return r_res;
	}

	public static boolean isEmptyRoi(Roi _r) {
		if (_r == null)
			return true;
		Rectangle rt = _r.getBounds();
		if ((rt.getWidth() == 0) && (rt.getHeight() == 0))
			return true;

		return false;
	}

	public static boolean isEmptyRoiArr(Roi[] _rs) {
		if (_rs == null)
			return true;
		return (_rs.length == 0);
	}

	public static Roi[] getTranslatedRois(ImageProcessor _ip, Roi _refRoi, Roi[] _posRois) {
		int nr = _posRois.length;
		Roi[] resRois = new Roi[nr];

		_ip.setRoi(_refRoi);
		ImageStatistics stat = _ip.getStatistics();
		double xRefCent = stat.xCentroid;
		double yRefCent = stat.yCentroid;
		double xPos, yPos;
		Roi resR;
		Overlay testO = new Overlay();
		for (int i = 0; i < nr; i++) {
			if (_posRois[i] == null) {
				resRois[i] = null;
			} else {
				testO.clear();
				_ip.setRoi(_posRois[i]);
				stat = _ip.getStatistics();
				xPos = stat.xCentroid;
				yPos = stat.yCentroid;
				resR = (Roi) _refRoi.clone();
				testO.add(resR);
				testO.translate(xPos - xRefCent, yPos - yRefCent);
				resRois[i] = resR;
			}
		}
		return resRois;
	}

	public static Roi FindClosestRoi(ImageProcessor _ip, Roi _refRoi, Roi[] _candRois) {
		int nr = _candRois.length;

		_ip.setRoi(_refRoi);
		ImageStatistics stat = _ip.getStatistics();
		double xRefCent = stat.xCentroid;
		double yRefCent = stat.yCentroid;
		double xCand, yCand;
		double minDist = _ip.getWidth() * _ip.getHeight();
		double candDist;
		Roi closestRoi = null;

		for (int i = 0; i < nr; i++) {
			_ip.setRoi(_candRois[i]);
			stat = _ip.getStatistics();
			xCand = stat.xCentroid - xRefCent;
			yCand = stat.yCentroid - yRefCent;
			candDist = xCand * xCand + yCand * yCand;
			if (candDist < minDist) {
				minDist = candDist;
				closestRoi = _candRois[i];
			}
		}
		return closestRoi;
	}

	public static void addRoisToOvl(Overlay _resOvl, Roi[] _adRois) throws Exception {
		if ((_resOvl == null) || (_adRois == null))
			throw new Exception("ROIManipulator:addRoisToOvl: one of inputs is null");
		for (Roi r : _adRois)
			_resOvl.add(r);

	}

	public static void addRoiListToOvl(Overlay _resOvl, List<Roi> _adRois) throws Exception {
		if ((_resOvl == null) || (_adRois == null))
			throw new Exception("ROIManipulator:addRoiListToOvl: one of inputs is null");
		for (Roi r : _adRois)
			_resOvl.add(r);

	}

	public static void addOvlToOvl(Overlay _resOvl, Overlay _adOvl) throws Exception {
		if ((_resOvl == null) || (_adOvl == null))
			throw new Exception("ROIManipulator:addOvlToOvl: one of overlays is null");
		Roi[] adRois = _adOvl.toArray();
		if (adRois != null)
			addRoisToOvl(_resOvl, adRois);
	}

	public static boolean objOnTheWay(Point2D.Double _st, Point2D.Double _end, Roi _robj) throws Exception {
		return objOnTheWay(_st.getX(), _st.getY(), _end.getX(), _end.getY(), _robj);
	}

	public static boolean objOnTheWay(double _xst, double _yst, double _xend, double _yend, Roi _robj)
			throws Exception {
		if (_robj == null)
			throw new Exception("ROIManipulator.objOnTheWay: object ROI is null");
		Line ln = new Line(_xst, _yst, _xend, _yend);
		ln.setStrokeWidth(1.01);
		FloatPolygon pgon = ln.getFloatPolygon();
		ShapeRoi p_ln = new ShapeRoi(new PolygonRoi(pgon, Roi.POLYGON));
		p_ln.and(new ShapeRoi(_robj));
		return !isEmptyRoi(p_ln);
	}

	public static ByteProcessor makeEDMProcessor(int _iwidth, int _iheight, Roi[] _rs) throws Exception {
		if (_rs == null)
			throw new Exception("ROIManipulator.makeEDMProcessor: ROI array is null");

		if (Edm == null)
			Edm = new EDM();

		IJ.setForegroundColor(255, 255, 255);
		IJ.setBackgroundColor(0, 0, 0);

		ByteProcessor ipr = new ByteProcessor(_iwidth, _iheight);
		ipr.setColor(Color.WHITE);
		for (Roi r : _rs) {
			if (r == null)
				continue;
			if (r.isArea())
				ipr.fill(r);
			else
				r.drawPixels(ipr);
		}
		ipr.resetRoi();
		ipr.invert();
		Edm.toEDM(ipr);

		return ipr;
	}

	public static ByteProcessor makeEDMProcessor(int _iwidth, int _iheight, Roi _r) throws Exception {
		if (_r == null)
			throw new Exception("ROIManipulator.makeEDMProcessor: ROI object is null");
		return makeEDMProcessor(_iwidth, _iheight, new Roi[] { _r });
	}

	public static ByteProcessor makeEDMProcessor(int _iwidth, int _iheight, double _dotX, double _dotY)
			throws Exception {
		return makeEDMProcessor(_iwidth, _iheight, new PointRoi(_dotX, _dotY));
	}

	public static boolean isParticleInside(Roi _outer, Roi _inner) {
		Rectangle rect = _inner.getBounds();
		double x = rect.getCenterX();
		double y = rect.getCenterY();

		return _outer.contains((int) x, (int) y);
	}

	public static Roi selBiggestRoi(ImagePlus _img, Roi[] _rcand) {
		if (_rcand == null)
			return null;
		if (_rcand.length == 0)
			return null;

		Roi br = null;
		double bsize = 0;
		double cur_sz;
		for (Roi rg : _rcand) {
			_img.setRoi(rg);
			cur_sz = _img.getStatistics(ImageStatistics.AREA).area;
			if (cur_sz > bsize) {
				bsize = cur_sz;
				br = rg;
			}
		}
		return br;
	}

	public static Roi getRandRoi(Roi[] _candRois) {
		if (_candRois == null)
			return null;
		if (_candRois.length == 0)
			return null;
		if (rndGen == null)
			rndGen = new Random();
		return _candRois[rndGen.nextInt(_candRois.length)];
	}

	public static void paintRoisStroke(Roi[] _rois, Color _newStrokeColor) {
		if (_rois == null)
			return;
		for (Roi r : _rois)
			r.setStrokeColor(_newStrokeColor);
	}

	/**
	 * Converts mask image (2D, stack or hyperstack) to the set of ROIs
	 * 
	 * @param _maskImage
	 *            is an input binary image 2-5D (multichannel is possible)
	 * @return array of ROIs. For stack and hyperstack each ROI saves it's
	 *         position index(es).
	 */
	public static Roi[] maskToRois(ImagePlus _maskImage) {
		return maskToRois(_maskImage,false);
	}
	
	public static Roi[] maskToRois(ImagePlus _maskImage,boolean _excludeOnEdges) {

		List<Roi> roiList = new ArrayList<Roi>();

		RoiManager roiManager = getEmptyRm();
		int analyzerOptions = ParticleAnalyzer.ADD_TO_MANAGER | ParticleAnalyzer.SHOW_NONE;
		if(_excludeOnEdges) {
			analyzerOptions = analyzerOptions | ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES;
		}
		boolean hyperStack = _maskImage.isHyperStack();
		// IJ.setThreshold(_maskImage, 128, 255);
		ImageStack maskStack = _maskImage.getStack();
		int nStackSlices = maskStack.getSize();
		ParticleAnalyzer particleAnalyzer = new ParticleAnalyzer(analyzerOptions, 0, null, 0, Double.POSITIVE_INFINITY,
				0, 1);

		Roi[] sliceRois;
		int[] hyperstackPosition;
		ImageProcessor currentProcessor;
		for (int sliceIndex = 1; sliceIndex <= nStackSlices; sliceIndex++) {
			currentProcessor = maskStack.getProcessor(sliceIndex);
			if (!currentProcessor.isBinary())
				throw new IllegalArgumentException("ROIManipulator2D.maskToRois: wrong format of input image");
			currentProcessor.setThreshold(128, 255, currentProcessor.getLutUpdateMode());
			particleAnalyzer.analyze(_maskImage, currentProcessor);
			currentProcessor.resetThreshold();
			sliceRois = roiManager.getRoisAsArray();
			if (isEmptyRoiArr(sliceRois))
				continue;
			for (Roi roi : sliceRois) {
				if (hyperStack) {
					hyperstackPosition = _maskImage.convertIndexToPosition(sliceIndex);
					roi.setPosition(hyperstackPosition[0], hyperstackPosition[1], hyperstackPosition[2]);
				} else if (nStackSlices > 1) {
					roi.setPosition(sliceIndex);
				} else {
					roi.setPosition(0);
				}
				roiList.add(roi);
			}

			roiManager.reset();
		}

		// IJ.resetThreshold(_maskImage);

		return roiList.toArray(new Roi[roiList.size()]);
	}

	public static ImagePlus roiToMask(Roi _roi, ImagePlus _referenceImage) {

		ImagePlus maskImage=IJ.createImage("Mask Image", "8-bit black", _referenceImage.getWidth(), _referenceImage.getHeight(), 1);
		ImageProcessor maskProcessor=maskImage.getProcessor();
		maskProcessor.setColor(255);
		maskProcessor.fill(_roi);
		
		return maskImage;
	}

	public static ImagePlus roisToMask(Roi[] _rois, ImagePlus _referenceImage) {

		ImagePlus maskImage=IJ.createImage("Mask Image", "8-bit black", _referenceImage.getWidth(), _referenceImage.getHeight(), 1);
		ImageProcessor maskProcessor=maskImage.getProcessor();
		maskProcessor.setColor(255);
		for (Roi roi:_rois) {
			maskProcessor.fill(roi);
		}
		return maskImage;
	}

	
	public static ShapeRoi getFrameRoi(ImagePlus _image, int _width) throws Exception {
		if (_image == null)
			throw new Exception("ROIManipulator2D.getFrameRoi: input image can not be null");

		ShapeRoi outerFrame = new ShapeRoi(new Roi(0, 0, _image.getWidth(), _image.getHeight()));

		ShapeRoi innerFrame = new ShapeRoi(
				new Roi(_width, _width, _image.getWidth() - 2 * _width, _image.getHeight() - 2 * _width));

		return outerFrame.not(innerFrame);

	}
}
