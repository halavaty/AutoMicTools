package automic.utils.roi;

import java.awt.Color;
import java.awt.Polygon;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.Timer;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.ImageCanvas;
import ij.gui.ImageWindow;
import ij.gui.OvalRoi;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.Toolbar;

public class ManualRoiDefiner {

	//region keys
	public static final String CIRCLE_REGION="circle";
	public static final String RECTANGLE_REGION="rectangle";
	public static final String POLYGON_REGION="polygon";

	
	private int initialLocationX=1000;
	private int initialLocationY=120;
	private int responceTime=30;
	private boolean useRoisTimeEnd=true;
	private boolean canMoveRoi=false;
	
	private String regionType=CIRCLE_REGION;
	private int bleachCircleRadius=40;
	
	private ImagePlus currentImage;
	private SelectionManadgement selectedRoiManager;
	
	public ManualRoiDefiner() {}

	public ManualRoiDefiner(int _imageLocationX,int _imageLocationY, int _responceTimeSeconds, boolean _useRoisTimeEnd, boolean _canMoveRoi, String _regionType, int _bleachCircleRadius) {
		initialLocationX=_imageLocationX;
		initialLocationY=_imageLocationY;
		responceTime=_responceTimeSeconds;
		useRoisTimeEnd=_useRoisTimeEnd;
		canMoveRoi=_canMoveRoi;
		regionType= _regionType;
		bleachCircleRadius=_bleachCircleRadius;
	}
	
	
	public Roi[] getManualRois(ImagePlus _inputImage)throws Exception {
		currentImage=_inputImage;
		
		selectedRoiManager= new SelectionManadgement();
		
		ImageWindow.setNextLocation(initialLocationX,initialLocationY);
		
		setImageJDrawingTool();
		

		//selecting photomanipulation regions
		
		currentImage.show();
		currentImage.getWindow().setAlwaysOnTop(true);
		
		currentImage.getWindow().setTitle("Selection Image");
		
		
		RoiMouseListener roiSelectionListener=new RoiMouseListener();
		RoiKeyboardListener roiKeyboardListener=new RoiKeyboardListener();
		ImageCanvas imgCanvas=currentImage.getCanvas();
		
		for( KeyListener kl : imgCanvas.getKeyListeners() ) {
	        imgCanvas.removeKeyListener( kl );
	    }
		
		
		imgCanvas.addMouseListener(roiSelectionListener);
		imgCanvas.addKeyListener(roiKeyboardListener);
		boolean selectionMade=getSelectionMadeFromDialog(currentImage.getWindow(),responceTime*1000);
		
		imgCanvas.removeMouseListener(roiSelectionListener);
		imgCanvas.removeKeyListener(roiKeyboardListener);
		
		currentImage.hide();

		
		//checking if selections were performed
		if ( selectedRoiManager.selectedRegions.isEmpty()) return null;

		if (!selectionMade) return null;

		return selectedRoiManager.getSelectedRois();

	}
	
	private void setImageJDrawingTool() {
		switch(regionType) {
			case CIRCLE_REGION: IJ.setTool("point"); break;
			case RECTANGLE_REGION: IJ.setTool(Toolbar.RECTANGLE); break;
			case POLYGON_REGION: IJ.setTool(Toolbar.POLYGON); break;
			default: throw new RuntimeException ("Region type specified wrongly");
		}

	}
	
	private int getExpectedSelectionType() {
		switch(regionType) {
			case CIRCLE_REGION: return Roi.POINT;
			case RECTANGLE_REGION: return Roi.RECTANGLE;
			case POLYGON_REGION: return Roi.POLYGON;
			default: throw new RuntimeException ("Region type specified wrongly");
		}

	}

	
	private Roi convertSelectedRoiToOverlayRoi(Roi _selectedRoi) {
		Roi overlayRoi=null;

		if(_selectedRoi.getType()!=getExpectedSelectionType())
			throw new RuntimeException ("Region does not match specified type");

		
		switch(regionType) {
			case CIRCLE_REGION: 
				
				Polygon pointPolygon=_selectedRoi.getPolygon();
				int centerX=pointPolygon.xpoints[0];
				int centerY=pointPolygon.ypoints[0];

				overlayRoi=new OvalRoi(centerX-bleachCircleRadius, centerY-bleachCircleRadius, 2*bleachCircleRadius, 2*bleachCircleRadius);
				break;
				
			case RECTANGLE_REGION: 
				overlayRoi=_selectedRoi;
				break;
			
			case POLYGON_REGION: 
				overlayRoi=_selectedRoi;
				break;
			
			default: throw new RuntimeException ("Region type specified wrongly");
		}
		
		overlayRoi.setStrokeColor(Color.pink);
		return overlayRoi;

	}

	
	class RoiMouseListener implements MouseListener{
		
		//MouseListener methods
		@Override
		public void mouseClicked(MouseEvent e) {}
		
		@Override
		public void mousePressed(MouseEvent e) {}
		
		@Override
		public void mouseReleased(MouseEvent e) {
			if (canMoveRoi)
				return;
			catchSelectedRoi(currentImage);
		}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}

	}
	
	class RoiKeyboardListener implements KeyListener{
		//KeyListener methods
		@Override
		public void  keyPressed(KeyEvent e) {
			
			int keyCode=e.getKeyCode();
			
			switch(keyCode) {
				case KeyEvent.VK_LEFT: //react on left arrow Key
					//remove last selected region
					selectedRoiManager.removeLasetSelectedRoi();
					selectedRoiManager.updateSelectionDisplay();
					break;

				case KeyEvent.VK_RIGHT: //react on right arrow Key
					//confirm region selection in case of "Can move Roi" mode
					if (!canMoveRoi)
						return;
					catchSelectedRoi(currentImage);
					break;

					
				default:return;
			}
		}
		
		@Override
		public void  keyReleased(KeyEvent e) {}

		@Override
		public void  keyTyped(KeyEvent e) {}

	}
	
	class SelectionManadgement{//singleton class to manage multiple seletions
		
		public List<Roi> selectedRegions;
		double[] xPositions;
		double[] yPositions;
		double[] radiusValues;

		
		
		public SelectionManadgement() {
			selectedRegions=new ArrayList<Roi>();
		}
		
		public void removeLasetSelectedRoi() {
			int nRegionsOld=selectedRegions.size();
			if (nRegionsOld<1)
				return;
			
			selectedRegions.remove(nRegionsOld-1);
		}
		
		
		public void updateCommandArrays(){
			xPositions=new double[selectedRegions.size()];
			yPositions=new double[selectedRegions.size()];
			radiusValues=new double[selectedRegions.size()];
			
			
			for (int iRoi=0;iRoi<selectedRegions.size();iRoi++){
				Rectangle bounds=selectedRegions.get(iRoi).getBounds();
				xPositions[iRoi]=bounds.getCenterX();
				yPositions[iRoi]=bounds.getCenterY();
				radiusValues[iRoi]=bleachCircleRadius;
			}
		}
		
		public void updateSelectionDisplay() {
			Overlay o=createOverlay();
			currentImage.setOverlay(o);
			IJ.log(String.format("%d ROIs are currently selected",selectedRoiManager.selectedRegions.size()));
		}
		
		public Roi[] getSelectedRois() {
			return selectedRegions.toArray(new Roi[selectedRegions.size()]);
		}

	}
	
	protected Overlay createOverlay(){
		Overlay o=new Overlay();
		List<Roi> selectedRegions=selectedRoiManager.selectedRegions;
		if (selectedRegions.isEmpty())
			return o;
		
		for(Roi selectedRegion:selectedRegions) {
			o.add(convertSelectedRoiToOverlayRoi(selectedRegion));
		}
		
		
		return o;
	}


	
	protected void catchSelectedRoi(ImagePlus _img) {
		
		Roi selectedRoi=_img.getRoi();
		
		if (selectedRoi==null)
			return;
		
		// check if ROI drawing is completed
		if(selectedRoi.getState()!=Roi.NORMAL)
			return;
		
		selectedRoiManager.selectedRegions.add(selectedRoi);
		_img.setRoi((Roi)null);
		selectedRoiManager.updateSelectionDisplay();

	}

	private boolean getSelectionMadeFromDialog(ImageWindow imageWindow,int _maximumDelayTimeMillis)throws Exception {
		final JOptionPane msg = new JOptionPane("Confirm with OK when regions are selected or press Cancel to skip all regions", JOptionPane.QUESTION_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
	    final JDialog dlg = msg.createDialog("Select region");
	    
	    dlg.setLocation(100, 100);
        dlg.setModal(false);
	    dlg.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
	    dlg.addComponentListener(new ComponentAdapter() {
	            
    		@Override
            public void componentShown(ComponentEvent e) {
                super.componentShown(e);
                final Timer t = new Timer(_maximumDelayTimeMillis,new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        dlg.setVisible(false);
                    }
                });
                t.start();
            }
        });
        dlg.setVisible(true);
        
        while(dlg.isVisible())
        	TimeUnit.MILLISECONDS.sleep(50);
        Object dialogValue=msg.getValue();
        
        if(dialogValue.equals(JOptionPane.OK_OPTION))
        	return true;
        if(dialogValue.equals(JOptionPane.CANCEL_OPTION))
        	return false;
        
		return useRoisTimeEnd;
	}



}
