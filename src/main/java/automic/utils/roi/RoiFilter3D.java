package automic.utils.roi;

import automic.utils.ArrIndUtils;
import ij.ImagePlus;
//import ij.measure.Calibration;
import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;
import mcib3d.image3d.ImageHandler;

public class RoiFilter3D {
	//constants for filtration methods
	public static final int AREA=1;
	public static final int VOLUME=2;
	public static final int INTEGRATED=4;
	public static final int MAX=8;
	public static final int RANGE=16;
	public static final int MEDIAN=32;
	public static final int MEAN=64;
	public static final int BORDER_IMG_XY=128;
	public static final int BORDER_IMG_Z=256;
	public static final int BORDER_OBJ=512;
	
	
	public static final int CLOSEST_DIST=1024;
	
	protected ImagePlus img=null;
	protected ImageHandler IHand=null;
	protected Objects3DPopulation objects=null;
	protected boolean[] Passed;
	protected int nObj;
	
	public RoiFilter3D(Objects3DPopulation _r3pop){
		if (_r3pop==null)
			objects=new Objects3DPopulation();
		else
			objects=_r3pop;
		nObj=objects.getNbObjects();
		
		Passed=new boolean[nObj];
		for (int i=0;i<nObj;i++)
			Passed[i]=true;
	}
	
	public Objects3DPopulation getObjects(){
		return objects;
	}
	
	public void setImagePlus(ImagePlus _img) throws Exception{
		if ((_img.getNDimensions()>3)||(_img.getNChannels()>1)||(_img.getNFrames()>1))
			throw new Exception ("RoiFilter3D:Wrong dimensions of input image");
		img=_img;
		IHand=ImageHandler.wrap(img);
	}
	
	public void setImageHandler(ImageHandler _ihandl) throws Exception{
		//to do: set dimensionality check?
		IHand=_ihandl;
		img=IHand.getImagePlus();
	}
	
	public boolean[] getPassedFlags(){
		return Passed;
	}
	
	public int[] getPassedInds(){
		return ArrIndUtils.getTrueInd(Passed);
	};
	
	public int getNObjects(){
		return nObj;
	}
	
	public int getNPassed(){
		return ArrIndUtils.getNTrue(Passed);
	}
	
	public Objects3DPopulation getPassedPopulation(){
		Objects3DPopulation outputPopulation=new Objects3DPopulation();
		
		for (int objectIndex=0; objectIndex<nObj;objectIndex++){
			if (Passed[objectIndex])
				outputPopulation.addObject(objects.getObject(objectIndex));
		}
		
		return outputPopulation;
	}
	
	public void setPassedFlags(boolean[] _flags)throws Exception{
		if (_flags.length!=nObj)
			throw new Exception ("Wrong array dimension");
		for (int i=0;i<nObj;i++)
			Passed[i]=_flags[i];
	}
	
	public void setPassedInds(int[] _inds)throws Exception{
		if (ArrIndUtils.getMaxInt(_inds)>nObj)
			throw new Exception("Wrong range of indexes");
		Passed=ArrIndUtils.getFlagsArr(_inds, nObj);
	}
	
	public void filterThr(int _filter, double _minv, double _maxv, boolean _inPixels)throws Exception{
		
		switch (_filter){
			case AREA:		this.filterThrArea(_minv, _maxv, _inPixels);	break;
			case VOLUME:	this.filterThrVolume(_minv, _maxv, _inPixels);	break;
			case INTEGRATED:this.filterThrIntegrated(_minv, _maxv, _inPixels);	break;
			case MEAN:		this.filterThrMean(_minv, _maxv);	break;
			case MAX: 		this.filterThrMax(_minv, _maxv);	break;
			case MEDIAN: 	this.filterThrMedian(_minv, _maxv);	break;
			case RANGE: 	this.filterThrRange(_minv, _maxv);	break;
			case BORDER_IMG_XY:	this.filterThrBorderImgXY(_minv, _maxv, _inPixels);	break;
			case BORDER_IMG_Z:	this.filterThrBorderImgZ (_minv, _maxv, _inPixels);	break;
			case BORDER_OBJ:	this.filterThrBorderObj (_minv, _maxv, _inPixels);	break;
			default:	throw new Exception("Undefined filter index");
		}
	}
	
	

	private void filterThrArea(double _min,double _max,boolean _inPixels){
		double val;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				if (_inPixels)
					val=objects.getObject(i).getAreaPixels();
				else
					val=objects.getObject(i).getAreaUnit();
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrVolume(double _min,double _max,boolean _inPixels){
		double val;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				if (_inPixels)
					val=objects.getObject(i).getVolumePixels();
				else
					val=objects.getObject(i).getVolumeUnit();
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrIntegrated(double _min,double _max,boolean _inPixels){
		double val;
		Object3D obj;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				obj=objects.getObject(i);
				val=obj.getIntegratedDensity(IHand);
				if(!_inPixels)
					val=val/obj.getVolumePixels()*obj.getVolumeUnit();
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrMean(double _min,double _max){
		double val;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=objects.getObject(i).getPixMeanValue(IHand);
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrMax(double _min,double _max){
		double val;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=objects.getObject(i).getPixMaxValue(IHand);
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrMedian(double _min,double _max){
		double val;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=objects.getObject(i).getPixMedianValue(IHand);
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrRange(double _min,double _max){
		double val;
		Object3D obj;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				obj=objects.getObject(i);
				val=obj.getPixMaxValue(IHand)-obj.getPixMinValue(IHand);
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrBorderImgXY(double _min,double _max,boolean _inPixels){
		double val;
		Object3D obj;
		int [] distarr=new int[4]; 
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				obj=objects.getObject(i);
				distarr[0]=obj.getXmin();
				distarr[1]=obj.getYmin();
				distarr[2]=IHand.sizeX-obj.getXmax()-1;
				distarr[3]=IHand.sizeY-obj.getYmax()-1;
				val=ArrIndUtils.getMinInt(distarr);
				if(!_inPixels)
					val=val*IHand.getScaleXY();
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrBorderImgZ(double _min,double _max,boolean _inPixels){
		double val;
		Object3D obj;
		int [] distarr=new int[2];
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				obj=objects.getObject(i);
				distarr[0]=obj.getZmin();
				distarr[1]=IHand.sizeZ-obj.getZmax()-1;
				val=ArrIndUtils.getMinInt(distarr);
				if(!_inPixels)
					val=val*IHand.getScaleZ();
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrBorderObj(double _min,double _max,boolean _inPixels){
		if(nObj<2) return; //do not check neighbours if there is only one objects
		
		//Calibration tempCalib=null;
		//if (_inPixels){
		//	tempCalib=objects.getCalibration();
		//	objects.setCalibration(new Calibration());
		//}
		
		double val;
		Object3D targ_obj,closest_obj;

		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				targ_obj=objects.getObject(i);
				closest_obj=objects.closestBorder(targ_obj);
				if (_inPixels)
					val=targ_obj.distBorderPixel(closest_obj);
				else
					val=targ_obj.distBorderUnit(closest_obj);
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
		
		//if (_inPixels)
		//	objects.setCalibration(tempCalib);
	}
	
	public void filterDistanceRefPopulation(Objects3DPopulation _referencePopulation, double _minDistance,double _maxDistance,boolean _inPixels){
		if ((_referencePopulation==null)||(_referencePopulation.getNbObjects()<1)){
			if (Double.isInfinite(_maxDistance))
				return;
			else{
				this.setAllPassedFlags(false);
				return;
			}
		}
		
		double distance;
		Object3D targetObject,closestReferenceObject;

		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				targetObject=objects.getObject(i);
				closestReferenceObject=_referencePopulation.closestBorder(targetObject);
				if (_inPixels)
					distance=targetObject.distBorderPixel(closestReferenceObject);
				else
					distance=targetObject.distBorderUnit(closestReferenceObject);
				Passed[i]=(distance>=_minDistance)&&(distance<=_maxDistance);
			}
		}
		
	}
	
	public void filterDistanceRefObject(Object3D _referenceObject, double _minDistance,double _maxDistance,boolean _inPixels){
		if (_referenceObject==null){
			if (Double.isInfinite(_maxDistance))
				return;
			else{
				this.setAllPassedFlags(false);
				return;
			}
		}
		
		double distance;
		Object3D targetObject;
		for (int i=0;i<nObj;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				targetObject=objects.getObject(i);
				if (_inPixels)
					distance=targetObject.distBorderPixel(_referenceObject);
				else
					distance=targetObject.distBorderUnit(_referenceObject);
				Passed[i]=(distance>=_minDistance)&&(distance<=_maxDistance);
			}
		}
		
	}
	
	private void setAllPassedFlags(boolean _value){
		for (int i=0;i<nObj;i++)
			Passed[i]=_value;
	}

}
