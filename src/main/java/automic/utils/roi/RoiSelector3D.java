package automic.utils.roi;

import java.io.File;
import java.util.Random;

import automic.utils.ArrIndUtils;
import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;

public class RoiSelector3D extends RoiFilter3D{
	//default string to store selection information inside ROIs
	public static final String selTag="Selected";
	public static final String passTag="Passed";
	public static final String nonFiltTag="";
	
	
	protected boolean[] Selected;
	
	public RoiSelector3D(Objects3DPopulation _r3pop){
		super(_r3pop);
		Selected=new boolean[nObj];
		for (int i=0;i<nObj;i++)
			Selected[i]=false;
	}
	
	public RoiSelector3D(File _fl) throws Exception{
		super(ROIManipulator3D.fileToPopulation(_fl));
		Selected=new boolean[nObj];
		for (int i=0;i<nObj;i++){
			Selected[i]=objects.getObject(i).getComment().equals(selTag);
			if (Selected[i]) continue;
			Passed[i]=objects.getObject(i).getComment().equals(passTag);
		}
	}
	
	public void setCommentTags(){
		Object3D curObj;
		for (int i=0;i<nObj;i++){
			curObj=this.objects.getObject(i);
			if(Selected[i]){
				curObj.setComment(selTag);
				continue;
			}
			if(Selected[i]){
				curObj.setComment(passTag);
				continue;
			}
			curObj.setComment(nonFiltTag);
		}
	}
	
	public void saveWithTags(String _pth, String _fnmNoExt){
		this.setCommentTags();
		ROIManipulator3D.populationToFile(_pth, _fnmNoExt, this.getObjects());
	}
	
	public boolean[] getSelectedFlags(){
		return Selected;
	}
	
	public int[] getSelInds(){
		return ArrIndUtils.getTrueInd(Selected);
	}
	
	public int getNSelected(){
		return ArrIndUtils.getNTrue(Selected);
	}
	
	/**
	 * 
	 * @param _nsel number of objects to select
	 */
	public void selectRandomObjects(int _nsel)throws Exception{
		Random rand=new Random();
		
		int[] passedInds=this.getPassedInds();
		int nPassed=passedInds.length;
		
		if (nPassed<_nsel)
			throw new Exception("RoiSelector3D.selectObjects: specified number of objects to select bigger that number of available objects");
		
		for (int i=0;i<nObj;i++)
			Selected[i]=false;
		
		int testInd;
		for (int i=0;i<_nsel;i++){
			testInd=passedInds[rand.nextInt(nPassed)];

			while(Selected[testInd])
				testInd=passedInds[rand.nextInt(nPassed)];
			
			Selected[testInd]=true;
		}
	}
	
	
	public Object3D getSelectedObject(int _ind){
		int[] selInds=ArrIndUtils.getTrueInd(Selected);
		if(selInds.length<=_ind)
			return null;
		return objects.getObject(selInds[_ind]);
	}
}
