package automic.utils.roi;

import java.io.File;
import java.util.zip.ZipFile;

import ij.gui.Overlay;

public class ROIManipulator{
	public static final String fileExt="zip";
	
	public static String getZippedExt(File _fl)throws Exception{
		if (_fl==null) throw new Exception("Null File input");
		ZipFile zipFl=new ZipFile(_fl);
		String nm=zipFl.entries().nextElement().getName();
		nm=nm.substring(nm.lastIndexOf('.')+1);
		zipFl.close();
		return nm;
	}
	
	public static String getZippedComment(File _fl)throws Exception{
		if (_fl==null) throw new Exception("Null File input");
		ZipFile zipFl=new ZipFile(_fl);
		String comment=zipFl.getComment();
		zipFl.close();
		return comment;
	}
	
	public static Overlay flsToOverlay(File _fl)throws Exception{
		String roiExt=getZippedExt(_fl);
		if(roiExt.equals("roi")){
			return ROIManipulator2D.flsToOverlay(_fl);
		}else if (roiExt.equals("3droi")){
			RoiSelector3D rsel=new RoiSelector3D(_fl);
			return ROIManipulator3D.selectorToOverlay(rsel);
		}else
			throw new Exception("Undefined type of ROI files");
	}
	
	public static Overlay flsToOverlay(String _pth, String _fnmNoExt)throws Exception{
		return flsToOverlay(new File(_pth,_fnmNoExt+"."+fileExt));
	}

}
