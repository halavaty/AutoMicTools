package automic.utils.roi;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.filter.ThresholdToSelection;
import ij.process.ByteProcessor;
import mcib3d.geom.Object3D;
import mcib3d.geom.Objects3DPopulation;
//import mcib_plugins.tools.RoiManager3D_2;

import java.awt.Color;
import java.awt.Rectangle;
import java.io.File;

import automic.utils.Utils;
import automic.utils.imagefiles.TIFProcessorImageJ;



/**
 * static methods for manipulating 3d ROIs. Uses 3d ImageJ suite (mcib3d) as a major dependency
 * @author Aliaksandr Halavatyi
 *
 */
public class ROIManipulator3D/* extends ROIManipulator */{
	public final static String fileExt="zip";

	//default colours for filtered ROIs
	public static Color SEL_COLOR=Color.YELLOW;
	public static Color PASS_COLOR=Color.CYAN;
	public static Color NONFILT_COLOR=Color.RED;
	

	public static Objects3DPopulation fileToPopulation(File _fl){
		Objects3DPopulation r3pop=new Objects3DPopulation();
		r3pop.loadObjects(_fl.getAbsolutePath());
		return r3pop;
	}
	
	/**
	 * 
	 * @param _pth
	 * @param _fileNmNoExt
	 */
	public static Objects3DPopulation fileToPopulation(String _pth, String _fileNmNoExt){
		return fileToPopulation(new File(_pth,_fileNmNoExt+"."+fileExt));
	}
	
	/**
	 * Based on mcib_plugins.tools.RoiManager3D_2.computeRois()
	 * @param _r3pop
	 * @param _selInd
	 * @param _col
	 * @return
	 */
	public static Overlay populationToOverlay(Objects3DPopulation _r3pop, Color _col){
		Overlay ovl=new Overlay();
		if (_r3pop==null||_r3pop.getNbObjects()<1) return ovl;

		//Object3D[] objs=r3pop.getObjectsArray();
		int nObj=_r3pop.getNbObjects();//objs.length;
		
		for (int i=0;i<nObj;i++){
			addObjectToOverlay(_r3pop.getObject(i), ovl,_col);
		}
		return ovl;
	}
	
	public static Overlay selectorToOverlay(RoiSelector3D _selector){
		Overlay ovl=new Overlay();
		Objects3DPopulation objs=_selector.getObjects();
		int nObj=objs.getNbObjects();
		boolean[] passedFlags=_selector.getPassedFlags();
		
		if (passedFlags.length!=nObj){
			Utils.generateErrorMessage("ROIManipulator3D.filterToOvl error", "Array dimensions do not match");
			return ovl;
		}
		boolean[] selFlags=_selector.getSelectedFlags();
		
		for (int i=0;i<nObj;i++){
			if (!passedFlags[i]){
				addObjectToOverlay(objs.getObject(i),ovl,NONFILT_COLOR);
				continue;
			}
			
			if (selFlags[i])
				addObjectToOverlay(objs.getObject(i),ovl,SEL_COLOR);
			else
				addObjectToOverlay(objs.getObject(i),ovl,PASS_COLOR);
		}
		
		
		return ovl; 
	}
	
	public static void addObjectToOverlay(Object3D _obj, Overlay _ovl,Color _col){
		int zmin,zmax;
		zmin=_obj.getZmin();
		zmax=_obj.getZmax();
		Roi tempRoi=null;

		int xPosition=_obj.getXmin();
        int yPosition=_obj.getYmin();
        int width =_obj.getXmax() - xPosition + 1;
        int height =_obj.getYmax() -yPosition + 1;

        _obj.translate(-xPosition, -yPosition, 0);
		
        ByteProcessor roiMaskProcessor = new ByteProcessor(width, height);
        roiMaskProcessor.setColor(0);
        
		for (int z=zmin;z<=zmax;z++){
			roiMaskProcessor.fill();

	        _obj.draw(roiMaskProcessor, z, 255);
	        if (roiMaskProcessor.getStatistics().max<2) continue;
	        roiMaskProcessor.setThreshold(1, 255, roiMaskProcessor.getLutUpdateMode());
	       
	        ThresholdToSelection tts = new ThresholdToSelection();
	        tempRoi=tts.convert(roiMaskProcessor);
	        
	        Rectangle shiftedBounds=tempRoi.getBounds();
	        tempRoi.setLocation(xPosition+shiftedBounds.x, yPosition+shiftedBounds.y);
			
			//tempRoi=createRoi(_obj,z);//tempObj.createRoi(z+1);
			tempRoi.setPosition(0, z+1, 0);
			tempRoi.setPosition(z+1);
			tempRoi.setStrokeColor(_col);
			_ovl.add(tempRoi);
		}
		
		//put object back to place
        _obj.translate(xPosition, yPosition, 0);
	}
	
	/**
	 * copied from Object3DVoxels and modified. Code in mcib seems to be outdated for this function
	 * @param z
	 * @return
	 */
//    public static Roi createRoi(Object3D _obj,int _z) {
//        // IJ.write("create roi " + z);
//        int sx =_obj.getXmax()+1;// _obj.getXmax() - _obj.getXmin() + 1;
//        int sy =_obj.getYmax()+1;//  _obj.getYmax() -_obj.getYmin() + 1;
//        ByteProcessor mask = new ByteProcessor(sx, sy);
//        // object black on white
//        //mask.invert();
//        _obj.draw(mask, _z, 255);
//        if (mask.getStatistics().max<2) return null;
//        mask.setThreshold(1, 255, mask.getLutUpdateMode());
//        ImagePlus maskPlus = new ImagePlus("mask " + _z, mask);
//        //maskPlus.show();
//        //IJ.run("Create Selection");
//       
//        ThresholdToSelection tts = new ThresholdToSelection();
//        tts.setup("", maskPlus);
//        tts.run(mask);
//        //maskPlus.updateAndDraw();
//        // IJ.write("sel=" + maskPlus.getRoi());
//        //maskPlus.hide();
//        Roi roi = maskPlus.getRoi();
//        //Rectangle rect = roi.getBounds();
//        //rect.x += _obj.getXmin();
//        //rect.y += _obj.getYmin();
//        
//        return roi;
//    }
	
	public static void populationToFile(String _pth, String _fileNmNoExt,Objects3DPopulation _r3pop){
		// fast fix: change object names if empty
		Object3D obj;
		int counter=0;
		
		int nobj=_r3pop.getNbObjects();
		for(int i=0;i<nobj;i++){
			obj=_r3pop.getObject(i);
			if(obj.getName().equals("")){
				obj.setName("obj"+counter);
				counter++;
			}
		}
		
		//
		_r3pop.saveObjects(new File(_pth,_fileNmNoExt+"."+fileExt).getAbsolutePath());
		
	}
	
	public static Object3D selectBiggestObject(Objects3DPopulation _candidateObjects) {
		if (_candidateObjects == null)
			return null;
		int nCanditateObjects=_candidateObjects.getNbObjects();
		if (nCanditateObjects == 0)
			return null;

		Object3D biggestObject = null;
		double biggestSize = 0;
		double currentSize;
		Object3D currentObject = null;
		for (int i=0; i<nCanditateObjects;i++) {
			currentObject=_candidateObjects.getObject(i);
			currentSize = currentObject.getVolumePixels();
			if (currentSize > biggestSize) {
				biggestSize = currentSize;
				biggestObject = currentObject;
			}
		}
		return biggestObject;
	}

/**	
	public static void objToRMnan3D(Objects3DPopulation _r3pop){
		 RoiManager3D_2 roimanager = new RoiManager3D_2();
 		 roimanager.setVisible(false);
 
 		 roimanager.addObjects3DPopulation(_r3pop);

 		 roimanager.setVisible(true);
	}
**/	
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = TIFProcessorImageJ.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// path to the test folder
		String pth="C:/tempDat/3DRoiTests";
		String inRoi3Fnm="testROIs3D";
		String inImgMaskFnm="test16mask.tif";

		ImagePlus imageMask=null;
		try{
			imageMask=TIFProcessorImageJ.openTIFImage(new File(pth,inImgMaskFnm)); 
		}catch (Exception ex){
			IJ.error("Can not open test file");
			return;
		}
		ImagePlus imageMaskCopy=imageMask.duplicate();
		imageMask.show();
		
		
		System.out.println(imageMask.getNChannels());
		System.out.println(imageMask.getNSlices());
		System.out.println(imageMask.getNFrames());
		
		IJ.resetMinAndMax(imageMask);
		
		Objects3DPopulation pp=fileToPopulation(pth, inRoi3Fnm);
		Overlay ovl=populationToOverlay(pp,Color.PINK);
		imageMask.setOverlay(ovl);
		
		//test comment functionality
		Object3D obj=pp.getObject(0);
		String comment=obj.getComment();
		System.out.println("Comment is: "+comment);
		obj.setComment("new comment");
		System.out.println("New comment is: "+obj.getComment());
		
		String inRoi3Fnm_com=inRoi3Fnm+"comment";
		ROIManipulator3D.populationToFile(pth, inRoi3Fnm_com, pp);
		
		Objects3DPopulation pp_new=ROIManipulator3D.fileToPopulation(pth, inRoi3Fnm_com);
		Object3D obj_new=pp_new.getObject(0);
		System.out.println("Comment after opening and saving file is: "+obj_new.getComment());
		
		System.out.println("Object value: "+obj_new.getValue());
		System.out.println("Object type: "+obj_new.getClass());
		System.out.println("Object type: "+obj_new.getClass());
		
		imageMaskCopy.show();
		IJ.resetMinAndMax(imageMaskCopy);
		Overlay overlay=new Overlay();
		addObjectToOverlay(obj_new, overlay, Color.ORANGE);
		imageMaskCopy.setOverlay(overlay);
		
		//Object3D obj_clone=obj_new
				
//		try{
//			SaveTIF(img,new File(pth,out_fnm));
//		}catch (Exception ex){
//			IJ.error("Can not save file");
//			return;
//		}
		
//		img.changes=false;
//		img.close();
		
//		try{
//			img=OpenTIF(new File(pth,out_fnm));
//		}catch (Exception ex){
//			IJ.error("Can not open new file");
//			return;
//		}
//		img.show();
	}
}
