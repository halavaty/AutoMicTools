package automic.utils.roi;
import java.awt.Polygon;
import java.util.Random;

import automic.utils.ArrIndUtils;
import ij.IJ;
import ij.gui.Roi;
import ij.gui.ShapeRoi;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import ij.measure.Calibration;
import ij.plugin.frame.RoiManager;



/**
 * 
 * Class for filtering particles based on range of parameters
 * 
 * @author Aliaksandr Halavatyi
 * Last Update: 10.04.2018
 *
 */
public class ParticleFilterer{
	//constants for filtration methods
	public static final int AREA=1;
	public static final int INTEGRATED=2;
	public static final int MAX=4;
	public static final int MIN=5;
	public static final int BORDER=8;
	public static final int RANGE=16;
	public static final int MEAN=32;
	public static final int MEDIAN=33;
	
	public static final int ASPECT_RATIO=64;
	public static final int LONG_AXIS=65;
	public static final int SHORT_AXIS=66;
	public static final int CIRCULARITY=67;
	public static final int SOLIDITY=68;
	
	
	
	private int wt;
	private int ht;
	private int nraw;
	private Roi[] RawROIs=null;
	private boolean [] Passed=null;
	private ImageStatistics [] RStats=null;
	
	private static final int stat_options=ImageStatistics.AREA|ImageStatistics.CENTER_OF_MASS|ImageStatistics.MIN_MAX|ImageStatistics.MEAN|ImageStatistics.MEDIAN|ImageStatistics.CENTROID|ImageStatistics.ELLIPSE;
	private static final Calibration defaultCalibration=new Calibration();
	
	/**
	 * Initiates the class. Gets the required statistics and parameters
	 * @param _img
	 * @param _rawRois
	 */
	public ParticleFilterer(ImageProcessor _ipr, Roi [] _rawRois) throws Exception{
		int i;
		if (_ipr==null)
			throw new Exception ("ParticleFilterer: image is not defined");
		if (_rawRois==null)
			throw new Exception ("ParticleFilterer: ROIs are not defined");

		RawROIs=_rawRois;
		nraw=RawROIs.length;
		wt=_ipr.getWidth();
		ht=_ipr.getHeight();
		Passed= new boolean[nraw];
		for (i=0;i<nraw;i++)
			Passed[i]=true;
		RStats=new ImageStatistics[nraw];
		
		
		for (i=0;i<nraw;i++){
			_ipr.setRoi(RawROIs[i]);
			RStats[i]=ImageStatistics.getStatistics(_ipr, stat_options, defaultCalibration);
		}
		_ipr.resetRoi();
	}
	
	public void setProcessor(ImageProcessor _ipr) throws Exception{
		if (_ipr==null)
			throw new Exception ("ParticleFilterer: image is not defined");
		
		for (int roiIndex=0; roiIndex<nraw;roiIndex++){
			if (Passed[roiIndex]){
				_ipr.setRoi(RawROIs[roiIndex]);
				RStats[roiIndex]=ImageStatistics.getStatistics(_ipr, stat_options, defaultCalibration);
			}else{
				RStats[roiIndex]=null;
			}
		}
		_ipr.resetRoi();
	}
	
	public void filterInRoi(Roi _bRoi, double _xZm, double _yZm, double _pixRatio)throws Exception{
		final double Zwidth=wt/_pixRatio;
		final double Zheight=ht/_pixRatio;
				
		for (int i=0;i<nraw;i++){
			double [] cpos;
			if (Passed[i]){//check only those ROIs which were not filtered out before
				cpos=ROIManipulator2D.calcERESPos(_xZm, _yZm, RStats[i].xCentroid, RStats[i].yCentroid, Zwidth, Zheight, _pixRatio);// .calcERESPos(_xZm, _yZm, RStats[i].xCentroid, RStats[i].yCentroid);
				Passed[i]=_bRoi.contains((int)cpos[0], (int)cpos[1]);
			}
		}
	}
	
	
	public void filterInRoi(Roi _refRoi)throws Exception{
		if(_refRoi==null)throw new Exception("ParticleFilterer.filterInRoi: argument can not be null");
		
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				Passed[i]=_refRoi.contains((int)RStats[i].xCentroid,(int)RStats[i].yCentroid); //(new ShapeRoi(RawROIs[i]).not(shapeRef)==null);
			}
		}		
	}

	public void filterNotInRoi(Roi _refRoi)throws Exception{
		if(_refRoi==null)throw new Exception("ParticleFilterer.filterNotInRoi: argument can not be null");
		ShapeRoi refRoiShape=new ShapeRoi(_refRoi);
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				Passed[i]=ROIManipulator2D.isEmptyRoi(new ShapeRoi(RawROIs[i]).and(refRoiShape));
			}
		}		
	}
	
	/**
	 * performs threshold filtration step. Updates flags of the selected ROIs
	 * @param _find filter index based on the defined public constants
	 * @param _min minimal threshold
	 * @param _max maximal threshold
	 */
	public void filterThr(int _find,double _min, double _max)throws Exception{
		this.filterThr(_find, _min, _max, false);
	}
	
	public void filterThr(int _find,double _min, double _max,boolean _passedToRm)throws Exception{
		switch (_find){
			case AREA:
				this.filterThrArea(_min,_max);
				break;
			case INTEGRATED:
				this.filterThrIntegr(_min,_max);
				break;
			case MAX:
				this.filterThrMax(_min,_max);
				break;
			case MIN:
				this.filterThrMin(_min,_max);
				break;
			case BORDER:
				this.filterThrBorder(_min);//only minimal distance to the border is interesting
				break;
			case RANGE:
				this.filterThrRange(_min, _max);
				break;
			case MEAN:
				this.filterThrMean(_min, _max);
				break;
			case MEDIAN:
				this.filterThrMedian(_min, _max);
				break;
			case ASPECT_RATIO:
				this.filterThrAspectRatio(_min, _max);
				break;
			case LONG_AXIS:
				this.filterThrLongAxis(_min, _max);
				break;
			case SHORT_AXIS:
				this.filterThrShortAxis(_min, _max);
				break;
			case CIRCULARITY:
				this.filterThrCircularity(_min, _max);
				break;
			case SOLIDITY:
				this.filterThrSolidity(_min, _max);
				break;

				
			default:
				throw new Exception("ParticleFilterer.filterThr: Undefined filter index");
		}
		
		if(!_passedToRm)return;
		
		RoiManager rm=ROIManipulator2D.getEmptyRm();
		Roi[] pRois=this.getPassedRois();
		
		if(pRois==null) return;
		for (Roi r:pRois)
			rm.addRoi(r);
		IJ.run("Labels...", "color=white font=12");
		
	}
	
	
	
	private void filterThrArea(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].area;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrIntegr(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].area*RStats[i].mean;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrMax(double _min,double _max){
		double v_max;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				v_max=RStats[i].max;
				Passed[i]=(v_max>=_min)&&(v_max<=_max);
			}
		}
	}

	private void filterThrMin(double _min,double _max){
		double v_min;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				v_min=RStats[i].min;
				Passed[i]=(v_min>=_min)&&(v_min<=_max);
			}
		}
	}

	
	private void filterThrBorder(double _mind){
		double v_x,v_y;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				v_x=RStats[i].xCenterOfMass;
				v_y=RStats[i].yCenterOfMass;
				Passed[i]=(v_x>=_mind)&&(v_y>=_mind)&&((wt-v_x)>=_mind)&&((ht-v_y)>=_mind);
			}
		}
	}
	
	private void filterThrMean(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].mean;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrMedian(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].median;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	
	private void filterThrRange(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].max-RStats[i].min;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	private void filterThrAspectRatio(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].major/RStats[i].minor;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrLongAxis(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].major;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrShortAxis(double _min,double _max){
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].minor;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrCircularity(double _min,double _max){
		double perimeter;
		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				perimeter=RawROIs[i].getLength();
				val=4.0*Math.PI*RStats[i].area/perimeter/perimeter;
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}

	private void filterThrSolidity(double _min,double _max){

		double val;
		for (int i=0;i<nraw;i++){
			if (Passed[i]){//check only those ROIs which were not filtered out before
				val=RStats[i].area/getPolygonArea(RawROIs[i].getConvexHull());
			
				Passed[i]=(val>=_min)&&(val<=_max);
			}
		}
	}
	
	public void selectRandomObjects(int _nsel)throws Exception{
		Random rand=new Random();
		
		int[] passedInds=this.getPassedInds();
		int nPassed=passedInds.length;
		
		if (nPassed<_nsel)
			throw new Exception("ParticleFilterer.selectObjects: specified number of objects to select bigger that number of available objects");
		
		
		while(passedInds.length>_nsel) {
			int testInd=passedInds[rand.nextInt(passedInds.length)];
			Passed[testInd]=false;
			
			passedInds=getPassedInds();
		}
	}

	
	private double getPolygonArea(Polygon p) {
		if (p==null) return Double.NaN;
		int carea = 0;
		int iminus1;
		for (int i=0; i<p.npoints; i++) {
			iminus1 = i-1;
			if (iminus1<0) iminus1=p.npoints-1;
			carea += (p.xpoints[i]+p.xpoints[iminus1])*(p.ypoints[i]-p.ypoints[iminus1]);
		}
		return (Math.abs(carea/2.0));
	}
	
	public Roi [] getPassedRois(){
		int i,j;
		int npassed=0;
		for (i=0;i<nraw;i++)
			if (Passed[i])
				npassed+=1;
		if (npassed==0) return null;
		Roi [] passedRois=new Roi[npassed];
		j=0;
		for (i=0;i<nraw;i++){
			if (Passed[i]){
				passedRois[j]=RawROIs[i];
				j+=1;;
			}
		}
		return passedRois;
		
	}
	
	public boolean[] getPassedFlags(){
		return Passed;
	}
	
	public int[] getPassedInds(){
		return ArrIndUtils.getTrueInd(Passed);
	}

}
