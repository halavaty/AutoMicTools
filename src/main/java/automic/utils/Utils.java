package automic.utils;

import ij.IJ;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public abstract class Utils {
	public static int getNumTag(String _fnm,char _tag)throws Exception{
		final Pattern patW=Pattern.compile("_W\\d\\d\\d\\d");
		final Pattern patP=Pattern.compile("_P\\d\\d\\d\\d");
		final Pattern patT=Pattern.compile("_T\\d\\d\\d\\d");

		Matcher match;
		if (_tag=='W')//(Character.compare('W', _tag)==0)
			match=patW.matcher(_fnm);
		else if (_tag=='P')
			match=patP.matcher(_fnm);
		else if (_tag=='T')
			match=patT.matcher(_fnm);
		else
			throw new Exception("No match for the file name");
		if(!match.find())	
			return -1;
		return Integer.parseInt(_fnm.substring(match.start()+2, match.end()));
	}
	
	public static void generateErrorMessage(String _title, String _msg){
		if (_title==null) _title="null";
		if (_msg==null)   _msg="null";
		IJ.error(_title, _msg);
	}
	
	public static void generateErrorMessage(String _title, Exception _ex){
		if (_ex==null)
			generateErrorMessage(_title,"null");
		else
			generateErrorMessage(_title,_ex.getMessage());
		
	}
	
	public static void generateWarningMessage(String _message){
		IJ.showMessage("[WARNING]", _message);
	}

	public static void checkJavaVersion(double _minVersion){

		if(Double.parseDouble(System.getProperty("java.specification.version"))<_minVersion)
			IJ.error("Wrong Java version. "+_minVersion+" or higher is required. Plugin might crash.");
	}
	
	public static String getTimeString(long _timeLong){
		long hours,minutes,seconds;
		hours=TimeUnit.MILLISECONDS.toHours(_timeLong);
		_timeLong-=TimeUnit.HOURS.toMillis(hours);
		minutes=TimeUnit.MILLISECONDS.toMinutes(_timeLong);
		_timeLong-=TimeUnit.MINUTES.toMillis(minutes);
		seconds=TimeUnit.MILLISECONDS.toSeconds(_timeLong);
		_timeLong-=TimeUnit.SECONDS.toMillis(seconds);
		
		return String.format("%dh %dmin %ds %dms",hours,minutes,seconds,_timeLong);
	}
	
}
