package automic.utils;
import ij.IJ;
import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.filter.MaximumFinder;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.frame.RoiManager;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;

/**
 * This implements individual image analysis steps in automated FRAP analysis as static methods
 * @author Aliaksandr Halavatyi
 * Last update: 30.10.2014
 *
 */
public class AutoSteps{
	/**
	 * Filters image with "Gaussian Blur" filter. Keeps Original Image non-modified
 	 * @param _img input image
	 * @param _width width of the filter
	 * @return filtered image
	 */
	
	private static Duplicator Dupl=null;
	
	public static ImagePlus SmoothGaus(ImagePlus _img,double _width){
		if (Dupl==null) Dupl=new Duplicator();
		ImagePlus sm_im=Dupl.run(_img);
		IJ.run(sm_im,"Gaussian Blur...", "sigma="+_width+" stack");//IJ.run is used as an example here. Can be any other command or list of commands
		return sm_im;
	}

	public static ImagePlus SmoothMedian(ImagePlus _img,double _radius){
		if (Dupl==null) Dupl=new Duplicator();
		ImagePlus sm_im=Dupl.run(_img);
		IJ.run(sm_im,"Median...", "radius="+_radius+" stack");//IJ.run is used as an example here. Can be any other command or list of commands
		return sm_im;
	}
	
	
	public static void SetLowThresh(ImagePlus _img,int _thr){
		IJ.setThreshold(_img,_thr,_img.getProcessor().maxValue());
	}

	public static void SetFractThresh(ImagePlus _img,double _factor){
		ImageStatistics st=_img.getStatistics();
		double mn=st.min;
		double mx=st.max;
		IJ.setThreshold(_img,mn+_factor*(mx-mn),_img.getProcessor().maxValue());
	}
	
	public static double getFractRange(ImagePlus _img, double _fract){

		ImageStatistics st=_img.getStatistics();
		double mn=st.min;
		double mx=st.max;
		return mn+_fract*(mx-mn);

	}
	
	public static double getFractCount(ImagePlus _img,double _fract){
		int [] hst=_img.getProcessor().getHistogram();
		double tot_px=_img.getWidth()*_img.getHeight();
		int nbins=hst.length;
		int cnt=0;
		for (int i=0;i<nbins;i++){
			cnt+=hst[i];
			if ((cnt/tot_px)>_fract)
				return i;
		}
		return nbins;
	}
	
	public static double getRange(ImagePlus _img){
		ImageStatistics st=_img.getStatistics();
		return st.max-st.min;
	}
		
	public static ImagePlus WaterSegm(ImagePlus _img, double _tol){
		MaximumFinder WSegm=new MaximumFinder();
		ImageProcessor pr=_img.getProcessor();
		ImageProcessor seg_pr=WSegm.findMaxima(pr,_tol,pr.getMinThreshold(),MaximumFinder.SEGMENTED,false,false);
		return new ImagePlus("Segmented Mask",seg_pr);
	}
	
	public static Roi [] AnalysePart(ImagePlus _img,double _min_sz, boolean _inc_holes,boolean _exc_edges, double _min_circ,double _max_circ){
		RoiManager rMan=RoiManager.getInstance();
		if (rMan==null)
			rMan=new RoiManager();
		rMan.runCommand("Reset");
		int part_opt=ParticleAnalyzer.SHOW_NONE|ParticleAnalyzer.ADD_TO_MANAGER;
		if (_inc_holes)
			part_opt=part_opt|ParticleAnalyzer.INCLUDE_HOLES;
		if (_exc_edges)
			part_opt=part_opt|ParticleAnalyzer.EXCLUDE_EDGE_PARTICLES;
		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,_min_sz,Double.POSITIVE_INFINITY,_min_circ,_max_circ);
		PartAn.analyze(_img);
		Roi [] rs=rMan.getRoisAsArray();
		if (rs==null) return null;
		if (rs.length==0) return null;
		return rs;
	}
	
	public static void DrawFrame(ImagePlus _img,int _width,int _icol){
		ImageProcessor ipr=_img.getProcessor();
		ipr.setColor(_icol);
		ipr.fillOutside(new Roi(_width,_width,_img.getWidth()-2*_width,_img.getWidth()-2*_width));
	}

}
