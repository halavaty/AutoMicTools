package automic.utils.segment;
import java.io.IOException;
import java.util.*;

import fiji.threshold.Auto_Local_Threshold;
import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.process.ImageProcessor;
import ij.process.ImageStatistics;
import ij.gui.Roi;
import ij.plugin.Duplicator;
import ij.plugin.ImageCalculator;
import ij.plugin.filter.ParticleAnalyzer;
import ij.plugin.filter.ThresholdToSelection;
import ij.plugin.frame.RoiManager;
import ij.measure.ResultsTable;
import ij.measure.Measurements;


// class to segment focal adhesions on the image
// input: ImagePlus or ImageProcessor, parameters
// output: List of ROIs in the ROI manager and/or 16 bit mask, each colour for another FA
/**
 * Segmentation of multiple objects on the image with multiple methods 
 * @author Aliaksandr Halavatyi
 *
 */



public class ParticleSegmentor{
	static final boolean DEBUG = false;
	private static String [] lmeth={"threshold",
									"local threshold",
									"TopHat",
									"watershed"
										  };//titles of implemented analysis methods
	private double mintr=0;				//minimal threshold
	
	//private static RoiManager RMan;	
	//private int rolling_ball;				//radius of rolling ball for subtracting background
	private int min_part_size;				//minimal FA size in pixels
	private int min_clust_size;				//minimal cluster size for watershed
	private static Duplicator dupl=null;
	private static ImageCalculator ic=null;
	private static ThresholdToSelection thr_sel=null;
	private static Auto_Local_Threshold LocThr=null;		//local thresholding
	private static RoiManager rMan=null;


	
	public static String [] getSegMethods(){
		return lmeth;
	}
	
	
	public ParticleSegmentor(/*int _roll,*/ int _min_sz, int _minClust) {
		/**
		 * Zero-value constructor for Segment_FAs. Creates an instance of 
		 * Segment_FAs. Initiates analysis parameters.
		 * 
		 * @param	_roll		rolling ball for subtracting background  
		 * @param	_min_sz		minimal particle size in pixels
		 * @param	_minClust	minimal cluster size for watershed
		 * 
		 */
		
		//this.rolling_ball=_roll;
		this.min_part_size=_min_sz;
		if (_minClust<0)
			this.min_clust_size=this.min_part_size;
		else
			this.min_clust_size=_minClust;
		
		if (dupl==null)
			dupl=new Duplicator();
		if (thr_sel==null)
			thr_sel=new ThresholdToSelection();
		if (LocThr==null)
			LocThr=new Auto_Local_Threshold();
		if (ic==null)
			ic=new ImageCalculator();
		rMan=RoiManager.getInstance();
		if(rMan==null)
			rMan=new RoiManager();

	}
	public ParticleSegmentor(/*int _roll,*/ int _min_sz) {
		this(/*_roll,*/_min_sz,-1);
	}
	
	public ParticleSegmentor(){
		this(0);
	}

	public ImagePlus SegSliceThreshold(ImagePlus _init,int _mask_ind, boolean _proi, double _mintr) throws IOException {
		return SegSliceThreshold(_init,_mask_ind,_proi,true,_mintr);
	}
	
	
	public ImagePlus SegSliceThreshold(ImagePlus _init,int _mask_ind, boolean _proi,boolean _4con, double _mintr) throws IOException {
		/**
		 * segments particles with constant threshold level 
		 * 
		 * @param	_init	initial image to segment
		 * @param	_croi	Roi that defines the cell in which focal adhesions have to be segmented. If null, the whole image is used
		 * @param	_mask_ind	static int from ParticleAnalyser class determining kind of output image
		 * @param	_proi	put segmented ROIs to the ROI manager
		 * @param	_mintr	minimal threshold level
		 * @return 	image with FA masks or null if the corresponding option is not selected
		 * @throws	IOException if input does not correspond to what is expected.
		 */
		
		//check for the correct image and method input
		if (((_init.getType()!=ImagePlus.GRAY8)&&(_init.getType()!=ImagePlus.GRAY16)&&(_init.getType()!=ImagePlus.GRAY32))||(_init.getNDimensions()!=2))
			throw new IOException();
		
		if (DEBUG)
			_init.show();
		
		ImageProcessor pinit=_init.getProcessor();
		//IJ.setThreshold(_init,_mintr, _init.getStatistics(ImageStatistics.MIN_MAX).max);
		pinit.setThreshold(_mintr, _init.getStatistics(ImageStatistics.MIN_MAX).max, pinit.getLutUpdateMode());

		//segmentation through particle analyser class
		int part_opt=_mask_ind|ParticleAnalyzer.INCLUDE_HOLES;
		if (_4con)
			part_opt=part_opt|ParticleAnalyzer.FOUR_CONNECTED;
		if (_proi)
			part_opt=part_opt|ParticleAnalyzer.ADD_TO_MANAGER;

		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,min_part_size,Double.POSITIVE_INFINITY,0,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(_init,pinit);

		pinit.resetThreshold();
		
		if(DEBUG)
			PartAn.getOutputImage().show();
		
		return PartAn.getOutputImage();
	}

	public ImagePlus SegSliceLocal(ImagePlus _init,int _mask_ind, boolean _proi,int _width, double _loctr) throws Exception {

		//check for the correct image and method input
		if (((_init.getType()!=ImagePlus.GRAY8)&&(_init.getType()!=ImagePlus.GRAY16)&&(_init.getType()!=ImagePlus.GRAY32))||(_init.getNDimensions()!=2))
			throw new IOException();
		
		if (DEBUG)
			_init.show();
		if(dupl==null)
			dupl=new Duplicator();
		ImagePlus mimg=dupl.run(_init);
		if (DEBUG)
			mimg.show();
		
		switch (mimg.getType()){
			case ImagePlus.GRAY8:
				break;
			case ImagePlus.GRAY16:
				IJ.setMinAndMax(mimg,0, 65536);
				IJ.run(mimg, "8-bit", "");
				break;
			case ImagePlus.GRAY32:
				IJ.setMinAndMax(mimg,0, 1);
				IJ.run(mimg, "8-bit", "");
				break;
			default:
				throw new Exception("ParticleSegmentor.SegSliceLocal: unsupported image format");
		}

		
		LocThr.exec(mimg,"Niblack",_width,_loctr,0,true);
		//		IJ.run(_init, "Analyze Particles...", "size=5-Infinity pixel circularity=0.00-1.00 show=Nothing display clear add");

		//segmentation through particle analyser class
		int part_opt=_mask_ind|ParticleAnalyzer.INCLUDE_HOLES|ParticleAnalyzer.FOUR_CONNECTED;
		if (_proi)
			part_opt=part_opt|ParticleAnalyzer.ADD_TO_MANAGER;

		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,0,null,min_part_size,Double.POSITIVE_INFINITY,0,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(mimg,mimg.getProcessor());

		if(DEBUG)
			PartAn.getOutputImage().show();
		mimg.changes=false;
		mimg.close();
		return PartAn.getOutputImage();
	}
	
	
	public ImagePlus SegSliceTopHat(ImagePlus _init,int _mask_ind, boolean _proi,double _hat_rad, int _thr,double _blur) throws IOException {

		IJ.run(_init,"Gaussian Blur...", "sigma="+_blur);
		ImagePlus back_im=dupl.run(_init);

		IJ.run(back_im, "Minimum...", "radius="+_hat_rad);
		IJ.run(back_im, "Maximum...", "radius="+_hat_rad);

		ImagePlus new_im=ic.run("Subtract create", _init, back_im);
		
		return SegSliceThreshold(new_im,_mask_ind,_proi,_thr);
	}

//	public ImagePlus SegStackTopHat(ImagePlus _init,int _mask_ind,double _hat_rad, int _thr,double _blur) throws IOException {
//		if(_init.getNDimensions()!=3)
//			throw new Exception();
//		ImageStack st=new ImageStack(_init.getWidth(),_init.getHeight());
//		int n=st.getSize();
//		
//		ImagePlus i1,i2;
//		for
//		
//		
//		
//	}
	
	public ImagePlus getTopHatImg(ImagePlus _img, double _hat_rad){
		ImagePlus back_im=dupl.run(_img);

		IJ.run(back_im, "Minimum...", "radius="+_hat_rad);
		IJ.run(back_im, "Maximum...", "radius="+_hat_rad);
		
		return ic.run("Subtract create", _img, back_im);
	}

	public ImagePlus getMedianSubtrImg(ImagePlus _img, double _filtRad){
		ImagePlus back_im=dupl.run(_img);

		IJ.run(back_im, "Median...", "radius="+_filtRad);
		
		return ic.run("Subtract create", _img, back_im);
	}
	
	
	public ImagePlus SegSliceWatershed(ImagePlus _init, boolean _proi, double _mintr) throws IOException {
		/**
		 * watershed segmentation for focal adhesions. Image has to be prepared before (e.g smoothing and background subtraction)
		 * 
		 * 
		 * @return 	image with FA masks or null if the corresponding option is not selected
		 * @throws	IOException if input does not correspond to what is expected.
		 */
		//check for the correct image and method input
		if (((_init.getType()!=ImagePlus.GRAY8)&&(_init.getType()!=ImagePlus.GRAY16)&&(_init.getType()!=ImagePlus.GRAY32))||(_init.getNDimensions()!=2))
			throw new IOException();
		ImageStatistics im_stat=_init.getStatistics(ImageStatistics.MIN_MAX);
		ImageProcessor im_proc=_init.getProcessor();
		int max_intens=(int)im_stat.max;
		int minThresholdInt=(int)_mintr;
		int lev_num=max_intens-minThresholdInt+1;
		int [] offset=new int[lev_num];
		int [] cur_cnt=new int[lev_num];
		int obj_pix_num=0;
		
		int [] hist=null;
		if (_init.getType()==ImagePlus.GRAY8)
			hist=im_stat.histogram;
		if (_init.getType()==ImagePlus.GRAY16)
			hist=im_stat.histogram16;
		
		offset[0]=0;
		cur_cnt[0]=0;
		obj_pix_num=hist[minThresholdInt];
		for (int i=1;i<lev_num;i++){
			offset[i]=offset[i-1]+hist[i+minThresholdInt-1];
			cur_cnt[i]=0;
			obj_pix_num+=hist[minThresholdInt+i];
		}

		int [] x_pix=new int[obj_pix_num];
		int [] y_pix=new int[obj_pix_num];
		int wd=_init.getWidth();
		int ht=_init.getHeight();

		int[][] im_pls=im_proc.getIntArray();
		int val,ind;
		for (int y=0;y<ht;y++){
			for (int x=0;x<wd;x++){
				val=im_pls[x][y];//im_proc.getPixel(x,y);
				if(val>=_mintr){
					val-=_mintr;
					ind=offset[val]+cur_cnt[val];
					x_pix[ind]=x;
					y_pix[ind]=y;
					cur_cnt[val]++;
				}
			}
		}
		
		//to do: test pixel sorting
		
		List<Integer> part_sz=new ArrayList<Integer>();
		
		//segment initial particles with maximal values
		im_proc.setThreshold(max_intens, max_intens, im_proc.getLutUpdateMode());
		int part_opt=ParticleAnalyzer.INCLUDE_HOLES|ParticleAnalyzer.SHOW_ROI_MASKS|ParticleAnalyzer.FOUR_CONNECTED;

		ResultsTable rt=new ResultsTable();
		ParticleAnalyzer PartAn=new ParticleAnalyzer(part_opt,Measurements.AREA,rt,1,Integer.MAX_VALUE,0,1);
		PartAn.setHideOutputImage(true);
		PartAn.analyze(_init,im_proc);
		
		if (DEBUG)
			rt.show("Results max part areas");
		//_init.hide();
		//
		
		ImagePlus mask_im=PartAn.getOutputImage();
		//test
		if (DEBUG){
			mask_im.show();
			IJ.run(mask_im, "Enhance Contrast", "saturated=0.001");
			//IJ.run("Set... ", "zoom=3200 x=2 y=2");
		}
		
		ImageProcessor mask_proc=mask_im.getProcessor();
		int[][] mask_pls=mask_proc.getIntArray();
		double [] areas=rt.getColumnAsDoubles(0);
		int num_part=rt.getCounter();
		int cnt_part=num_part;
		
		for(int i=0;i<num_part;i++)
			part_sz.add((int)areas[i]);
		
		
		int x,y,n_neib,biggest_ind, biggest_sz, test_sz,oldval;
		int [] neib=new int[4];
		for (int i=obj_pix_num-hist[max_intens]-1;i>-1;i--){
			x=x_pix[i];
			y=y_pix[i];
			n_neib=CheckNeighbours(mask_pls, x, y,wd,ht,neib);//find neighbours on the mask image

			switch (n_neib){
				case 0://no neighbours - create new particle
					num_part++;
					cnt_part++;
					mask_pls[x][y]=cnt_part;
					part_sz.add(1);
					break;
				
				case 1://one neighbour - add pixel to existing particle
					for(biggest_ind=0;biggest_ind<4;biggest_ind++)
						if (neib[biggest_ind]>0)
							break;
					if (neib[biggest_ind]==0) return null;
					val=neib[biggest_ind];
					mask_pls[x][y]=val;
					part_sz.set(val-1, part_sz.get(val-1)+1);
					break;
				default://more than one neighbour
					//find biggest particle
					biggest_ind=-1;
					biggest_sz=0;
					for (int j=0;j<4;j++){
						if (neib[j]>0){
							test_sz=part_sz.get(neib[j]-1);
							if (test_sz>biggest_sz){
								biggest_ind=j;
								biggest_sz=test_sz;
							}
						}
					}
					//add current pixel to the biggest particle
					val=neib[biggest_ind];
					mask_pls[x][y]=val;
					part_sz.set(val-1, biggest_sz+1);
					//if some of the neighbours are smaller than threshold - join with the biggest particle
					for (int j=0;j<n_neib;j++){
						if ((j==biggest_ind)||(neib[j]==0)) continue;
						test_sz=part_sz.get(neib[j]-1);
						if (test_sz<min_clust_size){
							oldval=neib[j];
							part_sz.set(val-1, part_sz.get(val-1)+part_sz.get(oldval-1));
							part_sz.set(oldval-1, 0);
							num_part--;
							if (j==0) RepaintMatr(mask_pls,x,y-1,wd,ht,val);	//repaint top
							if (j==1) RepaintMatr(mask_pls,x+1,y,wd,ht,val);	//repaint right
							if (j==2) RepaintMatr(mask_pls,x,y+1,wd,ht,val);	//repaint bottom
							if (j==3) RepaintMatr(mask_pls,x-1,y,wd,ht,val);	//repaint left
							
						}
					}
			}			
			
//			if (DEBUG){
//				mask_proc.setIntArray(mask_pls);
//				mask_im.updateAndRepaintWindow();
//			}
		}
		
		mask_proc.setIntArray(mask_pls);
		if (DEBUG){
			mask_proc.setIntArray(mask_pls);
			mask_im.updateAndRepaintWindow();
		}		
		
		
		//delete particles which are smaller than the smallest accepted size
		Roi droi;
		mask_proc.setColor(0);
		
		for(int k=0;k<cnt_part;k++){
			test_sz=part_sz.get(k);
			if ((test_sz>0)&&(test_sz<min_part_size)){
				mask_proc.setThreshold(k+1, k+1,mask_proc.getLutUpdateMode());
				droi=thr_sel.convert(mask_proc);
				mask_proc.fill(droi);
				part_sz.set(k, 0);
				num_part--;
			}
		}
			

		//repaint focal adhesions with consequent colours
		//if required, put the identified rois to the roi manager
		int col_cnt=1;
		for(int k=0;k<cnt_part;k++){
			if (part_sz.get(k)>0){
				mask_proc.setThreshold(k+1, k+1,mask_proc.getLutUpdateMode());
				mask_proc.setColor(col_cnt);
				droi=thr_sel.convert(mask_proc);
				mask_proc.fill(droi);
				if (_proi){
					droi.setName("prt"+col_cnt);
					rMan.addRoi(droi);
				}
				col_cnt++;
			}
			if ((col_cnt-1)>num_part){
				IJ.error("Segmentation error", "Counting particles problem");
				throw new IOException();
			}
		}

		if ((col_cnt-1)!=num_part){
			IJ.error("Segmentation error", "Counting particles problem");
			throw new IOException();
		}
	
		
		if (DEBUG){
			mask_im.updateAndRepaintWindow();
		}
		
		return mask_im;
	}
	
	private void RepaintMatr(int[][] _mtr,int _x, int _y,int _wd, int _ht,int _newval){
		int oldval=_mtr[_x][_y];
		if (_newval==oldval) return;
		_mtr[_x][_y]=_newval;
		//check top
		if (_y>0)
			if(_mtr[_x][_y-1]==oldval)
				RepaintMatr(_mtr,_x,_y-1,_wd,_ht,_newval);
		//check right
		if ((_x+1)<_wd)
			if(_mtr[_x+1][_y]==oldval)
				RepaintMatr(_mtr,_x+1,_y,_wd,_ht,_newval);
		//check bottom
		if ((_y+1)<_ht)
			if(_mtr[_x][_y+1]==oldval)
				RepaintMatr(_mtr,_x,_y+1,_wd,_ht,_newval);
		//check left
		if (_x>0)
			if(_mtr[_x-1][_y]==oldval)
				RepaintMatr(_mtr,_x-1,_y,_wd,_ht,_newval);
		
	}
	
	private int CheckNeighbours(int[][] _msk, int _x, int _y,int _wd, int _ht,int[] _neib){
		for(int i=0;i<4;i++)
			_neib[i]=0;
		//0-top;1-right;2-bottom;3-left
		
		//check top
		if (_y>0) _neib[0]=_msk[_x][_y-1];
			
		//check right
		if ((_x+1)<_wd) _neib[1]=_msk[_x+1][_y];
		
		//check bottom
		if ((_y+1)<_ht) _neib[2]=_msk[_x][_y+1];
		
		//check left
		if (_x>0) _neib[3]=_msk[_x-1][_y];
		
		
		//remove similarities and count
		int n_neib=0;
		/*
		for (int i=0;i<3;i++)
			if (_neib[i]>0){
				n_neib++;
				for(int j=i+1;j<4;j++)
					if (_neib[j]==_neib[i])
						_neib[j]=0;
			}
		*/	

		
		if (_neib[0]>0){
			n_neib++;
			if (_neib[1]==_neib[0]) _neib[1]=0;
			if (_neib[2]==_neib[0]) _neib[2]=0;
			if (_neib[3]==_neib[0]) _neib[3]=0;
		}
		if (_neib[1]>0){
			n_neib++;
			if (_neib[2]==_neib[1]) _neib[2]=0;
			if (_neib[3]==_neib[1]) _neib[3]=0;
		}
		if (_neib[2]>0){
			n_neib++;
			if (_neib[3]==_neib[2]) _neib[3]=0;
		}
		if (_neib[3]>0)
			n_neib++;

		
		//shift everything to the left
		//int cnt=0;
		//for (int i=0;i<4;i++){
		//	if (_neib[i]>0){
		//		_neib[cnt]=_neib[i];
		//		cnt++;
		//	}
		//}
		//for (int i=n_neib;i<4;i++)
		//	_neib[i]=0;
		
		return n_neib;
		
		
	}
	
	
	public ImagePlus segstack(ImagePlus _init,Roi [] _crois,boolean _proi,int _mask_ind,String _meth) throws Exception {
		/**
		 * runs segmentation for the 8-bit or 16-bit grayscale stack of images 
		 * 
		 * @param	_init	initial image to segment
		 * @param	_croi	stack of Rois that defines the cell shapes in which focal adhesions have to be segmented. Has to be the same size as initial image stack
		 * @param	_mask_ind	static int from ParticleAnalyser class determining kind of output image
		 * @param	_meth	string identifier for the method to use
		 * @return 	16 bit image stack with FA masks
		 * @throws	IOException if input does not correspond to what is expected.
		 */
		final boolean mask=(_mask_ind!=ParticleAnalyzer.SHOW_NONE);
		int mind=Arrays.asList(lmeth).indexOf(_meth);
		if (((_init.getType()!=ImagePlus.GRAY8)&&(_init.getType()!=ImagePlus.GRAY16)&&(_init.getType()!=ImagePlus.GRAY32))||(mind<0)||(mind>=lmeth.length)||(_init.isHyperStack())||(_init.getNDimensions()>3))//||(_crois==null)
			throw new IOException();

		//ImageStack sinit=_init.getStack();
		int pnum=_init.getStackSize();//sinit.getSize();
		if (_crois!=null)
			if (_crois.length!=pnum)
				throw new IOException();
			

		if (DEBUG)
			_init.show();
		//
		IJ.run(_init, "Select None", "");
		//IJ.run(_init,"Kalman Stack Filter", "acquisition_noise=0.5 bias=0.80");
//		IJ.run(_init, "Subtract Background...", "rolling="+rolling_ball+" stack");
//		_init.setSlice(1);
//		if (_crois!=null) _init.setRoi(_crois[0]);
//		_init.show();
//		IJ.run("Threshold...", "");
//		if (mintr==0)
//			IJ.setAutoThreshold(_init, "Otsu dark stack");
//		else
//			IJ.setThreshold(mintr, _init.getStatistics(ImageStatistics.MIN_MAX).max);
//		_init.show();
//
//		new WaitForUserDialog("Check lower threshold for focal adhesions").show();
//		mintr=(int)_init.getProcessor().getMinThreshold();
//		_init.getProcessor().resetThreshold();

		IJ.resetThreshold(_init);
		ImagePlus iseg;
		ImagePlus imask=null; //temporary image for segmented mask
		ImageStack smask=null;
		if (mask){
			smask=new ImageStack(_init.getWidth(),_init.getHeight()); //stack with masks
		}

		IJ.run(_init, "Select None", "");
		IJ.setForegroundColor(0,0,0);
		for (int j=0; j<pnum; j++){
			iseg=dupl.run(_init, j+1, j+1);//iseg=new ImagePlus("temp for segment",sinit.getProcessor(j+1));
			if (DEBUG)
				iseg.show();
			if (_crois!=null){
				if (_crois[j]!=null){
					iseg.setRoi(_crois[j]);
					IJ.run(iseg, "Make Inverse", "");
					IJ.run(iseg, "Fill", "slice");
					IJ.run(iseg, "Select None", "");
				}
			}
			
			switch (mind){
				case 0://threshold
					imask=SegSliceThreshold(iseg,_mask_ind,_proi, mintr);
					break;
					
				case 1://local threshod
					imask=SegSliceLocal(iseg,_mask_ind,_proi, 20,5.0);
					break;
				case 2://TopHat
					imask=SegSliceTopHat(iseg,_mask_ind,_proi,4.0,40,1.0);//!!! second last parameter is 7 before
					break;
				case 3://watershed
					try{ 
						imask=SegSliceWatershed(iseg,_proi, mintr);
					}catch(IOException e){
						throw new IOException();
					}
					break;
				default:
					IJ.error("Underfined FA segmentation method");
					return null;
			}
			if (mask)
				smask.addSlice("timestep "+j,imask.getProcessor());
			iseg.changes=false;
			iseg.close();
			if (imask!=null){	
				imask.changes=false;
				imask.close();
			}
			//IJ.log("Segmented slice "+(j+1)+" of "+pnum);
		}
		if(mask){
			ImagePlus out_im=new ImagePlus("mask stack",smask);
			IJ.resetMinAndMax(out_im);
			
			if (Arrays.asList(IJ.getLuts()).contains("glasbey")/*Executer.loadLut("glasbey")*/)
				IJ.run(out_im, "glasbey", "");
			return out_im;
		}
		else
			return null;
	}
	
	public void setMinimalThreshold(double _minimalThreshold){
		mintr=_minimalThreshold;
	}
}
