package automic.utils.logger;

import ij.IJ;
import ij.text.TextPanel;

public class IJLogLogger implements Logger{
	TextPanel outputTextPanel=null;
	
	public IJLogLogger(String _windowName) {
		IJ.log("");
	}
	
	@Override
	public void sendMessage(String _message){
		IJ.log(_message);
	}

	@Override
	public void sendErrorMessage(String _message){
		IJ.log(String.format("[ERROR]: %s", _message));
	}


	@Override
	public void sendWarningMessage(String _message){
		IJ.log(String.format("[WARNING]: %s", _message));
	}

	@Override
	public void sendInfoMessage(String _message){
		IJ.log(String.format("[INFO]: %s", _message));
		
	}
	
	
	@Override
	public void sendUpdatedMessage(String _message){
		IJ.log(String.format("\\Update: %s",_message));
	}
	@Override
	public void sendExceptionMessage(String _message,Exception _ex){
		String exceptionString=(_ex==null)?"null":_ex.getMessage();
		sendErrorMessage(String.format("Exception raised %s. Program message %s", exceptionString,_message));
	}
}
