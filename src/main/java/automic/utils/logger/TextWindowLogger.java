package automic.utils.logger;

import ij.text.TextPanel;
import ij.text.TextWindow;

public class TextWindowLogger implements Logger{
	private TextPanel outputTextPanel=null;
	private boolean saveToFile=false;
	private String saveFilePath="";
	
	
	public TextWindowLogger(String _windowName) {
		outputTextPanel=new TextWindow(_windowName,"",400,300).getTextPanel();
	}
	
	public void setFileSaving(boolean _saveToFile,String _saveFilePath) {
		saveToFile=_saveToFile;
		saveFilePath=_saveFilePath;
	}
	
	@Override
	public void sendMessage(String _message){
		outputTextPanel.append(_message);
		if (saveToFile) {
			outputTextPanel.saveAs(saveFilePath);
		}
	}
	

	@Override
	public void sendErrorMessage(String _message){
		sendMessage(String.format("[ERROR]: %s", _message));
	}


	@Override
	public void sendWarningMessage(String _message){
		sendMessage(String.format("[WARNING]: %s", _message));
	}

	@Override
	public void sendInfoMessage(String _message){
		sendMessage(String.format("[INFO]: %s", _message));
		
	}
	
	
	@Override
	public void sendUpdatedMessage(String _message){
		
		int outputLineIndex=outputTextPanel.getLineCount()-1;
		outputTextPanel.setLine(outputLineIndex, _message);
		if (saveToFile) {
			outputTextPanel.saveAs(saveFilePath);
		}

	}
	
	@Override
	public void sendExceptionMessage(String _message,Exception _ex){
		String exceptionString=(_ex==null)?"null":_ex.getMessage();
		sendErrorMessage(String.format("Exception raised: \"%s\". Program message: \"%s\"", exceptionString,_message));
	}
}
