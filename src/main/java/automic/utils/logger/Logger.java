package automic.utils.logger;

public interface Logger {
	public abstract void sendMessage(String _message);
	
	public abstract void sendErrorMessage(String _message);
	
	public abstract void sendWarningMessage(String _message);
	
	public abstract void sendInfoMessage(String _message);
	
	public abstract void sendUpdatedMessage(String _message);

	public abstract void sendExceptionMessage(String message,Exception _ex);
}


//to implement
//ConsoleLogger
//TextWindowLogger
//LogWindowLogger
//UserDialogLogger
//TextFileLogger
//Nowhere/empty logger