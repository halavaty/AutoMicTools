package automic.utils.logger;


public class NoLogger implements Logger{
	
	public NoLogger() {
	}
	
	@Override
	public void sendMessage(String _message){
	}

	@Override
	public void sendErrorMessage(String _message){
	}


	@Override
	public void sendWarningMessage(String _message){
	}

	@Override
	public void sendInfoMessage(String _message){
	}
	
	
	@Override
	public void sendUpdatedMessage(String _message){
	}
	
	@Override
	public void sendExceptionMessage(String _message,Exception _ex){
	}
}
