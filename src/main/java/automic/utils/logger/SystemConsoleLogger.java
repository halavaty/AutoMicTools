package automic.utils.logger;


public class SystemConsoleLogger implements Logger{
	
	public SystemConsoleLogger() {
	}
	
	@Override
	public void sendMessage(String _message){
		System.out.println(_message);
	}

	@Override
	public void sendErrorMessage(String _message){
		System.out.println(String.format("[ERROR]: %s", _message));
	}


	@Override
	public void sendWarningMessage(String _message){
		System.out.println(String.format("[WARNING]: %s", _message));
	}

	@Override
	public void sendInfoMessage(String _message){
		System.out.println(String.format("[INFO]: %s", _message));
		
	}
	
	
	@Override
	public void sendUpdatedMessage(String _message){
		throw new UnsupportedOperationException();
		//int outputLineIndex=outputTextPanel.getLineCount()-1;
		//outputTextPanel.setLine(outputLineIndex, _message);
	}
	
	@Override
	public void sendExceptionMessage(String _message,Exception _ex){
		String exceptionString=(_ex==null)?"null":_ex.getMessage();
		sendErrorMessage(String.format("Exception raised %s. Program message %s", exceptionString,_message));
	}
}
