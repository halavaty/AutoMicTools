package automic.utils.logger;

public class ApplicationLogger{
	private static Logger myLogger=null;
	
	public static Logger getLogger(){
		return myLogger;
	}
	
	public static void setLogger(Logger _logger){
		myLogger=_logger;
	}
	
	public static void resetLogger(){
		myLogger=null;
	}
}
