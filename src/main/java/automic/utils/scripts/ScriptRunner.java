package automic.utils.scripts;

import ij.ImagePlus;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.plugin.frame.RoiManager;

import java.io.File;
import java.util.HashMap;

import org.scijava.script.ScriptService;

import automic.utils.FileUtils;
import automic.utils.roi.ROIManipulator2D;

/**
 * Functions for running scripts provided by end users
 * @author halavaty
 *
 */
public abstract class ScriptRunner {
	
	/**
	 * Running ImageJ custom script which takes image as an input and returns an image with Roi overlay as an output. 
	 * "Script" can be written in either ImageJ macro language or in one of scripting languages supported by Fiji (e.g. Jython or Groovy).
	 * See script examples in the repository for specifying its inputs and outputs 
	 * 
	 * @param _inputImage - image to be provided to the script as an input
	 * @param _scriptPath - path of the script file
	 * @param _scriptService - ScriptService ImageJ2 object
	 * @return array of ROIs calculated by the script, null if overlay created by the script is empty
	 * @throws Exception - when the execution of the script fails 
	 */
	/*
	public static Roi[] runScriptImageRois(ImagePlus _inputImage, File _scriptFile, ScriptService _scriptService)throws Exception{
		RoiManager roiManager=ROIManipulator2D.getEmptyRm();
		
		String scriptFileExtensiton=FileUtils.getExtension(_scriptFile);
		
		
		Overlay segmentationOverlay=null;
		if (scriptFileExtensiton.equals("ijm")) {//running ImageJ macro
			_inputImage.show();
			
			_scriptService.run(_scriptFile, false,new HashMap<String,Object>()).get();
			
			_inputImage.hide();
			segmentationOverlay=_inputImage.getOverlay();
		}else {//running script defined in one of scripting languages
			._inputImage.changes.
		}
		
		if (segmentationOverlay==null)
			return null;
		
		Roi [] outputRoiArray=segmentationOverlay.toArray();

		if (ROIManipulator2D.isEmptyRoiArr(outputRoiArray)) {
			return null;
		}
		
		
		
		return outputRoiArray;
	}
	*/
}
