package automic.utils;

public class DebugVisualiserSettings {
	//debug visualiser options
	int zoomIterations;
	int firstXPosition;
	int firstYPosition;
	int numberOfWindowsPerColumn;
	
	public DebugVisualiserSettings(int _zoomIterations,int _firstX,int _firstY,int _numberOfWindowsPerColumn){
		zoomIterations=_zoomIterations;
		firstXPosition=_firstX;
		firstYPosition=_firstY;
		numberOfWindowsPerColumn=_numberOfWindowsPerColumn;
	}
	
}
