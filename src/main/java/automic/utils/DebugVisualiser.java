package automic.utils;

import ij.IJ;
import ij.ImagePlus;
import ij.gui.ImageWindow;
import ij.gui.Overlay;

import java.util.ArrayList;
import java.util.List;

public class DebugVisualiser{
	private int zoomIter;
	private int inX;
	private int inY;
	private int windPerCol;
	
	private int wd,ht;
	List<ImageWindow> Winds=null;
	
	
	public DebugVisualiser(int _zoomIter, int _inX, int _inY, int _windPerCol){
		this.zoomIter=_zoomIter;
		this.inX=_inX;
		this.inY=_inY;
		this.windPerCol=_windPerCol;
		
		Winds=new ArrayList<ImageWindow>();
	}
	
	public DebugVisualiser(DebugVisualiserSettings _options){
		this(_options.zoomIterations,_options.firstXPosition,_options.firstYPosition,_options.numberOfWindowsPerColumn);
	}
	
	public ImageWindow addImage(ImagePlus _img){
		
		_img.resetDisplayRange();
		
		ImageWindow wind=_img.getWindow();
		
		if(wind!=null){//window already exists. Modify content and return window
			_img.updateAndRepaintWindow();
			if(!Winds.contains(wind))
				this.placeAndAddWindow(wind);
		}else{
			_img.show();
			wind=_img.getWindow();
			this.placeAndAddWindow(wind);
		}

		return wind;
	}
	
	public ImageWindow addImage(ImagePlus _img,String _title){
		_img.setTitle(_title);
		ImageWindow wind=this.addImage(_img);
		return wind;
	}
	
	public ImageWindow addImage(ImagePlus _img,String _title,boolean _imageCopy){
		if(_imageCopy)
			return this.addImage(_img.duplicate(), _title);
		else
			return this.addImage(_img, _title);
	}
	
	public ImageWindow addImage(ImagePlus _img,String _title,boolean _imageCopy, Overlay _overlay){
		ImageWindow window=this.addImage(_img, _title, _imageCopy);
		window.getImagePlus().setOverlay(_overlay);
		return window;
	}

	
	
	private void placeAndAddWindow(ImageWindow _wind){
		int wnum=Winds.size();
		if(wnum==0)
			_wind.setLocation(inX,inY);
		
		if(zoomIter>0){
			for(int i=0;i<zoomIter;i++)
				IJ.run(_wind.getImagePlus(),"In [+]", "");
		}else if(zoomIter<0){
			for(int i=0;i<(-zoomIter);i++)
				IJ.run(_wind.getImagePlus(),"Out [-]", "");
		}
		
		if(wnum==0){
			wd=_wind.getWidth();
			ht=_wind.getHeight();
		}
		if(wnum>0)
			_wind.setLocation(inX+(wnum / windPerCol)*wd,inY+(wnum % windPerCol)*ht);
		Winds.add(_wind);
	}
	
	public void closeAllImages(){
		for (ImageWindow wind:Winds){
			if(wind!=null){
				wind.getImagePlus().changes=false;
				wind.close();
			}
		}
		Winds.clear();
	}
}
