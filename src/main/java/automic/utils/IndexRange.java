package automic.utils;

public class IndexRange {
	public int start;
	public int end;
	
	public IndexRange(int _startCode, int _endCode, int _maximumIndex){
		if((_startCode>=0)&&(_endCode>=0))
			start=_startCode;
		else
			start=0;
		if((_endCode>=0)&&(_endCode>=start)&&(_endCode<_maximumIndex))
			end=_endCode+1;
		else
			end=_maximumIndex;
	}
}
