package automic.utils.imagefiles;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;

import java.lang.Exception;
import java.io.File;

public abstract class OpenerWithImageJ {
	
	/**
	 *  Opens the image and returns corresponding ImagePlus.
	 *	Expects that file path corresponds to the full path to only one image
	 *  if '_withRois' is True, puts associated Rois to the Roi list. Opens new RoiManeger if necessary, clears it before adding ROIs
	 */
	protected static ImagePlus openImage(File _fl,String _absPth)throws Exception{
		if (!_fl.isFile())
			throw new Exception("OpenerWithImageJ.openImage: file is not found");

		ImagePlus img=IJ.openImage(_absPth);
		if (img==null)
			throw new Exception("OpenerWithImageJ.openImage: image is not opened");
		return img;
	}
	
	public static ImagePlus openImage(File _fl)throws Exception{
		return openImage(_fl,_fl.getAbsolutePath());
	}
	
	
	public static ImagePlus openImage(String _absPth)throws Exception{
		return openImage(new File(_absPth),_absPth);
	}

	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = OpenerWithImageJ.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// path to the test image
		String pth="C:\\Halavatyi_Work\\temp_data\\Test_open_save\\";
		String in_fnm="st3_multipos_AQ_W0008_P0026_T0001.lsm";
		String out_fnm=in_fnm.replaceAll(".lsm", ".tif");
		out_fnm=out_fnm.replaceFirst("AQ", "TR1");
		
		ImagePlus img=null;
		try{
			img=ImageOpenerWithBioformats.openImage(new File(pth,in_fnm));
		}catch (Exception ex){
			IJ.error("Can not open test file");
			return;
		}
		img.show();
	}
	
	
}
