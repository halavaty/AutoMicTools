package automic.utils.imagefiles;

import java.io.File;

import automic.table.TableModel;
import ij.ImagePlus;

public interface ImageOpenerInterface {
	
	public ImagePlus openImage(String _absolutePath)throws Exception;
	
	public ImagePlus openImage(File _file)throws Exception;
	
	//public ImagePlus openImageSeries(String _absolutePath);
	
	//public ImagePlus openImageSeries(File _file);
	
	public ImagePlus openImage(TableModel _table, int _rowIndex,String _imageFileTag, String _imageFileType)throws Exception;
}
