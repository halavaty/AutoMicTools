package automic.utils.imagefiles;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;

import java.lang.Exception;

import automic.utils.FileUtils;

import java.io.File;

public abstract class TIFProcessorImageJ extends OpenerWithImageJ{
	
	
	public static ImagePlus openTIFImage(File _imageFile)throws Exception{
		String absolutePath=_imageFile.getAbsolutePath();
		if (!FileUtils.getExtension(absolutePath).equals("tif"))
			throw new Exception("Can not open non-tif file");
		return openImage(_imageFile,absolutePath);
	}
	
	
	public static ImagePlus openTIFImage(String _absolutePath)throws Exception{
		if (!FileUtils.getExtension(_absolutePath).equals("tif"))
			throw new Exception("Can not open non-tif file");
		return openImage(_absolutePath);
	}
	
	private static void saveTIFImage(ImagePlus _image,File _fileToSave,String _absolutePath,boolean _overWrite)throws Exception{
		if(!_overWrite)
			if (_fileToSave.exists())
				throw new Exception("AH_TIFprocessor.SaveTIF: file already exists, saving vill be aborted");
		if (!_absolutePath.substring(_absolutePath.lastIndexOf(".")).equals(".tif"))
			throw new Exception("AH_TIFprocessor.SaveTIF: file is not TIF");
		if (!_fileToSave.getParentFile().isDirectory())
			throw new Exception("AH_TIFprocessor.SaveTIF: dedicated directory does not exist");
		IJ.saveAs(_image, "TIFF", _absolutePath);
	}

	public static void saveTIFImage(ImagePlus _image,File _fileToSave,boolean _overWrite)throws Exception{
		saveTIFImage(_image,_fileToSave,_fileToSave.getAbsolutePath(),_overWrite);
	}
	
	public static void saveTIFImage(ImagePlus _image,String _absolutePath,boolean _overWrite)throws Exception{
		saveTIFImage(_image,new File(_absolutePath),_absolutePath,_overWrite);
	}

	
	public static void saveTIFImage(ImagePlus _image,File _fileToSave)throws Exception{
		saveTIFImage(_image,_fileToSave,_fileToSave.getAbsolutePath(),false);
	}
	
	public static void saveTIFImage(ImagePlus _image,String _absolutePath)throws Exception{
		saveTIFImage(_image,new File(_absolutePath),_absolutePath,false);
	}

	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = TIFProcessorImageJ.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// path to the test image
		String pth="C:\\Halavatyi_Work\\temp_data\\Test_open_save\\";
		String in_fnm="st3_multipos_AQ_W0008_P0026_T0001.lsm";
		String out_fnm=in_fnm.replaceAll(".lsm", ".tif");
		out_fnm=out_fnm.replaceFirst("AQ", "TR1");
		
		ImagePlus img=null;
		try{
			img=ImageOpenerWithBioformats.openImage(new File(pth,in_fnm));
		}catch (Exception ex){
			IJ.error("Can not open test file");
			return;
		}
		img.show();
		
		try{
			saveTIFImage(img,new File(pth,out_fnm));
		}catch (Exception ex){
			IJ.error("Can not save file");
			return;
		}
		
		img.changes=false;
		img.close();
		
		try{
			img=openTIFImage(new File(pth,out_fnm));
		}catch (Exception ex){
			IJ.error("Can not open new file");
			return;
		}
		img.show();
	}
	
	
}
