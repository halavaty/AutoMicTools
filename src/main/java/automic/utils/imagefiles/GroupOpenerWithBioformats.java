package automic.utils.imagefiles;

import ij.ImagePlus;

import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

import java.lang.Exception;
import java.io.File;

public class GroupOpenerWithBioformats {
	
	public GroupOpenerWithBioformats(){}
	
	private static ImagePlus openImage(String _fpth,File _fl,boolean _autoscale)throws Exception{

		if (!_fl.isFile())
			throw new Exception("GroupOpenerWithBioformats.openImage: file is not found");

		ImagePlus img=null;
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(_autoscale);
		options.setId(_fpth);
		options.setGroupFiles(true);

		img=BF.openImagePlus(options)[0];
		
		return img;		
	}
	
	public static ImagePlus openImage(File _fl,boolean _autoscale)throws Exception{
		return openImage(_fl.getAbsolutePath(),_fl,_autoscale);
	}
	
	
	public static ImagePlus openImage(String _fpth,boolean _autoscale)throws Exception{
		return openImage(_fpth,new File(_fpth),_autoscale);
	}
	
	public static ImagePlus openImage(File _fl)throws Exception{
		return openImage(_fl,false);
	}
	
	public static ImagePlus openImage(String _fpth)throws Exception{
		return openImage(_fpth,false);
	}
}
