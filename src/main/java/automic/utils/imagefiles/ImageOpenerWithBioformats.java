package automic.utils.imagefiles;

import ij.ImagePlus;
import ij.gui.Roi;
import ij.plugin.frame.RoiManager;
import imagescience.image.Image;
import imagescience.transform.Turn;
import loci.plugins.BF;
import loci.plugins.util.ROIHandler;
import loci.formats.FormatException;
import loci.formats.ImageReader;
import loci.formats.MetadataTools;
import loci.formats.meta.IMetadata;

import loci.plugins.in.ImporterOptions;

import java.lang.Exception;

import automic.geom.Point3D;

import java.io.File;
import java.io.IOException;

public class ImageOpenerWithBioformats {
	
	public static boolean turnLineScans=true;
	
	public static String colorMode=ImporterOptions.COLOR_MODE_DEFAULT;
	
	private static RoiManager rMan;
	
	public ImageOpenerWithBioformats(){}
	
	private static ImagePlus[] openAllSeries(String _fpth,File _fl/*,boolean _withRois*/,boolean _autoscale)throws Exception{
		if (!_fl.isFile())
			throw new Exception("ImageOpenerWithBioformats.openAllSeries: file is not found");

		ImagePlus [] imgs=null;
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(_autoscale);
		options.setId(_fpth);
		options.setOpenAllSeries(true);
		options.setColorMode(colorMode);

		imgs=BF.openImagePlus(options);
		
		return imgs;		
	}
	

	
	public static ImagePlus[] openAllSeries(File _fl,boolean _autoscale)throws Exception{
		return openAllSeries(_fl.getAbsolutePath(),_fl,_autoscale);
	}
	
	public static ImagePlus[] openAllSeries(String _fpth,boolean _autoscale)throws Exception{
		return openAllSeries(_fpth,new File(_fpth),_autoscale);
	}
	
	public static ImagePlus[] openAllSeries(File _fl)throws Exception{
		return openAllSeries(_fl,false);
	}
	
	public static ImagePlus[] openAllSeries(String _fpth)throws Exception{
		return openAllSeries(_fpth,false);
	}
	
	private static ImagePlus openSingleSeries(String _fpth,File _fl,int _seriesIndex,boolean _autoscale)throws Exception{
		if (!_fl.isFile())
			throw new Exception("ImageOpenerWithBioformats.openSingleSeries: file is not found");

		ImagePlus [] imgs=null;
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(_autoscale);
		options.setId(_fpth);
		options.setOpenAllSeries(false);
		options.setSeriesOn(_seriesIndex, true);
		options.setColorMode(colorMode);

		imgs=BF.openImagePlus(options);
		
		ImagePlus img=imgs[0];
		
		if(turnLineScans&&((img.getWidth()==1)||(img.getHeight()==1))) {
			img=new Turn().run(Image.wrap(img), 0, 0, 1).imageplus();//img.duplicate();
			//IJ.run(transformedImage, "TransformJ Turn", "z-angle=0 y-angle=0 x-angle=90");
		}
		
		return img;		
	}

	public static ImagePlus openSingleSeries(File _fl,int _seriesIndex)throws Exception{
		return openSingleSeries(_fl.getAbsolutePath(),_fl,_seriesIndex,false);
	}
	
	public static ImagePlus openSingleSeries(String _fpth, int _seriesIndex)throws Exception{
		return openSingleSeries(_fpth,new File(_fpth),_seriesIndex,false);
	}

	

	
	/**
	 *  Opens the image and returns corresponding ImagePlus.
	 *	Expects that file path corresponds to the full path to only one image
	 *  if '_withRois' is True, puts associated Rois to the Roi list. Opens new RoiManeger if necessary, clears it before adding ROIs
	 */
	private static ImagePlus openImage(String _fpth,File _fl,boolean _autoscale)throws Exception{
		return openSingleSeries(_fpth, _fl, 0, _autoscale);
	}
	
	public static ImagePlus openImage(File _fl,boolean _autoscale)throws Exception{
		return openImage(_fl.getAbsolutePath(),_fl,_autoscale);
	}
	
	
	public static ImagePlus openImage(String _fpth,boolean _autoscale)throws Exception{
		return openImage(_fpth,new File(_fpth),_autoscale);
	}
	
	public static ImagePlus openImage(File _fl)throws Exception{
		return openImage(_fl,false);
	}
	
	public static ImagePlus openImage(String _fpth)throws Exception{
		return openImage(_fpth,false);
	}

	
	public static void imageRoisToRoiManager(String  _fullImagePath)throws FormatException,IOException{
		rMan=RoiManager.getInstance();
		if (rMan!=null)
			rMan.runCommand("Reset");
		else
			rMan=new RoiManager();
		ImageReader reader = new ImageReader();
		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		reader.setMetadataStore(omeMeta);
		reader.setId( _fullImagePath);

		ROIHandler.openROIs(omeMeta, BF.openImagePlus(_fullImagePath)/*null*/);
		reader.close();

	}
	
	public static Roi [] getImageRois(String _apth)throws Exception{
		imageRoisToRoiManager(_apth);
		Roi[] resultRois=rMan.getRoisAsArray();
		rMan.runCommand("Reset");
		return resultRois;
	}
	
	
	public static void getTimeTags(String _fpth,double [] _ttags)throws Exception{
		/**
		 * 	prefills created array if image exists and if size of array matches number of timesteps for the image
		 *  throws exception, if something fails
		 *	if array is not specified, throws an exception
		 */
		
		if (_ttags==null) throw new Exception();
		
		ImageReader reader = new ImageReader();
		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		reader.setMetadataStore(omeMeta);
		reader.setId(_fpth);
		
		int n_tpoints=omeMeta.getPixelsSizeT(0).getValue();
		if (_ttags.length!=n_tpoints){
			reader.close();
			throw new Exception();
		}
		
		if (n_tpoints==1){
			_ttags[0]=0;
			reader.close();
			return;
		}
		for (int m=0; m<n_tpoints; m++)
			_ttags[m]=omeMeta.getPlaneDeltaT(0,reader.getIndex(0,0,m)).value().doubleValue();
		reader.close();
		return;
	}
	
	public static ImagePlus openSingleChannel(File _fl,int _seriesIndex,int _channelIndexOneBased,boolean _autoscale)throws Exception{
		if (!_fl.isFile())
			throw new Exception("ImageOpenerWithBioformats.openSingleSeries: file is not found");

		ImagePlus [] imgs=null;
		ImporterOptions options = new ImporterOptions();
		options.setAutoscale(_autoscale);
		options.setId(_fl.getAbsolutePath());
		options.setOpenAllSeries(false);
		options.setSeriesOn(_seriesIndex, true);
		options.setCBegin(_seriesIndex, _channelIndexOneBased-1);
		options.setCEnd(_seriesIndex, _channelIndexOneBased-1);
		
		imgs=BF.openImagePlus(options);
		
		return imgs[0];		
	}
	
	public static Point3D getVoxelSizeUm(String _imageFilePath)throws Exception {
		
		ImageReader reader = new ImageReader();
		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		reader.setMetadataStore(omeMeta);
		reader.setId(_imageFilePath);
		
		double xSize=omeMeta.getPixelsPhysicalSizeX(0).value().doubleValue();
		double ySize=omeMeta.getPixelsPhysicalSizeY(0).value().doubleValue();
		double zSize;
		try {
			zSize=omeMeta.getPixelsPhysicalSizeZ(0).value().doubleValue();
		}catch(Exception ex) {
			zSize=0;
		}
		reader.close();
		
		return new Point3D(xSize, ySize, zSize);
	}
	
	public static Point3D getVoxelSizeNm(String _imageFilePath) throws Exception {
		Point3D voxelSizeInUm=getVoxelSizeUm(_imageFilePath);
		
		return new Point3D(voxelSizeInUm.getX()*1000, voxelSizeInUm.getY()*1000, voxelSizeInUm.getZ()*1000);
	}
	
	public static Point3D getImageSizePx(String _imageFilePath)throws Exception {
		
		ImageReader reader = new ImageReader();
		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
		reader.setMetadataStore(omeMeta);
		reader.setId(_imageFilePath);
		
		double xSize=omeMeta.getPixelsSizeX(0).getValue();
		double ySize=omeMeta.getPixelsSizeY(0).getValue();
		double zSize=omeMeta.getPixelsSizeZ(0).getValue();

		reader.close();
		
		return new Point3D(xSize, ySize, zSize);
	}
	

}
