package automic.utils.imagefiles;

import ij.IJ;
import ij.ImagePlus;
import loci.formats.FilePattern;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

//import loci.plugins.BF;
//import loci.plugins.in.ImporterOptions;

import java.lang.Exception;

public class PatternOpenerWithBioformats {
	
	public PatternOpenerWithBioformats(){}
	
	public static ImagePlus openImageMacro(String _filePathPattern,boolean _autoscale)throws Exception{
		//TODO: check if pattern is valid

		ImagePlus img=null;
		
		IJ.run("Bio-Formats",String.format("open=%s %s color_mode=Default group_files view=Hyperstack stack_order=XYCZT contains=[] name=%s",new FilePattern(_filePathPattern).getFiles()[0],_autoscale?"autoscale":"",_filePathPattern));
		img=IJ.getImage();
		img.hide();
		
		return img;		
	}
	
	public static ImagePlus openImageTempFile(String _filePathPattern,boolean _autoscale)throws Exception{
		//TODO: check if pattern is valid

		String tempDirectory=IJ.getDirectory("current");//IJ.getDirectory("temp"); 
		
		File tempFile=new File(tempDirectory,"automic.pattern");
		
		BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        writer.write(_filePathPattern);
        writer.close();
		
		ImporterOptions options = new ImporterOptions();
		options.setId(tempFile.getAbsolutePath());
		ImagePlus img=BF.openImagePlus(options)[0];
		
		return img;		
	}
	
	public static ImagePlus openImageDirect(String _filePathPattern,boolean _autoscale)throws Exception{
		//TODO: check if pattern is valid

		//String tempDirectory=IJ.getDirectory("temp"); 
		
		//File tempFile=new File(tempDirectory,"automic.pattern");
		
		//BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        //writer.write(_filePathPattern);
        //writer.close();
		
		ImporterOptions options = new ImporterOptions();
		options.setUsingPatternIds(true);
		options.setId(_filePathPattern);
		System.out.println(options.isUsingPatternIds());
		
		ImagePlus img=BF.openImagePlus(options)[0];
		
		return img;		
	}
	
	
	public static ImagePlus openImage(String _fpth)throws Exception{
		return openImageMacro(_fpth,false);
	}
	
	public static ImagePlus openImage(File _patternFile)throws Exception{
		return openImageMacro(_patternFile.getAbsolutePath(),false);
	}

}
