package automic.utils.imagefiles;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;

import java.lang.Exception;
import java.io.File;
//import io.MetaImage_Reader;

public class MhaOpenerWithClosingWindows {
	
	public MhaOpenerWithClosingWindows(){}
	
	/**
	 *  Opens the image and returns corresponding ImagePlus.
	 *	Expects that file path corresponds to the full path to only one image
	 *  if '_withRois' is True, puts associated Rois to the Roi list. Opens new RoiManeger if necessary, clears it before adding ROIs
	 */
	private static ImagePlus openImage(File _fl,String _absPth)throws Exception{
		if (!_fl.isFile())
			throw new Exception("OpenerWithClosingWindows.openImage: file is not found");

		ImagePlus img=null;
		img=IJ.openImage(_absPth);
		//IJ.run("MHD/MHA...", String.format("open=[%s]",_absPth));
		
		img=IJ.getImage();
		if (img==null)
			throw new Exception("OpenerWithClosingWindows.openImage: image is not opened");
		if (img.isVisible())
			img.hide();
		return img;
	}
	
	public static ImagePlus openImage(File _fl)throws Exception{
		return openImage(_fl,_fl.getAbsolutePath());
	}
	
	public static ImagePlus openImage(String _absPth)throws Exception{
		return openImage(new File(_absPth),_absPth);
	}
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = MhaOpenerWithClosingWindows.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();

		// path to the test image
		String pth="C:/tempDat/DataTestComputationalTools/MHA image Tischi";
		String in_fnm="result.0.mha";
		
		ImagePlus img=null;
		try{
			File fileToOpen=new File(pth,in_fnm);
			img=MhaOpenerWithClosingWindows.openImage(fileToOpen);
			System.out.println(img.getWindow());
		}catch (Exception ex){
			IJ.error("Can not open test file");
			return;
		}
		img.show();
		
	}
	
	
}
