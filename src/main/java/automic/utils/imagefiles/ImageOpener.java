package automic.utils.imagefiles;

import java.io.File;

import automic.table.TableModel;
import automic.utils.FileUtils;
import automic.utils.Utils;
import ij.IJ;
import ij.ImagePlus;

public class ImageOpener implements ImageOpenerInterface{
	
	public ImagePlus openImage(File _file,String _absolutePath)throws Exception{
		String fileName=_file.getName();
		String extension=FileUtils.getExtension(fileName);
		
		switch (extension){
			case "lsm": 	return ImageOpenerWithBioformats.openImage(_file);
			case "lif": 	return ImageOpenerWithBioformats.openImage(_file);
			case "czi": 	return ImageOpenerWithBioformats.openImage(_file);
			case "nd2": 	return ImageOpenerWithBioformats.openImage(_file);
			case "oir": 	return ImageOpenerWithBioformats.openImage(_file);
			case "mha": 	return MhaOpenerWithClosingWindows.openImage(_file);		
			case "tif":		return IJ.openImage(_absolutePath);
			case "tiff": 	return IJ.openImage(_absolutePath);
			case "zvi": 	return ImageOpenerWithBioformats.openImage(_file); // Zeiss Axiovision format, eg. used in Microbeam

			default:	Utils.generateWarningMessage("Unrecognised image file format: opening with default fiji tools");
						return IJ.openImage(_absolutePath);
		}
		
		
	}
	
	@Override
	public ImagePlus openImage(String _absolutePath)throws Exception{
		return this.openImage(new File(_absolutePath), _absolutePath);
		
	}
	
	@Override
	public ImagePlus openImage(File _file)throws Exception{
		return this.openImage(_file,_file.getAbsolutePath());
	}
	
	
	
	//public ImagePlus openImageSeries(String _absolutePath);
	
	//public ImagePlus openImageSeries(File _file);
	
	public ImagePlus openImage(TableModel _table, int _rowIndex,String _imageFileTag, String _imageFileType)throws Exception{
		File imageFile=_table.getFile(_rowIndex, _imageFileTag, _imageFileType);
		if (imageFile==null) return null;
		switch (_imageFileType){
			case "IMG":{
				return openImage(imageFile);
			}
			case "IMGS":{
				File folderFile=imageFile.getParentFile();
				String[] nameElements=imageFile.getName().split(" #");
				
				return ImageOpenerWithBioformats.openSingleSeries(new File(folderFile,nameElements[0]), Integer.parseInt(nameElements[1])-1);
			}
			default: throw new IllegalArgumentException("Unknown image type");
		}
	}

}
