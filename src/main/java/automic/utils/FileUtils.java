package automic.utils;

import java.io.File;

import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import ij.IJ;

public abstract class FileUtils{
	
	public static void createCleanFolders(String _rootPath,String[] _folderSubPaths)throws Exception{
		for(String folderName:_folderSubPaths)
			createCleanFolder(_rootPath, folderName);
	}
	
	public static void createCleanFolder(String _rootPath,String _folderName) throws Exception{
		createCleanFolder(new File(_rootPath,_folderName));
	}
	
	public static void createCleanFolder(File _folderFile) throws Exception{
		if(_folderFile.isDirectory()){
			printWarningMessage("Cleaning folder: "+_folderFile.toString());
			cleanFolder(_folderFile);
		}else
			if(!_folderFile.mkdirs())
				throw new Exception("Unable to create directory");
	}
 	
	public static void cleanFolder (File _folder){
		if (!_folder.isDirectory()) return;
		File[] fls=_folder.listFiles();
		for(File fl:fls){
			if(fl.isDirectory())
				cleanFolder(fl);
			fl.delete();
		}
	}
	
	public static String getExtension(String _nameWithExtension){
		return _nameWithExtension.substring(_nameWithExtension.lastIndexOf('.')+1);
	}
	
	public static String getExtension(File _file){
		return getExtension(_file.getName());
	}
	
	public static String cutExtension(String _nameWithExtension){
		return _nameWithExtension.substring(0, _nameWithExtension.lastIndexOf('.'));
	}

	public static String changeExtension(String _nameWithExtension, String _newExtension){
		return cutExtension(_nameWithExtension)+"."+_newExtension;
	}
	

	
	private static void printWarningMessage(String _message){
		Logger logger=ApplicationLogger.getLogger();
		if (logger==null)
			IJ.log(String.format("[WARNING]: %s", _message));
		else
			logger.sendWarningMessage(_message);
	}
}
