package automic.utils;

import java.util.Random;

public abstract class ArrIndUtils{
	public static int[] getTrueInd(boolean[] _flags){
		int size=0;
		for (boolean f:_flags)
			if(f)
				size++;

		int[] res=new int[size];
		int ia=0;

		int bsize=_flags.length;
		for (int i=0;i<bsize;i++)
			if(_flags[i]){
				res[ia]=i;
				ia++;
			}
		
		return res;
	}
	
	public static int getNTrue(boolean[] _flags){
		int nt=0;

		for (boolean p:_flags)
			if(p)
				nt++;
		
		return nt;
	}
	
	public static boolean[] getFlagsArr(int[] _inds, int _size){
		boolean[] res=new boolean[_size];
		for (int i=0;i<_size;i++)
			res[i]=false;
		
		if (_inds==null) return res;
		for (int id:_inds)
			res[id]=true;
		
		return res;
		
	}
	
	/**
	 * tests whether given index is in the array
	 * @param _targInd
	 * @param _refInds
	 * @return 
	 */
	public boolean isIndexInArray(int _targInd, int[] _refInds){
		if (_refInds==null) return false;
		for (int id:_refInds)
			if (id==_targInd)
				return true;
		return false;
	}
	
	/**
	 * 
	 * @param _cand
	 * @return maximum number from given array
	 */
	public static int getMaxInt(int[] _cand){
		int maxv=Integer.MIN_VALUE;
		for (int c:_cand){
			if (c>maxv)
				maxv=c;
		}
		return maxv;
	}

	/**
	 * 
	 * @param _cand
	 * @return minimum number from given array
	 */
	public static int getMinInt(int[] _cand){
		int minv=Integer.MAX_VALUE;
		for (int c:_cand){
			if (c<minv)
				minv=c;
		}
		return minv;
	}

	
	public static int[] getRandomIndexes(int _maxInd, int _nItemsToSelect){
		Random rnd=new Random();
		
		boolean[] flags=new boolean[_maxInd];
		
		for (int iFlag=0;iFlag<_maxInd;iFlag++)
			flags[iFlag]=false;
		
		for (int i=0;i<_nItemsToSelect;i++){
			int testInd=rnd.nextInt(_maxInd);

			while(flags[testInd])
				testInd=rnd.nextInt(_maxInd);
			
			flags[testInd]=true;
		}

		
		
		return getTrueInd(flags);
		
	}
	
}
