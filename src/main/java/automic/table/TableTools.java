package automic.table;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;


/**
 * class summarises static methods to move, merge and reorganise tables
 * @author Aliaksandr Halavatyi
 *
 */

public class TableTools{

	/**
	 * moves the table text file and changes all PathName-s in this file accordingly.
	 * @param _oldLocation
	 * @param _oldFileName
	 * @param _newLocation
	 * @param _newFileName
	 * @throws Exception
	 */
	public static void moveTable(String _oldLocation,String _oldFileName,String _newLocation,String _newFileName)throws Exception{
		TableModel tbModel=new TableModel(new File(_oldLocation,_oldFileName));
		tbModel.setRootPathDeep(_newLocation);
		tbModel.writeNewFile(_newFileName, true);
	}
	
	/**
	 * merges several tables. All input files are assumed to be in the same place. If input and output locations are not the same, changes all pathName-s in the resulting table.
	 * @param _inputLocation
	 * @param _inputFileNames
	 * @param _newLocation
	 * @param _mergedFileName
	 * @throws Exception if headers of some tables (number of columns or column names) are not the same 
	 */
	public static TableModel mergeTables(String _inputLocation, String[] _inputFileNames, String _mergedRootPath)throws Exception{
		final String summColumnName="InputTableName";
		final int nInputFiles=_inputFileNames.length;
		TableModel mergedTable=new TableModel(new File(_inputLocation,_inputFileNames[0]));
		addColumn(mergedTable, summColumnName, _inputFileNames[0]);

		TableModel addTable;
		for (int iFile=1;iFile<nInputFiles;iFile++){
			addTable=new TableModel(new File(_inputLocation,_inputFileNames[iFile]));
			addColumn(addTable, summColumnName, _inputFileNames[iFile]);
			copyAllRows(mergedTable, addTable);
		}
		
		if (!_inputLocation.equals(_mergedRootPath))
			mergedTable.setRootPathDeep(_mergedRootPath);
		
		return mergedTable;
	}
	
	/**
	 * 
	 * @param _conditionTableFile
	 * @param _tableFileTag
	 * @param _mergedRootPath
	 * @param _mergedFileName
	 * @throws Exception
	 */
	public static TableModel mergeTables(TableModel _conditionTable, String _tableFileTag, String _mergedRootPath)throws Exception{
		
		if(!_conditionTable.isRootPath(_mergedRootPath))
			_conditionTable.setRootPathDeep(_mergedRootPath);
		final int nInputFiles=_conditionTable.getRowCount();
		
		TableModel mergedTable=new TableModel(_conditionTable.getFile(0, _tableFileTag, "TXT"));
		if(!mergedTable.isRootPath(_mergedRootPath))
			mergedTable.setRootPathDeep(_mergedRootPath);
		addColumnsFromTable(mergedTable,_conditionTable,0);
		
		TableModel addTable;
		for (int iFile=1;iFile<nInputFiles;iFile++){
			addTable=new TableModel(_conditionTable.getFile(iFile, _tableFileTag, "TXT"));
			if(!addTable.isRootPath(_mergedRootPath))
				addTable.setRootPathDeep(_mergedRootPath);
			addColumnsFromTable(addTable,_conditionTable,iFile);
			copyAllRows(mergedTable, addTable);
			
		}
		
		return mergedTable;
	}
	
	/**
	 * 
	 * @param _conditionTableFile
	 * @param _rawTablesLocation
	 * @param _tagColumnName
	 * @param _tableNameTemplate
	 * @param _mergedRootPath
	 * @param _mergedFileName
	 * @throws Exception
	 */
	public static TableModel mergeTables(TableModel _conditionTable,String _rawTablesLocation, String _tagColumnName,String _tableNameTemplate, String _mergedRootPath)throws Exception{
		
		if(!_conditionTable.isRootPath(_mergedRootPath))
			_conditionTable.setRootPathDeep(_mergedRootPath);
		final int nInputFiles=_conditionTable.getRowCount();
		
		TableModel mergedTable=new TableModel(new File(_rawTablesLocation,String.format(_tableNameTemplate, _conditionTable.getStringValue(0, _tagColumnName))));
		if(!mergedTable.isRootPath(_mergedRootPath))
			mergedTable.setRootPathDeep(_mergedRootPath);
		addColumnsFromTable(mergedTable,_conditionTable,0);
		
		TableModel addTable;
		for (int iFile=1;iFile<nInputFiles;iFile++){
			addTable=new TableModel(new File(_rawTablesLocation,String.format(_tableNameTemplate, _conditionTable.getStringValue(iFile, _tagColumnName))));
			if(!addTable.isRootPath(_mergedRootPath))
				addTable.setRootPathDeep(_mergedRootPath);
			addColumnsFromTable(addTable,_conditionTable,iFile);
			copyAllRows(mergedTable, addTable);
			
		}
		
		return mergedTable;
	}
	
	public static void copyRow(TableModel _toTable, TableModel _fromTable, int _rowIndex)throws Exception{
		if (!tableHeadersMatch(_toTable, _fromTable))
			throw new Exception ("Header of tables do not match");
		@SuppressWarnings({"unchecked","rawtypes"})
		Vector<Vector> dataVector= _fromTable.getDataVector();
		_toTable.addRow(dataVector.get(_rowIndex));
	}

	public static void copyAllRows(TableModel _toTable, TableModel _fromTable)throws Exception{
		if (!tableHeadersMatch(_toTable, _fromTable))
			throw new Exception ("Header of tables do not match");
		@SuppressWarnings({"unchecked","rawtypes"})
		Vector<Vector> dataVector= _fromTable.getDataVector();
		final int nRows=_fromTable.getRowCount();
		for (int rowIndex=0;rowIndex<nRows;rowIndex++)
			_toTable.addRow(dataVector.get(rowIndex));
		
	}

	
	public static void addColumn(TableModel _table, String _columnName, Object _defaultValue)throws Exception{
		_table.addColumn(_columnName);
		int columnIndex=_table.getColumnIndex(_columnName);
		final int nDatasets=_table.getRowCount();
		for(int i=0;i<nDatasets;i++){
			_table.setValueAt(_defaultValue, i, columnIndex);
		}
	}
	
	public static void addColumnsFromTable(TableModel _modifiedTable, TableModel _referenceTable, int _referenceRowIndex) throws Exception{
		String[] _columnsToAdd=_referenceTable.getColumnNames();
		for (String columnName:_columnsToAdd)
			addColumn(_modifiedTable, columnName, _referenceTable.getValueAt(_referenceRowIndex, columnName));
	}
	
	
	
	public static boolean tableHeadersMatch(TableModel _table1, TableModel _table2){
		final int nColumns=_table1.getColumnCount();
		if (_table2.getColumnCount()!=nColumns)
			return false;
		for (int i=0;i<nColumns;i++)
			if(!_table1.getColumnName(i).equals(_table2.getColumnName(i)))
				return false;
		
		return true;
	}
	
	public static void tableTimeDelayInHours(TableModel _table, String _startColumnName, String _endColumnName, String _delayColumnTag)throws Exception{
		DateFormat format=TableSettings.dateTimeFormat;
		_table.addValueColumn(_delayColumnTag, "NUM");
		int nData=_table.getRowCount();
		String startString,endString;
		for (int i=0;i<nData;i++){
			startString=_table.getStringValue(i, _startColumnName);
			endString=_table.getStringValue(i, _endColumnName);
			if ((startString==null)||(endString==null))
				continue;
			_table.setNumericValue(timeDelayInHours(startString, endString, format), i, _delayColumnTag);
		}
	}

	public static void tableTimeDelayInMinutes(TableModel _table, String _startColumnName, String _endColumnName, String _delayColumnTag)throws Exception{
		DateFormat format=TableSettings.dateTimeFormat;
		_table.addValueColumn(_delayColumnTag, "NUM");
		int nData=_table.getRowCount();
		String startString,endString;
		for (int i=0;i<nData;i++){
			startString=_table.getStringValue(i, _startColumnName);
			endString=_table.getStringValue(i, _endColumnName);
			if ((startString==null)||(endString==null))
				continue;
			_table.setNumericValue(timeDelayInMinutes(startString, endString, format), i, _delayColumnTag);
		}
	}
	
	public static double timeDelayInHours(String _dateTimeStart, String _dateTimeEnd, DateFormat _dateTimeFormat)throws Exception{
		return (_dateTimeFormat.parse(_dateTimeEnd).getTime()-_dateTimeFormat.parse(_dateTimeStart).getTime())/(double)3600000;
	}

	public static double timeDelayInMinutes(String _dateTimeStart, String _dateTimeEnd, DateFormat _dateTimeFormat)throws Exception{
		return (_dateTimeFormat.parse(_dateTimeEnd).getTime()-_dateTimeFormat.parse(_dateTimeStart).getTime())/(double)60000;
	}
	
	/**
	 * removes duplicated file records for the given fileTag and fileType. Always keeps the first record.
	 * Use case - correcting experimental table when some fields are not cleaned during acquisition
	 * @param _table
	 * @param _fileTag
	 * @param _fileType
	 * @throws Exception
	 */
	public static void removeDuplicatedFileInfo(TableModel _table,String _fileTag, String _fileType)throws Exception{
		List<String> absoluteFileList=new ArrayList<String>();
		int nData=_table.getRowCount();
		
		String testPath;
		for (int i=0;i<nData;i++){
			testPath=_table.getFileAbsolutePathString(i, _fileTag, _fileType);
			if(testPath==null) continue;
			if(absoluteFileList.contains(testPath)){
				_table.cleanFileRecord(i, _fileTag, _fileType);
			}else{
				absoluteFileList.add(testPath);
			}
		}
			
	}
	
	public static double[] getAverageNumArray(TableModel _parentTable, String _childTableTag, String _numTag, String _factTag,String _factValue)throws Exception{
		int npoints=0;
		int nDataParent=_parentTable.getRowCount();
		
		
		double [] result=null;
		int fileCount=0;
		
		for (int i=0;i<nDataParent;i++){
			if(!_parentTable.getFactorValue(i, _factTag).equals(_factValue)) continue;
			File refFile=_parentTable.getFile(i, _childTableTag, "TXT");
			if(refFile==null)continue;
			TableModel refTable=new TableModel(refFile);
			if(fileCount==0){
				npoints=refTable.getRowCount();
				result=new double[npoints];
				for (int p=0;p<npoints;p++)
					result[p]=refTable.getNumericValue(p, _numTag);
				
			}else{
				for (int p=0;p<npoints;p++)
					result[p]+=refTable.getNumericValue(p, _numTag);
			}
			fileCount++;
		}
		
		if ((fileCount<1)||(npoints<1))
			return null;
		
		for (int i=0;i<npoints;i++)
			result[i]=result[i]/fileCount;

		return result;
	}

	
	/**
	 * simple test/debug code
	 */
	public static void main(String[] args)throws Exception{
		String rawTablePath="C:/tempDat/VSVG experiments/01042016-AutoFRAP-cargo/living";
		String[] rawTableNames={"summary_gr1_.txt","summary_gr2_.txt","summary_gr3_.txt","summary_gr3_2_.txt"};
		String mergeTablePath="C:/tempDat/VSVG experiments/01042016-AutoFRAP-cargo/living/Process1";
		String mergeTableName="summary_merge.txt";
		
		TableModel mergedTable1=TableTools.mergeTables(rawTablePath, rawTableNames, mergeTablePath);
		mergedTable1.writeNewFile(mergeTableName, true);
		
		String globalExperimentTableName="summary_global_manual.txt";
		String mergeTableName2="summary_merge2.txt";
		TableModel mergedTable2=TableTools.mergeTables(new TableModel(new File(rawTablePath,globalExperimentTableName)), "ExpTable", mergeTablePath);
		mergedTable2.writeNewFile(mergeTableName2, true);
		System.out.println("Test end 1");
	}
	
	
	
}
