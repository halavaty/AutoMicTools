package automic.table;

import java.awt.Point;

import ij.ImagePlus;
import ij.gui.ImageWindow;

/**
 * Save properties of the images visualised with Manual Control Frame
 * @author halavatyi
 *
 */

public class ImageProperties {
	String imageTag;
	
	ImagePlus image;
	
	ImageWindow imageWindow;
	
	Point imageLocation;
	
	Boolean stack;
	Boolean hyperStack;
	Boolean composite;
	
	Integer stackIndex;
	Integer channelIndex;
	Integer sliceIndex;
	Integer frameIndex;
	
	
	Integer nChannels;
	Double [] minIntensityValues;//contrast saved separately for each channel
	Double [] maxIntensityValues;
	

	public ImageProperties(String _imageTag, int _xWindowPosition, int _yWindowPosition){
		imageTag=_imageTag;
		imageLocation=new Point(_xWindowPosition, _yWindowPosition);
	}
	
	public void saveImageProperties(){
		if (image==null)
			return;
		
		imageWindow=image.getWindow();
		stack=(image.getNDimensions()>2);
		hyperStack=image.isHyperStack();
		
		stackIndex=(stack)?image.getCurrentSlice():null;
		
		channelIndex=(hyperStack)?image.getC():null;
		sliceIndex=(hyperStack)?image.getZ():null;
		frameIndex=(hyperStack)?image.getT():null;
		
		nChannels=(hyperStack)?image.getNChannels():null;
		
		if(hyperStack){
			channelIndex=image.getC();
			sliceIndex=image.getZ();
			frameIndex=image.getT();
			
			nChannels=(hyperStack)?image.getNChannels():null;
			minIntensityValues=new Double[nChannels];
			maxIntensityValues=new Double[nChannels];
		}else{
			channelIndex=null;
			sliceIndex=null;
			frameIndex=null;
			
			nChannels=null;
			minIntensityValues=new Double[1];
			maxIntensityValues=new Double[1];
		}
		
	}
}
