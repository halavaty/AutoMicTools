package automic.table;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import automic.utils.FileUtils;
import automic.utils.Utils;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import automic.utils.imagefiles.PatternOpenerWithBioformats;
import automic.utils.roi.ROIManipulator;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.GenericDialog;
import ij.gui.ImageWindow;
import ij.gui.Overlay;
import ij.gui.Roi;
import ij.gui.WaitForUserDialog;
import ij.plugin.PlugIn;
import ij.plugin.frame.RoiManager;
import ij.process.ImageStatistics;
import ij.io.OpenDialog;

/**
 * This plugin represents an interface to visualise the results of Automated Microscopy experiments.
 * <p>
 * WARNING: Java 1.7 or later is required. 
 * <p>
 * Last update: 02/03/2016
 * @author Aliaksandr Halavatyi
 */

//Previous version: ManualControlFrame5_plot_PostRois

@SuppressWarnings("serial")
public class ManualControlFrame extends JFrame implements PlugIn {
	
	private static final String nextString="Next>>";
	private static final String prevString="<<prev";
	private static final String saveString="Save";
	private static final String plotString_on="Show plot";
//	private static final String plotString_off="Hide plot";
	private static final String plotIdString="Get point";
	private static final String getRoiString="Get post Rois";
	
	private final String[] contrastOptions={"No adjustment","Default range","Min_max"}; 
//	private static final String sDefContr="Default contrast";
//	private static final String sMinMaxContr="Min-Max contrast";
	
	/**tag for the BOOL column used in combination with the data quality control checkbox*/
	public static final String successWord="Success";
	
	/**hight of frame in pixels when it is created*/
	public static final int FrameHEIGHT = 400;
	
	private int nImg;
	private int nPostRoi;
	
	private String[] ImgTags;
	private String[] PostRoiTags;
	
	private boolean [] VisImg;
	private boolean[][] VisRoi;
	
	private Point[] Img_Loc;
	
	private ImageWindow[] WindImg;
	private	ImagePlus [] Img;
	
	private static final int RMAN_X=1408;
	private static final int RMAN_Y=200;
	
	private static final Dimension butDim=new Dimension(100,20);
	
	private JTable dsets; 
	private JButton nextButton;
	private JButton prevButton;
	private JButton saveButton;
	private JButton plotButton;
	private JButton plotIdButton;
	private JButton getRoiButton;
	
	private JCheckBox chbUseful;
	private JCheckBox chbFitImg;
	private JCheckBox chbRepaintWindows;
	private JCheckBox chbShowOvl;
	private JCheckBox chbRemRoiPos;
//	private JRadioButton butContrDef;
//	private JRadioButton butContrMinMax;
	private JComboBox<String> comboContrast;
	
	private JPanel statusBar;
	private JLabel statusBarLabel;
	
	ImgRoiPanel[] ImgPanels;
	
	private int cur_ind=-1;

	private static RoiManager rMan;

	private TableModel tbModel;
	
	private ImageOpenerInterface imageOpener; 
    
    private boolean fitImg=false;
    private boolean repaintImg=false;
    
    //for Roi recording
    private int outTblInd=0;
	private static final String outRoiTg="ManualOVL";
	private String outRoiApth=null;
	private static String defOutTblNm="UpdatedTable.txt";
    
    class ChbShImgListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		Object source=e.getSource();
    		
    		for (int i=0;i<nImg;i++){
    			if (source==ImgPanels[i].getchbImg()){
    				if (e.getStateChange()==ItemEvent.SELECTED){
    					VisImg[i]=true;
    				}else{
    					VisImg[i]=false;
    				}
    				visualise(cur_ind);
    				break;
    			}
    				
    		}
    	}
    } 
    class ChbShRoiListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		int imgInd=-1;
    		boolean state=(e.getStateChange()==ItemEvent.SELECTED);
    		Object source=e.getSource();
    		for (int j=0;j<nImg;j++)
    			for (int i=0;i<=nPostRoi;i++)
    				if (source==ImgPanels[j].getchbRoi(i)){
    					imgInd=j;
    					VisRoi[imgInd][i]=state;
    					break;
    				}
    		visRois(imgInd);
    	}
    } 

    
    ChbShImgListener chbImgListener;
    ChbShRoiListener chbRoiListener;
    //
    
    /**
     * Constructs empty ManualControlFrame object without any associated data.
     * Needed to run as a plugin from ImageJ/Fiji menu
     * Use other constructors if called explicitly 
     */
    public ManualControlFrame() {
    	super("AutoMic Browser");
    	Utils.checkJavaVersion(1.7);
    	imageOpener=new ImageOpener();
	}
    
    /**
     * constructor based on previously created model
     * @param _tbModel TableModel that contains relevant data
     */
    public ManualControlFrame(TableModel _tbModel) {
    	this(_tbModel,true);
    }    

    public ManualControlFrame(TableModel _tbModel,boolean _imagesVisible){
    	this();
    	this.initialise(_tbModel,_imagesVisible);
    }
    
    /**
     * Constructs table model from the text file in which data are saved.
     * Creates TableModel from these data.
     * @param _abspth Absolute path to this file 
     */
    public ManualControlFrame(String _abspth) {
    	this(_abspth,true);
    }
    
    public ManualControlFrame(String _abspth,boolean _imagesVisible){
    	this();
    	this.initialise(_abspth,_imagesVisible);
    }
    

    /**
     * Runs dispose method of the parent class to destroy main window and release associated memory.
     * Closes all associated image windows if any.
     */
    @Override
    public void dispose() {
		for (int i=0;i<nImg;i++){
			if (WindImg[i]!=null){
				WindImg[i].dispose();
				WindImg[i]=null;
			}
		}

		super.dispose();
	}
    
    /**
     * Runs initialisation with provided data
     * @param _tbModel created TableModel
     */
	protected void initialise(TableModel _tbModel, boolean _imagesVisible) {
		tbModel=_tbModel;
		
		//create form event listeners
		chbImgListener=new ChbShImgListener();
		chbRoiListener=new ChbShRoiListener();
		
		//get tags for images and ROIs
		ImgTags=tbModel.getImageFileTags();
		PostRoiTags=tbModel.getFileTags("ROI");
		nImg=(ImgTags==null)?0:ImgTags.length;
		nPostRoi=(PostRoiTags==null)?0:PostRoiTags.length;
		
		//create arrays and fill in predefined values
		VisImg=new boolean[nImg];
		Img_Loc=new Point[nImg];
		WindImg=new ImageWindow[nImg];
		Img=new ImagePlus[nImg];
		
		VisRoi=new boolean[nImg][];
		for(int i=0;i<nImg;i++){
			Img[i]=null;
			WindImg[i]=null;
			VisImg[i]=_imagesVisible;
			Img_Loc[i]=new Point(2+200*i,2);
			
			VisRoi[i]=new boolean[nPostRoi+1];		//reserve one for experimental Rois
			for (int j=0;j<=nPostRoi;j++)
				VisRoi[i][j]=false;
		}
		
		rMan=ROIManipulator2D.getEmptyRm();
		rMan.setLocation(RMAN_X, RMAN_Y);

		generateGUI();
	}
	
	/**
	 * Runs initialisation with the data stored in a text file.
	 * @param _abspth absolute path to the text file with the data
	 */
	protected void initialise(String _abspth, boolean _imagesVisible) {
		File fl=new File(_abspth);
		if (!fl.exists()){
			IJ.error("Input error", "ManualMicControlFrame_: Wrong summary file name");
			return;
		}
		
		try{
			this.initialise(new TableModel(fl),_imagesVisible);
		}catch (Exception ex){
			IJ.error("ManualMicControlFrame_: Unable to create Table Model");
			return;
		}
	}
    
    class ImgRoiPanel extends JPanel{
    	int nPostRois;
    	JCheckBox chbShImg;
    	JCheckBox [] chbsShowRois;
    	
    	public ImgRoiPanel(String _imgNm, String[] _postRoiNms,boolean _imageVisible){
    		super(new GridLayout(0,1));
    		chbShImg=new JCheckBox(_imgNm,_imageVisible);
    		Font ofont=chbShImg.getFont();
    		Font ifont=new Font(ofont.getName(),Font.BOLD,ofont.getSize());
    		Font rfont=new Font(ofont.getName(),Font.ITALIC,ofont.getSize());
    		
    		chbShImg.setFont(ifont);
    		chbShImg.addItemListener(chbImgListener);
    		this.add(chbShImg);
    		
    		nPostRois=_postRoiNms.length;
    		chbsShowRois=new JCheckBox[nPostRois+1];//last checkbox reserved for experimental ROIs
    		for (int i=0;i<nPostRois;i++){
    			chbsShowRois[i]=new JCheckBox(_postRoiNms[i],false);
    			chbsShowRois[i].setFont(rfont);
    			chbsShowRois[i].addItemListener(chbRoiListener);
        		this.add(chbsShowRois[i]);
    		}
			chbsShowRois[nPostRois]=new JCheckBox("Exp ROIs",false);
			chbsShowRois[nPostRois].setFont(rfont);
			chbsShowRois[nPostRois].addItemListener(chbRoiListener);
    		this.add(chbsShowRois[nPostRois]);

    	}
    	
    	public JCheckBox getchbImg(){
    		return chbShImg;
    	}
    	
    	public JCheckBox getchbRoi(int _rind){
    		return chbsShowRois[_rind];
    	}
    	
    	public void setchbRoiEnabled(boolean _state){
    		for (JCheckBox chb:chbsShowRois)
    			chb.setEnabled(_state);
    	}
    }
	
	private void generateGUI(){
		KListener Listen=new KListener();
		
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        this.setFocusable(true);
        this.addKeyListener(Listen);
		
		Toolkit kit = Toolkit.getDefaultToolkit();
		this.setLocation( 5,
						 kit.getScreenSize().height - FrameHEIGHT-42);
		this.setSize(kit.getScreenSize().width-5,FrameHEIGHT);
		
		JPanel mainPanel=new JPanel(new BorderLayout(10,10)); //this.getContentPane();//Box mainBox = Box.createVerticalBox();
		
		JPanel upperPanel=new JPanel(new BorderLayout());
		
		dsets=new JTable(tbModel);
		dsets.setPreferredScrollableViewportSize(new Dimension(1800, 100));
		dsets.setFillsViewportHeight(true);
		dsets.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		dsets.setRowSelectionAllowed(true);
		dsets.setColumnSelectionAllowed(false);
		dsets.addKeyListener(Listen);
		dsets.getSelectionModel().addListSelectionListener(new ListHandler());
		JScrollPane scrollPane = new JScrollPane(dsets);
		upperPanel.add(scrollPane, BorderLayout.CENTER);
		
		mainPanel.add(upperPanel, BorderLayout.PAGE_START);//
		
		Box parBox=Box.createHorizontalBox();
//
		Box paramBox = Box.createVerticalBox();//new JPanel(new GridLayout(0,1));// Box.createHorizontalBox();
		chbUseful=new JCheckBox("Use dataset");
		paramBox.add(chbUseful);
		chbUseful.setAlignmentX(Box.LEFT_ALIGNMENT);
		chbUseful.addItemListener(new ChbUseListener());
		chbUseful.setEnabled(tbModel.hasValueTag(successWord, "BOOL"));
		if (chbUseful.isEnabled())
			chbUseful.addItemListener(new ChbUseListener());
		chbFitImg=new JCheckBox("Fit image to the frame");
		paramBox.add(chbFitImg);
		chbFitImg.setAlignmentX(Box.LEFT_ALIGNMENT);
		chbFitImg.addItemListener(new ChbFitListener());
		chbRepaintWindows=new JCheckBox("Repaint image windows each time");
		paramBox.add(chbRepaintWindows);
		chbRepaintWindows.setAlignmentX(Box.LEFT_ALIGNMENT);
		chbRepaintWindows.addItemListener(new ChbRepaintListener());
		chbShowOvl=new JCheckBox("Show overlays",true);
		paramBox.add(chbShowOvl);
		chbShowOvl.setAlignmentX(Box.LEFT_ALIGNMENT);
		chbShowOvl.addItemListener(new ChbShowOvlListener());
		chbRemRoiPos=new JCheckBox("Remove ROI stack position",false);
		paramBox.add(chbRemRoiPos);
		chbRemRoiPos.setAlignmentX(Box.LEFT_ALIGNMENT);
//		JPanel contrButPanel = new JPanel(new GridLayout(0,1));
//		ButtonGroup butGrContr=new ButtonGroup();
//		butContrDef=new JRadioButton("Default Contrast (0-255)");
//		butContrDef.addActionListener(rListener);
//		butContrDef.setActionCommand(sDefContr);
//		butGrContr.add(butContrDef);
//		paramPanel.add(butContrDef);
//		butContrMinMax=new JRadioButton("Min-max contrast");
//		butContrMinMax.addActionListener(rListener);
//		butContrMinMax.setActionCommand(sMinMaxContr);
//		butGrContr.add(butContrMinMax);
//		paramPanel.add(butContrMinMax);
		
		//replacing Button Group with ComboBox
		paramBox.add(Box.createVerticalGlue());
		Box contrastBox=Box.createHorizontalBox();
		
		contrastBox.setAlignmentX(LEFT_ALIGNMENT);
		contrastBox.setPreferredSize(new Dimension(200,30));

		contrastBox.add(Box.createHorizontalStrut(3));
		contrastBox.add(new JLabel("Image contrast"),BorderLayout.WEST);
		comboContrast=new JComboBox<String>(contrastOptions);
		comboContrast.addActionListener(contrListener);
		contrastBox.add(Box.createHorizontalStrut(5));
		contrastBox.add(comboContrast,BorderLayout.CENTER);
		contrastBox.add(Box.createHorizontalStrut(20));
		paramBox.add(contrastBox);
		
		parBox.add(paramBox);
//
		ImgPanels=new ImgRoiPanel[nImg];
		for (int i=0;i<nImg;i++){
			ImgPanels[i]=new ImgRoiPanel(ImgTags[i],PostRoiTags,VisImg[i]);
			parBox.add(ImgPanels[i]);
		}
		
		parBox.setPreferredSize(new Dimension(WIDTH,150));
		mainPanel.add(parBox,BorderLayout.CENTER);//
//
		Box buttonBox=Box.createHorizontalBox();
		buttonBox.setAlignmentX(CENTER_ALIGNMENT);
		buttonBox.setAlignmentY(BOTTOM_ALIGNMENT);
		
		buttonBox.add(Box.createGlue());
		
		prevButton = new JButton(prevString);
        prevButton.setActionCommand(prevString);
        prevButton.addActionListener(new PrevListener());
        prevButton.setPreferredSize(butDim);
        prevButton.setEnabled(false);
        buttonBox.add(prevButton);
        buttonBox.add(Box.createHorizontalStrut(5));

		nextButton = new JButton(nextString);
        nextButton.setActionCommand(nextString);
        nextButton.addActionListener(new NextListener());
        nextButton.setPreferredSize(butDim);
        if (tbModel.getRowCount()<2)
        	nextButton.setEnabled(false);
        buttonBox.add(nextButton);
        buttonBox.add(Box.createHorizontalStrut(5));
		
		saveButton = new JButton(saveString);
        saveButton.setActionCommand(saveString);
        saveButton.addActionListener(new SaveListener());
        saveButton.setPreferredSize(butDim);
        buttonBox.add(saveButton);

		plotButton = new JButton(plotString_on);
        plotButton.setActionCommand(plotString_on);
        plotButton.addActionListener(new PlotListener());
        plotButton.setPreferredSize(butDim);
        buttonBox.add(plotButton);

		plotIdButton = new JButton(plotIdString);
        plotIdButton.setActionCommand(plotIdString);
        plotIdButton.addActionListener(new PlotIdListener());
        plotIdButton.setPreferredSize(butDim);
        plotIdButton.setEnabled(false);
        buttonBox.add(plotIdButton);

		getRoiButton = new JButton(getRoiString);
        getRoiButton.setActionCommand(getRoiString);
        getRoiButton.addActionListener(new GetRoiListener());
        getRoiButton.setPreferredSize(butDim);
        buttonBox.add(getRoiButton);
        
        buttonBox.add(Box.createGlue());
        buttonBox.setPreferredSize(new Dimension(WIDTH,40));
        
        
		mainPanel.add(buttonBox,BorderLayout.PAGE_END);//


		
		comboContrast.setSelectedIndex(0);
		//butContrDef.setSelected(true);
    	if (tbModel.getRowCount()>0)
			dsets.setRowSelectionInterval(0,0);
    	
        Container contPane=this.getContentPane();
    	contPane.add(mainPanel, BorderLayout.CENTER);
        
        //add Status Bar to display RootPath. TO DO later - update if RootPath is changed
		this.statusBar=new JPanel();
		statusBar.setBorder(new BevelBorder(BevelBorder.LOWERED));
		statusBar.setPreferredSize(new Dimension(WIDTH,20));
		statusBar.setLayout(new BoxLayout(statusBar, BoxLayout.X_AXIS));
		statusBarLabel = new JLabel();
		statusBarLabel.setHorizontalAlignment(SwingConstants.LEFT);
		statusBar.add(statusBarLabel);
		this.writeStatusBarLabel();
		
		contPane.add(statusBar,BorderLayout.PAGE_END);//,BorderLayout.PAGE_END
	}

	private void writeStatusBarLabel(){
		this.statusBarLabel.setText("RootPath: "+tbModel.getRootPath());
	}
	
	class KListener extends KeyAdapter{
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			if (key==KeyEvent.VK_U){
				//IJ.log("U pressed");
				chbUseful.setSelected(!chbUseful.isSelected());
			}
		}
	
		public void keyReleased(KeyEvent e) {}
		public void keyTyped(KeyEvent e) {}
	}
	
	class ContrastListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
        	setContrast();
        }
    }
	private ContrastListener contrListener=new ContrastListener();

	

	class NextListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	int ind=dsets.getSelectedRow()+1;
        	dsets.setRowSelectionInterval(ind, ind);
        }
    }
	
    class PrevListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	int ind=dsets.getSelectedRow()-1;
        	dsets.setRowSelectionInterval(ind, ind);
        }
    }
    
    class SaveListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
        	//save data to the text file
        	//SaveDialog dls=new SaveDialog("Save modified summary file",tbModel.getRootPath(),"filtered",".txt");
        	//String pth=dls.getDirectory();
        	GenericDialog gd=new GenericDialog("Save current table");
        	gd.addStringField("Output FileName", defOutTblNm);
        	gd.showDialog();
            if (gd.wasCanceled()) return;
            defOutTblNm= gd.getNextString();
        	if (defOutTblNm==null)return;
        	try{
        		tbModel.writeNewFile(defOutTblNm, true);//RecordFileHandler.WriteFile(new File(fnm), datas);
        	}catch (Exception ex){
        		new WaitForUserDialog("Unable to create a file");
        		return;
        	}
        	IJ.log("Summary file successfully saved");
        	
        	
        }
    }

    class PlotListener implements ActionListener {
        boolean active=false;
    	public void actionPerformed(ActionEvent e) {
    		/* TO DO: reimplement plot listener with JFreeChart*/
//			int xind,yind;
//    		if (!Rengine.versionCheck()) {
//			    IJ.error("** Version mismatch - Java files don't match library version.");
//			    return;
//			}
//    		//show or hide plot
//    		if(active){//hide plot
//    			plotIdButton.setEnabled(false);
//    			plotButton.setText(plotString_on);
//    			re.eval("dev.off()");
//		        //re.end();
//		        //re=null;
//    		}else{//show plot
//    			String [] cnams=tbModel.getValTags("NUM");
//    			GenericDialog gd_plot=new GenericDialog("Plot settings");
//    			gd_plot.addChoice("X values", cnams, cnams[0]);
//    			gd_plot.addChoice("Y values", cnams, cnams[0]);
//    			
//    			gd_plot.showDialog();
//    			if (gd_plot.wasCanceled()){
//    				IJ.showMessage("Plotting was aborted by user.");
//    				return;
//    			}
//    			xind=gd_plot.getNextChoiceIndex();
//    			yind=gd_plot.getNextChoiceIndex();
//    			try{
//    				xv=tbModel.getDoubColumn(cnams[xind]);
//    				yv=tbModel.getDoubColumn(cnams[yind]);
//    			}catch (Exception ex){
//    				IJ.error("Selected columns do not contain numeric valuess");
//    				return;
//    			}
//    			
//    			IJ.log("Trying to create a plot");
//    			plotButton.setText(plotString_off);
//    			if(re==null){	
//    				re=new Rengine(new String[0], false, null);
//    				if (!re.waitForR()) {
//    					IJ.error("Cannot load R");
//    					return;
//    				}
//    			}
//    		    plotIdButton.setEnabled(true);
//    		        
//    		    try{
//    		     	//re.eval("library(JavaGD)");
//    		     	//re.eval("JavaGD()");
//    		     	re.eval("windows()");
//    		       	re.assign("xv", xv);
//    		       	re.assign("yv", yv);
//    		       	re.eval("plot(xv,yv,xlab='"+tbModel.getColumnName(xind)+"',ylab='"+tbModel.getColumnName(yind)+"')");
//    		    }catch (Exception ex){
//    		       	IJ.error("Failed to plot");
//    		        return;
//    		    }
//    		}
//    		active=!active;
        }
    }

    class PlotIdListener implements ActionListener {
    	public void actionPerformed(ActionEvent e) {
    		//identify point
    		/*reimplement Identifying point with JFreeChart */
//	       	ind=re.eval("identify(xv,yv,n=1,plot=F)").asInt();
//	       	IJ.log("Point index is "+ind);
//	       	dsets.setRowSelectionInterval(ind-1,ind-1);
        	
        }
    }
    
    class GetRoiListener implements ActionListener {
    	public void actionPerformed(ActionEvent e) {
    		try{
    			getRoisFromManager();
    		}catch (Exception ex){
				IJ.error("Unresolved Problem when Getting Rois");
			}
    	}
    }

    private void getRoisFromManager()throws Exception{
    	//RoiManager rmo=RoiManager.getInstance();
   		if (rMan==null){
			IJ.log("no ROI manager to pick up Rois");
			return;
		}
		Roi[] Rs=rMan.getRoisAsArray();
		if (Rs==null){
			IJ.log("Roi List is empty");
			return;
		}
		if (Rs.length==0){
			IJ.log("Roi List is empty");
			return;
		}
		
		//generate structures for saving ROIs
		File roiFldFl=new File(tbModel.getRootPath(),outRoiTg);
		if (!roiFldFl.isDirectory())
			roiFldFl.mkdir();
		outRoiApth=roiFldFl.getAbsolutePath();
		if(!tbModel.hasFileTag(outRoiTg, "ROI")){
			tbModel.addFileColumns(outRoiTg, "ROI");//tbModel.AddCols(adColsR);
		}
		//remove position tags if required
		if (this.chbRemRoiPos.isSelected())
			ROIManipulator2D.removeSliceInfo(Rs);
		
		String fnmNoExt=null;
		File rfl=null;
		
		do{
			outTblInd++;
			fnmNoExt=String.format("ManRois_%04d", outTblInd);
			rfl=new File(outRoiApth,fnmNoExt+".zip");
		}while (rfl.exists());
		
		tbModel.setFileAbsolutePath(rfl, cur_ind, outRoiTg, "ROI");
		ROIManipulator2D.saveRoisToFile(outRoiApth, fnmNoExt, Rs);
		rMan.runCommand("Reset");
    }
    
    
    class ListHandler implements ListSelectionListener {
    	public void valueChanged(ListSelectionEvent e) { 
    		if (e.getValueIsAdjusting() == false) {
    			cur_ind=dsets.getSelectedRow();
    			dsets.scrollRectToVisible(dsets.getCellRect(cur_ind,0, true));//!!!selected string always visible 
    			if ((cur_ind>-1)&&(chbUseful.isEnabled())){
    				try{
    					chbUseful.setSelected(tbModel.getBooleanValue(cur_ind, successWord));
    				}catch (Exception ex){
    					Utils.generateErrorMessage("ManualMicControlFrame_.ListHandler:Problem with accessing data usability flag",ex);
    				}
    				prevButton.setEnabled(cur_ind>0);
    				nextButton.setEnabled(cur_ind<(tbModel.getRowCount()-1));
    			}
    			visualise(cur_ind);
    		}
    	}
    }

    class ChbUseListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		try{
    			tbModel.setBooleanValue((e.getStateChange()==ItemEvent.SELECTED),cur_ind, successWord);
    		}catch (Exception ex){
				Utils.generateErrorMessage("ManualMicControlFrame_.ChbUseListener:Unable to change success flag",ex);
			}
    	}
    }

    class ChbFitListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		if (e.getStateChange()==ItemEvent.SELECTED){
    			fitImg=true;
    			fitImages();
    		}else
    			fitImg=false;
    	}
    }
    
    class ChbRepaintListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		if (e.getStateChange()==ItemEvent.SELECTED){
    			repaintImg=true;
    			visualise(cur_ind);
    		}else
    			repaintImg=false;
    	}
    }
    
    class ChbShowOvlListener implements ItemListener{
    	public void itemStateChanged(ItemEvent e) {
    		boolean state=(e.getStateChange()==ItemEvent.SELECTED);
    		for (ImgRoiPanel pn:ImgPanels)
    			pn.setchbRoiEnabled(state);
    		visRois();
    	}
    }

    private void visRois(){
    	if (!chbShowOvl.isSelected()){
    		this.clearOverlays();
        	//clear RoiManager
        	rMan.runCommand("Reset");
    		return;
    	}
    	
    	Overlay[] PostOvls=new Overlay[nPostRoi];
    	boolean needOvls[]=new boolean[nPostRoi+1];
    	
    	//check which overlays are needed to be opened
    	for (int i=0;i<=nPostRoi;i++){
    		needOvls[i]=false;
    		for (int j=0;j<nImg;j++){
    			if (!VisImg[j])continue;
    			if (VisRoi[j][i]){
    				needOvls[i]=true;
    				break;
    			}
    		}
    	}
    	
    	//open Post Overlays that are required
    	for (int i=0;i<nPostRoi;i++){
    		if(needOvls[i]){
    			try{
    				PostOvls[i]=ROIManipulator.flsToOverlay(tbModel.getFile(cur_ind, PostRoiTags[i], "ROI"));
    			}catch(Exception ex){
    				PostOvls[i]=new Overlay();
    			}
    		}
    		else{
    			PostOvls[i]=new Overlay();
    		}
    	}
    	
    	//put Overlays on images after combining and adding experimental Rois if necessary
    	Overlay imOvl,experOvl;
    	for (int j=0;j<nImg;j++){
    		if (Img[j]==null) continue;
    		if(!VisImg[j]) continue;
    		imOvl=new Overlay();
    		for (int i=0;i<nPostRoi;i++)
    			if(VisRoi[j][i])
    				try{
    					ROIManipulator2D.addOvlToOvl(imOvl,PostOvls[i]);
    				}catch(Exception ex){
    					Utils.generateErrorMessage("ManualMicControlFrame_:visRois", ex);
    				}
    		if (VisRoi[j][nPostRoi]&&(Img[j]!=null)){
    			experOvl=this.getExpOverlay(j);
    			if (experOvl!=null){
    				try{
    					ROIManipulator2D.addOvlToOvl(imOvl,experOvl);
    				}catch(Exception ex){
    					Utils.generateErrorMessage("ManualMicControlFrame_:visRois", ex);
    				}
    			}

    		}
    		Img[j].setOverlay(imOvl);
    	}
    	//clear RoiManager
    	rMan.runCommand("Reset");
    	
    }
    
    private void visRois(int _imgInd){//changeOverlay for Only one image
    	if (!chbShowOvl.isSelected()){
    		this.clearOverlays();
        	//clear RoiManager
        	rMan.runCommand("Reset");
    		return;
    	}
    	if(Img[_imgInd]==null)return;
    	
    	//put Overlays on images after combining and adding experimental Rois if necessary
    	Overlay imOvl,experOvl,postOvl;
    	imOvl=new Overlay();
    	for (int i=0;i<nPostRoi;i++)
    		if(VisRoi[_imgInd][i])
    			try{
    				postOvl=ROIManipulator.flsToOverlay(tbModel.getFile(cur_ind, PostRoiTags[i], "ROI"));
    				ROIManipulator2D.addOvlToOvl(imOvl,postOvl);
    			}catch(Exception ex){
    				IJ.log("Exception generated when handling overlays");
    			}
    	if (VisRoi[_imgInd][nPostRoi]){
    		experOvl=this.getExpOverlay(_imgInd);
    		if (experOvl!=null)
				try{
					ROIManipulator2D.addOvlToOvl(imOvl,experOvl);
				}catch(Exception ex){
					Utils.generateErrorMessage("ManualMicControlFrame_:visRois", ex);
				}
    	}
   		Img[_imgInd].setOverlay(imOvl);
    	//clear RoiManager
    	rMan.runCommand("Reset");
    }
    
    private void setContrast(){
    	switch (comboContrast.getSelectedIndex()){
    		case 0://"No adjustment"
    			break; 
    		case 1://"Default range"
    			this.setDefContr();
    			break;
    		case 2://"Min_max"
    			this.setMinMaxContr();
    			break;
    		default:
            	Utils.generateErrorMessage("Uknown contrast adjustment method", "Can not process contract preference opotion");
    }
//    	
//    	if (butContrDef.isSelected())
//    		this.setDefContr();
//    	if (butContrMinMax.isSelected())
//    		this.setMinMaxContr();
    }
    
    /**
     * xghxgfx
     * @param _imIndjh j
     * @return gvyy
     */
    private Overlay getExpOverlay(int _imInd){
    	//show Overlays
    	Overlay o=null;
    	File fl;
    	
    	try{
    		fl=tbModel.getFile(cur_ind, ImgTags[_imInd], "IMG");
    		o=ROIManipulator.flsToOverlay(fl.getParent(), FileUtils.cutExtension(fl.getName()));
    	}catch(Exception ex){
    			//AH_Utils.generErrMsg("ManualMicControlFrame_.getExpOverlay: problem with getting correct path", ex);
    	}
    	return o;
    }
    
    private void clearOverlays(){
    	for (int i=0; i<nImg;i++)
    		if(Img[i]!=null)
    			Img[i].setOverlay(null);
    }
    
    private void setDefContr(ImagePlus _img){
    	int mxlev;
    	if (_img==null) return;
    	if ((_img.getType()==ImagePlus.GRAY8)||(_img.getType()==ImagePlus.COLOR_RGB))
    		mxlev=255;
    	else if (_img.getType()==ImagePlus.GRAY16)
    		mxlev=65535;
    	else if (_img.getType()==ImagePlus.GRAY32)// assume normalised images
    		mxlev=65535;//1;
    	else
    		return;
    	IJ.setMinAndMax(_img,0,mxlev);
    }
    
    private void setDefContr(){
    	for (int i=0;i<nImg;i++)
    		if (VisImg[i])
    			setDefContr(Img[i]);
    }
    
    private void setMinMaxContr(ImagePlus _img){
    	if (_img==null) return;
    	if ((_img.getType()==ImagePlus.GRAY8)||(_img.getType()==ImagePlus.GRAY16)||(_img.getType()==ImagePlus.GRAY32))
    		IJ.run(_img, "Enhance Contrast...", "saturated=0.5");
       	if (_img.getType()==ImagePlus.COLOR_RGB){
       		ImageStatistics stat=_img.getStatistics();
       		IJ.setMinAndMax(_img,stat.min,stat.max);
       	}
    }
    
    private void setMinMaxContr(){
    	for (int i=0;i<nImg;i++)
    		if (VisImg[i])
    			setMinMaxContr(Img[i]);

    }
    
    private void fitImages(){
    	for (int i=0;i<nImg;i++)
    		if (VisImg[i]&&Img[i]!=null)
    			Img[i].getCanvas().fitToWindow();
    }
    

    private ImagePlus openImageFile(int _ind,String _tag)throws Exception{
    	
    	ImagePlus img=null;

    	if (tbModel.hasFileTag(_tag, "IMG")) {
    	
	    	File fl=tbModel.getFile(_ind, _tag,"IMG");
	    	if(fl==null)
	    		throw new Exception("ManualControlFrame.openImageFile: file is not specified");
	    	if (!fl.isFile())
	    		throw new Exception("ManualControlFrame.openImageFile: file does not exist");
	    	
	    	
	    	img=imageOpener.openImage(fl);
    	}else if (tbModel.hasFileTag(_tag, "IMGR")) {
    		File fl=new File(tbModel.getFileParentPathString(_ind, _tag,"IMGR"),tbModel.getFileName(_ind, _tag,"IMGR"));
    		img=PatternOpenerWithBioformats.openImage(fl);
    	}else {
    		throw new Exception("ManualControlFrame.openImageFile: can not recognise image type");
    	}
    	
    	return img;
    }
    
    
    private void visualise(int _ind){
    	for (int i=0;i<nImg;i++){
    		if (!VisImg[i]){//if image not visualisable: close if image was opened before and continue
    			closeImg(i);
    			continue;
    		}
    		if (repaintImg)
    			closeImg(i);
    		
    		try{
    			Img[i]=this.openImageFile(_ind,ImgTags[i]);
    			if (WindImg[i]==null){//now visualisation window, should be called in the beginning
    				ImageWindow.setNextLocation(Img_Loc[i]);
    	    		Img[i].setTitle(ImgTags[i]+" Visualisation window");
    	    		Img[i].show();
    	    		WindImg[i]=Img[i].getWindow();
    			}else{//existing visualisation window, should be called each time new item is selected
    				Img[i].setPosition(1,1,1);
    				WindImg[i].setImage(Img[i]);
    			}
    		}catch (Exception e){//exception will be also thrown when there is no image 
    			closeImg(i);
    		}
    		ImgPanels[i].setchbRoiEnabled(Img[i]!=null);
    	}

    	if(fitImg)
			this.fitImages();
    	this.visRois();
    	this.setContrast();
    	this.toFront();
    }
    
    private void closeImg(int _imInd){
		if (WindImg[_imInd]!=null){
			Img_Loc[_imInd].setLocation(WindImg[_imInd].getLocation());
			Img[_imInd].close();
			WindImg[_imInd]=null;
			Img[_imInd]=null;
		}
    }

    /**
     * method is called when class used as Fiji/ImageJ plugin via GUI.
     * Opens dialog for selecting data file and creates frame with the data
     *  
     */
    @Override
    public void run(String arg) {
		if (arg!=""){//test mode
			this.initialise(arg,false);
		}else{//normal mode
			//select input file and folder
			OpenDialog dlo = new OpenDialog ("Choose text file with experiment summary", null);
			String abs_pth=dlo.getPath();
			if (abs_pth==null) return;
			this.initialise(abs_pth,false);
		}
		this.setVisible(true);
	}
	
    /**
     * Main method for debugging.
     * @param args unused
     * @throws Exception if one is generated by the class instance during test
     */
	public static void main(String[] args)throws Exception{
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		//Class<?> clazz =ManualControlFrame.class;
		//String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		//String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		//System.setProperty("plugins.dir", pluginsDir);

		System.getProperties().setProperty("plugins.dir", "C:/Halavatyi_Work/running soft/Fiji.app_new/plugins");
		
		// start ImageJ
		new ImageJ();
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		//IJ.runPlugIn(clazz.getName(),"");
		//IJ.runPlugIn(clazz.getName(),"C:/tempDat/Petra/test--data--fiji/analysis_results.txt");
		//IJ.runPlugIn(clazz.getName(),"C:/tempDat/STC1_project_(FV)/siRNA--HelaKyoto/20170412--HelaKyoto--Sec31-A568--Sec24B-A488--fixed--siRNA--49h/Analysis-20170417--selected-cells-20170607--back3/analysisSummary_Step_QuatifyCellAndEresDistrib_step12_final.txt");
		//IJ.runPlugIn(clazz.getName(),"C:\\tempDat\\AFRAP CHX\\20112015--AutoFRAP--+-CHX\\Analysis November 2015\\summary_gr1_Form3_PostRois_FRAPQ.txt");
		//IJ.runPlugIn(clazz.getName(),"C:\\Halavatyi_Work\\temp_data\\Afrap\\summary_gr1_.txt");
		//IJ.runPlugIn(clazz.getName(),"C:\\tempDat\\2colExp\\02122015-AutoFRAP-2col-control\\Analysis Dec2015\\summary_1p1_mov_LZ_proj.txt");
		//IJ.runPlugIn(clazz.getName(),"C:/tempDat/2colExp/Latest WT/23012015__AutoFRAP__Sec23 Golgi A1 low/Analysis_November_2015/summary_tst8_withOldFit_final.txt");
		
		String svPath="C:/tempDat/";
		//String svNm="testTable.txt";
		TableModel tbModel=new TableModel(svPath);


		tbModel.addFileColumns("DAPI","IMG");
		tbModel.addFileColumns("CellMask","IMG");
		tbModel.addFileColumns("SegSpots","ROI");
		
		tbModel.addValueColumn("Count", "NUM");
		tbModel.addValueColumn("QualControl", "BOOL");
		
		tbModel.addColumn("RegExp");
		
		
//		
		tbModel.addRow();
		tbModel.addRow();
		
		tbModel.setFileAbsolutePath("C:/tempDat/DAPI1.tif",0,"DAPI","IMG");
		//tbModel.setFileAPth("C:/tempDat/kaka*kuku.tif",0,"DAPI","IMG");
		tbModel.setStringValue("kaka*kuku", 0, "RegExp");
		
		tbModel.setNumericValue(33, 0, "Count");
		tbModel.setBooleanValue(false, 1, "QualControl");
		//tbModel.writeNew(svNm,true);
		
		ManualControlFrame frame=new ManualControlFrame(tbModel);
		frame.setVisible(true);

	}
}