package automic.table;
import ij.IJ;
import ij.ImageJ;
import ij.io.DirectoryChooser;
import ij.io.OpenDialog;
import ij.measure.ResultsTable;
import ij.plugin.PlugIn;
import ij.text.TextWindow;

import java.io.File;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.apache.commons.io.FileUtils;

import automic.utils.Utils;


public class TableValidator implements PlugIn {
	
	public static String[] fileExtensions={"lsm","tif","zip","txt"};
	
	private static String summ_pth="";
	private static String summ_fnm="";
	private static String img_pth="";
	
	private static TextWindow outputWindow=null;
	private static int outputCounter=0;
	
	/**
	 * method is called by ImageJ GUI. Appearing dialogs specify locations of summary file
	 * and folder with the images
	 */
	@Override
	public void run(String arg) {
		if (!arg.equals("no dialog")){
			//select input file and folder
			OpenDialog dlo = new OpenDialog ("Choose tested summary file", null);
			summ_pth=dlo.getDirectory();
			summ_fnm=dlo.getFileName();
			if (summ_fnm==null){
				return; //stop if input file was not selected 
			}
			DirectoryChooser.setDefaultDirectory(summ_pth);
			DirectoryChooser drc=new DirectoryChooser("Select the folder with experimental files");
			img_pth=drc.getDirectory();
			if(img_pth==null)
				return;
		}
		try{
			validateAll(summ_pth,summ_fnm, img_pth);
		}catch (Exception ex){
			Utils.generateErrorMessage("TableValidator exception", ex);
		}
	}

	public static void validateAll(String _summaryFolderPath,String _summaryFileName, String _experimentFolderPath)throws Exception{
		createNewOutputWindow();
		checkFilesInFolder(_experimentFolderPath);
		validateSummaryFile(new File(_summaryFolderPath,_summaryFileName), _experimentFolderPath);
	}
	
	private static void createNewOutputWindow(){
		outputCounter++;
		outputWindow=new TextWindow("TableModel Validation output "+outputCounter,"Start Validation",600,500);
		outputWindow.setLocation(5, 130);
	}
	
	
	/**
	 * runs validation procedure. Writes results to the ImageJ log window. Currently only counts LSM files and checks associated pathes.
	 * @param _summaryFile	Specifies location of the text file that contains data
	 * @param _experimentFolder	Folder in which images are located
	 * @throws Exception if it generated by TableModel Object, or if input arguments do not specify existing file and directory
	 */
	public static void validateSummaryFile(File _summaryFile, String _experimentFolderPath)throws Exception{
		if(!_summaryFile.isFile())throw new Exception("Summary file should specify existing file");
		if(!new File(_experimentFolderPath).isDirectory())throw new Exception("Experiment folder should be existing directory");

		validateTableModel(new TableModel(_summaryFile), _experimentFolderPath);
	}
	
	public static void validateTableModel(TableModel _tableModel, String _experimentFolderPath)throws Exception{
		if(outputWindow==null)
			createNewOutputWindow();
		
		outputWindow.append("Start table validation");

		int ndat=_tableModel.getRowCount();
		outputWindow.append("Summary file contains: "+ndat+" datasets. ");
		outputWindow.append("Generating results table with file information (TYPE/TAG/NUMBER LISTED/NUMBER UNIQUE/NUMBER EXISTS/IN ROOT FOLDER)");
		
		
		ResultsTable resultsTable=new ResultsTable();
		
		
		final String[] fileTypes=TableModel.getFileTypes();
		
		HashSet<String> filePathSet=new HashSet<String>();

		//count for each tag and type separately
		for (String type:fileTypes){
			String[] fileTags=_tableModel.getFileTags(type);
			
			for (String tag:fileTags){
				filePathSet.clear();
				int fileNumber=0;
				for (int dataIndex=0;dataIndex<ndat;dataIndex++){
					File testFile=_tableModel.getFile(dataIndex, tag, type);
					if (testFile==null)continue;
					fileNumber++;
					filePathSet.add(testFile.getAbsolutePath());
					
				}
				int fileNumberUnique=filePathSet.size();
				int fileNumberExists=0;
				int fileNumberInRootFolder=0;
				for (String path:filePathSet){
					if(!new File(path).isFile())continue;
					fileNumberExists++;
					if(Paths.get(path).startsWith(_experimentFolderPath))
						fileNumberInRootFolder++;
				}
				
				resultsTable.incrementCounter();
				resultsTable.addValue("TYPE", type);
				resultsTable.addValue("TAG", tag);
				resultsTable.addValue("NUMBER LISTED", fileNumber);
				resultsTable.addValue("NUMBER UNIQUE", fileNumberUnique);
				resultsTable.addValue("NUMBER EXISTS", fileNumberExists);
				resultsTable.addValue("IN ROOT FOLDER", fileNumberInRootFolder);
			}
		}
		
		//count for each for all tags and types together
		int fileNumber=0;
		int fileNumberExists=0;
		int fileNumberInRootFolder=0;
		filePathSet.clear();
		for (String type:fileTypes){
			String[] fileTags=_tableModel.getFileTags(type);
			
			for (String tag:fileTags){
				for (int dataIndex=0;dataIndex<ndat;dataIndex++){
					File testFile=_tableModel.getFile(dataIndex, tag, type);
					if (testFile==null)continue;
					fileNumber++;
					filePathSet.add(testFile.getAbsolutePath());
				}
			}
		}
		for (String path:filePathSet){
			if(!new File(path).isFile())continue;
			fileNumberExists++;
			if(Paths.get(path).startsWith(_experimentFolderPath))
				fileNumberInRootFolder++;
		}
		int fileNumberUnique=filePathSet.size();
		resultsTable.incrementCounter();
		resultsTable.addValue("TYPE", "All");
		resultsTable.addValue("TAG", "All");
		resultsTable.addValue("NUMBER LISTED", fileNumber);
		resultsTable.addValue("NUMBER UNIQUE", fileNumberUnique);
		resultsTable.addValue("NUMBER EXISTS", fileNumberExists);
		resultsTable.addValue("IN ROOT FOLDER", fileNumberInRootFolder);
		
		resultsTable.show("File counts in TableModel "+outputCounter);
		outputWindow.append("End table validation");
		
	}
	
	public static void checkFilesInFolder(String _folderPath){
		if(outputWindow==null)
			createNewOutputWindow();

		outputWindow.append("Start Experiment Folder Information");
		
		//count lsm files in the folder
		File rootFile=new File(_folderPath);
		Collection<File> files=getFileCollection(rootFile);
		outputWindow.append("Specified experimental folder contains "+files.size()+" files, including:");
		for (String ext:fileExtensions){
			outputWindow.append(" - "+ext+" :"+ countRemoveFilesExtension(files, ext)+" files;");
		}
		outputWindow.append(" - Unspecified extension files :"+files.size()+":");
		for (File file:files){
			outputWindow.append("-- "+file.getAbsolutePath());
		}
		
		outputWindow.append("End Experiment Folder Information");
	}
	
	/**
	 * Counts files with the given extension in the given directory and its subdirectories.
	 * @param _rootFile	path to the root directory
	 * @param _extension		extension without dot
	 * @return number of found files
	 */
	public static int countFilesExtension(File _rootFile, String _extension){
		if(_rootFile==null) return 0;
		return countFilesExtension(getFileCollection(_rootFile), _extension);
	}
	
	public static int countFilesExtension(Collection<File> _files, String _extension){
	    if((_files==null)||(_extension==null)) return 0;
		final String dext="."+_extension;
		int count =0;
	    for (File file: _files) {
	            if(file.getName().endsWith(dext))
	            	count++;
	    }
		return count;
	}
	
	private static int countRemoveFilesExtension(Collection<File> _files, String _extension){
	    if((_files==null)||(_extension==null)) return 0;
		final String dext="."+_extension;
		int count =0;
		File file;
	    for (Iterator<File> iterator=_files.iterator();iterator.hasNext();){
	    	file=iterator.next();
	    	if(file.getName().endsWith(dext)){
	    		count++;
	    		iterator.remove();
	    	}
	    }
		
		return count;
	}
	
	public static Collection<File> getFileCollection(File _rootFile){
		return FileUtils.listFiles(_rootFile, null, true);
	}
	
	
	/**
	 * 
	 * @param _rootFl specification of the directory in which counted files are located
	 * @return number of found files
	 */
	@Deprecated
	public static int countFilesLsm(File _rootFl){
		return countFilesExtension(_rootFl, "lsm");
	}
	

	
	/**
	 * Main method for debugging.
	 * @param args unused
	 */
	public static void main(String[] args) {
		// set the plugins.dir property to make the plugin appear in the Plugins menu
		Class<?> clazz = TableValidator.class;
		String url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".class").toString();
		String pluginsDir = url.substring(5, url.length() - clazz.getName().length() - 6);
		System.setProperty("plugins.dir", pluginsDir);

		// start ImageJ
		new ImageJ();
		//DEBUG=true;
		//Debug.run(plugin, parameters);
		//new WaitForUserDialog("test Maven project").show();
		
		// open the Clown sample
		//ImagePlus image = IJ.openImage("http://imagej.net/images/clown.jpg");
		//image.show();
		
		summ_pth="C:/tempDat/VSVG experiments/01042016-AutoFRAP-cargo/living\\Process1";
		img_pth="C:/tempDat/VSVG experiments/01042016-AutoFRAP-cargo/living";

		summ_fnm="summary_MergedConditioned.txt";
		
		IJ.runPlugIn(clazz.getName(), "no dialog");

		//checkFilesInFolder("D:/tempDat/AutoFRAP_test");
	}

}
