package automic.table;

import ij.ImagePlus;
import ij.gui.Roi;
import mcib3d.geom.Objects3DPopulation;

import java.lang.Exception;
import java.util.HashMap;
import java.util.Map;

import automic.utils.FileUtils;
import automic.utils.imagefiles.TIFProcessorImageJ;
import automic.utils.roi.ROIManipulator2D;
import automic.utils.roi.ROIManipulator3D;

import java.io.File;

/**
 * Last Modified: 12/04/2016
 * @author Aliaksandr Halavatyi
 */

public class TableProcessor{
	TableModel table;
	
	class FileRecord{
		public String type;
		public String tableTag;
		public String folderAbsolutePath;
		public File folderFile;
		
		public FileRecord(String _folderSubPath,String _type)throws Exception{
			type=_type;
			folderFile=new File(table.getRootPath(), _folderSubPath);
			folderAbsolutePath=folderFile.getAbsolutePath();
			//potential errors possible if rootpath of the table changed when wrapping table processor exists
			//in future can be solved with proper event listener
			FileUtils.createCleanFolder(folderFile);
			tableTag=folderFile.getName();
		}
	};
	
	private Map<String, FileRecord> addtionalFileRecords;
	private Map<String, String> addtionalValueColumns;
	
	public TableProcessor(TableModel _table){
		table=_table;
		addtionalFileRecords=new HashMap<String, FileRecord>();
		addtionalValueColumns=new HashMap<String, String>();
	}
	
	public TableModel getTableModel(){
		return table;
	}
	
	public void addFileColumns(String _folderSubPath, String _type)throws Exception{
		FileRecord fileRecord=new FileRecord( _folderSubPath, _type);
		String tag=fileRecord.tableTag;
		table.addFileColumns(tag, _type);
		addtionalFileRecords.put(tag, fileRecord);
	}
	
	public void addValueColumn(String _tag,String _type){
		if(_type==null)
			table.addColumn(_tag);
		else
			table.addValueColumn(_tag, _type);
		
		addtionalValueColumns.put(_tag, _type);
	}
	

	
	public void saveImageToTable(int _rowIndex, ImagePlus _image,String _imageTag, String _fileNameWithoutExtension)throws Exception{
		if(_image==null)
			throw new Exception("Can not save null image");
		FileRecord imageRecord=addtionalFileRecords.get(_imageTag);
		if ((imageRecord==null)||(!imageRecord.type.equals("IMG")))
			throw new Exception("TableProcessor.saveImageToTable: wrond Image tag");
		File imageFile=new File(imageRecord.folderFile,_fileNameWithoutExtension+".tif");
		TIFProcessorImageJ.saveTIFImage(_image, imageFile, true);
		//Add record to the table
		table.setFileAbsolutePath(imageFile, _rowIndex, _imageTag, "IMG");
	}
	
	public void saveRoiToTable(int _rowIndex, Roi _roi,String _roiTag, String _fileNameWithoutExtension)throws Exception{
		if(_roi==null)
			throw new Exception("Null ROI can not be saved");
		this.saveRoisToTable(_rowIndex, new Roi[]{_roi}, _roiTag,_fileNameWithoutExtension);
	}
	
	public void saveRoisToTable(int _rowIndex, Roi[] _rois,String _roiTag, String _fileNameWithoutExtension)throws Exception{
		if (_rois==null)
			throw new Exception("Null ROI array can not be saved");
		FileRecord roiRecord=addtionalFileRecords.get(_roiTag);
		if ((roiRecord==null)||(!roiRecord.type.equals("ROI")))
			throw new Exception("TableProcessor.saveRoisToTable: wrong Roi tag");
		ROIManipulator2D.saveRoisToFile(roiRecord.folderAbsolutePath, _fileNameWithoutExtension, _rois);

		//Add record to the table
		table.setFileAbsolutePath(new File(roiRecord.folderFile,_fileNameWithoutExtension+"."+ROIManipulator2D.ext), _rowIndex, _roiTag, "ROI");
	}

	public void save3DRoisToTable(int _rowIndex, Objects3DPopulation _roi3DPopulation,String _roiTag, String _fileNameWithoutExtension)throws Exception{
		if (_roi3DPopulation==null)
			throw new Exception("Null 3D ROI population can not be saved");
		if (_roi3DPopulation.getNbObjects()<1)
			throw new Exception("Empty 3D ROI population can not be saved");
		FileRecord roiRecord=addtionalFileRecords.get(_roiTag);
		if ((roiRecord==null)||(!roiRecord.type.equals("ROI")))
			throw new Exception("TableProcessor.saveRoisToTable: wrong Roi tag");
		ROIManipulator3D.populationToFile(roiRecord.folderAbsolutePath, _fileNameWithoutExtension, _roi3DPopulation);

		//Add record to the table
		table.setFileAbsolutePath(new File(roiRecord.folderFile,_fileNameWithoutExtension+"."+ROIManipulator2D.ext), _rowIndex, _roiTag, "ROI");
	}

	
	public void saveTableToTable(int _rowIndex, TableModel _childTable,String _tableTextTag, String _fileNameWithoutExtension)throws Exception{
		if (_childTable==null)
			throw new Exception("Null table can not be saved");
		FileRecord txtRecord=addtionalFileRecords.get(_tableTextTag);
		if ((txtRecord==null)||(!txtRecord.type.equals("TXT")))
			throw new Exception("TableProcessor.saveTableToTable: wrond table record tag");
		_childTable.setRootPathDeep(txtRecord.folderAbsolutePath);
		_childTable.writeNewFile(_fileNameWithoutExtension+".txt", true);
		//Add record to the table
		table.setFileAbsolutePath(new File(txtRecord.folderAbsolutePath,_fileNameWithoutExtension+".txt"), _rowIndex, _tableTextTag, "TXT");
	}
	
	public String getFolderAbsolutePath(String _fileColumnTag,String _fileColumnType)throws Exception{
		FileRecord fileRecord=addtionalFileRecords.get(_fileColumnTag);
		if ((fileRecord==null)||(!fileRecord.type.equals(_fileColumnType)))
			throw new Exception("TableProcessor.saveTableToTable: wrond table record tag");
		return fileRecord.folderAbsolutePath;
	}
	
	public File getFile(int _rowIndex, String _tag, String _type)throws Exception{
		return table.getFile(_rowIndex, _tag, _type);
	}
	
	public void setFile(File _file,int _rowIndex, String _tag, String _type)throws Exception{
		table.setFileAbsolutePath(_file, _rowIndex, _tag, _type);
	}
	
	public String getFileAbsolutePath(int _rowIndex, String _tag, String _type)throws Exception{
		return table.getFileAbsolutePathString(_rowIndex, _tag, _type);
	}

	public void setFileAbsolutePath(String _fileAbsolutePath,int _rowIndex, String _tag, String _type)throws Exception{
		table.setFileAbsolutePath(_fileAbsolutePath, _rowIndex, _tag, _type);
	}

	public Object getValue(int _rowIndex, String _tag, String _type)throws Exception{
		if(_type==null)
			return table.getStringValue(_rowIndex, _tag);
		else
			return table.getClassifiedValue(_rowIndex, _tag, _type);
	}

	public void setValue(Object _value,int _rowIndex, String _tag, String _type)throws Exception{
		if(_type==null)
			table.setStringValue((String)_value, _rowIndex, _tag);
		else
			table.setClassifiedValue(_value, _rowIndex, _tag, _type);
	}
	
	public String[] getUniqueFactorValues( String _factorTag)throws Exception{
		return table.getUniqueFactorValues(_factorTag);
	}

	
}
