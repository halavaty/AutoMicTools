package automic.table;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *Class for specifying column properties for AutomicTableModel
 *Contains List of column names, Tags for different column types and methods for manipulation with column names 
 */
@SuppressWarnings("serial")
public class ColumnSpecification{
	public static final List<String> fileTypes=Arrays.asList("IMG","IMGS","IMGR","ROI","TXT");//identifiable types of files
	public static final List<String> imageFileTypes=Arrays.asList("IMG","IMGS","IMGR");//identifiable types of files
	public static final List<String> valueTypes=Arrays.asList("NUM","BOOL","FACT");// = {};	//identifiable types of values
//	private static final int nFileTypes=fileTypes.size();
//	private static final int nValTypes= valueTypes.size();
	
	private List<String> columnNames;// = {};
	private Map<String,List<String>> fileTags;
	private Map<String,List<String>> valTags;
	
	private static final String tagPattern="([a-zA-Z0-9+-.]+)";

	private static final Map<String,Pattern> PatternsFPth=new HashMap<String,Pattern>(){
		{
			for (String tp:fileTypes)
				this.put(tp,Pattern.compile("PathName_"+tagPattern+"_"+tp));
		}
	};
	
	private static final Map<String,Pattern> PatternsFnm=new HashMap<String,Pattern>(){
		{
			for (String tp:fileTypes)
				this.put(tp,Pattern.compile("FileName_"+tagPattern+"_"+tp));
		}
	};

	private static final Map<String,Pattern> PatternsVal=new HashMap<String,Pattern>(){
		{
			for (String tp:valueTypes)
				this.put(tp,Pattern.compile(tagPattern+"_"+tp));
		}
	};

	
//	private Pattern[] PatternsFPth=new Pattern[nFileTypes];		//pattern for path headers
//	private Pattern[] PatternsFnm= new Pattern[nFileTypes];		//pattern for file name headers
//	private Pattern[] PatternsVals=new Pattern[nValTypes];		//pattern for value headers
	//these patterns have to be non-modifiable. can we make them we fix them and initialise in the beginning? Method genStaticFields has to be called only once and not each time when instance is created
	
	public ColumnSpecification(){
		initialiseMaps();
		columnNames=new ArrayList<String>();
	}

	public ColumnSpecification(List<String> _colNames){
		initialiseMaps();
		columnNames=new ArrayList<String>(_colNames);// to have list modifiable for sure
		identifyTags(columnNames);
	}
	
	public ColumnSpecification(String[] _colNames){
		this(new ArrayList<String>(Arrays.asList(_colNames)));
	}
	
	public ColumnSpecification(String [] _imgTags, String[] _roiTags,String[] _txtTags,
			   String[] _numTags, String[] _boolTags, String[] _factTags, String[] _otherNames){
		
		initialiseMaps();

		List<String> targList;
		//specify tags lists
		if (_imgTags!=null){
			targList=fileTags.get("IMG");
			for (String tg:_imgTags)
				targList.add(tg);
		}
		
		if (_roiTags!=null){
			targList=fileTags.get("ROI");
			for (String tg:_roiTags)
				targList.add(tg);
		}
			
		if (_txtTags!=null){
			targList=fileTags.get("TXT");
			for (String tg:_txtTags)
				targList.add(tg);
		}
		
		if (_numTags!=null){
			targList=valTags.get("NUM");
			for (String tg:_numTags)
				targList.add(tg);
		}

		if (_boolTags!=null){
			targList=valTags.get("BOOL");
			for (String tg:_boolTags)
				targList.add(tg);
		}
		
		if (_factTags!=null){
			targList=valTags.get("FACT");
			for (String tg:_factTags)
				targList.add(tg);
		}
		
		
		//make and fill columns list
		columnNames= new ArrayList<String>();
		for (String tp:fileTypes)
			for (String tg:fileTags.get(tp)){
				columnNames.add("PathName_"+tg+"_"+tp);
				columnNames.add("FileName_"+tg+"_"+tp);
			}
		
		for (String tp:valueTypes)
			for (String tg:valTags.get(tp))
				columnNames.add(tg+"_"+tp);

		if (_otherNames!=null)
			for (String s:_otherNames)
				columnNames.add(s);
	}
	
	private void initialiseMaps(){
		fileTags=new HashMap<String,List<String>>(fileTypes.size()+1,1);
		for (String tp:fileTypes)
			fileTags.put(tp, new ArrayList<String>());//  .add(new ArrayList<String>());
		valTags=new HashMap<String,List<String>>(valueTypes.size()+1,1);
		for (String tp:valueTypes)
			valTags.put(tp, new ArrayList<String>());//.add(new ArrayList<String>());
		
	}
	
	/**
	 * identify tags belonging to the different categories
	 */
	private void identifyTags(List<String> _columnNames){
		for (String cn:_columnNames){
			identifyTag(cn);
		}//for (String cn:columnNames)
	}
	
	private void identifyTag(String _columnName){
		Matcher mt=null;
		String testTag; 
		
		for(String tp:fileTypes){
			mt=PatternsFPth.get(tp).matcher(_columnName);
			if(mt.matches()){
				testTag=mt.group(1);
//				if(!columnNames.contains(PatternsFnm.get(tp).pattern().replace(tagPattern,testTag)))
//					continue;
				fileTags.get(tp).add(testTag);
				return;
			}
		}
		for(String tp:valueTypes){
			mt=PatternsVal.get(tp).matcher(_columnName);
			if(mt.matches()){
				valTags.get(tp).add(mt.group(1));
				return;
			}
		}
	}
	
	public void addColumns(String[] _columnNames){
		if (_columnNames==null) return;
		for (String s:_columnNames)
			columnNames.add(s);
		this.identifyTags(Arrays.asList(_columnNames));
	}
	
	public void addColumn(String _columnName){
		if (_columnName==null) return;
		columnNames.add(_columnName);
		this.identifyTag(_columnName);
	}
	
	public void addFileColumns(String _tag, String _type){
		fileTags.get(_type).add(_tag);
		columnNames.add(PatternsFPth.get(_type).pattern().replace(tagPattern,_tag));
		columnNames.add(PatternsFnm.get(_type).pattern().replace(tagPattern,_tag));
	}
	
	public void addValueColumn(String _tag, String _type){
		valTags.get(_type).add(_tag);
		columnNames.add(PatternsVal.get(_type).pattern().replace(tagPattern,_tag));
	}
	
	public int getColumnsNumber(){
		return columnNames.size();
	}
	public boolean hasFileTag(String _tag, String _type){
//		int typeInd=fileTypes.indexOf(_type);
//		if (typeInd<0) return false;
		return fileTags.get(_type).contains(_tag);
		
	}

	public boolean hasValueTag(String _tag, String _type){
//		int typeInd=valTypes.indexOf(_type);
//		if (typeInd<0) return false;
		return valTags.get(_type).contains(_tag);
	}
	
	public Class<?> getColumnClass(int _colInd){
		String colNm=columnNames.get(_colInd);
		if (PatternsVal.get("BOOL").matcher(colNm).matches())
			return boolean.class;
		if (PatternsVal.get("NUM").matcher(colNm).matches())
			return double.class;
		
		return String.class;
	}
	
	public boolean isColumnBoolean(int _columnIndex){
		return PatternsVal.get("BOOL").matcher(columnNames.get(_columnIndex)).matches();
	}
	
	public boolean isColumnNumeric(int _columnIndex){
		return PatternsVal.get("NUM").matcher(columnNames.get(_columnIndex)).matches();
	}
	
	public int getColumnIndex(String _columnName){
		return columnNames.indexOf(_columnName);
	}
	
	public int getPathColumnIndex(String _tag, String _type){
		String expectNm=PatternsFPth.get(_type).pattern().replace(tagPattern,_tag);
		return columnNames.indexOf(expectNm);
	}

	public int getFileColumnIndex(String _tag, String _type){
		String expectNm=PatternsFnm.get(_type).pattern().replace(tagPattern,_tag);
		return columnNames.indexOf(expectNm);
	}
	
	public int getValueColumnIndex(String _tag, String _type){
		String expectNm=PatternsVal.get(_type).pattern().replace(tagPattern,_tag);
		return columnNames.indexOf(expectNm);
	}
	
	public String[] getFileTags(String _type){
		List <String> trgList=fileTags.get(_type);
		return trgList.toArray(new String[trgList.size()]);
	}

	public String[] getImageFileTags(){
		List<String> imageTagList=new ArrayList<String>();
		
		List <String> typeList;
		for (String type:imageFileTypes) {
			typeList=fileTags.get(type);
			for (String tag:typeList)
				imageTagList.add(tag);
		}
		
		return imageTagList.toArray(new String[imageTagList.size()]);
	}

	
	public String[] getValueTags(String _type){
		List <String> trgList=valTags.get(_type);
		return trgList.toArray(new String[trgList.size()]);
	}

	
	public String[] getColumnNames(){
		return columnNames.toArray(new String[columnNames.size()]);
	}
	
	public String getColumnName(int _columnIndex){
		return columnNames.get(_columnIndex);
	}
}






