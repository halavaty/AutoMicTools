package automic.table;

import ij.IJ;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

//import javax.swing.table.AbstractTableModel;
import javax.swing.table.DefaultTableModel;

import org.apache.commons.io.FilenameUtils;

import automic.utils.Utils;


/**
 * A class to store and manipulate summary tables from the datasets for Automated Microscopy experiments.
 * Modified from on AutoFRAPTableModel class
 * Last Update: 02/03/2016
 * <p>
 * WARNING: requires at least Java 1.7 for file path operations
 * 
 * @author Aliasandr Halavatyi
 */

@SuppressWarnings("serial")
public class TableModel extends DefaultTableModel {
	private String delim="\t";
	public static final String NullString=""; 
	public static final String rootw="root";
	public static final Path rootWordPath=Paths.get(rootw);
	
	ColumnSpecification colSpec=null;

	private String rootpath;
	private File rootFl;
	private Path rootPth;

	/**
	 * Create table without any columns and rows
	 * @param _rootPath place where summary files will be stored and relative to which PathName data are calculated
	 */
	public TableModel(String _rootPath){
		super();
		Utils.checkJavaVersion(1.7);
		colSpec=new ColumnSpecification();
		this.setRootPath(_rootPath);
	}
	
	public TableModel(int _nRows, ColumnSpecification _colSpec, String _rootPath){
    	super(_colSpec.getColumnNames(),_nRows);
    	Utils.checkJavaVersion(1.7);
    	colSpec=_colSpec;
    	this.setRootPath(_rootPath);
    }

	/**
	 * creates table with specified number of datasets and given columnNames. Sets RootPath
	 * @param _nRow	number of datasets
	 * @param _columnNames columnnames
	 * @param _rootPath	rootpath
	 */
	public TableModel(int _nRow, List<String> _columnNames, String _rootPath){
    	this (_nRow,new ColumnSpecification(_columnNames),_rootPath);
    }
    
	/**
	 * creates table with specified number of datasets and given columnNames. Sets RootPath
	 * @param _nRows	number of datasets
	 * @param _rootPath	rootpath
	 * @param _columnNames columnnames
	 */	
	public TableModel(int _nRows, String [] _columnNames, String _rootPath){
		this(_nRows,Arrays.asList(_columnNames),_rootPath);
	}

	/**
	 * creates table for which column tags for different datatypes are provided
	 * @param _nRows			number of datasets (rows)
	 * @param _imgNames		image tags
	 * @param _roiNames		ROI tags
	 * @param _txtNames		TXT tags
	 * @param _numNames		tags for numeric columns
	 * @param _boolNames	tags for boolean columns
	 * @param _factNames	tags for factor columns
	 * @param _otherNames	tags for general (non-classified columns)
	 * @param _rootPath			rootpath
	 */
	@Deprecated
	public TableModel(int _nRows, String [] _imgNames, String[] _roiNames,
							String[] _txtNames, String[] _numNames, String[] _boolNames,String[] _factNames, String[] _otherNames, String _rootPath){

		this(_nRows,new ColumnSpecification(_imgNames,_roiNames,_txtNames,_numNames,_boolNames,_factNames,_otherNames),_rootPath);
	}
	
	
	/**
	 * constructor from the text file. sets rootpath to the location of this file
	 * @param _inputFile specifies file location
	 * @throws Exception if file does not exist or data could not be imported
	 */
	public TableModel(File _inputFile)throws Exception{
		this(_inputFile.getParent());
		readFile(_inputFile);
	}
	
	public TableModel(File _inputFile,String _delimiter)throws Exception{
		this(_inputFile.getParent());
		delim=_delimiter;
		readFile(_inputFile);
	}
	
	private void readFile(File _inputFile)throws Exception{
		
		if(!_inputFile.isFile()) throw new Exception("TableModel: File does not exist");
		BufferedReader buff_read=null;
		try{
			buff_read = new BufferedReader(new FileReader(_inputFile));
		}catch (Exception e){
			throw new Exception("TableModel: Unable to open input file.");
		}
		String fline;
		fline=buff_read.readLine();// read title line
		String[] splLine;
		splLine=fline.split(delim,-1);
		for (String cn:splLine)
			this.addColumn(cn);
		
		fline=buff_read.readLine();// read first data line
		while((fline!=null)&&(!fline.equals(""))){
			splLine=fline.split(delim,-1);
			this.addRow(decodeStringArray(splLine));
			fline=buff_read.readLine();// read next data line
		}
		buff_read.close();
		
	}
	
	private Object[] decodeStringArray(String[] _stringArray){
		int nCol=colSpec.getColumnsNumber();
		Object[] oarr=new Object[nCol];
		for (int i=0;i<nCol;i++){
			if (_stringArray[i].equals(NullString)){
				oarr[i]=null;
				continue;
			}
			if (colSpec.isColumnBoolean(i)){
				oarr[i]=(_stringArray[i].equals("1"))?true:false;
				continue;
			}
			if (colSpec.isColumnNumeric(i)){
				oarr[i]=Double.parseDouble(_stringArray[i]);
				continue;
			}
			oarr[i]=_stringArray[i];
			
		}
		
		return oarr;
	}
	
	private String codeObjectToString(Object obj){
		if (obj==null)
			return NullString;
		if (obj instanceof Double)
			return Double.toString((Double)obj);
		if (obj instanceof Integer)
			return Integer.toString((Integer)obj);
		if (obj instanceof Boolean)
			return (Boolean)obj?"1":"0";
		
		return (String)obj;
	}
	
	@Override
	public Class<?> getColumnClass(int _columnIndex){
		//return String.class;
		return colSpec.getColumnClass(_columnIndex);

	}
	
	public void addColumn (String _columnName){
		colSpec.addColumn((String)_columnName);
		super.addColumn(_columnName);
		//Should fill with default values?
	}
	
	/**
	 * adds new row to the bottom of the table
	 */
	public void addRow(){
		this.addRows(1);
	}
	
	/**
	 * adds several rows to the bottom of the table
	 * @param _nRows number of the rows to add
	 */
	public void addRows(int _nRows){
		Object [] rowData=new Object[this.getColumnCount()];
		for(int i=0;i<_nRows;i++)		
			this.addRow(rowData);
	}

	
	public void addColumns (String[] _adiiltionalTitles){
		for (String title:_adiiltionalTitles)
			this.addColumn(title);
	}
	
	public void addFileColumns(String _tag, String _type){
		colSpec.addFileColumns(_tag, _type);
		int ncol=this.getColumnCount();
		super.addColumn(colSpec.getColumnName(ncol));
		super.addColumn(colSpec.getColumnName(ncol+1));
	}
	
	public void addValueColumn(String _tag, String _type){
		colSpec.addValueColumn(_tag, _type);
		super.addColumn(colSpec.getColumnName(this.getColumnCount()));
	}

	
	public void writeNewFile(String _fileName, boolean _writeData) throws Exception{
  		File fl=new File(rootFl,_fileName);
		fl.createNewFile();
		BufferedWriter bufsumfl = new BufferedWriter(new FileWriter(fl));
		int ncol=this.getColumnCount();
		int ndat=this.getRowCount();
		for (int i=0;i<ncol;i++){
			if (i>0)
				bufsumfl.write(delim);
			bufsumfl.write(colSpec.getColumnName(i));
		}
		
		Object val;
		if (_writeData){
			for (int i=0;i<ndat;i++){
				bufsumfl.newLine();
				for (int j=0;j<ncol;j++){
					if (j>0)
						bufsumfl.write(delim);
					val=this.getValueAt(i, j);
					bufsumfl.write(this.codeObjectToString(val));
				}
			}
		}
		bufsumfl.close();
	}
	
	public void addRecordToFile(String _fileName, int _rowIndex)throws Exception{
		File fl=new File(rootFl,_fileName);
		fl.setReadable(true);
		fl.setWritable(true);

		BufferedWriter bufsumfl = new BufferedWriter(new FileWriter(fl,true));
		bufsumfl.newLine();
		int ncol=this.getColumnCount();
		Object val;
		for (int j=0;j<ncol;j++){
			if (j>0)
				bufsumfl.write(delim);
			val=this.getValueAt(_rowIndex, j);
			bufsumfl.write(this.codeObjectToString(val));
		}
		bufsumfl.close();
	}
	
	public void writeRecordOnline(String _fileName, int _rowIndex)throws Exception{
		if (!new File(rootFl,_fileName).exists())
			this.writeNewFile(_fileName,false);
		this.addRecordToFile(_fileName, _rowIndex);
	}
	
	/**
	 * 
	 * @return string representation of the current rootpath
	 */
	public String getRootPath(){
		return rootpath;
	}
	
	/**
	 * Sets the rootpath for this table. Does not update PathName records in the table,
	 * so care should be taken when using this method explicitly.
	 * @param _newRootPath path to set. Should correspond to the valid directory
	 */
	public void setRootPath(String _newRootPath){
		try{
			rootFl=new File(_newRootPath).getCanonicalFile();
		}catch (IOException ex) {
			throw new RuntimeException("TableModel: Can not create canonical path for the rootPath");
		}
		
		if (!rootFl.isDirectory())
			throw new RuntimeException("TableModel: specified rootpath does not define existing directory. Unexpected plugin behaviour possible");
			
		rootPth=rootFl.toPath();
		rootpath=rootFl.getAbsolutePath();
	}
	
	/**
	 * changes rootpath and updates relative pathes to already stored file information accordingly
	 * @param _newRootPath new rootpath location
	 */
	public void setRootPathDeep(String _newRootPath)throws Exception{
		Path oldPath=this.rootPth;
		this.setRootPath(_newRootPath);
		
		List<String> fileTypes= ColumnSpecification.fileTypes;
		String[] tags;
		Path filePath;
		String pathString;
		int columnIndex;
		int nDat=this.getRowCount();
		for (String tp:fileTypes){
			tags=colSpec.getFileTags(tp);
			for(String tag:tags){
				columnIndex=colSpec.getPathColumnIndex(tag, tp);
				this.checkColumnIndex(columnIndex);
				for (int iDat=0;iDat<nDat;iDat++){
					pathString=(String)this.getValueAt(iDat, columnIndex);
					if(pathString==null)continue;
					filePath=resolveRelativePath(Paths.get(pathString),oldPath);
					this.setValueAt(relativizePath(filePath, rootPth).toString(), iDat, columnIndex);
				}
			}
		}
	}
	
	/**
	 * checks if provided path refers to the same location as rootpath of this tablemodel
	 * @param _testPath
	 * @return
	 * @throws Exception
	 */
	public boolean isRootPath(String _testPath)throws Exception{
		return Files.isSameFile(rootPth, Paths.get(_testPath));
	}
	
	private static Path relativizePath(Path _absolutePath,Path _referencePath){
		if ((_absolutePath==null)||(_referencePath==null)) return null;
		Path tempPath=_referencePath.relativize(_absolutePath);
		return rootWordPath.resolve(tempPath);
		
	}
	
	private static Path resolveRelativePath(Path _relativePath,Path _referencePath){
		if ((_relativePath==null)||(_referencePath==null)) return null;
		Path tempPath=rootWordPath.relativize(_relativePath);
		return _referencePath.resolve(tempPath);
	}
	
	
	/**
	 * removes data in the single table row
	 * @param _rowIndex index of the dataset (row of the table)
	 */
	public void cleanRecord(int _rowIndex)/*throws Exception*/{
		
		int ncol=this.getColumnCount();
		for (int j=0;j<ncol;j++)
			this.setValueAt(null, _rowIndex, j);
		this.fireTableRowsUpdated(_rowIndex, _rowIndex);//to update table in the viewer
	}

	public void cleanColumn(String _columnName)throws Exception{
		int columnIndex=colSpec.getColumnIndex(_columnName);
		if (columnIndex<0)
			throw new IndexOutOfBoundsException("TableModel.cleanCol: column is not found");
		this.cleanColumn(columnIndex);
	}
	
	public void cleanColumn(int _columnIndex)throws Exception{
		int nrow=this.getRowCount();
		for (int i=0;i<nrow;i++){
			this.setValueAt(null, i, _columnIndex);
		}
		this.fireTableDataChanged();//to update table in the viewer
	}
	
	public void cleanRecordField(int _rowIndex, int _columnIndex)throws Exception{
		this.checkTableIndexes(_rowIndex, _columnIndex);
		this.setValueAt(null, _rowIndex, _columnIndex);
		this.fireTableCellUpdated(_rowIndex, _columnIndex);//to update table in the viewer
	}
	
	public void cleanRecordField(int _rowIndex, String _columnName)throws Exception{
		this.cleanRecordField(_rowIndex, colSpec.getColumnIndex(_columnName));
	}
	
	public void cleanFileRecord(int _rowIndex, String _tag, String _type)throws Exception{
		int ipth=colSpec.getPathColumnIndex(_tag, _type);
		int ifnm=colSpec.getFileColumnIndex(_tag, _type);
		
		this.cleanRecordField(_rowIndex, ipth);
		this.cleanRecordField(_rowIndex, ifnm);
		
	}
	
	public String getStringValue(int _rowIndex, int _columnIndex)throws Exception{
		this.checkTableIndexes(_rowIndex, _columnIndex);

		return (String)this.getValueAt(_rowIndex, _columnIndex);
	}
	
	public String getStringValue(int _rowIndex, String _columnName)throws Exception{
		return this.getStringValue(_rowIndex, colSpec.getColumnIndex(_columnName));
	}
	
	public void setStringValue(String _value, int _rowIndex, int _columnIndex)throws Exception{
		this.checkTableIndexes(_rowIndex, _columnIndex);
		this.setValueAt(_value, _rowIndex, _columnIndex);
		this.fireTableCellUpdated(_rowIndex, _columnIndex);//to update table in the viewer
		
	}
	
	public void setStringValue(String _value, int _rowIndex, String _columnName)throws Exception{
		this.setStringValue(_value, _rowIndex, colSpec.getColumnIndex(_columnName));
	}
	
	public String getFileName(int _rowIndex, String _tag, String _type)throws Exception{
		return this.getStringValue(_rowIndex, colSpec.getFileColumnIndex(_tag, _type));
	}
	
	public void setFileName(String _fileName,int _rowIndex, String _tag, String _type)throws Exception{
		this.setStringValue(_fileName, _rowIndex,colSpec.getFileColumnIndex(_tag, _type));
	}

	protected String getPathNameValue(int _rowIndex, String _tag, String _type)throws Exception{
		return this.getStringValue(_rowIndex, colSpec.getPathColumnIndex(_tag, _type));
	}
	
	protected void setPathNameValue(String _pathName, int _rowIndex, String _tag, String _type)throws Exception{
		String pathNameUnix=FilenameUtils.separatorsToUnix(_pathName);
		this.setStringValue(pathNameUnix,_rowIndex, colSpec.getPathColumnIndex(_tag, _type));
	}
	
	public double getNumericValue(int _rowIndex, String _tag)throws Exception{
		Object objectValue=this.getClassifiedValue(_rowIndex, _tag, "NUM");
		return (objectValue!=null)?(Double)objectValue:Double.NaN;
	}
	
	public void setNumericValue(double _value,int _rowIndex, String _tag)throws Exception{
		this.setClassifiedValue(_value, _rowIndex, _tag, "NUM");
	}
	
	public boolean getBooleanValue(int _rowIndex, String _tag)throws Exception{
		return (Boolean) this.getClassifiedValue(_rowIndex, _tag, "BOOL");
	}
	
	public void setBooleanValue(boolean _value,int _rowIndex, String _tag)throws Exception{
		this.setClassifiedValue(_value, _rowIndex, _tag, "BOOL");
	}

	public String getFactorValue(int _rowIndex, String _tag)throws Exception{
		return (String) this.getClassifiedValue(_rowIndex, _tag, "FACT");
	}
	
	public void setFactorValue(String _value,int _rowIndex, String _tag)throws Exception{
		this.setClassifiedValue(_value, _rowIndex, _tag, "FACT");
	}
	
	public void setClassifiedValue(Object _value,int _rowIndex, String _tag,String _type)throws Exception{
		int columnIndex=colSpec.getValueColumnIndex(_tag,_type);
		this.checkTableIndexes(_rowIndex, columnIndex);
		this.setValueAt(_value,_rowIndex, columnIndex);
		this.fireTableCellUpdated(_rowIndex, columnIndex);
	}

	public Object getClassifiedValue(int _rowIndex, String _tag,String _type)throws Exception{
		int columnIndex=colSpec.getValueColumnIndex(_tag, _type);
		this.checkTableIndexes(_rowIndex, columnIndex);
		return this.getValueAt(_rowIndex, columnIndex);
	}
	
//	public Object[] getClassifiedValueArray(String _tag,String _type){
//		Object[] resultArray;
//		int columnIndex=colSpec.getValueColumnIndex(_tag, _type);
//		final int nData=this.getRowCount();
//		
//		//TO DO: make properly with class variable afterwards
////		Class<?> colClass=colSpec.getColumnClass(columnIndex);
////		colClass.g
////		
////		=new colClass //  [nData];
//		switch (_type){
//			case "NUM":	resultArray=new double[nData]; break;
//			case "BOOL":
//			case "FACT":
//			default:
//		}
//		
//		
//		return resultArray;
//	}
	
	public String[] getFactorArray(String _tag)throws Exception{
		int columnIndex=colSpec.getValueColumnIndex(_tag, "FACT");
		final int nData=this.getRowCount();
		String[] resultArray=new String[nData];
		for(int i=0;i<nData;i++)
			resultArray[i]=(String)this.getValueAt(i, columnIndex);
		return resultArray;
	}

	public String[] getUniqueFactorValues(String _tag)throws Exception{
		String[] factorArray=this.getFactorArray(_tag);
		Set<String> set = new LinkedHashSet<String>(Arrays.asList(factorArray));
		return (String[])set.toArray(new String[set.size()]);
	}
	
	public Path getFileAbsolutePath(int _rowIndex, String _tag, String _type)throws Exception{
		String pth=this.getPathNameValue(_rowIndex, _tag, _type);
		String fnm=getFileName(_rowIndex,_tag,_type);
		if((pth==null)||(fnm==null)) return null;
		return resolveRelativePath(Paths.get(pth,fnm),rootPth).normalize();
	}

	public File getFile(int _rowIndex, String _tag, String _type)throws Exception{
		Path path=this.getFileAbsolutePath(_rowIndex, _tag, _type);
		return (path!=null)? path.toFile():null;
	}
	
	public String getFileAbsolutePathString(int _rowIndex, String _tag, String _type)throws Exception{
		Path path=this.getFileAbsolutePath(_rowIndex, _tag, _type);
		return (path!=null)?path.toString():null;
	}

	public Path getFileParentPath(int _rowIndex, String _tag, String _type)throws Exception{
		String pth=this.getPathNameValue(_rowIndex, _tag, _type);
		if(pth==null) return null;
		return resolveRelativePath(Paths.get(pth),rootPth).normalize();
	}
	
	public String getFileParentPathString(int _rowIndex, String _tag, String _type)throws Exception{
		Path path=this.getFileParentPath(_rowIndex, _tag, _type);
		return (path!=null)?path.toString():null;
	}

	
	public void setFileAbsolutePath(String _absoluteFolderPath,String _fileName,int _rowIndex,String _tag, String _type)throws Exception{
		this.setFileAbsolutePath(Paths.get(_absoluteFolderPath,_fileName),_rowIndex, _tag, _type);
	}

	public void setFileAbsolutePath(String _absolutePath,int _rowIndex,String _tag, String _type)throws Exception{
		this.setFileAbsolutePath(Paths.get(_absolutePath),_rowIndex, _tag, _type);
	}
	
	public void setFileAbsolutePath(File _file,int _rowIndex,String _tag, String _type)throws Exception{
		this.setFileAbsolutePath(_file.toPath(), _rowIndex, _tag, _type);
	}
	
	public void setFileAbsolutePath(Path _absolutePath,int _rowIndex,String _tag, String _type)throws Exception{

		String  rpth,fnm;
		if(_absolutePath==null){
			rpth=NullString;
			fnm=NullString;
		}
		else{
			rpth=relativizePath(_absolutePath.getParent(), rootPth).toString();
			fnm=_absolutePath.getFileName().toString();
		}
		
		this.setPathNameValue(rpth, _rowIndex, _tag, _type);
		this.setFileName(fnm, _rowIndex, _tag, _type);
		//this.setStringValue(rpth, _rowIndex, colSpec.getPathColumnIndex(_tag, _type));
		//this.setStringValue(fnm, _rowIndex, colSpec.getFileColumnIndex(_tag, _type));
	}
	
	public void setFileParentPath(String _parentPath,int _rowIndex,String _tag, String _type)throws Exception{
		this.setFileParentPath(Paths.get(_parentPath),_rowIndex, _tag, _type);
	}

	
	public void setFileParentPath(Path _parentPath,int _rowIndex,String _tag, String _type)throws Exception{

		String  rpth;
		if(_parentPath==null){
			rpth=NullString;
		}
		else{
			rpth=relativizePath(_parentPath, rootPth).toString();
		}
		
		this.setPathNameValue(rpth, _rowIndex, _tag, _type);
		//this.setStringValue(rpth, _rowIndex, colSpec.getPathColumnIndex(_tag, _type));
		
	}
	
	public void refresh(Object[][] objects){
	    //make the changes to the table, then call fireTableChanged
	    fireTableChanged(null);
	}
	
	public double[] getNumericColumn(String _tag)throws Exception{
		int columnIndex=colSpec.getValueColumnIndex(_tag, "NUM");
		this.checkColumnIndex(columnIndex);
		
		int ndat=this.getRowCount();
		double[] res=new double[ndat];
		
		for (int i=0;i<ndat;i++){
			res[i]=(Double)this.getValueAt(i, columnIndex);//Double.parseDouble(sdata[i][icol]);
		}
		return res;
	}
	
	/**
	 * identifies dataset in the table by the value in the specified column
	 * @param _value		value of the cell as a string
	 * @param _columnName	full column name
	 * @return			index of specified dataset, e.g. row index in the table (0 based)
	 * @throws Exception	when specified column is not defined in this table
	 */
	public int getRowIndexByValue(Object _value,String _columnName)throws Exception{
		int columnIndex=colSpec.getColumnIndex(_columnName);
		this.checkColumnIndex(columnIndex);
		final int dnum=getRowCount();
		for (int i=0;i<dnum;i++){
			if (this.getValueAt(i, columnIndex).equals(_value))
				return i;
		}
		return -1;
	}

	public int getRowIndexByFileName(String _value,String _tag,String _type)throws Exception{
		int columnIndex=colSpec.getFileColumnIndex(_tag, _type);
		this.checkColumnIndex(columnIndex);
		final int dnum=getRowCount();
		Object testValue;
		for (int i=0;i<dnum;i++){
			testValue=this.getValueAt(i, columnIndex);
			if(testValue==null) continue;
			if (testValue.equals(_value))
				return i;
		}
		return -1;
	}
	
    public String getColumnName(int _columnIndex){
        try{
        	return colSpec.getColumnName(_columnIndex);
        }catch(Exception ex){
        	IJ.error("Error in TableModel.getColumnName", "Could not find column with the specified index");
        	return null;
        }
    }

    public String [] getColumnNames() {
        return colSpec.getColumnNames();
    }
    
    public String [] getFileTags(String _type){
    	return colSpec.getFileTags(_type);
    }
    
    public String [] getImageFileTags(){
    	return colSpec.getImageFileTags();
    }
    
    
    public String [] getValueTags(String _type){
    	return colSpec.getValueTags(_type);
    }
    
    public boolean hasFileTag(String _tag, String _type){
    	return colSpec.hasFileTag(_tag, _type);
    }
    
    public boolean hasValueTag(String _tag, String _type){
    	return colSpec.hasValueTag(_tag, _type);
    }
    
	private void checkTableIndexes(int _rowIndex,int _columnIndex)throws IndexOutOfBoundsException{
		this.checkRowIndex(_rowIndex);
		this.checkColumnIndex(_columnIndex);
		return;
	}
	
	private void checkRowIndex(int _rowIndex){
		if ((_rowIndex<0)||(_rowIndex>=getRowCount()))
			throw new IndexOutOfBoundsException("TableModel: dataset index out of bounds");
	}
	
	private void checkColumnIndex(int _columnIndex){
		if (_columnIndex<0)
			throw new IndexOutOfBoundsException("TableModel: column not found");
		if (_columnIndex>=getColumnCount())
			throw new IndexOutOfBoundsException("TableModel: column index is bigger then the number of available columns");
	}
	
	public static String[] getFileTypes(){
		List<String> typeList=ColumnSpecification.fileTypes;
		return typeList.toArray(new String[typeList.size()]);
	}
	
	public ColumnSpecification getColumnSpecification(){
		return colSpec;
	}
	
	public int getColumnIndex(String _columnName){
		return colSpec.getColumnIndex(_columnName);
	}
	
	public Object getValueAt(int _rowIndex, String _columnName){
		return this.getValueAt(_rowIndex, this.getColumnIndex(_columnName));
	}

    /**
     * Main method for debugging
     * @param args unused
     * @throws Exception if one is generated by the class instance during test
     */
    public static void main (String[] args)throws Exception{//function for the test

    	
    	
    	System.out.println("Test start");
//    	String pth="C:/tempDat/2colExp/Jan-Feb2016/22012016-AutoFRAP-2col-siRNA/summary_gr1_.txt";
//    	TableModel tb=null;
//    	
//    	
//    	try{
//    		tb=new TableModel(new File(pth));
//    	}catch (Exception ex){
//    		Utils.generErrMsg("AutoMicTableModel: exception in the test", ex);
//    		return;
//    	}
//    	String [] itags=tb.getFileTags("IMG");
//    	String [] rtags=tb.getFileTags("ROI");
//    	System.out.println(itags==null);
//    	System.out.println(rtags==null);
//    	
//    	System.out.println("Image Tags");
//    	for (String s:itags)
//    		System.out.println(s);
//
//    	System.out.println("Roi Tags");
//    	for (String s:rtags)
//    		System.out.println(s);
//    	
//    	System.out.println(tb.hasFileTag("AFocus", "IMG"));
//    	System.out.println(tb.hasFileTag("TR1", "IMG"));
//    	System.out.println(tb.hasFileTag("AQ", "ROI"));
//    	

		String svPath="C:/tempDat/";
//		String svNm="testTable.txt";
		TableModel tbModel=new TableModel(svPath);
		//tbModel.setRootPath(svPath);
		tbModel.addFileColumns("DAPI","IMG");
		tbModel.addFileColumns("CellMask","IMG");
		tbModel.addFileColumns("SegSpots","ROI");


		
		tbModel.addValueColumn("Count", "NUM");
		tbModel.addValueColumn("QualControl", "BOOL");
		tbModel.addColumn("RegExp");

		
		tbModel.addRow();
		tbModel.addRow();
		
		tbModel.setFileAbsolutePath("C:/tempDat/subfolder/DAPI1.tif",0,"DAPI","IMG");
		
		tbModel.setNumericValue(33, 0, "Count");
		tbModel.setStringValue("kaka*kuku", 0, "RegExp");
		tbModel.writeNewFile("testFile.txt", true);
    	
		System.out.println(tbModel.getFileAbsolutePathString(0, "DAPI", "IMG"));
		
		System.out.println("test end");
    	

    	
    	
//    	final String[] testImgTags={"Img1","Img2","Img3"};
//    	final String[] testRoiTags=null;
//    	final String[] testTxtTags={"Txt1","Txt2"};
//    	final String[] testNumTags={"N1","N2"};
//    	final String[] testBoolTags=null;
//    	final String[] testFactTags={"F1","F2","F3"};
    	
//    	String TEST_COL_NAME = "FileName_AF1_IMG";
//    	System.out.println(TEST_COL_NAME);
//    	System.out.println(TEST_COL_NAME.matches(PatternFileImg));
    	
//    	String[] ss=TEST_COL_NAME.split(PatternFileImg);
//    	for (String s:ss)
//    		System.out.println(s);
//    	System.out.println(PatternFileImg.replace("\\w+", "NewTag"));
    	
//    	Pattern pt=Pattern.compile(PatternFileImg);
//   	Matcher mt=pt.matcher(TEST_COL_NAME);
//    	System.out.println(mt.matches());
//    	System.out.println(mt.groupCount());
    	
//    	for (int i=0;i<=mt.groupCount();i++)
//    		System.out.println(mt.group(i));
      }
}
