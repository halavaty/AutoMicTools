package automic.table;

import ij.ImagePlus;
import ij.gui.Roi;

import java.lang.Exception;
import java.util.HashMap;
import java.util.Map;

import automic.utils.imagefiles.TIFProcessorImageJ;
import automic.utils.roi.ROIManipulator2D;

import java.io.File;

/**
 * Last Modified: 21/02/2016
 * @author Aliaksandr Halavatyi
 */

@SuppressWarnings("serial")
public class TableProcessor_obsolete extends TableModel {
//	private static boolean DEBUG=true;
	
	class FileRecord{
		public String type;
		public String dirNm;
		public String dirApth;
		public File dirFl;
		
		public FileRecord(String _type,String _dirName){
			type=_type;
			dirNm=_dirName;
			dirFl=new File(getRootPath(),dirNm);
			dirApth=dirFl.getAbsolutePath();
		}
		
		public void CreateFldIfNew(){
			if (dirFl.exists())return;
			dirFl.mkdir();
		}
	};
	
	private Map<String, FileRecord> AdImgMap;
	private Map<String, FileRecord> AdRoiMap;
	private Map<String, FileRecord> AdTxtMap;

	
	public TableProcessor_obsolete(File _fl, String[] _adImgs, String [] _adRois, String [] _adTxts,String[] _adNums, String[] _adBools, String[] _adFacts, String[] _adFields)throws Exception{
		super(_fl);

		AdImgMap=(_adImgs==null)?new HashMap<String, FileRecord>(0):new HashMap<String, FileRecord>(_adImgs.length);
		AdRoiMap=(_adRois==null)?new HashMap<String, FileRecord>(0):new HashMap<String, FileRecord>(_adRois.length);
		AdTxtMap=(_adTxts==null)?new HashMap<String, FileRecord>(0):new HashMap<String, FileRecord>(_adTxts.length);
		
		FileRecord frec;
		
		if (_adImgs!=null)	
			for (String tg:_adImgs){
				frec=new FileRecord("IMG",tg);
				frec.CreateFldIfNew();
				AdImgMap.put(tg, frec);
				super.addFileColumns(tg, "IMG");//AddCol(tg,"IMG");
			}

		if (_adRois!=null)	
			for (String tg:_adRois){
				frec=new FileRecord("ROI",tg);
				frec.CreateFldIfNew();
				AdRoiMap.put(tg, frec);
				super.addFileColumns(tg, "ROI");//AddCol(tg,"ROI");
			}
		
		if (_adTxts!=null)	
			for (String tg:_adTxts){
				frec=new FileRecord("TXT",tg);
				frec.CreateFldIfNew();
				AdTxtMap.put(tg, frec);
				super.addFileColumns(tg, "TXT");//AddCol(tg,"TXT");
			}

		if (_adNums!=null)
			for (String nm:_adNums)
				super.addValueColumn(nm, "NUM");//AddCol(nm,"NUM");

		if (_adBools!=null)
			for (String nm:_adBools)
				super.addValueColumn(nm, "BOOL");//AddCol(nm,"BOOL");

		if (_adFacts!=null)
			for (String nm:_adFacts)
				super.addValueColumn(nm, "FACT");//AddCol(nm,"FACT");

		if (_adFields!=null)
			for (String nm:_adFields)
				super.addColumn(nm);//AddCol(nm,"NULL");
	}
	
	public void svImgTbl(int _dind, ImagePlus _img,String _imgTag, String _fnmNoExt)throws Exception{
		if(_img==null)return;
		FileRecord iRec=AdImgMap.get(_imgTag);
		if (iRec==null) throw new Exception("AutoMicTableProcessor.svImgTbl: wrond Image tag");
		File iFl=new File(iRec.dirApth,_fnmNoExt+".tif");
		TIFProcessorImageJ.saveTIFImage(_img, iFl, true);
		//Add record to the table
		setFileAbsolutePath(iFl, _dind, _imgTag, "IMG");
	}
	
	public void svRoiTbl(int _dind, Roi _r,String _roiTag, String _fnmNoExt)throws Exception{
		if(_r==null)return;
		Roi[] RoiSv=new Roi[1];
		RoiSv[0]=_r;
		this.svRoisTbl(_dind, RoiSv, _roiTag,_fnmNoExt);
	}
	
	public void svRoisTbl(int _dind, Roi[] _rs,String _roiTag, String _fnmNoExt)throws Exception{
		if (_rs==null) return;
		FileRecord rRec=AdRoiMap.get(_roiTag);
		if (rRec==null) throw new Exception("AutoMicTableProcessor.svRoisTbl: wrond Roi tag");
		ROIManipulator2D.saveRoisToFile(rRec.dirApth, _fnmNoExt, _rs);
		//Add record to the table
		setFileAbsolutePath(new File(rRec.dirFl,_fnmNoExt+"."+ROIManipulator2D.ext), _dind, _roiTag, "ROI");
	}
	
	public void svSubTblToTbl(int _dind, TableModel _subTbl,String _recTag, String _fnmNoExt)throws Exception{
		if (_subTbl==null) return;
		FileRecord sRec=AdTxtMap.get(_recTag);
		if (sRec==null) throw new Exception("AutoMicTableProcessor.svSubTblToTbl: wrond File record tag");
		_subTbl.writeNewFile(_fnmNoExt+".txt", true);
		//Add record to the table
		setFileAbsolutePath(new File(sRec.dirApth,_fnmNoExt+".txt"), _dind, _recTag, "TXT");
	}

	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {}
}
