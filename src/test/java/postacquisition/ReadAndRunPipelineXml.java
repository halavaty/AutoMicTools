package postacquisition;

import java.io.File;

import automic.postaq.pipeline.PipelineInput;
import automic.postaq.pipeline.PipelineInterface;
import automic.postaq.pipeline.PipelineRunner;
import automic.postaq.pipeline.xml.PipelineXmlReader;
import automic.table.ManualControlFrame;
import automic.table.TableModel;
import automic.utils.Utils;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.TextWindowLogger;
import ij.ImageJ;

public class ReadAndRunPipelineXml {
	static String pipelinePath="C:/tempDat/STC1_project_(FV)/Analysis_Pipelines";//"C:/tempDat/Cell_stacks_drug_Fatima_Summer2016/Drug--HeLaKyoto/10082016--HelaKyoto--drugs--stacks--fixed/Analysis_Pipelines";//"C:/tempDat/testPipelineXML";//"C:/Halavatyi_Work/Fiji_prog/AutoFRAP_JavaTools/pipelines";
	static String pipelineFileName="Quantify_Eres-2chan-19112016-Kyoto.xml";//"Extract_Projections_and_Slices-17082016.xml";//"YFP_stacks.xml";
	
	public static PipelineInput getDefaultInput(){
		PipelineInput protocolInput=new PipelineInput();
		protocolInput.dataPath="C:/tempDat/STC1_project_(FV)/siRNA--HelaKyoto/20170407--HelaKyoto--Sec31-A568--Sec24B-A488--fixed--siRNA--49h";
		protocolInput.subPathAnalysis ="Analysis-20170418";
		protocolInput.globalExperimentTableName="summary_global_manual.txt";
		protocolInput.startStep=-1;
		protocolInput.endStep=-1;
		protocolInput.startDataset=-1;
		protocolInput.endDataset=-1;
		
		return protocolInput;
	}
	
	public static void runTest(){
		ApplicationLogger.setLogger(new TextWindowLogger("AutoMic analysis log"));

		
		PipelineXmlReader xmlReader=new PipelineXmlReader(new File(pipelinePath,pipelineFileName).getAbsolutePath());
		PipelineInterface pipelineObject=null;
		
		try{
			pipelineObject=xmlReader.getPipeline();
		}catch(Exception ex){
			Utils.generateErrorMessage("Pipeline loading crashed", ex);
		}
		
		PipelineRunner	pipelineRunner=new PipelineRunner(pipelineObject, getDefaultInput());
		try{
			pipelineRunner.runPipeline();
		}
		catch (Exception ex){
			Utils.generateErrorMessage("Pipeline running crashed", ex);
		}
		try{
			ManualControlFrame frame=new ManualControlFrame(new TableModel(pipelineRunner.getLastOutputTableFile()));
			frame.setVisible(true);
		}catch(Exception ex){
			Utils.generateErrorMessage("Result visualisation crashed", ex);
		}
		
		ApplicationLogger.resetLogger();
	}
	
	
	/**
	 * Main method for debugging.
	 *
	 * For debugging, it is convenient to have a method that starts ImageJ, loads an
	 * image and calls the plugin, e.g. after setting breakpoints.
	 *
	 * @param args unused
	 */
	public static void main(String[] args) {

		// start ImageJ
		new ImageJ();
		runTest();
		
	}
}
