package open_image;

import ij.ImagePlus;
import ij.IJ;

import automic.utils.imagefiles.PatternOpenerWithBioformats;
import ij.ImageJ;

public abstract class PatternOpener_DifferentMethods {
	
	
	
	//static String pathRegExp="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z00002--T00000--A568.tif";
	//static String testPattern="X:/group/ALMFstuff/Aliaksandr/test_data/patternFormationTest/image--L0000--S00--U00--V00--J52--E02--O00--X03--Y00--T0000--Z<00-20>--C0<0-1>.ome.tif";
	//static String testPattern="X:/group/ALMFstuff/Aliaksandr/test_data/patternFormationTest/image--L0000--S00--U00--V00--J52--E02--O00--X03--Y00--T0000--Z(?<slice>\\d+)--C0<0-1>.ome.tif";
	static String testPattern="D:\\tempDat\\to_delete\\raw_data\\tt--(\\\\d+).tif";


	public static void main(String[] args)throws Exception{
		
		System.getProperties().setProperty("plugins.dir", "D:/Alex_work/runnung soft/Fiji.app_new/plugins");
		
		new ImageJ();
		
		long startTime1=System.currentTimeMillis();
		ImagePlus img=PatternOpenerWithBioformats.openImageMacro(testPattern, false);
		//img.hide();
		IJ.log("Time 1 ="+(System.currentTimeMillis()-startTime1));
		
		long startTime2=System.currentTimeMillis();
		ImagePlus img2=PatternOpenerWithBioformats.openImageTempFile(testPattern, false);
		IJ.log("Time 2 ="+(System.currentTimeMillis()-startTime2));
		//ImagePlus img=PatternOpenerWithBioformats.openImageDirect(testPattern, false);
		
		
		//System.out.println(img);
		img.show();
		img2.show();
		
	}
}
