package open_image;

import ij.ImagePlus;
import loci.plugins.BF;
import loci.plugins.in.ImportProcess;
import loci.formats.FilePattern;
import loci.plugins.in.ImporterOptions;
import automic.utils.imagefiles.PatternOpenerWithBioformats;
import ij.ImageJ;

public abstract class PatternOpener {
	
	
	
	//static String pathRegExp="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z00002--T00000--A568.tif";
	static String path="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z0000[0-4]--T00000--Blank01.tif";
	static String pathRegExp="D:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z0000[0-4]--T00000--Blank01.tif";
	
	public static void main(String[] args)throws Exception{
		
		new ImageJ();
		
		ImporterOptions options = new ImporterOptions();
		//System.out.println("Group Files Info:");
		//System.out.println(options.getGroupFilesInfo());
		//System.out.println("Id Info:");
		//System.out.println(options.getIdInfo());
		
		options.setGroupFiles(true);
		//System.out.println(options.isViewStandard());
		
		options.setId(pathRegExp);
		//ImportProcess process = new ImportProcess(options);
		//System.out.println(process.execute());
		ImagePlus img=BF.openImagePlus(options)[0];
		//System.out.println(img);
		//ImagePlus img=PatternOpenerWithBioformats.openImage(pathRegExp, false);
		
		img.show();
		//FilePattern fp=new FilePattern(path);
		System.out.println(FilePattern.findPattern(path));
		
	}
}
