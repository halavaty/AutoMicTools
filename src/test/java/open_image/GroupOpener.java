package open_image;



import ij.ImagePlus;
import ij.plugin.Concatenator;
import loci.plugins.BF;
import loci.plugins.in.ImporterOptions;
import ij.ImageJ;

public abstract class GroupOpener {
	
	
	
	static String path_before="C:\\tempDat\\TLRC\\Anita--FLIPR\\Example_MD_data_20170301\\FLIPR-Test-COC-plate-baseline\\W0001--A1--A1\\P001--A1--A1\\FLIPR-Test-COC-plate-baseline--A1--A1--W0001--P001--T00005--Z001--C02.ome.tif";
	static String path_after="C:\\tempDat\\TLRC\\Anita--FLIPR\\Example_MD_data_20170301\\FLIPR-Test-COC-plate-NFA\\W0001--A1--A1\\P001--A1--A1\\FLIPR-Test-COC-plate-NFA--A1--A1--W0001--P001--T00005--Z001--C02.ome.tif";
	
	
	public static void main(String[] args)throws Exception{
		
		new ImageJ();
		
		ImporterOptions options = new ImporterOptions();
		System.out.println(options.getGroupFilesInfo());
		options.setGroupFiles(true);
		
		options.setId(path_before);
		ImagePlus img_before=BF.openImagePlus(options)[0];
		options.setId(path_after);
		ImagePlus img_after=BF.openImagePlus(options)[0];
		
		ImagePlus img_concatenated=new Concatenator().concatenateHyperstacks(new ImagePlus[]{img_before,img_after}, "Concatenated", false);
		
		img_concatenated.show();
		
		
		
		//img_before.show();
		//img_after.show();
		
		
		
		//new LociImporter().run("location=[Local machine] windowless=false ");
		//IJ.run("Bio-Formats", "open=C:\\tempDat\\TLRC\\Anita--FLIPR\\Example_MD_data_20170301\\FLIPR-Test-COC-plate-baseline\\W0001--A1--A1\\P001--A1--A1\\FLIPR-Test-COC-plate-baseline--A1--A1--W0001--P001--T00001--Z001--C02.ome.tif color_mode=Default group_files rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT");
	}
}
