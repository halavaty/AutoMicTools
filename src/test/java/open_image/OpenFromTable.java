package open_image;



import ij.ImagePlus;

import java.io.File;

import automic.table.TableModel;
import automic.utils.imagefiles.ImageOpener;
import automic.utils.imagefiles.ImageOpenerInterface;
import ij.ImageJ;

public abstract class OpenFromTable {
	
	
	
	static String tablePath="C:/tempDat/STC1_project_(FV)/STC1_overexpression--live_imaging--drug/20170509--Live-cell imaging";
	static String tableName="summary_manual.txt";
	
	static int rowIndex=3;
	static String imageTag="Part1";
	static String imageType="IMGS";
	
	
	public static void main(String[] args)throws Exception{
		
		new ImageJ();
		TableModel inputTable=new TableModel(new File(tablePath,tableName));
		ImageOpenerInterface imageOpener=new ImageOpener();
		ImagePlus testImage=imageOpener.openImage(inputTable, rowIndex, imageTag, imageType);
		testImage.show();
		
	}
}
