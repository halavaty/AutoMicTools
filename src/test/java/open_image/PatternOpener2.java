package open_image;

import ij.IJ;
import ij.ImagePlus;
import loci.plugins.BF;
import loci.plugins.in.ImportProcess;
import loci.formats.FilePattern;
import loci.formats.ImageReader;
import loci.formats.MetadataTools;
import loci.formats.meta.IMetadata;
import loci.plugins.in.ImporterOptions;
import automic.utils.imagefiles.PatternOpenerWithBioformats;
import ij.ImageJ;

public abstract class PatternOpener2 {
	
	
	
	//static String pathRegExp="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z00002--T00000--A568.tif";
	static String path="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z0000[0-4]--T00000--Blank01.tif";
	static String pathRegExp="D:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z0000[0-4]--T00000--Blank01.tif";
	
	public static void main(String[] args)throws Exception{
		
		//http://imagej.net/docs/menus/plugins.html#dir
		
		new ImageJ();
		
		System.getProperties().setProperty("plugins.dir", "C:/Halavatyi_Work/running soft/Fiji.app_new/plugins");
		
//		ImporterOptions options = new ImporterOptions();
//		//System.out.println("Group Files Info:");
//		//System.out.println(options.getGroupFilesInfo());
//		//System.out.println("Id Info:");
//		//System.out.println(options.getIdInfo());
//		
//		options.setGroupFiles(true);
//		//System.out.println(options.isViewStandard());
//		
//		options.setId(pathRegExp);
//		//ImportProcess process = new ImportProcess(options);
//		//System.out.println(process.execute());
//		ImagePlus img=BF.openImagePlus(options)[0];
//		//System.out.println(img);
//		//ImagePlus img=PatternOpenerWithBioformats.openImage(pathRegExp, false);
//		
//		img.show();
//		//FilePattern fp=new FilePattern(path);
//		System.out.println(FilePattern.findPattern(path));
//		
//		ImageReader reader = new ImageReader();
//		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
//		reader.setMetadataStore(omeMeta);
//		reader.setId(pathRegExp);
//		reader.setGroupFiles(true);
		//reader
		
		//IJ.run("Image Sequence...", "open=Z:\\halavaty\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data2\\Medium_1dmedium_Ecad1568_Ncad488--W00014--P00002--Z00000--T00000--d_hoechst.tif file=(Medium_1dmedium_Ecad1568_Ncad488--W00014--P00002--Z0000.*--T00000--d_hoechst) sort");
		IJ.run("Bio-Formats", "open=C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00003--Z00002--T00000--GFP.tif autoscale color_mode=Default group_files rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT axis_1_number_of_images=3 axis_1_axis_first_image=1 axis_1_axis_increment=1 axis_2_number_of_images=5 axis_2_axis_first_image=0 axis_2_axis_increment=1 contains=[] name=C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00002--Z0000<0-4>--T00000--GFP.tif");
	}
}
