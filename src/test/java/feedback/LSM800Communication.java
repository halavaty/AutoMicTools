package feedback;

import java.io.File;

import automic.online.microscope.ZeissLSM800;


public abstract class LSM800Communication {
	
	
	public static void main(String[] args)throws Exception{
		System.out.println("Test started");
		
		double[] x= {3.0,2.2,1};
		double[] y= {2.0,5.1,0};
		double[] z= {0,1,2};
		String nextJob="HZ";
		String sourceFilePath="C:\\tempDat\\AutoMic_test\\LZ.czi";
		
		ZeissLSM800.submitJobPixels(new File(sourceFilePath),nextJob, x, y, z);
		
		System.out.println("Test finished");
		
	}
}
