package roi;

import java.io.File;

import automic.online.microscope.ZeissLSM800;
import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import automic.utils.roi.ROIManipulator2D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Roi;
//import ij.gui.WaitForUserDialog;
import ij.plugin.frame.RoiManager;

public abstract class PolygonRoisForZenBlue {
	static final String imageFilePath 	="D:\\tempDat\\AutoMic_test\\001.tif";
	//static final Class<?> testPipelineClass		=FLIPRPipeline.class;
	
	
	
	
	public static void main(String[] args)throws Exception {
		// start ImageJ
		new ImageJ();

		Logger logger=new TextWindowLogger("Roi Visualisation Test");
		ApplicationLogger.setLogger(logger);
		
		try{
			
			
			ImagePlus img=IJ.openImage(imageFilePath);
			img.show();
			RoiManager rmManager=ROIManipulator2D.getEmptyRm();
			
			
			//new WaitForUserDialog("Add test Rois to Roi Manager").show();
			
			IJ.setRawThreshold(img, 5, 255, null);
			IJ.run(img, "Analyze Particles...", "size=100-Infinity exclude include add");

			
			
			Roi[] rs=rmManager.getRoisAsArray();
			System.out.println(rs[0].getTypeAsString());
			
			
			
			ZeissLSM800.submitJobPixels(new File(imageFilePath), "jjj", null, rs);
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Crash During 3d ROI visualisation Test", ex);
		}
	}
}
