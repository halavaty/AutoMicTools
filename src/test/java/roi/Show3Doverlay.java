package roi;

import java.awt.Color;
import java.io.File;

import automic.utils.logger.ApplicationLogger;
import automic.utils.logger.Logger;
import automic.utils.logger.TextWindowLogger;
import automic.utils.roi.ROIManipulator3D;
import ij.IJ;
import ij.ImageJ;
import ij.ImagePlus;
import ij.gui.Overlay;
import mcib3d.geom.Objects3DPopulation;

public abstract class Show3Doverlay {
	static final String rois3dFilePath	="X:\\group\\ALMFstuff\\Aliaksandr\\User_data\\TLRC--Dario\\granules--201903\\fullset--splittedDatasets-part--fiji\\All.Nuclei.3D\\20190122_B130--001.zip";
	static final String imageFilePath 	="X:\\group\\ALMFstuff\\Aliaksandr\\User_data\\TLRC--Dario\\granules--201903\\fullset--splittedDatasets-part\\20190122_B130--001.tif";
	//static final Class<?> testPipelineClass		=FLIPRPipeline.class;
	
	
	
	
	public static void main(String[] args)throws Exception {
		// start ImageJ
		new ImageJ();

		Logger logger=new TextWindowLogger("Roi Visualisation Test");
		ApplicationLogger.setLogger(logger);
		
		try{
			ImagePlus img=IJ.openImage(imageFilePath);
			img.show();
			Objects3DPopulation objects= ROIManipulator3D.fileToPopulation(new File(rois3dFilePath));
			Overlay o=ROIManipulator3D.populationToOverlay(objects, Color.orange);
			img.setOverlay(o);
			
		}
		catch (Exception ex){
			logger.sendExceptionMessage("Crash During 3d ROI visualisation Test", ex);
		}
	}
}
