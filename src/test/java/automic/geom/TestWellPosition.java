package automic.geom;

import java.util.LinkedHashMap;
import java.util.Map;

public class TestWellPosition {
	public static void main(String[] args){
		WellPosition wp1=new WellPosition("A01", 1);
		WellPosition wp1_1=new WellPosition("A01", 1);
		WellPosition wp1_2=new WellPosition("A01", 2);
		WellPosition wp2_1=new WellPosition("B02", 1);
		WellPosition wp2_2=new WellPosition("B02", 2);
		
		
		System.out.println(String.format("(%s) compared to (%s): %s", wp1, wp1_1,(wp1.compareTo(wp1_1))));
		System.out.println(String.format("(%s) compared to (%s): %s", wp1, wp1_2,(wp1.compareTo(wp1_2))));
		System.out.println(String.format("(%s) compared to (%s): %s", wp1, wp2_1,(wp1.compareTo(wp2_1))));
		System.out.println(String.format("(%s) compared to (%s): %s", wp2_2, wp2_1,(wp2_2.compareTo(wp2_1))));
		
		
		Map<WellPosition, Point3D> platePositions=new LinkedHashMap<WellPosition, Point3D>();
		System.out.println(platePositions.containsKey(wp1));
		platePositions.put(wp1_1, new Point3D(0, 0, 0));
		System.out.println(platePositions.containsKey(wp1));
		platePositions.put(wp1, new Point3D(0, 0, 0));
		System.out.println(platePositions.containsKey(wp1));
		
		System.out.println(platePositions.keySet());
		
		System.out.println(String.format("(%s) hash code is: %d", wp1, wp1.hashCode()));
		System.out.println(String.format("(%s) hash code is: %d", wp1_1, wp1_1.hashCode()));
		System.out.println(String.format("(%s) hash code is: %d", wp1_2, wp1_2.hashCode()));
		System.out.println(String.format("(%s) hash code is: %d", wp2_1, wp2_1.hashCode()));

	
		
	}

}
