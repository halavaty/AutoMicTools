package automic.table.test;

import loci.formats.FilePattern;

import java.io.File;
//import java.nio.file.Path;
//import java.nio.file.Paths;

import automic.table.ManualControlFrame;
import automic.table.TableModel;
import ij.ImageJ;

public abstract class TablePattern {
	
	
	
	//static String pathRegExp="C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00001--Z00002--T00000--A568.tif";
	static String fileLocation="X:/group/ALMFstuff/Aliaksandr/test_data/patternFormationTest";
	static String fileName="image--L0000--S00--U00--V00--J52--E02--O00--X03--Y00--T0000--Z00--C00.ome.tif";
	
	public static void main(String[] args)throws Exception{
		
		
		new ImageJ();
		
		TableModel tbl=new TableModel(fileLocation);
		
		tbl.addFileColumns("High.Zoom", "IMGR");
		
		
		
		
		
		String ptrn=FilePattern.findPattern(fileName,fileLocation);
		
		File filePtrn=new File(ptrn);
		System.out.println(filePtrn);
		
		//gives an error Path can not handle special characters which are not allowed in file names
		//Path pthPtrn=Paths.get(ptrn);
		//System.out.println(pthPtrn);
		
		String folderToSet=filePtrn.getParent();
		String nameToSet=filePtrn.getName();
		
		
		System.out.println(ptrn);
		
		
		tbl.addRow();
		
		//tbl.setFileAbsolutePath(ptrn, 0, "High.Zoom", "IMGR");
		tbl.setFileAbsolutePath(folderToSet,fileName, 0, "High.Zoom", "IMGR");
		//tbl.setPathNameValue(folderToSet, 0, "High.Zoom", "IMGR");
		tbl.setFileName(nameToSet, 0, "High.Zoom", "IMGR");
		
		
		new ManualControlFrame(tbl).setVisible(true);
		
		System.out.println("Path from table: "+tbl.getFileParentPathString(0, "High.Zoom", "IMGR"));
		System.out.println("Name from table: "+tbl.getFileName(0, "High.Zoom", "IMGR"));
		
		
		
		//System.getProperties().setProperty("plugins.dir", "C:/Halavatyi_Work/running soft/Fiji.app_new/plugins");
		
//		ImporterOptions options = new ImporterOptions();
//		//System.out.println("Group Files Info:");
//		//System.out.println(options.getGroupFilesInfo());
//		//System.out.println("Id Info:");
//		//System.out.println(options.getIdInfo());
//		
//		options.setGroupFiles(true);
//		//System.out.println(options.isViewStandard());
//		
//		options.setId(pathRegExp);
//		//ImportProcess process = new ImportProcess(options);
//		//System.out.println(process.execute());
//		ImagePlus img=BF.openImagePlus(options)[0];
//		//System.out.println(img);
//		//ImagePlus img=PatternOpenerWithBioformats.openImage(pathRegExp, false);
//		
//		img.show();
//		//FilePattern fp=new FilePattern(path);
//		System.out.println(FilePattern.findPattern(path));
//		
//		ImageReader reader = new ImageReader();
//		IMetadata omeMeta = MetadataTools.createOMEXMLMetadata();
//		reader.setMetadataStore(omeMeta);
//		reader.setId(pathRegExp);
//		reader.setGroupFiles(true);
		//reader
		
		//IJ.run("Image Sequence...", "open=Z:\\halavaty\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data2\\Medium_1dmedium_Ecad1568_Ncad488--W00014--P00002--Z00000--T00000--d_hoechst.tif file=(Medium_1dmedium_Ecad1568_Ncad488--W00014--P00002--Z0000.*--T00000--d_hoechst) sort");
		//IJ.run("Bio-Formats", "open=C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00003--Z00002--T00000--GFP.tif autoscale color_mode=Default group_files rois_import=[ROI manager] view=Hyperstack stack_order=XYCZT axis_1_number_of_images=3 axis_1_axis_first_image=1 axis_1_axis_increment=1 axis_2_number_of_images=5 axis_2_axis_first_image=0 axis_2_axis_increment=1 contains=[] name=C:\\tempDat\\STC1_project_(FV)\\emt--test--subset--fv\\raw_data\\Medium_119_3d_1dmedium_Ecad1568_Ncad488--W00015--P00002--Z0000<0-4>--T00000--GFP.tif");
	}
}
