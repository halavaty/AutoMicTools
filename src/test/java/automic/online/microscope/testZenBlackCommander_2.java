package automic.online.microscope;

import java.io.File;
import java.util.Map;

import org.jruby.ir.operands.Self;

import automic.geom.Point3D;
import automic.online.microscope.MicroscopeCommanderFactory;
import automic.online.microscope.MicroscopeCommanderInterface;
import automic.parameters.ParameterCollection;
import automic.online.microscope.ZenBlackCommander;
import ij.gui.Roi;
import ij.plugin.frame.RoiManager;

public class testZenBlackCommander_2 {
	
//	private Map<String,Class<? extends MicroscopeCommanderInterface>> microscopeCommanderMap;
//	microscopeCommanderMap=MicroscopeCommanderFactory.identifyCommanders();
//	ParameterCollection commanderparams = new ParameterCollection();
	MicroscopeCommanderInterface commander= MicroscopeCommanderFactory.createMicroscopeCommander(ZenBlackCommander.class, null);
	
	
public static void main(String[] args)throws Exception{
	System.out.println("Working Directory = " + System.getProperty("user.dir"));
	  
	MicroscopeCommanderInterface commander= MicroscopeCommanderFactory.createMicroscopeCommander(ZenBlackCommander.class, null);
	
	File _sourceImageFile=new File(System.getProperty("user.dir"),"exampledata/AutoMicTestFolder/dummytimestamp12345/Selection/Selection--W0000--P0001-T0001.nd2");
	File _sourceROIFile=new File(System.getProperty("user.dir"),"exampledata/AutoMicTestFolder/dummytimestamp12345/Selection/Selection--W0000--P0001-T0001.zip");
	
	RoiManager rm = RoiManager.getRoiManager();
	
	//Comment out for empty ROI test:
	rm.runCommand("Open", _sourceROIFile.getPath());
	Roi[] rois = rm.getRoisAsArray();
	Point3D p1 = new Point3D(null, 12.3, null);
	Point3D p2 = new Point3D(1.1, 12.3, 4.56);
	Point3D[] pa = {p1,p2};
	commander.submitJobPixels(_sourceImageFile, "dummy", pa);

	System.exit(0);
	return;
}
}