//Example Fiji Macro for using with AutoMicTools protocol.
//Takes currently active image as an input
//Segmented ROIs need to be added to Image Overlay for being processed

//rename image to be able to close intermediate images at the end
rename("origin");
//remove any existing overlay
run("Remove Overlay");

//run segmentation to get ROIs to ROI manager
run("Set Measurements...", "area mean centroid center display redirect=None decimal=6");
run("Set Scale...", "distance=0 known=0 unit=pixel");
run("Duplicate...", "duplicate channels=1");
rename("channelImage");

run("Gaussian Blur...", "sigma=2");
setAutoThreshold("Default dark");
roiManager("reset");
run("Analyze Particles...", "size=200-Infinity pixel include add exclude");
run("Set Scale...", "distance=0 known=0 unit=pixel");
run("Set Measurements...", "area mean centroid center display redirect=None decimal=6");
selectWindow("channelImage");
close();

//add ROIs from ROI manager to image overlay. Only ROIs in Image overlay will be considered by AutoMicTools
roiManager("show all without labels");
run("From ROI Manager");

//remove Rois from ROI manager
roiManager("reset");



//close intermediate images at the end
if (roiManager("count")>0) {
    selectWindow("origin");
    run("Select All");
    run("From ROI Manager");
}

