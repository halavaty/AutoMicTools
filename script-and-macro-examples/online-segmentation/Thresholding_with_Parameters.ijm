#@ImagePlus (label="Input Image") inputImage
#@Integer( label="Lower Threshold", value=125, persist=false,required=true) threshold

#@OUTPUT ImagePlus (label="Output Image") outputImage
setBatchMode(true);

selectImage(inputImage);


print(threshold);
run("Duplicate...", " ");
setThreshold(threshold, 255, "raw");
run("Convert to Mask");

outputImage=getImageID();