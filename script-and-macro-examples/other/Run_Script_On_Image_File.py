from loci.plugins import BF

from ij import IJ

#@ File (label = "Analysis Script Path", value="C:/tempdat/Valeriy - temp/Sinem-forInscoper/Scripts/Segment_Nuclei_Jython_IJ2_parameters.py") scriptFile
#@ File (label = "Image File Path", value="C:/tempdat/Valeriy - temp/Sinem-forInscoper/ExampleImages/Selection--W0000--P0001-T0001.tif") imageFile
#@ ScriptService scriptService

micImage=BF.openImagePlus(imageFile.getAbsolutePath())[0]
#micImage.show()

inputs={'inputImage':micImage, 'channelIndex':1, 'threshold':10,'filter_radius':2.0,'min_size_pixels':2000,'max_size_pixels':10000, 'min_circularity':0.3,'max_circularity':1.0}
scriptModule = scriptService.run(scriptFile, False,inputs).get()
mask = scriptModule.getOutput("maskImage")
#mask.show()

regionPoints = scriptModule.getOutput('jsonString')
#print regionPoints

imagePathNoExt=imageFile.getAbsolutePath()[:-4]
#print filePathNoExt

#save mask image
maskPath=imagePathNoExt+'-mask.tif'
IJ.saveAsTiff(mask,maskPath)

#save JSON string with Roi outline coordinates
jsonPath=imagePathNoExt+'-feedback.json'
f = open(jsonPath, "a")
f.write(regionPoints)
f.close()