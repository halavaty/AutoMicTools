from ij import IJ
from ij.plugin import ZProjector, Duplicator
from ij.plugin.filter import ParticleAnalyzer
from ij.plugin.frame import RoiManager

from com.eclipsesource.json import JsonObject

'''
simple function to segment nuclei
the analysis can be modified to include any complexity. E.g. running AI algorithms
input - image to be segmented.
output segmented regions represented as mask and as ImageJ ROIs (mask image can be converted to ImageJ ROIs at any Point)
'''


#@ ImagePlus inputImage
#@ Integer (label = "Segmentation channel (1-based)", value=1) channelIndex
#@ Integer (label = "Threshold", value=10) threshold
#@ Double (label = "Filter radius", value=2.0) filter_radius

#@ Integer (label = "Minimum size in pixels", value=2000) min_size_pixels
#@ Integer (label = "Maximum size in pixels", value=3000) max_size_pixels
#@ Double (label = "Minimum circularity", value=0.3) min_circularity
#@ Double (label = "Maximum circularity", value=0.5) max_circularity

#@OUTPUT ImagePlus maskImage
#@OUTPUT String jsonString

def segmentNuclei(image):
    IJ.run("Set Measurements...", "area mean centroid center display redirect=None decimal=6");
    IJ.run(image, "Set Scale...", "distance=0 known=0 unit=pixel")


    #extract the required channel
    channelImage = Duplicator().run(image, channelIndex, channelIndex, 1, image.getNSlices(), 1, 1)
    
    
    #denoise by filtration
    IJ.run(channelImage, "Median...", "radius={}".format(filter_radius))
    
    #project 3D->2D
    maxProjector=ZProjector();
    maxProjector.setMethod(ZProjector.MAX_METHOD);
    maxProjector.setImage(channelImage);
    maxProjector.doProjection();
    projectedImage=maxProjector.getProjection()
    
    #clear RoiManager content
    roiManager=RoiManager.getInstance()
    if roiManager:
        roiManager.runCommand("Reset")
    else:
        roiManager=RoiManager()
    
    #segment by simple thresholding
    IJ.setRawThreshold(projectedImage, 15, 65535)
    segmentation_options=ParticleAnalyzer.INCLUDE_HOLES|ParticleAnalyzer.ADD_TO_MANAGER|ParticleAnalyzer.SHOW_MASKS
    PartAn=ParticleAnalyzer(segmentation_options,0,None,min_size_pixels,max_size_pixels,min_circularity,max_circularity);
    
    PartAn.setHideOutputImage(True);
    PartAn.analyze(projectedImage,projectedImage.getProcessor());
    IJ.resetThreshold(projectedImage)
    
    #get mask image
    maskImage=PartAn.getOutputImage()
    
    #get content of ROIManager as Array of ROIs
    segmentedRois=roiManager.getRoisAsArray()
    
    return maskImage,segmentedRois    

def convertRoisToJson(areaRois):
    jRois=JsonObject()
    
    for thisRoi in areaRois:
        poly=thisRoi.getPolygon();
        currentJRoi=JsonObject()
        for iPosition in range(0,poly.npoints):
            thisJPosition=JsonObject()
            thisJPosition.add('X', poly.xpoints[iPosition])
            thisJPosition.add('Y', poly.ypoints[iPosition])
            currentJRoi.add("P_%03d" %iPosition, thisJPosition)
            
        jRois.add(thisRoi.getName(),currentJRoi)
    
    return jRois

#this is the main script
# 1. Segment Nuclei
maskImage,nucleiRegions=segmentNuclei(inputImage)

# 2. Convert  Roi to the set of outline points. We use Json format, but it can always be adjusted depending which input microscope might take
jsonRegions=convertRoisToJson(nucleiRegions)
jsonString=jsonRegions.toString()

#print jsonString
#print type(jsonString)

