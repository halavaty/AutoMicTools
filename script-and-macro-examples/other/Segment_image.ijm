run("Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
run("Duplicate...", "duplicate channels=1");
run("Z Project...", "projection=Median");
setAutoThreshold("Default dark");
//run("Threshold...");
setThreshold(5000, 1000000000000000000000000000000.0000);
run("Set Measurements...", "centroid center display redirect=None decimal=6");
run("Analyze Particles...", "size=700-Infinity pixel circularity=0.0-0.7 display clear include add");
