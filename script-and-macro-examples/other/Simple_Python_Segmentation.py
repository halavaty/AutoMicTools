import sys
from skimage import io, img_as_ubyte
from skimage.filters import threshold_otsu

args=sys.argv

inputPath=sys.argv[1]
outputPath=sys.argv[2]

image = io.imread(inputPath)

#thresh = threshold_otsu(image)
thresh = 6000
segmentedImage = image < thresh

segmentedImage=img_as_ubyte(segmentedImage)

io.imsave(outputPath, segmentedImage)

print('Done')


