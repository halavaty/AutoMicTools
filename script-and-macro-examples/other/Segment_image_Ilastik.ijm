pathToIlastikProject="D:\\Alex_work\\feedback-microscopy-workshop-data\\external_tools_examples\\Spindle-segmentation.ilp"

run("Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
run("Duplicate...", "duplicate channels=1");
run("Z Project...", "projection=Median");
imageTitle=getTitle();

run("Run Pixel Classification Prediction", "projectfilename="+pathToIlastikProject+" inputimage="+imageTitle+" pixelclassificationtype=Segmentation");
run("Enhance Contrast", "saturated=0.35");
setThreshold(1, 1); //Threshold should correspond to the index of the target class in the output image
run("Set Measurements...", "centroid center display redirect=None decimal=6");
run("Analyze Particles...", "size=1000-Infinity pixel circularity=0.00-0.8 display clear include exclude add");
resetThreshold();
run("Enhance Contrast", "saturated=0.35");
