from loci.plugins import BF

from ij import IJ

#@ File (label = "Analysis Macro Path", value="C:/Halavatyi-work/Fiji-prog/AutoMicTools/script-and-macro-examples/online-segmentation/Thresholding_with_Parameters.py") macroFile
#@ File (label = "Image File Path", value="C:/tempdat/Sarkis/Tail_templates/tail1.tif") imageFile
#@ ScriptService scriptService

micImage=BF.openImagePlus(imageFile.getAbsolutePath())[0]
micImage.show()

inputs={'inputImage':micImage}#,'threshold':125
scriptModule = scriptService.run(macroFile, True,inputs).get()
print(scriptModule.getInputs())
mask = scriptModule.getOutput("outputImage")
#mask.show()
print(mask)
