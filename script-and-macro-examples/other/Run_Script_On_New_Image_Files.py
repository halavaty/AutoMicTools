from loci.plugins import BF
from ij import IJ
from ij.gui import WaitForUserDialog
from org.apache.commons.io.monitor import FileAlterationListenerAdaptor, FileAlterationObserver, FileAlterationMonitor
from java.io import FileFilter


#@ File (label = "Analysis Script Path", value="C:/Halavatyi-work/Fiji-prog/AutoMicTools/script-and-macro-examples/Segment_Nuclei_Jython_IJ2_parameters.py") scriptFile
#@ File (label = "Monitored Folder Path", value="C:/tempdat/AutoMic-test") experimentFolderFile
#@ String (label = "Image File Extension", value=".tif") imageFileExtension
#@ ScriptService scriptService

#define action on newly appearing image Files
class NewImageListener(FileAlterationListenerAdaptor):
    def onFileCreate(self, imageFile):
        print "New File path: ", imageFile.getAbsolutePath()

        #open image
        micImage=BF.openImagePlus(imageFile.getAbsolutePath())[0]

        #run image analysis script
        inputs={'inputImage':micImage, 'channelIndex':1, 'threshold':10,'filter_radius':2.0,'min_size_pixels':2000,'max_size_pixels':10000, 'min_circularity':0.3,'max_circularity':1.0}
        scriptModule = scriptService.run(scriptFile, False,inputs).get()
        mask = scriptModule.getOutput("maskImage")

        regionPoints = scriptModule.getOutput('jsonString')

        #save mask image
        imagePathNoExt=imageFile.getAbsolutePath()[:-4]
        maskPath=imagePathNoExt+'-mask.tif'
        IJ.saveAsTiff(mask,maskPath)

        #save JSON string with Roi outline coordinates
        jsonPath=imagePathNoExt+'-feedback.json'
        f = open(jsonPath, "a")
        f.write(regionPoints)
        f.close()


#set up filter for the files to be processed
class ImageFileFilter(FileFilter):
    def accept(self, testFile):
        return (testFile.getAbsolutePath()[-4:]==imageFileExtension)

# configure experiment folder observer        
experimentObserver = FileAlterationObserver(experimentFolderFile,ImageFileFilter())
experimentObserver.addListener(NewImageListener())

#configure and start experiment folder monitor
experimentMonitor=FileAlterationMonitor(2000)
experimentMonitor.addObserver(experimentObserver)
experimentMonitor.start()
WaitForUserDialog('Stop Experiment monitor').show()
experimentMonitor.stop(10000)

