pythonPath="C:\\anaconda\\python.exe"
scriptPath="D:\\Alex_work\\Fiji_prog\\AutoMicTools\\script-and-macro-examples\\Simple_Python_Segmentation.py"
tempImagePath="D:\\Alex_work\\feedback-microscopy-workshop-data\\external_tools_examples\\image.tif"
tempMaskPath="D:\\Alex_work\\feedback-microscopy-workshop-data\\external_tools_examples\\mask.tif"


run("Set Scale...", "distance=0 known=0 pixel=1 unit=pixel");
run("Duplicate...", "duplicate channels=1");
run("Z Project...", "projection=Median");

saveAs("Tiff", tempImagePath);

command=pythonPath+" "+scriptPath+" "+tempImagePath+" "+tempMaskPath
print(command)
exec(command);
open(tempMaskPath);
imageTitle=getTitle();

run("Enhance Contrast", "saturated=0.35");
setThreshold(0, 0); //Threshold should correspond to the index of the target class in the output image
run("Set Measurements...", "centroid center display redirect=None decimal=6");
run("Analyze Particles...", "size=1000-Infinity pixel circularity=0.00-0.8 display clear include exclude add");
resetThreshold();
run("Enhance Contrast", "saturated=0.35");
