
'''
Example of custom AutoMicTools JobDistributor scripted in Jython

Created on 14 March 2023

@author: Aliaksandr Halavatyi
'''

from net.imagej import ImageJ

from automic.online.jobdistributors import JobDistributor_Abstract
from automic.online.jobs import Job_Default
from automic.online.jobs.common import Job_AutofocusInit_Commander
from automic.online.jobs.frap import Job_GetMultipleBleachRois_Script_Commander
from automic.online.jobs.common import Job_GetMultipleImagingPositions_Script_Commander
from automic.online.jobs.common import Job_RecordFinish



#@ ImageJ ij2


class My_JobDistributor_In_Jython(JobDistributor_Abstract):


    def __init__(self):
        '''
        Constructor. No need to change anything here. Keep as it is.
        
        '''
        Job_Default.ij2=ij2
    
    
    def fillJobList(self):
        '''
        Here one needs to specify which analysis jobs need to be performed on which images.
        Each analysisJob is specified as a separate Java class.
        There is also a possibility to customise the jobs using scripting languages.
        Input parameters for addImageJob method
        - reference to the Job  Java class
        - Unique string tag identifying target image files for this job.
        - Column name tag for the AutoMic summary table.
        - Boolean variable iddicating whether results of the online analysis should be displayed 
        
        '''
        #Here are the jobs for thePhotoactivation workflow, otherwise accessible via:
        #Plugins › Auto Mic Tools › Applications › Photomanipulation › AF LowZoom and PA - Script
        self.addImageJob(Job_AutofocusInit_Commander,                "AF--",  "AF",  True)
        self.addImageJob(Job_GetMultipleBleachRois_Script_Commander, "Selection--", "Selection", True)
        self.addImageJob(Job_RecordFinish,                           "PA--", "PA",   True)
        
        #Comment previous 2 lines of code and uncomment the next 2 lines to convert the pipeline to the targeted imaging, as accessible via:
        #Plugins › Auto Mic Tools › Applications › Imaging › AF LowZoom HighZoom - script
        #self.addImageJob(Job_GetMultipleImagingPositions_Script_Commander,   "LowZoom--", "LowZoom", True)
        #self.addImageJob(Job_RecordFinish,                                   "HighZoom--", "HighZoom",True)


        

if __name__ in ('__main__','__builtin__'):
    
    distributorToRun=My_JobDistributor_In_Jython()
    distributorToRun.run('')
    
    
