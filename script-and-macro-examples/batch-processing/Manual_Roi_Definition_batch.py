'''


'''


from os import path
import re
from ij.gui import WaitForUserDialog
#from java.io import File
from loci.plugins import BF
from automic.table import TableModel, ManualControlFrame#, TableProcessor
from automic.utils.roi import ManualRoiDefiner, ROIManipulator2D
from automic.utils import FileUtils



#@ File (label = "Choose input summary table file", value="C:/tempdat/AutoMic-test/summary_AutoMicTools.txt",style="file") inputDataFile
#@ String (label = "Selection Image column tag (should exist)", value="Selection") inputImageTag
#@ String (label = "Bleach Rois column tag (column will be created)", value="PA.Regions") outputRoiTag
#@ Boolean (label = "Can move ROIs", value=False) canMoveRoi


roiDefiner=ManualRoiDefiner(1000,120,300,False,canMoveRoi,ManualRoiDefiner.POLYGON_REGION,0)


def saveRoisTable(_regions,_baseName, _table, _iDataset, _tag):
    if ROIManipulator2D.isEmptyRoiArr(_regions):
        return
    
    targetFolder = path.join(_table.getRootPath(),_tag)
    ROIManipulator2D.saveRoisToFile(targetFolder,_baseName,_regions)
    _table.setFileAbsolutePath(targetFolder,'%s.zip' %_baseName,_iDataset,_tag,'ROI')

def processDataset(iDataSet, tbModel):

    filepath = tbModel.getFileAbsolutePathString(iDataSet, inputImageTag, "IMG")

    filename = tbModel.getFileName(iDataSet, inputImageTag, "IMG")
    basename=path.splitext(filename)[0]

    print("Processing: "+filepath)

    originalImage=BF.openImagePlus(filepath)[0]
    print originalImage
    
    identifiedRois=roiDefiner.getManualRois(originalImage)
    
    saveRoisTable(identifiedRois,basename, tbModel, iDataSet, outputRoiTag)

    baseNameRegExp=r'.*--W[0-9]{4}--P([0-9]{4})-{1,2}T[0-9]{4}'
    reg_match = re.search(baseNameRegExp, basename)
    print reg_match.groups()
    tbModel.setNumericValue(int(reg_match.group(1)),iDataset,'PositionIndex')

class StopKeyListener(KeyListener):
    def StopKeyListener():
        super
        self.stopPressed=False
    
    def keyPressed(e):
        keyCode=e.getKeyCode()
            if keyCode==KeyEvent.Q:
                self.stopPressed=True
    
    def keyReleased(e):pass
    
    def keyTyped(e):pass
    
    def wasStopPressed():
        return self.stopPressed


if __name__ in ('__main__','__builtin__'):
    #open table file from the initial experiment
    experimentTable=TableModel(inputDataFile)
    #add file column for ROIs and create dedicated subfolder
    experimentTable.addFileColumns(outputRoiTag,'ROI')
    FileUtils.createCleanFolder(experimentTable.getRootPath(),outputRoiTag)
    #add column for the position index
    experimentTable.addValueColumn('PositionIndex','NUM')

    #Open AutoMic Browser to see when files are created
    frame=ManualControlFrame(experimentTable,False)
    frame.setVisible(True)
    
    nDatasets=experimentTable.getRowCount()
    #print nDatasets
    
    #start listening for the HotKey stopping annotation
    stopKeyListener=StopKeyListener()
    
    for iDataset in range(0,nDatasets):
        processDataset(iDataset,experimentTable)
        experimentTable.writeNewFile('processed.txt',True)
    
    
    print 'Processing Finished'
