**AutoMicTools** package
=============================================

## General description

A Java library containing collection of Fiji plugins to design, debug and perform Automated Feedback Microscopy experiments.

## Automated Feedback Microscopy Methodology

Classical high throughput (HTM) is the method of choice for automated microscopy when the positions of the imaged objects and required imaging settings are known before the start of image acquisition. In many cases, however, such decisions have to be made during imaging. Examples of such experiments include:
- High resolution imaging of specific rare object phenotypes;
- Imaging of moving objects over an extended period of time;
- Performing FRAP, FCS and certain types of FRET experiments automatically.

To perform these and other types of complex fluorescent microscopy experiments in a high-throughput manner, they need to be automated using Adaptive Feedback Microscopy technology. In these techniques low-zoom images are first acquired in predefined positions and automatically transferred to the image analysis program. Specially designed image processing procedures identify cells, organelles or other structures of interest and transfer these data back to the microscope software, which triggers the respective high content microscopy imaging or measurement modality.

## Main package features

- Running automated feedback microscopy workflows on several types of the microscopes.
- API to customise feedback microscopy workflows for particular research needs.
- Logging information about acquired images and automated decision making during feedback microscopy experiments.
- Interactive browsing of experiment results.

Customisable workflow procedures trigger execution of image analysis functions. Information about all acquired images, identified regions and decisions made by the system is stored in a special data structure that is exported from the software as a text table. Our visualisation module imports this table and provides a GUI to efficiently navigate through the acquired datasets and show segmentation results as overlays on acquired images. The data is also used by post-acquisition analysis procedures to import raw images and link quantifications to corresponding image and ROI files.


# Documentation

- [Installation guidelines](./Documentation/manual.md)


## Contributors

- Aliaksandr Halavatyi (ALMF, EMBL): development and support.
- Manuel Gunkel (ALMF, EMBL): development and support.


## Related projects

The ***AutoMicTools*** library can be extended by implemeting custom image analysis routines (called *Jobs*) and protocols (called *Jobdistributors*) to create coplex workflows satisfying needs of particular research projects. The examples of projects which customise and expand the AutoMicTools functionality are:

- [Fly embryo Screen](https://git.embl.de/grp-almf/feedback-fly-embryo-crocker)
- [AiryScan imaging of moving objects](https://git.embl.de/grp-almf/track-npc-feedback) 



## Contact

Please contact Aliaksandr Halavatyi (aliaksandr.halavatyi(at)embl.de) if you have any questions or suggestions.